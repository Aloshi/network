#include <util/Log.h>

#include <client/ServerConnection.h>

#include <Sim.h>
#include <scene/SceneContainer.h>
#include <Move.h>
#include <gfx/Sprite.h>  // for ScreenSprite
#include <script/ScriptEngine.h>
#include <script/ScriptInput.h>
#include <util/GetDPI.h>

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include <fstream>
#include <string>

#include <alogui/Desktop.h>

#include <gui/Console.h>
#include <gui/ActionMap.h>

std::string getIP()
{
	std::string ip;

	std::ifstream file("serverip.txt");
	if (!file.good()) {
		LOG(LogError) << "Could not find serverip.txt in working directory. Assuming localhost.";
		ip = "localhost";
	} else {
		std::getline(file, ip);
	}

	LOG(LogInfo) << "Connecting to " << ip << "...";
	return ip;
}

bool isClient() {
	return true;
}

bool isServer() {
	return false;
}

int main(int argc, const char** argv)
{
	float dpiScale = getDPIScale();

	Log::open();
	alogui::Desktop::get()->setDPIScale(dpiScale);
	ScriptEngine::create(argc, argv);
	Console::create();

	if (!InitializeYojimbo()) {
		LOG(LogError) << "Could not initialize yojimbo";
		return 1;
	}
	yojimbo_log_level(YOJIMBO_LOG_LEVEL_INFO);

	ServerConnection serverConnection;
	NetObject::setServerConnection(&serverConnection);
	dukglue_register_global(ScriptEngine::get(), &serverConnection, "ServerConnection");

	ScriptEngine::exec("scripts/main.js");

	sf::RenderWindow window(sf::VideoMode((int) (800 * dpiScale), (int) (600 * dpiScale)), "Client");
	Camera::setRenderTarget(&window);
	ScriptInput_setRenderWindow(&window);
	bool vsync = false;

	Console::open();

	sf::Clock clock;
	
	Move currentMove;

	// run the program as long as the window is open
	while (window.isOpen())
	{
		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Tilde) {
				Console::toggle();
				continue;
			}

			if (event.type == sf::Event::TextEntered && event.text.unicode == '`') {
				continue;
			}

			if (alogui::Desktop::get()->handleEvent(event))
				continue;

			if (ActionMap::processEvent(event))
				continue;

			// "close requested" event: we close the window
			switch (event.type) {
			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::KeyPressed:
				if (event.key.code == sf::Keyboard::C)
					serverConnection.connect(getIP().c_str(), 28000);
				else if (event.key.code == sf::Keyboard::V) {
					window.setVerticalSyncEnabled((vsync = !vsync));
					LOG(LogInfo) << (vsync ? "Enabled vsync!" : "Disabled vsync!");
				}
				else if (event.key.code == sf::Keyboard::Left || event.key.code == sf::Keyboard::A)
					currentMove.x = -1;
				else if (event.key.code == sf::Keyboard::Right || event.key.code == sf::Keyboard::D)
					currentMove.x = 1;
				else if (event.key.code == sf::Keyboard::Down || event.key.code == sf::Keyboard::S)
					currentMove.y = 1;
				else if (event.key.code == sf::Keyboard::Up || event.key.code == sf::Keyboard::W)
					currentMove.y = -1;
				break;
			case sf::Event::KeyReleased:
				if ((event.key.code == sf::Keyboard::Left || event.key.code == sf::Keyboard::A) && currentMove.x < 0)
					currentMove.x = 0;
				else if ((event.key.code == sf::Keyboard::Right || event.key.code == sf::Keyboard::D) && currentMove.x > 0)
					currentMove.x = 0;
				else if ((event.key.code == sf::Keyboard::Down || event.key.code == sf::Keyboard::S) && currentMove.y > 0)
					currentMove.y = 0;
				else if ((event.key.code == sf::Keyboard::Up || event.key.code == sf::Keyboard::W) && currentMove.y < 0)
					currentMove.y = 0;
				break;
			}
		}

		const double elapsedSeconds = clock.getElapsedTime().asSeconds();
		const double elapsedTime = clock.restart().asMicroseconds() * TIMESCALE;

		// clear the window with black color
		window.clear(sf::Color::Black);

		// update
		serverConnection.processPackets();
		serverConnection.sendPackets();

		// about to tick?
		if (Sim::accumulator() + elapsedTime >= TICK_RATE_MICROSECONDS) {
			serverConnection.sendMovePacket(currentMove);
			
			// prediction
			if (serverConnection.controlObject())
				serverConnection.controlObject()->addMoveClientside(currentMove);

			currentMove.seq_num++;
		}

		Sim::advanceTime(elapsedTime);
		serverConnection.advanceTime(elapsedSeconds);

		// update asynchronous scripts
		ScriptEngine::pump_events();
		
		alogui::Desktop::get()->update(static_cast<float>(elapsedTime / 1e6));
		ScreenSprite::updateAll(elapsedTime / 1e6);

		// draw everything here...
		serverConnection.camera()->render(window);
		ScreenSprite::drawAll(window);
		alogui::Desktop::get()->draw(window);
		// window.draw(...);

		// end the current frame
		window.display();

		// if we don't sleep, the client will start to get scheduled inconsistently
		// by the system scheduler, causing choppy performance
		sf::sleep(sf::microseconds((sf::Int64) (1)));  // 100000
	}

	serverConnection.disconnect();
	ScriptInput_setRenderWindow(NULL);
	NetObject::setServerConnection(NULL);
	ShutdownYojimbo();
	Console::destroy();
	ScriptEngine::destroy();
	Log::close();
}
