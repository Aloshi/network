#include <catch.hpp>

#include <collision/ConvexShape.h>

ConvexShape makeBoxFromSegment(const Vector2f& size) {
	ConvexShape shape({
		Vector2f(size.x, 0.0f),
		Vector2f(size.y, 0.0f),
		Vector2f(size.y, 1.0f),
		Vector2f(size.x, 1.0f),
	});
	return shape;
}

/*TEST_CASE("ConvexShape mtv works", "[collision]") {
	// a / b / normal / expectedMTV
	// remember a has the mtv applied to it, b is fixed
	static const int ntests = 8;
	Vector2f tests[ntests][4] = {
		{ Vector2f(0, 4), Vector2f(-1, 1), Vector2f(1, 0), Vector2f(1, 0) },
		{ Vector2f(0, 4), Vector2f(-1, 1), Vector2f(-1, 0), Vector2f(1, 0) },

		{ Vector2f(-2, 2), Vector2f(0, 4), Vector2f(-1, 0), Vector2f(-2, 0) },
		{ Vector2f(-2, 2), Vector2f(0, 4), Vector2f(1, 0), Vector2f(-2, 0) },

		{ Vector2f(2, 3), Vector2f(-5, 5), Vector2f(1, 0), Vector2f(3, 0) },
		{ Vector2f(2, 3), Vector2f(-5, 5), Vector2f(-1, 0), Vector2f(3, 0) },

		{ Vector2f(-3, -2), Vector2f(-5, 5), Vector2f(-1, 0), Vector2f(-3, 0), },
		{ Vector2f(-3, -2), Vector2f(-5, 5), Vector2f(1, 0), Vector2f(-3, 0), },
	};

	for (int i = 0; i < ntests; i++) {
		ConvexShape shapeA = makeBoxFromSegment(tests[i][0]);
		ConvexShape shapeB = makeBoxFromSegment(tests[i][1]);
		const Vector2f& normal = tests[i][2];
		const Vector2f& expectedMtv = tests[i][3];

		Vector2f a = shapeA.project(normal, Vector2f(0, 0));
		Vector2f b = shapeB.project(normal, Vector2f(0, 0));
		float minOverlap = 1e16f;
		Vector2f overlapVec;
		ConvexShape::calc_mtv(a, b, normal, &minOverlap, &overlapVec);
		INFO("shapeA interval: " << a);
		INFO("shapeB interval: " << b);
		INFO("Set MTV? " << (minOverlap != 1e16));
		REQUIRE(overlapVec * minOverlap == expectedMtv);
	}
}*/

ConvexShape makeBox(const Vector2f& size, const Vector2f& center = Vector2f(0, 0)) {
	ConvexShape shape({
		Vector2f(-size.x / 2.0f, -size.y / 2.0f) + center,
		Vector2f(+size.x / 2.0f, -size.y / 2.0f) + center,
		Vector2f(+size.x / 2.0f, +size.y / 2.0f) + center,
		Vector2f(-size.x / 2.0f, +size.y / 2.0f) + center,
		});
	return shape;
}

TEST_CASE ("ConvexShape works", "[collision]") {
  ConvexShape shape1 = makeBox(Vector2f(32, 32));
  ConvexShape shape2 = makeBox(Vector2f(128, 16));

  // start/end/otherPos/expectedNormal
  static const int ntests = 4;
  Vector2f tests[ntests][4] = {
	  { Vector2f(0, 30), Vector2f(0, 0), Vector2f(0, 0), Vector2f(0, 1) },  // up -> down
	  { Vector2f(0, -30), Vector2f(0, 0), Vector2f(0, 0), Vector2f(0, -1) },  // down -> up
	  { Vector2f(10, 30), Vector2f(0, 0), Vector2f(0, 0), Vector2f(0, 1) },  // up and right -> down
	  { Vector2f(0, 0), Vector2f(0, 0), Vector2f(0, 0), Vector2f(0, 0) },  // test mtv
	  // TODO test no collision
  };
  for (int i = 0; i < ntests; i++) {
	  const Vector2f& start = tests[i][0];
	  const Vector2f& end = tests[i][1];
	  const Vector2f& otherPos = tests[i][2];
	  const Vector2f& expectedNormal = tests[i][3];

	  INFO("i = " << i);

	  CollisionInfo info;
	  bool collides = shape1.findCollision(start, end, &shape2, otherPos, &info);
	  INFO("First test - Time of collision: " << info.time << ", normal: " << info.normal << ", mtv: " << info.mtv);
	  REQUIRE(collides);
	  if (!expectedNormal.isZero())
		  REQUIRE(info.normal == expectedNormal);

	  Vector2f newPos = start + (end - start) * info.time + info.mtv;
	  bool stillCollides = shape1.findCollision(newPos, newPos, &shape2, otherPos, &info);
	  INFO("Second test - Time of collision: " << info.time << ", normal: " << info.normal << ", mtv: " << info.mtv);
	  REQUIRE_FALSE(stillCollides);
  }
}

TEST_CASE("ConvexShape overlapping edge", "[collision]") {
	ConvexShape shapeA = makeBox(Vector2f(8, 8));
	ConvexShape shapeB = makeBox(Vector2f(16, 8), Vector2f(0, -4));

	Vector2f start(0, 4), otherPos(0, 0);
	Vector2f velocity(2, -2);
	Vector2f end = start + velocity;

	static const Vector2f offset(5500, 5500);  // test for floating point problems at higher values
	start += offset;
	end += offset;
	otherPos += offset;

	CollisionInfo info;
	bool collides = shapeA.findCollision(start, end, &shapeB, otherPos, &info);
	INFO("First test - Time of collision: " << info.time << ", normal: " << info.normal << ", mtv: " << info.mtv);
	REQUIRE(collides);
	REQUIRE(info.normal == Vector2f(0, 1));
	REQUIRE(info.time == 0.0f);

	start = start + velocity * info.time + info.mtv;
	velocity = velocity - info.normal.project(velocity);
	REQUIRE(velocity.x == Approx(2));
	REQUIRE(velocity.y == Approx(0));

	end = start + velocity * (1.0f - info.time);
	bool stillCollides = shapeA.findCollision(start, end, &shapeB, otherPos, &info);
	INFO("Second test - Time of collision: " << info.time << ", normal: " << info.normal << ", mtv: " << info.mtv);
	REQUIRE_FALSE(stillCollides);
	REQUIRE(end.x == Approx(2 + offset.x));
	REQUIRE(end.y == Approx(4 + offset.y));
}

TEST_CASE("ConvexShape overlapping edge, test 2", "[collision]") {
	ConvexShape shapeA({ Vector2f(-16.f, -16.f), Vector2f(16.f, -16.f), Vector2f(16.f, 16.f), Vector2f(-16.f, 16.f) });
	ConvexShape shapeB({
		Vector2f(-395.f, -15.f), Vector2f(395.f, -15.f),
		Vector2f(395.f, 15.f), Vector2f(-395.f, 15.f)
	});

	Vector2f start(400, 545.080017f), otherPos(400, 580);
	Vector2f velocity(0, 235.200057f);

	for (int timestep = 0; timestep < 2; timestep++) {
		velocity += Vector2f(0, 9.8f);
		Vector2f end = start + velocity;

		CollisionInfo info;
		bool collides = shapeA.findCollision(start, end, &shapeB, otherPos, &info);
		INFO("Timestep " << timestep << ", first test - Time of collision: " << info.time << ", normal: " << info.normal << ", mtv: " << info.mtv);
		REQUIRE(collides);
		REQUIRE(info.normal == Vector2f(0, -1));
		// REQUIRE(info.time == 0.0f);

		start = start + velocity * info.time + info.mtv;
		velocity = velocity - info.normal.project(velocity);
		REQUIRE(velocity.y == Approx(0));

		end = start + velocity * (1.0f - info.time);
		bool stillCollides = shapeA.findCollision(start, end, &shapeB, otherPos, &info);
		INFO("Timestep " << timestep << ", second test - Time of collision: " << info.time << ", normal: " << info.normal << ", mtv: " << info.mtv);
		REQUIRE_FALSE(stillCollides);
	}

	// jump
	{
		velocity += Vector2f(0, -200.8f);
		Vector2f end = start + velocity;

		CollisionInfo info;
		bool collides = shapeA.findCollision(start, end, &shapeB, otherPos, &info);
		INFO("Jump - Time of collision: " << info.time << ", normal: " << info.normal << ", mtv: " << info.mtv);
		REQUIRE_FALSE(collides);
	}
}

TEST_CASE("ConvexShape overlapping edge, test 2 but upside down", "[collision]") {
	ConvexShape shapeA({ Vector2f(-16.f, -16.f), Vector2f(16.f, -16.f), Vector2f(16.f, 16.f), Vector2f(-16.f, 16.f) });
	ConvexShape shapeB({
		Vector2f(-395.f, -15.f), Vector2f(395.f, -15.f),
		Vector2f(395.f, 15.f), Vector2f(-395.f, 15.f)
		});

	Vector2f start(400, 545.080017f + 60.0f), otherPos(400, 580);
	Vector2f velocity(0, -235.200057f);

	for (int timestep = 0; timestep < 2; timestep++) {
		velocity += Vector2f(0, -9.8f);
		Vector2f end = start + velocity;

		CollisionInfo info;
		bool collides = shapeA.findCollision(start, end, &shapeB, otherPos, &info);
		INFO("Timestep " << timestep << ", first test - Time of collision: " << info.time << ", normal: " << info.normal << ", mtv: " << info.mtv);
		REQUIRE(collides);
		REQUIRE(info.normal == Vector2f(0, 1));
		// REQUIRE(info.time == 0.0f);

		start = start + velocity * info.time + info.mtv;
		velocity = velocity - info.normal.project(velocity);
		REQUIRE(velocity.y == Approx(0));

		end = start + velocity * (1.0f - info.time);
		bool stillCollides = shapeA.findCollision(start, end, &shapeB, otherPos, &info);
		INFO("Timestep " << timestep << ", second test - Time of collision: " << info.time << ", normal: " << info.normal << ", mtv: " << info.mtv);
		REQUIRE_FALSE(stillCollides);
	}
}