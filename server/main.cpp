#include <iostream>

#include <SFML/System.hpp>

#include <server/Server.h>
#include <Sim.h>
#include <script/ScriptEngine.h>
#include <util/Log.h>

#define NET_UPDATE_RATE_MICROSECONDS (TICK_RATE_MICROSECONDS * 3)

bool isClient() {
	return false;
}

bool isServer() {
	return true;
}

int main(int argc, const char** argv)
{
	Log::open();
	ScriptEngine::create(argc, argv);

	if (!InitializeYojimbo()) {
		LOG(LogError) << "Could not initialize yojimbo";
		return 1;
	}
	yojimbo_log_level(YOJIMBO_LOG_LEVEL_INFO);

	std::string bindAddress = "127.0.0.1";
	for (int i = 1; i < argc - 1; i++) {
		if (strcmp(argv[i], "--bind") == 0) {
			bindAddress = argv[i + 1];
			break;
		}
	}

	Server server;
	server.listen(bindAddress, 28000, 10);
	dukglue_register_global(ScriptEngine::get(), &server, "Server");

	ScriptEngine::exec("scripts/main.js");

	sf::Clock clock;
	double netUpdateAccumulator = 0;
	while (true) {
		const double elapsedSeconds = clock.getElapsedTime().asSeconds();
		const double elapsedTime = clock.restart().asMicroseconds() * TIMESCALE;
		netUpdateAccumulator += elapsedTime;

		server.processPackets();

		Sim::advanceTime(elapsedTime);
    server.advanceTime();

		// update only once if NET_UPDATE_RATE_MICROSECONDS is reached
		if (netUpdateAccumulator >= NET_UPDATE_RATE_MICROSECONDS) {
			server.sendNetUpdates();  // push a net update packet into the queue
			while (netUpdateAccumulator >= NET_UPDATE_RATE_MICROSECONDS)
				netUpdateAccumulator -= NET_UPDATE_RATE_MICROSECONDS;
		}

    server.sendPackets();

		// update asynchronous scripts
		ScriptEngine::pump_events();
		
		// if we don't sleep, the server will start to get scheduled inconsistently
		// by the system scheduler, causing choppy performance
		sf::sleep(sf::microseconds((sf::Int64) (TICK_RATE_MICROSECONDS * TIMESCALE)));
	}

  server.stop();

	ScriptEngine::destroy();
	Log::close();
}
