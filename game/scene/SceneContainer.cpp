#include "SceneContainer.h"
#include "SceneObject.h"
#include "Sim.h"

#include <algorithm>

const float SceneContainer::BIN_SIZE = 16.f;
const float SceneContainer::TOTAL_BIN_SIZE = SceneContainer::BIN_SIZE * SceneContainer::NUM_BINS;

SceneContainer::SceneContainer(const std::string& name) : mName(name)
{
}

SceneContainer::~SceneContainer()
{
	for (auto it = mObjects.begin(); it != mObjects.end(); it++) {
		delete *it;
	}
}

void SceneContainer::addObject(SceneObject* obj)
{
	assert(obj->mContainer == NULL);

	mObjects.push_back(obj);
	obj->mContainer = this;

	auto wb = obj->worldBox();
	obj->mBins = calcBins(wb);
	insertIntoBins(obj->mBins, obj);
}

void SceneContainer::removeObject(SceneObject* obj)
{
	assert(obj->mContainer == this);

	removeFromBins(obj->mBins, obj);

	auto it = std::find(mObjects.begin(), mObjects.end(), obj);
	assert(it != mObjects.end());
	mObjects.erase(it);
}

void SceneContainer::notifyObjectDirty(SceneObject* obj)
{
	Box2<uint32_t> newBins = calcBins(obj->worldBox());

	// if we ended up in the same bins, do nothing
	if (newBins == obj->mBins)
		return;

	removeFromBins(obj->mBins, obj);
	obj->mBins = newBins;
	insertIntoBins(obj->mBins, obj);
}


// min_out is the first valid bin, max_out is the last valid bin
// (i.e. the range is inclusive on both sides).

// [ 0 1 2 3 4 ]               the bin range (3, 4, 0, 1)
//     |   ^ min
//     ^ max
// is represented as:
// [ 0 1 2 3 4 ] 5 6 7 ...     min-to-max range (3, 4, 5, 6)
//         ^ min |
//               ^ max

// So use the output like this:
// for (int x = min_out; x <= max_out; x++) {
//   bin(x % NUM_BINS, ...);
// }
void SceneContainer::calcBinRange(float min, float max, uint32_t& min_out, uint32_t& max_out)
{
	assert(std::isfinite(min));
	assert(std::isfinite(max));
	assert(max >= min);

	if (max - min >= TOTAL_BIN_SIZE) {
		min = 0;
		max = NUM_BINS - 1;
		return;
	}

	float minCoord = fmodf(min, TOTAL_BIN_SIZE);
	if (minCoord < 0.0f) {
		minCoord += TOTAL_BIN_SIZE;
		if (minCoord >= TOTAL_BIN_SIZE)
			minCoord = TOTAL_BIN_SIZE - 0.01f;
	}
	
	float maxCoord = fmodf(max, TOTAL_BIN_SIZE);
	if (maxCoord < 0.0f) {
		maxCoord += TOTAL_BIN_SIZE;
		if (maxCoord >= TOTAL_BIN_SIZE)
			maxCoord = TOTAL_BIN_SIZE - 0.01f;
	}

	assert(minCoord >= 0.0f && minCoord < TOTAL_BIN_SIZE);
	assert(maxCoord >= 0.0f && maxCoord < TOTAL_BIN_SIZE);

	min_out = static_cast<uint32_t>(minCoord / BIN_SIZE);
	max_out = static_cast<uint32_t>(maxCoord / BIN_SIZE);

	assert(min_out < NUM_BINS);
	assert(max_out < NUM_BINS);

	if (min_out > max_out)
		max_out += NUM_BINS;
}

Box2<uint32_t> SceneContainer::calcBins(const Box2f& worldBox)
{
	Box2<uint32_t> bins;
	calcBinRange(worldBox.min.x, worldBox.max.x, bins.min.x, bins.max.x);
	calcBinRange(worldBox.min.y, worldBox.max.y, bins.min.y, bins.max.y);
	return bins;
}

void SceneContainer::removeFromBins(const Box2<uint32_t>& bins, SceneObject* obj)
{
	for (uint32_t y = bins.min.y; y <= bins.max.y; y++) {
		for (uint32_t x = bins.min.x; x <= bins.max.x; x++) {
			auto& b = bin(x % NUM_BINS, y % NUM_BINS);
			auto it = std::find(b.begin(), b.end(), obj);
			assert(it != b.end());
			b.erase(it);
		}
	}
}

void SceneContainer::insertIntoBins(const Box2<uint32_t>& bins, SceneObject* obj)
{
	for (uint32_t y = bins.min.y; y <= bins.max.y; y++) {
		for (uint32_t x = bins.min.x; x <= bins.max.x; x++) {
			bin(x % NUM_BINS, y % NUM_BINS).push_back(obj);
		}
	}
}

std::vector<SceneObject*> SceneContainer::findObjects(const Box2f& box, TypeMask acceptMask, TypeMask rejectMask)
{
	Box2<uint32_t> bins = calcBins(box);

	std::vector<SceneObject*> results;
	for (uint32_t y = bins.min.y; y <= bins.max.y; y++) {
		for (uint32_t x = bins.min.x; x <= bins.max.x; x++) {
			auto& b = bin(x % NUM_BINS, y % NUM_BINS);
			for (unsigned int i = 0; i < b.size(); i++) {
				// TODO further narrow by position

				SceneObject* obj = b[i];

				TypeMask checkMask = obj->typeMask();
				// filter by acceptMask
				if ((checkMask & acceptMask) == 0)
					continue;
				// filter by rejectMask
				if ((checkMask & rejectMask) != 0)
					continue;

				bool alreadyFound = false;
				for (unsigned int j = 0; j < results.size(); j++) {
					if (results[j] == obj) {
						alreadyFound = true;
						break;
					}
				}
				if (!alreadyFound) {
					results.push_back(obj);
				}
			}
		}
	}

	return results;
}

void SceneContainer::tick()
{
	for (auto it = mObjects.begin(); it != mObjects.end(); it++) {
		(*it)->tick();
	}
}

void SceneContainer::render(sf::RenderTarget& target)
{
	float lerp = static_cast<float>(Sim::accumulator() / TICK_RATE_MICROSECONDS);
	for (auto it = mObjects.begin(); it != mObjects.end(); it++) {
		(*it)->render(target, lerp);
	}
}
