#pragma once

#include <vector>
#include <list>
#include <stdint.h>
#include <script/ScriptObject.h>
#include <scene/SceneObjectTypeMask.h>
#include <math/Box2.h>

class SceneObject;

namespace sf {
	class RenderTarget;
}

SCRIPT_CLASS("SceneContainer", SceneContainer, void, "A 'map' which contains SceneObjects.");
class SceneContainer : ScriptObject
{
public:
	SceneContainer(const std::string& name);
	virtual ~SceneContainer();

	void addObject(SceneObject* obj);
	void removeObject(SceneObject* obj);
	void notifyObjectDirty(SceneObject* obj);

	void tick();
	void render(sf::RenderTarget& window);

	std::vector<SceneObject*> findObjects(const Box2f& box, TypeMask acceptMask = SCENEOBJECT_TYPE_ANY, TypeMask rejectMask = 0);

	SCRIPT_METHOD(SceneContainer, "findObjects",
		static_cast<std::vector<SceneObject*> (SceneContainer::*)(const Vector2f&, const Vector2f&, TypeMask)>(&SceneContainer::findObjects),
		"Find objects in the scene within a box that match a given typemask.");
	inline std::vector<SceneObject*> findObjects(const Vector2f& min, const Vector2f& max, TypeMask acceptMask = SCENEOBJECT_TYPE_ANY) {
		return findObjects(Box2f(min, max), acceptMask);
	}

	SCRIPT_PROPERTY(SceneContainer, "name", &SceneContainer::name, nullptr, "Get the scene's name.");
	inline const std::string& name() const { return mName; }

private:
	std::string mName;

	static const uint32_t NUM_BINS = 128;
	static const float BIN_SIZE;
	static const float TOTAL_BIN_SIZE;

	static Box2<uint32_t> calcBins(const Box2f& worldBox);
	static void calcBinRange(float min, float max, uint32_t& min_out, uint32_t& max_out);
	inline std::vector<SceneObject*>& bin(uint32_t x, uint32_t y) { return mBins[NUM_BINS * y + x]; }

	void removeFromBins(const Box2<uint32_t>& bins, SceneObject* obj);
	void insertIntoBins(const Box2<uint32_t>& bins, SceneObject* obj);

	std::vector<SceneObject*> mObjects;
	std::vector<SceneObject*> mBins[NUM_BINS * NUM_BINS];
};
