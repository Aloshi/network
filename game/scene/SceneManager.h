#pragma once

#include <script/ScriptEngine.h>
#include <vector>
#include <map>
#include <string>

class SceneContainer;

SCRIPT_CLASS("SceneManager", SceneManager, void, "Keeps a reference to running scenes.");
class SceneManager
{
public:
	static SceneManager* get();

	virtual ~SceneManager();

	SCRIPT_METHOD(SceneManager, "getScene", &SceneManager::getScene, "Get a reference to a named scene. If the scene does not exist, it will be created.");
	SceneContainer* getScene(const std::string& name);

	void tickAllScenes();

	// TODO
	// void removeInactiveScenes(const std::vector<SceneContainer*>& activeScenes);

private:
	SceneManager() {}
	SceneManager(const SceneManager&) = delete;
	SceneManager& operator= (const SceneManager&) = delete;

	std::map<std::string, SceneContainer*> mScenes;
};
