#include <scene/SceneObjectGroup.h>
#include <scene/SceneObject.h>

SceneObjectGroup::~SceneObjectGroup()
{
	while (!mObjects.empty())
		remove(mObjects.front());
}

void SceneObjectGroup::add(SceneObject* obj)
{
	if (!obj)
		return;

	for (unsigned int i = 0; i < mObjects.size(); i++) {
		if (mObjects[i] == obj)
			return;  // object already in group
	}

	mObjects.push_back(obj);

	// add to head of list
	SceneObject::GroupData* data = new SceneObject::GroupData();
	data->group = this;
	data->next = obj->mGroupDataHead;
	obj->mGroupDataHead = data;
}

void SceneObjectGroup::remove(SceneObject* obj)
{
	if (!obj)
		return;

	for (auto it = mObjects.begin(); it != mObjects.end(); it++) {
		if (*it == obj) {
			mObjects.erase(it);

			// remove this group from the object's GroupData list
			SceneObject::GroupData* prev = NULL;
			SceneObject::GroupData* groupData = obj->mGroupDataHead;
			while (groupData) {
				if (groupData->group == this) {
					if (prev)
						prev->next = groupData->next;
					else
						obj->mGroupDataHead = groupData->next;

					delete groupData;
					return;
				}

				prev = groupData;
				groupData = groupData->next;
			}
			assert(false);  // not found in SceneObject::mGroupDataHead list, something got corrupted somewhere
			break;
		}
	}

	// object not in group
}

SceneObject* SceneObjectGroup::get(unsigned int idx)
{
	return mObjects.at(idx);
}

unsigned int SceneObjectGroup::size() const
{
	return mObjects.size();
}

void SceneObjectGroup::deleteAll()
{
	while (!mObjects.empty())
		delete mObjects.front();
}

void SceneObjectGroup::clear()
{
	while (!mObjects.empty())
		remove(mObjects.front());
}

void SceneObjectGroup::removeInternalOnly(SceneObject* obj)
{
	for (auto it = mObjects.begin(); it != mObjects.end(); it++) {
		if (*it == obj) {
			mObjects.erase(it);
			return;
		}
	}
	assert(false);
}
