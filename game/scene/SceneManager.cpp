#include "SceneManager.h"
#include "SceneContainer.h"

SceneManager* SceneManager::get()
{
	static SceneManager* manager = new SceneManager();
	return manager;
}

SceneManager::~SceneManager()
{
	for (auto it = mScenes.begin(); it != mScenes.end(); it++) {
		delete it->second;
	}
}

SceneContainer* SceneManager::getScene(const std::string& name)
{
	auto it = mScenes.find(name);
	if (it != mScenes.end())
		return it->second;

	SceneContainer* newContainer = new SceneContainer(name);
	mScenes[name] = newContainer;
	return newContainer;
}

void SceneManager::tickAllScenes()
{
	for (auto it = mScenes.begin(); it != mScenes.end(); it++) {
		it->second->tick();
	}
}
