#pragma once

#include <net/NetObject.h>
#include <collision/CollisionShape.h>
#include <scene/SceneObjectTypeMask.h>
#include <math/Box2.h>

class SceneContainer;
class SceneObjectGroup;
class ShapeList;

namespace sf {
class RenderTarget;
}

SCRIPT_CLASS("SceneObject", SceneObject, NetObject, "Abstract base class for objects contained in a scene.");
class SceneObject : public NetObject
{
public:
	SceneObject(SceneContainer* container);
	virtual ~SceneObject();

	virtual void tick() = 0;
	virtual void render(sf::RenderTarget& target, float lerp) = 0;

	virtual void packConstructorArgs(Client* client, BitStreamWriter& stream) override;
	virtual DirtyMask packUpdate(Client* client, DirtyMask remainingDirtyMask, BitStreamWriter& stream) override { return 0; }
	virtual void unpackUpdate(ServerConnection* conn, BitStreamReader& stream, unsigned int timestamp) override {}

	virtual Box2f worldBox() const;
	virtual void buildCollisionList(ShapeList* list, const Box2f& worldRegion) {}

	SCRIPT_PROPERTY(SceneObject, "typeMask", &SceneObject::typeMask, nullptr, "Get the SceneObject type mask for this object.");
	virtual TypeMask typeMask() const { return SCENEOBJECT_TYPE_DEFAULT; }

	SCRIPT_PROPERTY(SceneObject, "scene", &SceneObject::container, nullptr, "Get the SceneObject's container.");
	inline SceneContainer* container() const { return mContainer; }

	inline const Transform& transform() const { return mTransform; }
	inline const Vector2f& position() const { return mTransform.position; }

	void setTransform(const Transform& newTransform);
	void setPosition(const Vector2f& newPosition);
	inline void setPosition(float x, float y) { setPosition(Vector2f(x, y)); }

	SCRIPT_PROPERTY(SceneObject, "position", &SceneObject::position,
		static_cast<void (SceneObject::*)(const Vector2f&)>(&SceneObject::setPosition), "The SceneObject's position.");

	SCRIPT_METHOD(SceneObject, "setPosition",
		static_cast<void (SceneObject::*)(float, float)>(&SceneObject::setPosition), "Set the object's position.");

	SCRIPT_METHOD(SceneObject, "safeDelete", &SceneObject::safeDelete, "Schedule this object to be deleted at the end of the tick.");
	void safeDelete();

protected:
	virtual void onTransformChanged() {}
	void notifyWorldBoxChanged();

	enum SceneObjectFlags {
		FLAG_POSITION = BIT(0),
		NEXT_FREE_FLAG = BIT(1)
	};

private:
	SceneContainer* mContainer;

	// This is used by SceneContainer to make notifyObjectDirty() simpler.
	// This way we don't need to keep the old transform when calculating
	// what bins we *were* in when transform changes.
	Box2<uint32_t> mBins;

	Transform mTransform;

	friend SceneContainer;

	// This linked list is used with SceneObjectGroup to efficiently
	// update groups when SceneObjects are destroyed.
	struct GroupData {
		SceneObjectGroup* group;
		GroupData* next;
	};
	GroupData* mGroupDataHead;

	friend SceneObjectGroup;
};
