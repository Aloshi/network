#pragma once

#include <type_traits>

#include "SceneObject.h"

// Pointer to a SceneObject that will automatically reset
// to nullptr when the object it's pointing to is deleted.
template<typename T>
class SceneObjectPtr
{
	static_assert(std::is_base_of<SceneObject, T>::value, "SceneObjectPtr must be a subclass of SceneObject.");

public:
	SceneObjectPtr(T* obj = nullptr)
		: mConnection(NetObject::registerRemovedCallback(std::bind(&SceneObjectPtr<T>::checkRemoved, this, std::placeholders::_1)))
	{
		reset(obj);
	}

	SceneObjectPtr(const SceneObjectPtr<T>& rhs)
		: mConnection(NetObject::registerRemovedCallback(std::bind(&SceneObjectPtr<T>::checkRemoved, this, std::placeholders::_1)))
	{
		reset(rhs);
	}

	SceneObjectPtr& operator=(const SceneObjectPtr<T>& rhs) {
		reset(rhs);
		return *this;
	}

	SceneObjectPtr& operator=(T* rhs) {
		reset(rhs);
		return *this;
	}

	operator bool() const {
		return mPtr != NULL;
	}

	operator T*() const {
		return mPtr;
	}

	void reset(T* obj = nullptr)
	{
		mPtr = obj;
	}

	inline T* get() const { return mPtr; }
	inline T& operator*() const { assert(mPtr != nullptr); return *mPtr; }
	inline T* operator->() const { return mPtr; }

private:
	void checkRemoved(NetObject* obj) {
		if (obj == mPtr) {
			reset(nullptr);
		}
	}

	T* mPtr;
	const NetObject::SignalReceipt mConnection;
};
