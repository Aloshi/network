#include "SceneObject.h"
#include "SceneContainer.h"

#include <Sim.h>

#include <util/BitStream.h>
#include <scene/SceneObjectGroup.h>

SceneObject::SceneObject(SceneContainer* container)
{
	mContainer = NULL;
	mGroupDataHead = NULL;

	if (container)
		container->addObject(this);
}

SceneObject::~SceneObject()
{
	GroupData* groupData = mGroupDataHead;
	while (groupData) {
		groupData->group->removeInternalOnly(this);
		GroupData* next = mGroupDataHead->next;
		delete groupData;
		groupData = next;
	}

	if (mContainer)
		mContainer->removeObject(this);
}

Box2f SceneObject::worldBox() const
{
	return Box2f(mTransform.position, mTransform.position);
}

void SceneObject::setTransform(const Transform& newTransform)
{
	mTransform = newTransform;
	setDirty(FLAG_POSITION);
	onTransformChanged();
	notifyWorldBoxChanged();
}

void SceneObject::setPosition(const Vector2f& newPosition)
{
	mTransform.position = newPosition;
	setDirty(FLAG_POSITION);
	onTransformChanged();
	notifyWorldBoxChanged();
}

void SceneObject::safeDelete()
{
	Sim::schedule(0, [this] { delete this; });
}

void SceneObject::notifyWorldBoxChanged()
{
	if (mContainer)
		mContainer->notifyObjectDirty(this);
}

void SceneObject::packConstructorArgs(Client* client, BitStreamWriter& stream)
{
	assert(mContainer != NULL);
	stream.writeString(mContainer->name());
}