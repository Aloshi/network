#pragma once

#include <script/ScriptObject.h>

class SceneObject;

SCRIPT_CLASS("SceneObjectGroup", SceneObjectGroup, void,
	"An efficient unordered collection of SceneObjects. When contained objects are destroyed, they are automatically removed from the group.");
class SceneObjectGroup : public ScriptObject
{
public:
	SCRIPT_CONSTRUCTOR_MANAGED(SceneObjectGroup, "SceneObjectGroup");
	virtual ~SceneObjectGroup();

	SCRIPT_METHOD(SceneObjectGroup, "add", &SceneObjectGroup::add,
		"Add a SceneObject to the group. Does nothing if the object is already in the group or null.");
	void add(SceneObject* obj);

	SCRIPT_METHOD(SceneObjectGroup, "remove", &SceneObjectGroup::remove,
		"Remove a SceneObject from the group. Does nothing if the object is not in the group or null.");
	void remove(SceneObject* obj);

	SCRIPT_METHOD(SceneObjectGroup, "get", &SceneObjectGroup::get, "Access items in the group.");
	SceneObject* get(unsigned int idx);

	SCRIPT_METHOD(SceneObjectGroup, "size", &SceneObjectGroup::size, "Get the number of items in the group.");
	unsigned int size() const;

	SCRIPT_METHOD(SceneObjectGroup, "deleteAll", &SceneObjectGroup::deleteAll,
		"Delete all objects in the group (emptying it). The group is not deleted.");
	void deleteAll();

	SCRIPT_METHOD(SceneObjectGroup, "clear", &SceneObjectGroup::clear,
		"Removes all objects in the group (emptying it). The objects are not deleted.");
	void clear();

protected:
	// Removes obj from mObjects, assuming other code will handle
	// updating the obj->mGroupDataHead linked list.
	void removeInternalOnly(SceneObject* obj);

	friend SceneObject;
	std::vector<SceneObject*> mObjects;
};