#pragma once

#include <script/ScriptEngine.h>
#include <alogui/Window.h>

SCRIPT_CLASS("alogui::Window", alogui::Window, alogui::Container, "C++-oriented Window widget.");
SCRIPT_PROPERTY(alogui::Window, "title", nullptr, &alogui::Window::setTitle, "Set the title of the window.");
SCRIPT_PROPERTY(alogui::Window, "canClose", nullptr, &alogui::Window::setCanClose,
	"Set if this window can be closed via the close button in the top right or not.");

SCRIPT_CLASS("ScriptWindow", ScriptWindow, alogui::Window, "Scriptable Window widget.");
class ScriptWindow : public alogui::Window
{
public:
	SCRIPT_CONSTRUCTOR(ScriptWindow, "Window");
	virtual ~ScriptWindow();

	SCRIPT_METHOD(ScriptWindow, "open", &ScriptWindow::open, "Open this Window. If the Window is already open, does nothing.");
	void open();

	SCRIPT_METHOD(ScriptWindow, "close", &ScriptWindow::close, "Close this Window. If the Window is already closed, does nothing.");
	void close();

	SCRIPT_PROPERTY(ScriptWindow, "onWake", &ScriptWindow::getWakeCallback, &ScriptWindow::setWakeCallback, "Callback when the window is opened.");
	const DukValue& getWakeCallback() const;
	void setWakeCallback(DukValue cb);

	SCRIPT_PROPERTY(ScriptWindow, "onSleep", &ScriptWindow::getSleepCallback, &ScriptWindow::setSleepCallback, "Callback when the window is closed.");
	const DukValue& getSleepCallback() const;
	void setSleepCallback(DukValue cb);

	void onWake() override;
	void onSleep() override;

private:
	DukValue mWakeCallback;
	DukValue mSleepCallback;
};