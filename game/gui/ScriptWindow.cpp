#include "ScriptWindow.h"

#include <alogui/Widget.h>
#include <alogui/Window.h>
#include <alogui/Desktop.h>

ScriptWindow::~ScriptWindow() {
	ScriptEngine::invalidate(this);
}

void ScriptWindow::open() {
	alogui::Desktop::get()->add(this);
}

void ScriptWindow::close() {
	alogui::Desktop::get()->remove(this);
}

const DukValue& ScriptWindow::getWakeCallback() const {
	return mWakeCallback;
}
void ScriptWindow::setWakeCallback(DukValue cb) {
	mWakeCallback = cb;
}

const DukValue& ScriptWindow::getSleepCallback() const {
	return mSleepCallback;
}
void ScriptWindow::setSleepCallback(DukValue cb) {
	mSleepCallback = cb;
}

void ScriptWindow::onWake() {
	if (mWakeCallback.type() != DukValue::UNDEFINED) {
		ScriptEngine::pcall_noresult(mWakeCallback);
	}
}

void ScriptWindow::onSleep() {
	if (mSleepCallback.type() != DukValue::UNDEFINED) {
		ScriptEngine::pcall_noresult(mSleepCallback);
	}
}