#pragma once

#include <script/ScriptEngine.h>
#include <alogui/widgets/Button.h>

SCRIPT_CLASS("alogui::Button", alogui::Button, alogui::Widget, "C++-oriented Button widget.");
SCRIPT_PROPERTY(alogui::Button, "text", &alogui::Button::getText, &alogui::Button::setText, "Change the text on this Button.");

SCRIPT_CLASS("ScriptButton", ScriptButton, alogui::Button, "A scriptable Button.");
class ScriptButton : public alogui::Button {
public:
	SCRIPT_CONSTRUCTOR(ScriptButton, "Button");
	virtual ~ScriptButton();

	SCRIPT_PROPERTY(ScriptButton, "onClick", &ScriptButton::getClickCallback, &ScriptButton::setClickCallback, "Callback triggered when the button is 'clicked.'");
	const DukValue& getClickCallback() const;
	void setClickCallback(DukValue cb);

protected:
	void onClick() override;

private:
	DukValue mClickCallback;
};