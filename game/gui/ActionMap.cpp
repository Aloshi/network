#include "ActionMap.h"

#include <script/ScriptInput.h>  // for getMousePosition()
#include <util/Log.h>

#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>

std::vector<ActionMap*> ActionMap::gActionMaps;

// helper that returns an std::function that pcalls cb with the specified argument types,
// then coerces the return value to a bool and returns it (returns false for undefined)
template <typename... ArgsT>
std::function<bool(ArgsT...)> script_pcall_coerce_bool(DukValue& cb) {
	return [cb](ArgsT... args) -> bool {
		duk_context* ctx = ScriptEngine::get();
		duk_int_t rc = dukglue_pcall_raw(ctx, cb, args...);
		if (rc != 0)
			throw DukErrorException(ctx, rc);  // also pops error

		bool result = duk_to_boolean(ctx, -1);
		duk_pop(ctx);  // remove result from stack
		return result;
	};
}

class ActionMap::BtnCallback : public ActionMap::ICallback {
public:
	BtnCallback(const std::vector<sf::Keyboard::Key>& keys, const std::vector<sf::Mouse::Button>& mouseBtns, const std::function<bool(bool)>& cb)
	{
		mActive = false;
		mCallback = cb;

		mKeys.resize(keys.size());
		for (unsigned int i = 0; i < keys.size(); i++) {
			mKeys[i].first = keys[i];
			mKeys[i].second = false;
		}

		mMouseBtns.resize(mouseBtns.size());
		for (unsigned int i = 0; i < mouseBtns.size(); i++) {
			mMouseBtns[i].first = mouseBtns[i];
			mMouseBtns[i].second = false;
		}
	}

	BtnCallback(std::vector<sf::Keyboard::Key>& keys, std::vector<sf::Mouse::Button>& mouseBtns, DukValue cb)
		: BtnCallback(keys, mouseBtns, script_pcall_coerce_bool<bool>(cb))
	{
	}

	bool onEvent(const sf::Event& e) override {
		using namespace sf;
		if (e.type != Event::KeyPressed && e.type != Event::KeyReleased
			&& e.type != Event::MouseButtonPressed && e.type != Event::MouseButtonReleased)
			return false;

		bool newKeyState = true;
		for (unsigned int i = 0; i < mKeys.size(); i++) {
			if ((e.type == Event::KeyPressed || e.type == Event::KeyReleased)
				&& mKeys[i].first == e.key.code) {
				mKeys[i].second = (e.type == Event::KeyPressed);
			}
			if (!mKeys[i].second)
				newKeyState = false;
		}

		bool newMouseState = true;
		for (unsigned int i = 0; i < mMouseBtns.size(); i++) {
			if ((e.type == Event::MouseButtonPressed || e.type == Event::MouseButtonReleased)
				&& mMouseBtns[i].first == e.mouseButton.button) {
				mMouseBtns[i].second = (e.type == Event::MouseButtonPressed);
			}
			if (!mMouseBtns[i].second)
				newMouseState = false;
		}

		bool newState = newKeyState && newMouseState;
		if (newState != mActive) {
			mActive = newState;
			bool cbResult = mCallback(newState);

			if (cbResult && newState)
				return true;
		}
		return false;
	}

	void onPop() override {
		for (unsigned int i = 0; i < mKeys.size(); i++)
			mKeys[i].second = false;
		for (unsigned int i = 0; i < mMouseBtns.size(); i++)
			mMouseBtns[i].second = false;

		if (mActive) {
			mActive = false;
			mCallback(false);
		}
	}

private:
	bool mActive;
	std::vector< std::pair<sf::Keyboard::Key, bool> > mKeys;
	std::vector< std::pair<sf::Mouse::Button, bool> > mMouseBtns;
	std::function<bool(bool)> mCallback;
};

class ActionMap::MouseMovedCallback : public ActionMap::ICallback {
public:
	MouseMovedCallback(const std::function<void(int x, int y, int dx, int dy)>& cb)
		: mCallback(cb)
	{
	}

	MouseMovedCallback(DukValue cb)
		: MouseMovedCallback([cb](int x, int y, int dx, int dy) { dukglue_pcall<void>(ScriptEngine::get(), cb, x, y, dx, dy); })
	{
	}

	bool onEvent(const sf::Event& e) override
	{
		if (e.type == sf::Event::MouseMoved) {
			Vector2i newPos(e.mouseMove.x, e.mouseMove.y);
			Vector2i delta = newPos - mPrevPos;
			mPrevPos = newPos;
			mCallback(newPos.x, newPos.y, delta.x, delta.y);
		}

		// mousemove event is never consumed
		return false;
	}

	void onPush() override {
		mPrevPos = getMousePosition();
	}

private:
	Vector2i mPrevPos;
	std::function<void(int x, int y, int dx, int dy)> mCallback;
};

class ActionMap::MouseScrolledCallback : public ActionMap::ICallback {
public:
	enum Modifiers {
		MOD_CONTROL = (1 << 0),
		MOD_SHIFT = (1 << 1),
		MOD_ALT = (1 << 2),
		MOD_IGNORE = (1 << 3)
	};

	MouseScrolledCallback(sf::Mouse::Wheel dir, uint32_t modifiers, const std::function<bool(float delta)>& cb)
		: mDir(dir), mModifiers(modifiers), mCallback(cb)
	{
	}

	MouseScrolledCallback(sf::Mouse::Wheel dir, uint32_t modifiers, DukValue cb)
		: MouseScrolledCallback(dir, modifiers, script_pcall_coerce_bool<float>(cb))
	{
	}

	bool onEvent(const sf::Event& e) override
	{
		if (e.type == sf::Event::MouseWheelScrolled && e.mouseWheelScroll.wheel == mDir) {
			if ((mModifiers & MOD_IGNORE) == 0) {
				bool ctrl = (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) || sf::Keyboard::isKeyPressed(sf::Keyboard::RControl));
				bool shift = (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) || sf::Keyboard::isKeyPressed(sf::Keyboard::RShift));
				bool alt = (sf::Keyboard::isKeyPressed(sf::Keyboard::LAlt) || sf::Keyboard::isKeyPressed(sf::Keyboard::RAlt));
				if (((mModifiers & MOD_CONTROL) != 0) != ctrl)
					return false;
				if (((mModifiers & MOD_SHIFT) != 0) != shift)
					return false;
				if (((mModifiers & MOD_ALT) != 0) != alt)
					return false;
			}


			return mCallback(e.mouseWheelScroll.delta);
		}

		return false;
	}

private:
	sf::Mouse::Wheel mDir;
	uint32_t mModifiers;
	std::function<bool(float delta)> mCallback;
};


ActionMap::~ActionMap()
{
	pop();
}

void ActionMap::push()
{
	pop();
	gActionMaps.push_back(this);

	for (unsigned int i = 0; i < mCallbacks.size(); i++) {
		mCallbacks.at(i)->onPush();
	}
}

void ActionMap::pop()
{
	auto it = std::find(gActionMaps.begin(), gActionMaps.end(), this);
	if (it != gActionMaps.end()) {
		gActionMaps.erase(it);

		for (unsigned int i = 0; i < mCallbacks.size(); i++) {
			mCallbacks.at(i)->onPop();
		}
	}
}

bool ActionMap::processEvent(const sf::Event& e)
{
	for (unsigned int i = 0; i < gActionMaps.size(); i++) {
		ActionMap* map = gActionMaps[i];
		for (unsigned int j = 0; j < map->mCallbacks.size(); j++) {
			try {
				if (map->mCallbacks[j]->onEvent(e)) {
					return true;
				}
			}  catch (DukException& e) {
				LOG(LogError) << "Error processing ActionMap callback:\n" << e.what();
			}
		}
	}

	return false;
}

std::pair< std::vector<sf::Keyboard::Key>, std::vector<sf::Mouse::Button> > ActionMap::parseInputStr_kbm(const char* str)
{
	static const std::map<std::string, int> keys_map = {
		{ "a", 0 },
		{ "b", 1 },
		{ "c", 2 },
		{ "d", 3 },
		{ "e", 4 },
		{ "f", 5 },
		{ "g", 6 },
		{ "h", 7 },
		{ "i", 8 },
		{ "j", 9 },
		{ "k", 10 },
		{ "l", 11 },
		{ "m", 12 },
		{ "n", 13 },
		{ "o", 14 },
		{ "p", 15 },
		{ "q", 16 },
		{ "r", 17 },
		{ "s", 18 },
		{ "t", 19 },
		{ "u", 20 },
		{ "v", 21 },
		{ "w", 22 },
		{ "x", 23 },
		{ "y", 24 },
		{ "z", 25 },
		{ "0", 26 },
		{ "1", 27 },
		{ "2", 28 },
		{ "3", 29 },
		{ "4", 30 },
		{ "5", 31 },
		{ "6", 32 },
		{ "7", 33 },
		{ "8", 34 },
		{ "9", 35 },
		{ "escape", 36 },
		{ "lcontrol", 37 },
		{ "lshift", 38 },
		{ "lalt", 39 },
		{ "lsystem", 40 },
		{ "rcontrol", 41 },
		{ "rshift", 42 },
		{ "ralt", 43 },
		{ "rsystem", 44 },
		{ "menu", 45 },
		{ "lbracket", 46 },
		{ "rbracket", 47 },
		{ "semicolon", 48 },
		{ "comma", 49 },
		{ "period", 50 },
		{ "quote", 51 },
		{ "slash", 52 },
		{ "backslash", 53 },
		{ "tilde", 54 },
		{ "equal", 55 },
		{ "dash", 56 },
		{ "space", 57 },
		{ "return", 58 },
		{ "backspace", 59 },
		{ "tab", 60 },
		{ "pageup", 61 },
		{ "pagedown", 62 },
		{ "end", 63 },
		{ "home", 64 },
		{ "insert", 65 },
		{ "delete", 66 },
		{ "add", 67 },
		{ "subtract", 68 },
		{ "multiply", 69 },
		{ "divide", 70 },
		{ "left", 71 },
		{ "right", 72 },
		{ "up", 73 },
		{ "down", 74 },
		{ "numpad0", 75 },
		{ "numpad1", 76 },
		{ "numpad2", 77 },
		{ "numpad3", 78 },
		{ "numpad4", 79 },
		{ "numpad5", 80 },
		{ "numpad6", 81 },
		{ "numpad7", 82 },
		{ "numpad8", 83 },
		{ "numpad9", 84 },
		{ "f1", 85 },
		{ "f2", 86 },
		{ "f3", 87 },
		{ "f4", 88 },
		{ "f5", 89 },
		{ "f6", 90 },
		{ "f7", 91 },
		{ "f8", 92 },
		{ "f9", 93 },
		{ "f10", 94 },
		{ "f11", 95 },
		{ "f12", 96 },
		{ "f13", 97 },
		{ "f14", 98 },
		{ "f15", 99 },
		{ "pause", 100 },
	};

	static const std::map<std::string, sf::Mouse::Button> mouse_btns_map = {
		{ "mouse0", sf::Mouse::Left },
		{ "mouse1", sf::Mouse::Right },
		{ "mouse2", sf::Mouse::Middle },
	};

	std::stringstream ss;
	ss << str;

	std::vector<sf::Keyboard::Key> keys;
	std::vector<sf::Mouse::Button> mouseBtns;
	std::string token;
	while (ss >> token) {
		if (token == "ctrl") {
			keys.push_back(sf::Keyboard::LControl);
			//keys.push_back(sf::Keyboard::RControl);
			continue;
		} else if (token == "alt") {
			keys.push_back(sf::Keyboard::LAlt);
			//keys.push_back(sf::Keyboard::RAlt);
			continue;
		} else if (token == "shift") {
			keys.push_back(sf::Keyboard::LShift);
			//keys.push_back(sf::Keyboard::RShift);
			continue;
		}

		auto key_it = keys_map.find(token);
		if (key_it != keys_map.end()) {
			keys.push_back((sf::Keyboard::Key) key_it->second);
			continue;
		}

		auto mouseBtnIt = mouse_btns_map.find(token);
		if (mouseBtnIt != mouse_btns_map.end()) {
			mouseBtns.push_back(mouseBtnIt->second);
			continue;
		}

		throw std::runtime_error("Unknown button input");
	}

	return std::pair<decltype(keys), decltype(mouseBtns)>(keys, mouseBtns);
}

void ActionMap::bind(const char* device, const char* input_str, DukValue func)
{
	if (strcmp(device, "kbm") == 0 || strcmp(device, "keyboard") == 0 || strcmp(device, "mouse") == 0) {

		// some examples for the "mouseScrolled" input:
		// "mouseScrolled" -> mouse scrolled vertically only when not holding any modifier keys, func called with the delta as the argument
		// "ctrl mouseScrolled" -> mouse scrolled vertically while holding (only) control
		// "ctrl shift mouseScrolled" -> mouse scrolled vertically while holding both control and shift
		// "any mouseScrolled" -> mouse scrolled vertically, with modifiers being whatever (func always called when scroll wheel is scrolled)
		// "mouseScrolledHorizontal" -> mouse scrolled horizontally when not holding any modifier keys
		// "ctrl mouseScrolledHorizontal" -> mouse scrolled horizontally while holding (only) control
		if (strstr(input_str, "mouseScrolled") != NULL) {
			uint32_t modifiers = 0;
			if (strstr(input_str, "ctrl") != NULL)
				modifiers |= MouseScrolledCallback::MOD_CONTROL;
			if (strstr(input_str, "shift") != NULL)
				modifiers |= MouseScrolledCallback::MOD_SHIFT;
			if (strstr(input_str, "alt") != NULL)
				modifiers |= MouseScrolledCallback::MOD_ALT;
			if (strstr(input_str, "any") != NULL)
				modifiers |= MouseScrolledCallback::MOD_IGNORE;

			sf::Mouse::Wheel dir = sf::Mouse::VerticalWheel;
			if (strstr(input_str, "mouseScrolledHorizontal"))
				dir = sf::Mouse::HorizontalWheel;

			mCallbacks.insert(mCallbacks.begin(), std::make_unique<MouseScrolledCallback>(dir, modifiers, func));
		} else if (strcmp(input_str, "mouseMoved") == 0) {
			mCallbacks.insert(mCallbacks.begin(), std::make_unique<MouseMovedCallback>(func));
		} else {
			auto pair = parseInputStr_kbm(input_str);
			mCallbacks.insert(mCallbacks.begin(), std::make_unique<BtnCallback>(pair.first, pair.second, func));
		}
	}
}