#include "ScriptButton.h"

ScriptButton::~ScriptButton() {
	ScriptEngine::invalidate(this);
}

const DukValue& ScriptButton::getClickCallback() const {
	return mClickCallback;
}
void ScriptButton::setClickCallback(DukValue cb) {
	mClickCallback = std::move(cb);
}

void ScriptButton::onClick() {
	if (mClickCallback.type() != DukValue::UNDEFINED) {
		ScriptEngine::pcall_noresult(mClickCallback);
	}
}