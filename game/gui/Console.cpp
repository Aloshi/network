#include "Console.h"

#include <alogui/Desktop.h>
#include <script/ScriptEngine.h>

#include <assert.h>

Console* Console::sInstance = NULL;

void Console::create()
{
	assert(sInstance == NULL);
	sInstance = new Console();
}

void Console::destroy()
{
	assert(sInstance != NULL);
	delete sInstance;
	sInstance = NULL;
}

void Console::open()
{
	alogui::Desktop::get()->add(widget());
}

void Console::close()
{
	alogui::Desktop::get()->remove(widget());
}

void Console::toggle()
{
	if (!alogui::Desktop::get()->added(widget()))
		open();
	else
		close();
}

alogui::Widget* Console::widget()
{
	assert(sInstance != NULL);
	return sInstance->mWindow;
}

Console::Console()
{
	using namespace alogui;

	mWindow = new Window();
	mContainer = new alogui::BoxContainer();
	mScroll = new ScrollContainer();
	mQueue = new MessageQueue();
	mEdit = new LineEdit();

	mWindow->add(mContainer);
	
	mContainer->add(mScroll);
	mContainer->add(mEdit);

	mScroll->add(mQueue);

	mWindow->setTitle("Console");

	mEdit->setEnterPressedCallback([] (alogui::LineEdit* edit) {
		const std::string text = edit->getText();
		edit->setText("");

		// save to log first
		{
			LOG(LogInfo) << "=> " << text;
		}

		duk_context* ctx = ScriptEngine::get();
		try {
			DukValue v = dukglue_peval<DukValue>(ctx, text.c_str());

			v.push();
			const char* result = duk_safe_to_string(ctx, -1);
			LOG(LogDebug) << result;
			duk_pop(ctx);
		} catch (DukException& e) {
			LOG(LogError) << e.what();
		}
	});

	// show log messages in the message queue
	mLogConnection = std::move(Log::registerCallback([this](int lvl, const std::string& msg) {
		sf::Color color = sf::Color::White;
		if (lvl == LogError)
			color = sf::Color::Red;
		else if (lvl == LogWarning)
			color = sf::Color(128, 128, 128);
		else if (lvl == LogDebug)
			color = sf::Color::Green;

		this->mQueue->push(msg, color);
	}));

	mContainer->setMinimumSize(alogui::Vector2i((int) (400 * alogui::Desktop::get()->getDPIScale()), (int) (250 * alogui::Desktop::get()->getDPIScale())));
}

Console::~Console()
{
	Log::unregisterCallback(mLogConnection);

	delete mEdit;
	delete mQueue;
	delete mScroll;
	delete mContainer;
	delete mWindow;
}
