#pragma once

#include <alogui/widgets/MessageQueue.h>
#include <script/ScriptEngine.h>

SCRIPT_CLASS("alogui::MessageQueue", alogui::MessageQueue, alogui::Widget, "C++-oriented MessageQueue widget.");

SCRIPT_CLASS("ScriptMessageQueue", ScriptMessageQueue, alogui::MessageQueue, "A scriptable MessageQueue widget.");
class ScriptMessageQueue : public alogui::MessageQueue {
public:
	SCRIPT_CONSTRUCTOR(ScriptMessageQueue, "MessageQueue");
	virtual ~ScriptMessageQueue() {
		ScriptEngine::invalidate(this);
	}

	SCRIPT_METHOD(ScriptMessageQueue, "push", static_cast<void (ScriptMessageQueue::*)(const std::string&)>(&ScriptMessageQueue::push),
		"Push a message to the bottom of the widget.");
	void push(const std::string& msg) {
		alogui::MessageQueue::push(msg, sf::Color::White);
	}
};