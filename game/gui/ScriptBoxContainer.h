#pragma once

#include <script/ScriptEngine.h>
#include <alogui/containers/BoxContainer.h>

// BoxContainer
SCRIPT_CLASS("BoxContainer", alogui::BoxContainer, alogui::Container, "Container that lays things out in a horizontal or vertical line.");

SCRIPT_CLASS("ScriptBoxContainer", ScriptBoxContainer, alogui::BoxContainer, "A scriptable BoxContainer.")
class ScriptBoxContainer : public alogui::BoxContainer {
public:
	SCRIPT_CONSTRUCTOR(ScriptBoxContainer, "BoxContainer");

	inline virtual ~ScriptBoxContainer() {
		ScriptEngine::invalidate(this);
	}

	SCRIPT_METHOD(ScriptBoxContainer, "setOrientation", static_cast<void (ScriptBoxContainer::*)(int)>(&ScriptBoxContainer::setOrientation),
		"Change the axis items are layed out along - 0 for vertical (default), 1 for horizontal.");
	inline void setOrientation(int orientation) {
		assert(orientation == 0 || orientation == 1);
		alogui::BoxContainer::setOrientation((alogui::BoxContainer::Orientation) orientation);
	}
};