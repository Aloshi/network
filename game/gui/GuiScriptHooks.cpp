#include "GuiScriptHooks.h"

#include <script/ScriptEngine.h>
#include <alogui/Widget.h>
#include <alogui/Container.h>
#include <alogui/containers/BoxContainer.h>

// Widget
SCRIPT_CLASS("Widget", alogui::Widget, void, "Base class for GUI widgets.");
SCRIPT_METHOD(alogui::Widget, "setPosition", static_cast<void (alogui::Widget::*)(int, int)>(&alogui::Widget::setPosition),
	"Set the widget's position. May be ignored if the widget is in a container.");
SCRIPT_METHOD(alogui::Widget, "setSize", static_cast<void (alogui::Widget::*)(int, int)>(&alogui::Widget::setSize),
	"Set the widget's size. May be ignored if the widget is in a container.");
SCRIPT_METHOD(alogui::Widget, "setMinimumSize", static_cast<void (alogui::Widget::*)(int, int)>(&alogui::Widget::setMinimumSize),
	"Set the minimum size of this widget.");
SCRIPT_METHOD(alogui::Widget, "setFixedSize", static_cast<void (alogui::Widget::*)(int, int)>(&alogui::Widget::setFixedSize),
	"Set a fixed size (min and max) for this widget.");

SCRIPT_PROPERTY(alogui::Widget, "position", &alogui::Widget::getPosition,
	static_cast<void (alogui::Widget::*)(alogui::Vector2i)>(&alogui::Widget::setPosition),
	"The widget's position as a Vector2i.");
SCRIPT_PROPERTY(alogui::Widget, "width", &alogui::Widget::width, nullptr, "The widget's width.");

// Container
SCRIPT_CLASS("Container", alogui::Container, alogui::Widget, "Abstract base class for GUI 'containers.'");
SCRIPT_METHOD(alogui::Container, "add", &alogui::Container::add, "Add a child to the container.");
SCRIPT_METHOD(alogui::Container, "remove", &alogui::Container::remove, "Remove a child from the container.");
