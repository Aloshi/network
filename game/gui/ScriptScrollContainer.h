#pragma once

#include <alogui/containers/ScrollContainer.h>
#include <script/ScriptEngine.h>

SCRIPT_CLASS("alogui::ScrollContainer", alogui::ScrollContainer, alogui::Container, "C++-oriented ScrollContainer.");

SCRIPT_CLASS("ScriptScrollContainer", ScriptScrollContainer, alogui::ScrollContainer, "A scriptable ScrollContainer container.");
class ScriptScrollContainer : public alogui::ScrollContainer {
public:
	SCRIPT_CONSTRUCTOR(ScriptScrollContainer, "ScrollContainer");
	virtual ~ScriptScrollContainer() {
		ScriptEngine::invalidate(this);
	}
};