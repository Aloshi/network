#pragma once

#include <script/ScriptEngine.h>
#include <alogui/widgets/Gallery.h>

SCRIPT_CLASS("alogui::Gallery", alogui::Gallery, alogui::Widget, "C++-oriented Gallery widget.");
SCRIPT_METHOD(alogui::Gallery, "clear", &alogui::Gallery::clear, "Remove all entries in the gallery.");

SCRIPT_CLASS("ScriptGallery", ScriptGallery, alogui::Gallery, "A scriptable Gallery widget.");
class ScriptGallery : public alogui::Gallery {
public:
	SCRIPT_CONSTRUCTOR(ScriptGallery, "Gallery");
	virtual ~ScriptGallery() {
		ScriptEngine::invalidate(this);
	}

	SCRIPT_PROPERTY(ScriptGallery, "onSelected", &ScriptGallery::getSelectedCallback, &ScriptGallery::setSelectedCallback,
		"Callback triggered when an image in the gallery is selected (clicked).");
	inline const DukValue& getSelectedCallback() const { return mSelectedCallback; }
	inline void setSelectedCallback(DukValue cb) {
		mSelectedCallback = std::move(cb);
	}

	SCRIPT_METHOD(ScriptGallery, "add", static_cast<void(ScriptGallery::*)(const char*, const std::shared_ptr<Texture>&, const DukValue&)>(&ScriptGallery::add),
		"Add a label/texture pair.");
	inline void add(const char* str, const std::shared_ptr<Texture>& tex, const DukValue& udata = DukValue()) {
		mTextureRefs.push_back(tex);
		mUserDatas.push_back(udata);
		Gallery::add(str, &tex->data());
	}

	SCRIPT_METHOD(ScriptGallery, "getLabelText", &ScriptGallery::getLabelText, "Get the label text for a given index.");
	inline std::string getLabelText(int idx) {
		return getLabel(idx)->getText();
	}

	SCRIPT_METHOD(ScriptGallery, "getUserData", &ScriptGallery::getUserData, "Get the user data provided in add() for a given index.");
	inline DukValue getUserData(int idx) {
		return mUserDatas.at(idx);
	}

	SCRIPT_METHOD(ScriptGallery, "setSelectedIdx", &ScriptGallery::setSelectedIdx, "Set the current selection by index (-1 for no selection).")

protected:
	void onRemoved(int idx) override {
		mTextureRefs.erase(mTextureRefs.begin() + idx);
		mUserDatas.erase(mUserDatas.begin() + idx);
	}

	void onSelected(int idx) override {
		if (mSelectedCallback.type() != DukValue::UNDEFINED) {
			ScriptEngine::pcall_noresult(mSelectedCallback, idx);
		}
	}

private:
	// we need to keep these textures from being deleted while they're part of this Gallery
	std::vector< std::shared_ptr<Texture> > mTextureRefs;
	std::vector<DukValue> mUserDatas;
	DukValue mSelectedCallback;
};