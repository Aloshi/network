#pragma once

#include <script/ScriptEngine.h>

#include <alogui/widgets/LineEdit.h>

SCRIPT_CLASS("alogui::LineEdit", alogui::LineEdit, alogui::Widget, "C++-oriented LineEdit widget.");
SCRIPT_PROPERTY(alogui::LineEdit, "text", &alogui::LineEdit::getText, &alogui::LineEdit::setText, "Currently entered text.");

SCRIPT_CLASS("ScriptLineEdit", ScriptLineEdit, alogui::LineEdit, "A scriptable LineEdit widget.");
class ScriptLineEdit : public alogui::LineEdit {
public:
	SCRIPT_CONSTRUCTOR(ScriptLineEdit, "LineEdit");
	virtual ~ScriptLineEdit() {
		ScriptEngine::invalidate(this);
	}

	SCRIPT_PROPERTY(ScriptLineEdit, "onEnterPressed", &ScriptLineEdit::getEnterCallback,
		&ScriptLineEdit::setEnterCallback, "Callback fired when the Enter key is pressed.");
	inline const DukValue& getEnterCallback() const { return mEnterCallback; }
	inline void setEnterCallback(DukValue v) { mEnterCallback = std::move(v); }

protected:
	void onEnterPressed() override {
		if (mEnterCallback.type() != DukValue::UNDEFINED) {
			ScriptEngine::pcall_noresult(mEnterCallback);
		}
	}

private:
	DukValue mEnterCallback;
};