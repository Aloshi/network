#pragma once

#include <script/ScriptEngine.h>
#include <alogui/widgets/Label.h>

SCRIPT_CLASS("alogui::Label", alogui::Label, alogui::Widget, "C++-oriented label widget.");
SCRIPT_PROPERTY(alogui::Label, "text", &alogui::Label::getText, &alogui::Label::setText, "Displayed text.");

SCRIPT_CLASS("ScriptLabel", ScriptLabel, alogui::Label, "A scriptable Label widget.");
class ScriptLabel : public alogui::Label {
public:
	SCRIPT_CONSTRUCTOR(ScriptLabel, "Label");
	virtual ~ScriptLabel() {
		ScriptEngine::invalidate(this);
	}
	
	SCRIPT_PROPERTY(ScriptLabel, "color", &ScriptLabel::getTextColorInt, &ScriptLabel::setTextColorInt,
		"The text color (uint32 wich each byte representing RGBA).");
	uint32_t getTextColorInt() const {
		return alogui::Label::getTextColor().toInteger();
	}
	void setTextColorInt(uint32_t color) {
		alogui::Label::setTextColor(sf::Color(color));
	}
};