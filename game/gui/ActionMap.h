#pragma once

#include <vector>
#include <map>
#include <memory>

#include <script/ScriptObject.h>
#include <SFML/Window/Event.hpp>

SCRIPT_CLASS("ActionMap", ActionMap, void, "Used for setting up input bindings.");
class ActionMap : public ScriptObject {
protected:
	class ICallback {
	public:
		virtual ~ICallback() {}
		virtual bool onEvent(const sf::Event& e) = 0;
		virtual void onPush() {}
		virtual void onPop() {}
	};

	class BtnCallback;
	class MouseMovedCallback;
	class MouseScrolledCallback;

public:
	static bool processEvent(const sf::Event& e);

	SCRIPT_CONSTRUCTOR(ActionMap, "ActionMap");

	virtual ~ActionMap();

	SCRIPT_METHOD(ActionMap, "push", &ActionMap::push, "Activate this ActionMap.");
	void push();

	SCRIPT_METHOD(ActionMap, "pop", &ActionMap::pop, "Deactivate this ActionMap.");
	void pop();

	SCRIPT_METHOD(ActionMap, "bind", static_cast<void (ActionMap::*)(const char*, const char*, DukValue)>(&ActionMap::bind),
		"Register an input -> function mapping.");
	void bind(const char* device, const char* input_str, DukValue func);
	
private:
	static std::vector<ActionMap*> gActionMaps;

	std::pair< std::vector<sf::Keyboard::Key>, std::vector<sf::Mouse::Button> > parseInputStr_kbm(const char* input_str);
	std::vector< std::unique_ptr<ICallback> > mCallbacks;
};