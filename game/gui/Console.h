#pragma once

#include <alogui/Window.h>
#include <alogui/containers/BoxContainer.h>
#include <alogui/containers/ScrollContainer.h>
#include <alogui/widgets/MessageQueue.h>
#include <alogui/widgets/LineEdit.h>

#include <util/Log.h>

class Console
{
public:
	static void create();
	static void destroy();

	static void open();
	static void close();
	static void toggle();
	static alogui::Widget* widget();

private:
	Console();
	~Console();

	static Console* sInstance;

	alogui::Window* mWindow;
	alogui::BoxContainer* mContainer;
	alogui::ScrollContainer* mScroll;
	alogui::MessageQueue* mQueue;
	alogui::LineEdit* mEdit;
	size_t mLogConnection;
};