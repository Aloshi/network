#include "ServerConnection.h"

#include <yojimbo.h>
#include <sodium/randombytes.h>
#include <util/BitStream.h>

#include <util/Log.h>
#include <scene/SceneManager.h>

// for creation, should probably move this to a factory function
#include <game/Player.h>
#include <game/StaticShape.h>
#include <game/TileSet.h>
#include <game/TileChunk.h>
#include <game/Coin.h>
#include <game/Observer.h>
#include <components/TriggerComponent.h>
#include <components/SpriteComponent.h>

ServerConnection::ServerConnection()
{
	mNetwork = NULL;
}

ServerConnection::~ServerConnection()
{
	disconnect();
}

yojimbo::MessageFactory* ServerConnection::CreateMessageFactory(yojimbo::Allocator& allocator)
{
	return YOJIMBO_NEW(allocator, NetMessageFactory, allocator);
}

void ServerConnection::connect(const char* host, unsigned int port)
{
	disconnect();

	// TODO not this
	uint8_t privateKey[yojimbo::KeyBytes];
	memset(privateKey, 0, yojimbo::KeyBytes);

	mLastHeartbeat = 0;
	mNetTime = 0.0;
	mNetwork = new yojimbo::Client(yojimbo::GetDefaultAllocator(), yojimbo::Address("0.0.0.0", 0), NetConfig, *this, 0.0);
	uint64_t clientId;
	randombytes_buf(&clientId, sizeof(clientId));
	mNetwork->InsecureConnect(privateKey, clientId, yojimbo::Address(host, port));
}

void ServerConnection::disconnect()
{
	if (mNetwork) {
		mNetwork->Disconnect();
		delete mNetwork;
		mNetwork = NULL;

		onDisconnected();
	}
}

void ServerConnection::onDisconnected() {
	mCamera.setFollowPawn(NULL);
	mControlObject = nullptr;

	// delete ghosts
	for (auto it = mGhostMap.begin(); it != mGhostMap.end(); it++) {
		delete it->second;
	}
	mGhostMap.clear();
}

void ServerConnection::processPackets()
{
	if (mNetwork == NULL || !mNetwork->IsConnected())
		return;

	mNetwork->ReceivePackets();

	yojimbo::Message* message;
	for (int channelIdx = 0; channelIdx < NetConfig.numChannels; channelIdx++) {
		while ((message = mNetwork->ReceiveMessage(channelIdx)) != NULL)
		{
			handlePacket(message);
			mNetwork->ReleaseMessage(message);
		}
	}
}

void ServerConnection::advanceTime(double elapsedSeconds)
{
	if (mNetwork) {
		mNetTime += elapsedSeconds;
		bool was_connected = mNetwork->IsConnected();
		mNetwork->AdvanceTime(mNetTime);
		bool now_connected = mNetwork->IsConnected();

		if (was_connected && !now_connected)
			disconnect();
	}
}

void ServerConnection::sendPackets()
{
	if (mNetwork) {
		// work-around for yojimbo issue #69
		yojimbo::NetworkInfo info;
		mNetwork->GetNetworkInfo(info);
		if (seq_num_greater(info.numPacketsSent, mLastHeartbeat + HEARTBEAT_LENGTH)) {
			sendHeartbeatMessages();
			mLastHeartbeat = info.numPacketsSent;
		}

		mNetwork->SendPackets();
	}
}

void ServerConnection::sendHeartbeatMessages()
{
	// LOG(LogNetDebug) << "Sending heartbeat message on all channels...";
	for (int channelIdx = 0; channelIdx < NUM_NET_CHANNELS; channelIdx++) {
		auto msg = (HeartbeatMessage*)mNetwork->CreateMessage(ID_HEARTBEAT);
		mNetwork->SendMessage(channelIdx, msg);
	}
}

void ServerConnection::handlePacket(yojimbo::Message* msg)
{
	NetMessageTypes type = (NetMessageTypes) msg->GetType();

	switch (type) {
	case ID_CREATE_GHOSTS:
		readNewGhosts((CreateGhostsMessage*) msg);
		break;

	case ID_UPDATE_GHOSTS:
		readGhostUpdates((UpdateGhostsMessage*) msg);
		break;

	case ID_DESTROY_GHOSTS:
		readGhostDestroys((DestroyGhostsMessage*) msg);
		break;

	case ID_CONTROL_OBJECT_CHANGED:
		readControlObject((ControlObjectChangedMessage*) msg);
		break;

	case ID_CLIENTCMD:
		readClientCommand((ClientCmdMessage*) msg);
		break;

	case ID_SERVERCMD_RESPONSE:
		readServerCmdResponse((ServerCmdResponseMessage*) msg);
		break;

	default:
		LOG(LogError) << "Received unknown packet type " << type;
	}
}

NetObject* createNetObject(NetObjectTypeID type, BitStreamReader& stream)
{
	SceneContainer* scene = NULL;

  stream.readCheck();  // DEBUG

	// TODO move this into the classes somehow...
	if (type == CLASS_PLAYER || type == CLASS_STATICSHAPE || type == CLASS_TILECHUNK || type == CLASS_ENTITY || type == CLASS_COIN || type == CLASS_OBSERVER) {
		std::string sceneID;
		stream.readString(sceneID);
		scene = SceneManager::get()->getScene(sceneID);
	}

  stream.readCheck();  // DEBUG

	assert(stream.good());

	switch (type) {
	case CLASS_PLAYER:
		return new Player(scene);
	case CLASS_STATICSHAPE:
		return new StaticShape(scene);
	case CLASS_TILECHUNK:
		return new TileChunk(scene);
	case CLASS_ENTITY:
		return new Entity(scene);
	case CLASS_TILESET:
		return new TileSet();
	case CLASS_COIN:
		return new Coin(scene);
	case CLASS_OBSERVER:
		return new Observer(scene);
	case COMPONENT_TRIGGER:
		return new TriggerComponent(NULL);
	case COMPONENT_SPRITE:
		return new SpriteComponent(NULL);
	}

  LOG(LogError) << "Unknown object type " << type;
	assert(false);  // unknown object type
	return NULL;
}

void ServerConnection::readNewGhosts(CreateGhostsMessage* msg)
{
	LOG(LogNetDebug) << "Reading new ghosts...";
	LOG(LogNetDebug) << "  (" << msg->ghostIDs.size() << " new ghosts)";

	sendPacketAck(msg->packetID);

	BitStreamReader stream(msg->streamData, msg->streamBytes);
	for (size_t i = 0; i < msg->ghostIDs.size(); i++) {
		GhostID id = msg->ghostIDs[i];
		NetObjectTypeID type = msg->objectTypes[i];

		LOG(LogNetDebug) << "id " << id << " = ghost of type " << type;
		if (mGhostMap.find(id) != mGhostMap.end()) {
			LOG(LogNetDebug) << "Got create request for ghost ID " << id << ", but it was already created!";
			assert(false);
			continue;
		}

		NetObject* object = createNetObject(type, stream);
		mGhostMap[id] = object;
	}
}

void ServerConnection::readGhostDestroys(DestroyGhostsMessage* msg)
{
	LOG(LogNetDebug) << "Destroying ghosts";
	LOG(LogNetDebug) << "  (destroying " << msg->ghostIDs.size() << " ghosts)";

	sendPacketAck(msg->packetID);

	for (size_t i = 0; i < msg->ghostIDs.size(); i++) {
		GhostID id = msg->ghostIDs[i];

		auto it = mGhostMap.find(id);
		if (it == mGhostMap.end()) {
			LOG(LogError) << "ERROR - server tried to destroy ghost we don't have! (ghost ID: " << id << ")";
			continue;
		}
		delete it->second;
		mGhostMap.erase(it);
	}
}

void ServerConnection::readGhostUpdates(UpdateGhostsMessage* msg)
{
	mLogger.receivedPacket(NetLoggerEvent::PACKET_UPDATE, msg->packetID, mNetwork->GetClientId());

	sendPacketAck(msg->packetID);

	const unsigned int timestamp = msg->lastReceivedMoveNumber;

	BitStreamReader stream(msg->streamData, msg->streamBytes);
	for (unsigned int i = 0; i < msg->ghostIDs.size(); i++) {
		GhostID id = msg->ghostIDs[i];

		auto it = mGhostMap.find(id);
		assert(it != mGhostMap.end());

    stream.readCheck();  // DEBUG
		it->second->unpackUpdate(this, stream, timestamp);
    stream.readCheck();  // DEBUG
		assert(stream.good());
	}
}

void ServerConnection::readControlObject(ControlObjectChangedMessage* msg)
{
	if (msg->ghostID == 0) {
		mControlObject = NULL;
	} else {
		NetObject* controlObject = resolveGhostID(msg->ghostID);
		assert(controlObject != NULL);  // received unknown ghost ID for control object!
		mControlObject = dynamic_cast<Pawn*>(controlObject);
		assert(mControlObject != nullptr);  // received non-Player ghost ID for control object!
		mCamera.setFollowPawn(mControlObject);
		LOG(LogNetDebug) << "Read control object!";
	}
}

void ServerConnection::sendPacketAck(PacketID id)
{
	auto msg = (UnreliableSequencedAckMessage*) mNetwork->CreateMessage(ID_PACKET_ACK);
  msg->sequenceNumber = id;
	mNetwork->SendMessage(CHANNEL_ACKS, msg);
}

void ServerConnection::sendMovePacket(const Move& move)
{
	if (mNetwork == NULL || !mNetwork->IsConnected())
		return;

	auto msg = (MoveMessage*)mNetwork->CreateMessage(ID_MOVE_PLAYER);
	msg->move = move;
	mNetwork->SendMessage(CHANNEL_MOVES, msg);

	yojimbo::MeasureStream measure(yojimbo::GetDefaultAllocator());
	msg->Serialize(measure);
	mLogger.sentPacket(NetLoggerEvent::PACKET_MOVE, move.seq_num, mNetwork->GetClientId(), measure.GetBytesProcessed(), {});

	mLastMove = move;
}

NetObject* ServerConnection::resolveGhostID(GhostID id)
{
	if (id == 0)
		return NULL;

	auto it = mGhostMap.find(id);
	if (it == mGhostMap.end())
		return NULL;

	return it->second;
}

GhostID ServerConnection::getGhostID(const NetObject* obj)
{
	for (auto it = mGhostMap.cbegin(); it != mGhostMap.cend(); it++) {
		if (it->second == obj)
			return it->first;
	}

	return 0;
}

void ServerConnection::readClientCommand(ClientCmdMessage* msg)
{
	duk_context* ctx = ScriptEngine::get();

	// read command name
	const std::string& cmdName = msg->cmdName;

	// try and push the function onto the stack
	duk_get_global_string(ctx, "ClientCmd");
	duk_get_prop_lstring(ctx, -1, cmdName.c_str(), cmdName.size());
	duk_remove(ctx, -2);

	// did something exist by that name?
	if (duk_is_undefined(ctx, -1)) {
		LOG(LogWarning) << "Server tried to call undefined ClientCmd '" << cmdName << "'.";
		duk_pop(ctx);  // pop the undefined
		return;
	}

	// was it callable (i.e. a function)?
	if (!duk_is_callable(ctx, -1)) {
		LOG(LogWarning) << "Server tried to call non-callable ClientCmd '" << cmdName << "'.";
		duk_pop(ctx);  // pop the object, whatever it was
		return;
	}

	// read the number of arguments
	size_t n_args = msg->arguments.size();

	// was it a sane value?
	if (n_args > MAX_N_CLIENTCMD_ARGS) {
		LOG(LogWarning) << "Server tried to call ClientCmd '" << cmdName << "' with too many arguments (sent "
			<< n_args << " args, max is " << MAX_N_CLIENTCMD_ARGS << ").";
		duk_pop(ctx);  // pop the function
		return;
	}

	// push the arguments onto the stack
	duk_require_stack(ctx, n_args);
	for (size_t i = 0; i < n_args; i++) {
		const auto& buff = msg->arguments[i];
		try {
			DukValue v = DukValue::deserialize(ctx, buff.data(), buff.size());
			v.push();
		} catch (DukException& e) {
			LOG(LogError) << "Server tried to call ClientCmd '" << cmdName << "', but argument #" << (i + 1) << " could not be deserialized:\n"
				<< e.what();
			duk_pop_n(ctx, i + 1);  // also pop function
			return;
		}
	}

	// call ClientCmdxxx(args...)
	int rc = duk_pcall(ctx, n_args);
	if (rc != DUK_EXEC_SUCCESS) {
		duk_get_prop_string(ctx, -1, "stack");
		LOG(LogError) << "Error executing ClientCmd '" << cmdName << "':\n"
			<< duk_safe_to_string(ctx, -1);
		duk_pop_2(ctx);
	}

	duk_pop(ctx);  // pop return or error value
}

duk_ret_t ServerConnection::sendServerCmd(duk_context* ctx, bool with_response)
{
	duk_idx_t n_calling_args = duk_get_top(ctx);
	if (n_calling_args == 0) {
		return duk_error(ctx, DUK_ERR_ERROR, "Missing command name.");
	}

	const char* cmdName = duk_require_string(ctx, 0);

	if (mNetwork == NULL || !mNetwork->IsConnected()) {
		const char* errorMsg = "Cannot send ServerCmd '%s' - not connected.";
		if (with_response) {
			uint32_t promiseID = ScriptEngine::promise_push();
			duk_push_error_object(ctx, DUK_ERR_ERROR, errorMsg, cmdName);
			ScriptEngine::promise_reject(promiseID, DukValue::take_from_stack(ctx));
			return 1;
		}
		else {
			return duk_error(ctx, DUK_ERR_ERROR, errorMsg, cmdName);
		}
	}

	size_t n_cmd_args = n_calling_args - 1;  // TODO validate!

	std::vector< std::vector<char> > arguments(n_cmd_args);
	for (uint8_t i = 0; i < arguments.size(); i++) {
		try {
			DukValue arg = DukValue::copy_from_stack(ctx, 1 + i);
			auto& buff = arguments[i];
			buff = arg.serialize();

			if (buff.size() > UINT16_MAX) {
				duk_error(ctx, DUK_ERR_ERROR, "Argument %d is too large to send (%d bytes)", (i + 1), buff.size());
				return DUK_RET_ERROR;
			}
		} catch (DukException&) {
			duk_error(ctx, DUK_ERR_ERROR, "Failed to serialize argument %d (bad type?).", (i + 1));
			return DUK_RET_ERROR;
		}
	}

	auto msg = (ServerCmdMessage*)mNetwork->CreateMessage(ID_SERVERCMD);
	msg->promiseID = with_response ? ScriptEngine::promise_push() : 0;
	msg->cmdName = cmdName;
	msg->arguments = std::move(arguments);  // arguments is now invalid
	mNetwork->SendMessage(CHANNEL_RPC, msg);

	return with_response ? 1 : 0;
}

duk_ret_t ServerConnection::sendServerCmdWithReply(duk_context* ctx)
{
	return sendServerCmd(ctx, true);
}

duk_ret_t ServerConnection::sendServerCmdNoReply(duk_context * ctx)
{
	return sendServerCmd(ctx, false);
}

void ServerConnection::readServerCmdResponse(ServerCmdResponseMessage* msg)
{
	DukValue response = DukValue::deserialize(ScriptEngine::get(), msg->response.data(), msg->response.size());

	if (msg->ok)
		ScriptEngine::promise_resolve(msg->promiseID, response);
	else
		ScriptEngine::promise_reject(msg->promiseID, response);
}
