#pragma once

#include <script/ScriptEngine.h>
#include <script/ScriptObject.h>

#include <stdint.h>
#include <map>

#include <Sim.h>
#include <Camera.h>
#include <Pawn.h>
#include <scene/SceneObjectPtr.h>
#include <net/NetObject.h>
#include <net/PacketMessageTypes.h>
#include <net/GhostInfo.h>
#include <net/NetLogger.h>

SCRIPT_CLASS("ServerConnection", ServerConnection, void, "Represents a connection to a server. Acts as a singleton in script.");
class ServerConnection : public ScriptObject, public yojimbo::Adapter
{
public:
	ServerConnection();
	virtual ~ServerConnection();

	SCRIPT_METHOD(ServerConnection, "connect", &ServerConnection::connect, "Connect to a remote server.");
	void connect(const char* host, unsigned int port);

	SCRIPT_METHOD(ServerConnection, "disconnect", &ServerConnection::disconnect, "Cleanly disconnect from the server.");
	void disconnect();

	void advanceTime(double elapsedSeconds);
	void processPackets();
	void sendPackets();

	SCRIPT_METHOD(ServerConnection, "resolveGhostID", &ServerConnection::resolveGhostID, "Get the object corresponding to a ghost ID. Returns NULL if the ghost ID does not map to a valid object.")
	NetObject* resolveGhostID(GhostID id);

	SCRIPT_METHOD(ServerConnection, "getGhostID", &ServerConnection::getGhostID, "Get the corresponding ghost ID of a NetObject. Returns 0 if object is not a ghost.");
	GhostID getGhostID(const NetObject* obj);
	
	void sendMovePacket(const Move& move);
	inline const Move& lastMove() const {
		return mLastMove;
	}

	SCRIPT_PROPERTY(ServerConnection, "controlObject", &ServerConnection::controlObject, nullptr, "Get the current control object.");
	inline Pawn* controlObject() const { return mControlObject.get(); }

	SCRIPT_PROPERTY(ServerConection, "camera", &ServerConnection::camera, nullptr, "Get the camera.");
	inline Camera* camera() { return &mCamera; }

	SCRIPT_METHOD_VARARGS(ServerConnection, "sendServerCmdWithReply", &ServerConnection::sendServerCmdWithReply, "Send an RPC to the server (guaranteed). Returns a Promise.");
	duk_ret_t sendServerCmdWithReply(duk_context* ctx);

	SCRIPT_METHOD_VARARGS(ServerConnection, "sendServerCmd", &ServerConnection::sendServerCmdNoReply, "Send an RPC to the server (guaranteed). Does not return a Promise.");
	duk_ret_t sendServerCmdNoReply(duk_context* ctx);

	static const int MAX_N_CLIENTCMD_ARGS = 20;

	SCRIPT_PROPERTY(ServerConnection, "logger", &ServerConnection::logger, nullptr, "Get the NetLogger for the client.");
	NetLogger* logger() { return &mLogger; }

protected:
	yojimbo::MessageFactory* CreateMessageFactory(yojimbo::Allocator& allocator) override;

private:
	ServerConnection(const ServerConnection& rhs) = delete;
	ServerConnection& operator= (const ServerConnection& rhs) = delete;

	yojimbo::Client* mNetwork;
	double mNetTime;  // note: this does not match Sim::getTimeSeconds() due to the fact that this needs to be reset to 0 at the start of every connection
	static const uint64_t HEARTBEAT_LENGTH = 10000;  // every X packets, send an empty message in all channels to work around yojimbo issue #69
	uint64_t mLastHeartbeat;  // for yojimbo issue #69 hack
	void sendHeartbeatMessages();

	std::map<GhostID, NetObject*> mGhostMap;

	SceneObjectPtr<Pawn> mControlObject;
	Camera mCamera;  // I don't like putting this here, but it needs to be updated when mControlObject changes
	Move mLastMove;

	void onDisconnected();
	void handlePacket(yojimbo::Message* msg);

	void readNewGhosts(CreateGhostsMessage* msg);
	void readGhostUpdates(UpdateGhostsMessage* msg);
	void readGhostDestroys(DestroyGhostsMessage* msg);
	void readControlObject(ControlObjectChangedMessage* msg);
	void readClientCommand(ClientCmdMessage* msg);
	void readServerCmdResponse(ServerCmdResponseMessage* msg);

	void sendPacketAck(PacketID id);

	duk_ret_t sendServerCmd(duk_context* ctx, bool with_response);

	NetLogger mLogger;
};
