#include "GetDPI.h"

#ifdef WIN32  // Windows
#include <Windows.h>
unsigned int getDPI()
{
  return GetDpiForSystem();
}

#else  // unhandled platform
unsigned int getDPI() {
  return 96;
}
#endif

float getDPIScale()
{
  return getDPI() / 96.0f;
}
