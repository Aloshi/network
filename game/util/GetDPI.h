#pragma once

#include <script/ScriptEngine.h>

SCRIPT_FUNCTION("getDPIScale", getDPIScale, "Get the DPI scale (system DPI / 96).");
float getDPIScale();
