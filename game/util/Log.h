#pragma once

#define LOG(level) Log().getStream(level)

#include <string>
#include <sstream>
#include <assert.h>
#include <iostream>

#include <util/Signal.h>

enum LogLevel : int { LogNetDebug = -1, LogTrace = 0, LogDebug = 1, LogInfo = 2, LogWarning = 3, LogError = 4, LogFatal = 5 };

class Log
{
public:
	~Log();
	std::ostringstream& getStream(LogLevel level);

	static std::string getLogPath();

	static void flush();
	static void open();
	static void close();

	inline static size_t registerCallback(const std::function<void(int, const std::string&)>& cb) {
		return sSignal.connect(cb);
	}
	inline static void unregisterCallback(size_t id) {
		sSignal.disconnect(id);
	}

protected:
	std::ostringstream mStream;

	static Simple::Signal<void(int, const std::string&)> sSignal;
	static FILE* sFile;

private:
	LogLevel mMsgLevel;
};
