#pragma once

#include <stdint.h>

// 16-bit sequence numbers (with wrap-around)

inline bool seq_num_greater(uint16_t s1, uint16_t s2) {
	return ((s1 > s2) && (s1 - s2 <= 32768)) ||
		((s1 < s2) && (s2 - s1  > 32768));
}

inline bool seq_num_greater(uint64_t s1, uint64_t s2) {
	return ((s1 > s2) && (s1 - s2 <= 9223372036854775808ULL)) ||
		((s1 < s2) && (s2 - s1  > 9223372036854775808ULL));
}

inline bool seq_num_less(uint16_t s1, uint16_t s2) {
	return seq_num_greater(s2, s1);
}

inline int seq_num_dist(uint16_t a, uint16_t b) {
	static const int32_t CYCLE_LENGTH = (UINT16_MAX + 1);
	int32_t d = (b - a) % CYCLE_LENGTH;
	if (d * 2 >= CYCLE_LENGTH) d -= CYCLE_LENGTH;
	if (d * 2 < -CYCLE_LENGTH) d += CYCLE_LENGTH;
	return d;
}

inline int seq_num_sub(uint16_t a, uint16_t b) {
	int32_t val = a - b;
	if (val < 0)
		val += (UINT16_MAX + 1);
	return static_cast<uint16_t>(val);
}
