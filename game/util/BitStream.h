#pragma once

#include <yojimbo.h>

#include <net/GhostInfo.h>

#include <math/Vector2.h>

class BitStreamWriter {
public:
	struct State {
	protected:
		friend BitStreamWriter;
		yojimbo::WriteStream::State yojimboState;
		bool overrun;
	public:
		inline int bitsWritten() const {
			return yojimboState.bitsWritten;
		}
		inline int bytesWritten() const {
			return (yojimboState.bitsWritten + 7) / 8;
		}
	};

	// writer does not take ownership of data
	BitStreamWriter(uint8_t* data, size_t nBytes, yojimbo::Allocator& alloc = yojimbo::GetDefaultAllocator());

	BitStreamWriter(const BitStreamWriter& rhs) = delete;
	BitStreamWriter& operator=(const BitStreamWriter& rhs) = delete;

	// base operations
	bool writeBool(bool val);  // returns value that was written
	void writeInt(int32_t value, int32_t min, int32_t max);
	// void writeIntRelative(int32_t value, int32_t prev);
	// void writeBytes(void* data, int32_t nBytes);
	void writeFloat(float val);
	void writeString(const char* str);
	void writeString(const std::string& str);
	void writeCheck();
	void flush();

	State state() const;
	void setState(const State& rhs);

	int bitsWritten() const;
	inline int bytesWritten() const {
		return (bitsWritten() + 7) / 8;
	}

	void takeBuffer(uint8_t** buffer_out, size_t* nBytes_out);  // invalidates this

	// generic convenience functions
	inline void writeUint8(int32_t value) { writeInt(value, 0, UINT8_MAX); }
	inline void writeUint16(int32_t value) { writeInt(value, 0, UINT16_MAX); }
	inline void writeInt16(int32_t value) { writeInt(value, INT16_MIN, INT16_MAX); }
	void writeUint32(uint32_t value);
	inline void writeInt32(int32_t value) { writeInt(value, INT32_MIN, INT32_MAX); }

	// specialized convenience functions
	inline void writeGhostID(GhostID id) { writeInt(id, 0, MAX_GHOSTS - 1); }

	inline void writeVector2f(const Vector2f& vec) {
		writeFloat(vec.x);
		writeFloat(vec.y);
	}
	inline void writeVector2i(const Vector2i& vec) {
		writeInt32(vec.x);
		writeInt32(vec.y);
	}

	inline bool bufferOverrun() const {
		return mOverrun;
	}

protected:
	bool wouldWritePastEnd(int32_t nBits) const;

	bool mOverrun;
	yojimbo::WriteStream mStream;
};

class BitStreamReader {
public:
	// reader does not take ownership of data
	BitStreamReader(const uint8_t* data, size_t nBytes, yojimbo::Allocator& alloc = yojimbo::GetDefaultAllocator());

	BitStreamReader(const BitStreamReader& rhs) = delete;
	BitStreamReader& operator=(const BitStreamReader& rhs) = delete;

	int bitsRead() const;

	// base operations
	bool readBool();
	void readInt(int32_t& value, int32_t min, int32_t max);
	// void readIntRelative(int32_t& value, int32_t prev);
	// void readBytes(void* data, int32_t nBytes);
	float readFloat();
	void readString(std::string& str);
	void readCheck();

	// generic convenience functions
	inline uint8_t readUint8() {
		int32_t temp;
		readInt(temp, 0, UINT8_MAX);
		return (uint8_t)temp;
	}
	inline uint16_t readUint16() {
		int32_t temp;
		readInt(temp, 0, UINT16_MAX);
		return (uint16_t) temp;
	}
	inline int16_t readInt16() {
		int32_t temp;
		readInt(temp, INT16_MIN, INT16_MAX);
		return (int16_t) temp;
	}
	uint32_t readUint32();
	inline int32_t readInt32() {
		int32_t temp;
		readInt(temp, INT32_MIN, INT32_MAX);
		return temp;
	}

	// specialized convenience functions
	inline GhostID readGhostID() {
		int32_t temp;
		readInt(temp, 0, MAX_GHOSTS - 1);
		return (GhostID) temp;
	}

	inline Vector2f readVector2f() {
    float x = readFloat();
    float y = readFloat();
		return Vector2f(x, y);  // don't do a one-liner here - argument evaluation order is undefined
	}
	inline Vector2i readVector2i() {
    int x = readInt32();
    int y = readInt32();
		return Vector2i(x, y);
	}

	inline bool good() const {
		return !mError;
	}

protected:
	bool mError;
	yojimbo::ReadStream mStream;
};
