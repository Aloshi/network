#include <util/BitStream.h>

// TODO CHECK FOR/SET mOverrun EVERYWHERE!

BitStreamWriter::BitStreamWriter(uint8_t* data, size_t nBytes, yojimbo::Allocator& alloc)
	: mOverrun(false), mStream(alloc, data, nBytes)
{
}

bool BitStreamWriter::writeBool(bool val) {
	if (!wouldWritePastEnd(1)) {
		uint32_t int_val = val ? 1 : 0;
		bool ok = mStream.SerializeBits(int_val, 1);
		assert(ok);
	}
	else {
		mOverrun = true;
	}

	return val;
}

void BitStreamWriter::writeInt(int32_t value, int32_t min, int32_t max) {
	if (wouldWritePastEnd(yojimbo::bits_required(min, max)))
		mOverrun = true;
	else
		mStream.SerializeInteger(value, min, max);
}

void BitStreamWriter::writeUint32(uint32_t val) {
	if (wouldWritePastEnd(32))
		mOverrun = true;
	else
		mStream.SerializeBits(val, 32);
}

void BitStreamWriter::writeFloat(float val) {
	if (wouldWritePastEnd(32)) {
		mOverrun = true;
		return;
	}

	uint32_t int_value;
	memcpy(&int_value, &val, 4);
	mStream.SerializeBits(int_value, 32);
}

void BitStreamWriter::writeString(const char* str) {
	uint32_t len = strlen(str);

	if (wouldWritePastEnd(32 + mStream.GetAlignBits() + len * 8)) {
		mOverrun = true;
		return;
	}

	writeUint32(len);  // TODO this is dumb
	mStream.SerializeBytes((uint8_t*)str, len);
}

void BitStreamWriter::writeCheck() {
	if (wouldWritePastEnd(mStream.GetAlignBits() + 32))
		mOverrun = true;
	else
		mStream.SerializeCheck();
}

void BitStreamWriter::writeString(const std::string& str) {
	if (wouldWritePastEnd(32 + mStream.GetAlignBits() + str.size() * 8)) {
		mOverrun = true;
		return;
	}

	writeUint32(str.size());  // TODO this is dumb
	mStream.SerializeBytes((uint8_t*)str.data(), str.size());
}

void BitStreamWriter::flush() {
	mStream.Flush();
}

BitStreamWriter::State BitStreamWriter::state() const {
	State s;
	s.yojimboState = mStream.GetState();
	s.overrun = mOverrun;
	return s;
}

void BitStreamWriter::setState(const BitStreamWriter::State& rhs) {
	mStream.SetState(rhs.yojimboState);
	mOverrun = rhs.overrun;
}

int BitStreamWriter::bitsWritten() const {
	return mStream.GetBitsProcessed();
}

void BitStreamWriter::takeBuffer(uint8_t** buffer_out, size_t* nBytes_out)
{
	*buffer_out = (uint8_t*)mStream.GetData();
	*nBytes_out = mStream.GetBytesProcessed();
	mStream = yojimbo::WriteStream(mStream.GetAllocator(), NULL, 0);
}

bool BitStreamWriter::wouldWritePastEnd(int32_t nBits) const
{
	return mStream.GetBitsAvailable() < nBits;
}

// ------------------

BitStreamReader::BitStreamReader(const uint8_t* data, size_t nBytes, yojimbo::Allocator& alloc)
	: mError(false), mStream(alloc, data, nBytes)
{
}

int BitStreamReader::bitsRead() const
{
	return mStream.GetBitsProcessed();
}

bool BitStreamReader::readBool() {
	uint32_t val;
	bool ok = mStream.SerializeBits(val, 1);
	assert(ok);

	return (val != 0);
}

void BitStreamReader::readInt(int32_t& value, int32_t min, int32_t max) {
	bool ok = mStream.SerializeInteger(value, min, max);
	assert(ok);
}

uint32_t BitStreamReader::readUint32() {
	uint32_t val;
	bool ok = mStream.SerializeBits(val, 32);
	assert(ok);

	return val;
}

float BitStreamReader::readFloat() {
	uint32_t int_value;
	bool ok = mStream.SerializeBits(int_value, 32);
	assert(ok);

	float val;
	memcpy(&val, &int_value, 4);
	return val;
}

void BitStreamReader::readString(std::string& str) {
	uint32_t len = readUint32();  // TODO this is dumb
	str.resize(len, '\0');
	bool ok = mStream.SerializeBytes((uint8_t*)str.data(), len);
	assert(ok);
}

void BitStreamReader::readCheck() {
	if (!mStream.SerializeCheck())
		throw std::runtime_error("Stream check failed");
}
