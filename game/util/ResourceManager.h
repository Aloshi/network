#pragma once

#include <memory>
#include <map>

template <typename KeyT, typename ValT>
class ResourceManager {
public:
	std::shared_ptr<ValT> get(const KeyT& key) {
		auto it = gMap.find(key);
		if (it == gMap.end() || it->second.expired()) {
			std::shared_ptr<ValT> val = acquire(key);
			gMap[key] = val;
			return val;
		} else {
			return it->second.lock();
		}
	}

protected:
	// filled in by implementation
	virtual std::shared_ptr<ValT> acquire(const KeyT& key) = 0;

private:
	std::map<KeyT, std::weak_ptr<ValT> > gMap;
};