#include "Log.h"
#include <stdio.h>
#include <assert.h>
#include <iostream>

FILE* Log::sFile = NULL;
Simple::Signal<void(int, const std::string&)> Log::sSignal;

std::string Log::getLogPath()
{
	return "log.txt";
}

void Log::open()
{
	assert(sFile == NULL);
	sFile = fopen(getLogPath().c_str(), "w");
}

std::ostringstream& Log::getStream(LogLevel level)
{
	/*switch (level) {
		case LogError:
			mStream << "ERROR: \t";
			break;
		case LogWarning:
			mStream << "WARNING: \t";
			break;
		default:
			break;
	}*/

	mMsgLevel = level;

	return mStream;
}

void Log::flush()
{
	fflush(sFile);
}

void Log::close()
{
	fclose(sFile);
	sFile = NULL;
}

Log::~Log()
{
	mStream << std::endl;
	
	if(sFile == NULL)
	{
		// not open yet, print to stdout
		std::cerr << "ERROR - tried to write to log file before it was open! The following won't be logged:\n";
		std::cerr << mStream.str();
		return;
	}

	// write to log file
	fprintf(sFile, "%s", mStream.str().c_str());

	// also print to system console
	fprintf(stderr, "%s", mStream.str().c_str());

	// trigger callbacks
	sSignal.emit(mMsgLevel, mStream.str());
}
