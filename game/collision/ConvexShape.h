#pragma once

#include "CollisionShape.h"

#include <vector>

class ConvexShape : public CollisionShape
{
public:
	ConvexShape() {}
	ConvexShape(const std::vector<Vector2f>& verts);

	void setVertices(const std::vector<Vector2f>& verts);

	Box2f calcWorldBox(const Transform& transform) const override;

	bool findCollision(Vector2f thisStartPos, Vector2f thisEndPos, const ConvexShape* other,
					   Vector2f otherPos, CollisionInfo* info_out) const override;

	void debugDraw(sf::RenderTarget& target, const Transform& transform, sf::Color color) const override;

	inline const std::vector<Vector2f>& vertices() const { return mVertices; }

	static void calc_mtv(const Vector2f& a, const Vector2f& b, Vector2f normal, float v, float* prevMinOverlap, bool* prevOnEdge, Vector2f* overlapVec);

	// NOTE: the axis must be normalized to get accurate projections
	Vector2f project(const Vector2f& axis_normalized, const Vector2f& pos) const;

private:
	std::vector<Vector2f> mVertices;
	std::vector<Vector2f> mNormals;
};
