#include "ConvexShape.h"

#include <SFML/Graphics.hpp>

#include <algorithm>

ConvexShape::ConvexShape(const std::vector<Vector2f>& verts)
{
	setVertices(verts);
}

void ConvexShape::setVertices(const std::vector<Vector2f>& verts)
{
	mVertices = verts;
	mNormals.clear();

	if (verts.size() > 1) {
		for (unsigned int i = 0; i < verts.size(); i++) {
			const Vector2f edge = verts[(i + 1) % verts.size()] - verts[i];
			const Vector2f normal = Vector2f(edge.y, -edge.x).normalized();
			mNormals.push_back(normal);
		}
	}
}

bool COLLISION_DEBUG = false;

// given a and b overlap, what is the minimum amount along normal that can be added to a to make it so a and b do not overlap?
// can assume a and b are already overlapping
void ConvexShape::calc_mtv(const Vector2f& a, const Vector2f& b, Vector2f normal, float v, float* prevMinOverlap, bool* prevOnEdge, Vector2f* overlapVec) {
	assert(a.x <= b.y && b.x <= a.y);
	assert(!normal.isZero());

	// TODO pretty sure this is wrong
	// 2 possible directions (left or right)
	float overlap1 = b.y - a.x;  // shift a right
	float overlap2 = b.x - a.y;  // shift a left
	float overlap;
	if (fabs(overlap1) < fabs(overlap2)) {
		overlap = overlap1;
	} else {
		overlap = overlap2;
	}

	if (overlap < 0.0f) {  // TODO this is probably wrong
		overlap = -overlap;
		normal = normal * -1.0f;
	}
	if (a.y == b.x) {
		normal = normal * -1.0f;
	}

	bool onEdge = overlap < 1e-3 && fabs(v) < 1e-3;
	if (onEdge)
		overlap = 0.0f;

	// std::cout << "consider mtv " << normal << " with overlap " << overlap << " (intervals: " << a << " and " << b << "), edge:" << onEdge << std::endl;

	if (overlap >= 0.0f && overlap <= *prevMinOverlap && !*prevOnEdge) {
		*prevMinOverlap = overlap;
		*overlapVec = normal;
		*prevOnEdge = onEdge;
		// std::cout << "  mtv set: " << normal << ", onEdge: " << onEdge << std::endl;
	}
}

// TODO change this to use applyTransform() on every vertex
bool ConvexShape::findCollision(Vector2f startPos, Vector2f endPos, const ConvexShape* other, Vector2f otherPos, CollisionInfo* info) const
{
	if (mVertices.empty() || other->mVertices.empty())
		return false;

	const Vector2f velocity = (endPos - startPos);
	info->normal = Vector2f(0, 0);
	info->time = 0.0f;
	
	float minOverlapAmt = 1e16f;
	Vector2f minOverlapVec(0, 0);
	bool minOnEdge = false;

	std::vector<Vector2f> axes = mNormals;
	axes.insert(axes.end(), other->mNormals.begin(), other->mNormals.end());

	// if we are sweeping, the axis of velocity might be an axis of separation,
	// so we also check velocity's normal
	if (startPos != endPos && !velocity.isZero())
		axes.push_back(Vector2f(-velocity.y, velocity.x).normalized());

	float t_min = 1e16f;  // earliest no-longer-colliding time
	float t_max = -1e16f;  // latest started-colliding time
	for (unsigned int i = 0; i < axes.size(); i++) {
		const auto& normal = axes[i];
		
		const Vector2f thisStartProj = this->project(normal, startPos);
		const Vector2f otherProj = other->project(normal, otherPos);

		// check for intersect when projected onto this axis
		const float v = velocity.dot(normal);  // projection without redundant normalization (since normal is already magnitude 1)
		if (fabs(v) < 1e-3) {
			// body is not moving on this axis, use static test
			if (otherProj.x < thisStartProj.y && otherProj.y > thisStartProj.x) {
				// already colliding on this axis (i.e. colliding forever)
				// in this case, t_enter = -infinity, t_exit = infinity, so updating our ts would be useless, i.e.
				// min(t_min, t_exit) = t_min
				// max(t_max, t_enter) = t_max

				// but we should check if this axis is the mtv...
				t_max = std::max(t_max, 0.0f);
				t_min = std::min(t_min, 1.0f);  // not necessary, just gets around an assert
				calc_mtv(thisStartProj, otherProj, normal, v, &minOverlapAmt, &minOnEdge, &minOverlapVec);
			} else {
				return false;  // separating axis found
			}
		} else {
			float t_enter = (otherProj.x - thisStartProj.y) / v;
			float t_exit = (otherProj.y - thisStartProj.x) / v;
			if (v < 0)  // if velocity is against normal, times should be swapped
				std::swap(t_enter, t_exit);

			assert(t_enter <= t_exit);

			// early-out if t_enter > 1.0 or t_exit < 0.0 (no collision on this axis from 0 <= t <= 1, separating axis found)
			if (t_exit < 0.0f || t_enter > 1.0f)
				return false;

			// if intersecting during t = 0, snap t_enter to 0
			if (t_enter <= 0.0f && t_exit >= 0.0f) {
				if (t_exit == 0.0f)  // declare separating if moving away from shared edge
					return false;

				t_enter = 0.0f;
				calc_mtv(thisStartProj, otherProj, normal, v, &minOverlapAmt, &minOnEdge, &minOverlapVec);
			}

			t_min = std::min(t_min, t_exit);  // update earliest no-longer-colliding time
			t_max = std::max(t_max, t_enter);  // update latest started-colliding time

			if (t_max > t_min)  // early-out
				return false;

			if (t_enter == t_max) {  // if the max did anything
				// if we turn out to be colliding, it'll be on the axis that had the max t_enter
				// so record the collision normal (used if we return true)
				info->normal = normal * (v < 0 ? 1.0f : -1.0f);
			}
		}
	}

	// no separating axes found
	assert(t_max <= t_min);

	assert(t_max != -1e16f && t_min != 1e16f);
	info->time = t_max;

	// std::cout << std::endl;

	if (info->time == 0.0f) {
		assert(!minOverlapVec.isZero());
		assert(minOverlapAmt >= 0.0f && minOverlapAmt != 1e16f);
		info->normal = minOverlapVec;
		info->mtv = minOverlapVec * minOverlapAmt;
	} else {
		assert(!info->normal.isZero());

		if (minOnEdge) {
			info->normal = minOverlapVec;
		}

		info->mtv = Vector2f(0, 0);
	}

	assert(info->time >= 0.f && info->time <= 1.0f);
	return true;
}

Vector2f ConvexShape::project(const Vector2f& axis, const Vector2f& pos) const
{
	assert(fabs(axis.len() - 1.0f) < 1e-6f);

	float min = axis.dot(mVertices[0] + pos);
	float max = min;

	for (unsigned int i = 1; i < mVertices.size(); i++) {
		float p = axis.dot(mVertices[i] + pos);
		min = std::min(min, p);
		max = std::max(max, p);
	}

	return Vector2f(min, max);
}

// TODO change this to use applyTransform() on each vertex
void ConvexShape::debugDraw(sf::RenderTarget& target, const Transform& transform, sf::Color color) const
{
	if (mVertices.empty())
		return;

	if (mVertices.size() == 1) {
		sf::CircleShape shape;
		shape.setPosition(mVertices[0] + transform.position);
		shape.setFillColor(color);
		shape.setRadius(1.0f);
		target.draw(shape);
		return;
	}

	std::vector<sf::Vertex> verts;
	for (unsigned int i = 0; i < mVertices.size(); i++) {
		verts.push_back(sf::Vertex(mVertices[i] + transform.position, color));
	}
	verts.push_back(sf::Vertex(mVertices[0] + transform.position, color));

	target.draw(verts.data(), verts.size(), sf::LinesStrip);

	// draw normals
	/*for (unsigned int i = 0; i < mNormals.size(); i++) {
	sf::RectangleShape rect(sf::Vector2f(mNormals[i].x * 8.f + 1, mNormals[i].y * 8.f + 1));
	rect.setPosition(pos);
	rect.setFillColor(color);
	target.draw(rect);
	}*/
}

// TODO change this to use applyTransform() on each vertex
Box2f ConvexShape::calcWorldBox(const Transform& transform) const
{
	if (mVertices.size() == 0)
		return Box2f(transform.position, transform.position);
	
	Box2f box(mVertices[0], mVertices[0]);
	for (unsigned int i = 1; i < mVertices.size(); i++) {
		Vector2f vert = mVertices[i];
		box.min.x = std::min(box.min.x, vert.x);
		box.min.y = std::min(box.min.y, vert.y);
		box.max.x = std::max(box.max.x, vert.x);
		box.max.y = std::max(box.max.y, vert.y);
	}

	box.min += transform.position;
	box.max += transform.position;
	return box;
}
