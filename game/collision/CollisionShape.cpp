#include "CollisionShape.h"

#include "ConvexShape.h"

bool CollisionShape::findCollision(Vector2f thisPos, const CollisionShape* other, Vector2f otherStartPos, Vector2f otherEndPos, CollisionInfo* info_out) const {
	return ((ConvexShape*)other)->findCollision(otherStartPos, otherEndPos, (ConvexShape*) this, thisPos, info_out);
}
