#include "ShapeList.h"

void ShapeList::add(SceneObject* source, CollisionShape* shape, const Transform& transform)
{
	mShapes.push_back(ShapeListEntry { source, shape, transform });
}

void ShapeList::clear()
{
	mShapes.clear();
}

bool ShapeList::findFirstCollision(const CollisionShape* shape, const Transform& start, const Vector2f& endPos, CollisionInfo* info_out)
{
	assert(info_out != NULL);
	info_out->time = 2.0f;

	int col_idx = -1;
	for (unsigned int i = 0; i < mShapes.size(); i++) {
		CollisionInfo check_info;
		const bool collided = mShapes[i].shape->findCollision(mShapes[i].transform.position, shape, start.position, endPos, &check_info);
		if (collided && check_info.time < info_out->time) {
			*info_out = check_info;
			col_idx = i;
		} else if (collided && check_info.time == info_out->time) {
			Vector2f old_dist = mShapes[col_idx].transform.position - start.position;
			Vector2f new_dist = mShapes[i].transform.position - start.position;
			if (new_dist.len() < old_dist.len()) {
				*info_out = check_info;
				col_idx = i;
			}
		}
	}

	info_out->hitObject = (col_idx != -1) ? mShapes[col_idx].source : NULL;

	return (info_out->time != 2.0f);
}

bool ShapeList::findAllCollisions(const CollisionShape* shape, const Transform& start, const Vector2f& endPos, std::vector<CollisionInfo>* infos_out)
{
	assert(infos_out != NULL);
	infos_out->clear();

	for (unsigned int i = 0; i < mShapes.size(); i++) {
		CollisionInfo check_info;
		const bool collided = mShapes[i].shape->findCollision(mShapes[i].transform.position, shape, start.position, endPos, &check_info);
		if (collided) {
			check_info.hitObject = mShapes[i].source;
			infos_out->push_back(check_info);
		}
	}

	return !infos_out->empty();
}