#pragma once

#include "CollisionShape.h"

#include <vector>

class SceneObject;

class ShapeList
{
public:
	void add(SceneObject* source, CollisionShape* shape, const Transform& transform);
	void clear();

	bool findFirstCollision(const CollisionShape* shape, const Transform& start, const Vector2f& endPos, CollisionInfo* info_out);
	bool findAllCollisions(const CollisionShape* shape, const Transform& start, const Vector2f& endPos, std::vector<CollisionInfo>* infos_out);

	inline unsigned int size() const { return mShapes.size(); }
private:
	struct ShapeListEntry
	{
		SceneObject* source;
		CollisionShape* shape;
		Transform transform;
	};

	std::vector<ShapeListEntry> mShapes;
};
