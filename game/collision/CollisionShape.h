#pragma once

#include <math/Vector2.h>
#include <math/Box2.h>

struct Transform
{
	Vector2f position;
	// float rotation;
	// Vector2f scale;
};

class CollisionShape;
class SceneObject;

struct CollisionInfo
{
	Vector2f normal;
	Vector2f mtv;  // only set if time == 0
	float time;

	SceneObject* hitObject;
};

class ConvexShape;

namespace sf {
	class RenderTarget;
	class Color;
}

class CollisionShape
{
public:
	// if I actually decide to have different CollisionShapes beyond ConvexShape,
	// I can modify this to use double dispatch/the visitor pattern to resolve types
	// for now, we'll just hardcode everything to ConvexShape
	bool findCollision(Vector2f thisPos, const CollisionShape* other, Vector2f otherStartPos,
					   Vector2f otherEndPos, CollisionInfo* info_out) const;

	virtual Box2f calcWorldBox(const Transform& transform) const = 0;
	virtual bool findCollision(Vector2f thisStartPos, Vector2f thisEndPos, const ConvexShape* other,
							   Vector2f otherPos, CollisionInfo* info_out) const = 0;
	
	virtual void debugDraw(sf::RenderTarget& target, const Transform& transform, sf::Color color) const = 0;
};
