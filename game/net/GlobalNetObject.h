#pragma once

#include <net/NetObject.h>

// A net object that is synchronized to all connected clients
class GlobalNetObject : public NetObject {
public:
	typedef Simple::Connection<void(NetObject*)> SignalReceipt;
	static SignalReceipt registerAddedCallback(const std::function<void(NetObject*)>& cb);

	static inline const std::vector<GlobalNetObject*>& all() {
		return smGlobalNetObjs;
	}

	GlobalNetObject();
	virtual ~GlobalNetObject();

private:
	static Simple::Signal<void(NetObject*)> smAddedSignal;
	static std::vector<GlobalNetObject*> smGlobalNetObjs;
};