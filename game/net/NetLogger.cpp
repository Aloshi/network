#include <net/NetLogger.h>

static const std::string ANY_GUID;

NetLogger::~NetLogger()
{
	stop();
}

void NetLogger::start(const std::string& path, bool append)
{
	stop();

	std::ios_base::openmode mode = std::ios_base::out;
	if (!append)
		mode |= std::ios_base::trunc;
	out_.open(path, mode);
}

void NetLogger::stop()
{
	if (out_.is_open())
		out_.close();
}

void NetLogger::sentPacket(NetLoggerEvent::PacketType type, uint16_t seq_num, ClientGUID to,
	uint32_t size, const std::vector<GhostID>& affects)
{
	NetLoggerEvent ev {
		type, NetLoggerEvent::SENT, seq_num, size, std::to_string(to),
		std::chrono::system_clock::now(), affects
	};
	this->writeEvent(&ev);
}

void NetLogger::receivedPacket(NetLoggerEvent::PacketType type, uint16_t seq_num, ClientGUID me)
{
	NetLoggerEvent ev {
		type, NetLoggerEvent::RECEIVED, seq_num, 0, std::to_string(me),
		std::chrono::system_clock::now()
	};
	this->writeEvent(&ev);
}

void NetLogger::ignoredPacket(NetLoggerEvent::PacketType type, uint16_t seq_num, ClientGUID me)
{
	NetLoggerEvent ev {
		type, NetLoggerEvent::IGNORED, seq_num, 0, std::to_string(me),
		std::chrono::system_clock::now()
	};
	this->writeEvent(&ev);
}

void NetLogger::writeEvent(const NetLoggerEvent* ev)
{
	if (!out_.is_open() || out_.bad())
		return;

	// NOTE: this assumes NetLoggerEvent::clock's epoch is fixed
	// (i.e. it will break with high resolution clock, since the "start of the epoch"
	// is typically defined as "when the machine booted")
	static const char* sep = " ";
	static const char* eol = "\n";
	auto timestamp_ms = std::chrono::duration_cast<std::chrono::milliseconds>(ev->timestamp.time_since_epoch());
	out_ << ev->type << sep << ev->event << sep << ev->seq_num << sep << ev->size << sep << ev->client_guid << sep
		<< std::to_string(ev->timestamp.time_since_epoch().count()) << sep << std::to_string(timestamp_ms.count()) << sep << ev->affects.size() << sep;
	for (unsigned int i = 0; i < ev->affects.size(); i++) {
		out_ << ev->affects[i] << sep;
	}
	out_ << eol;
}

void NetAnalyzer::load(std::ifstream& in)
{
	pairsDirty_ = true;
	clientsDirty_ = true;

	std::string line;
	uint32_t n_begin = events_.size();
	while (std::getline(in, line)) {
		std::istringstream liness(line);

		NetLoggerEvent ev;
		{
			std::string type, event, seq_num, size;
			liness >> type >> event >> seq_num >> size >> ev.client_guid;
			ev.type = (NetLoggerEvent::PacketType) std::stoi(type);
			ev.event = (NetLoggerEvent::EventType) std::stoi(event);
			ev.seq_num = (uint16_t) std::stoi(seq_num);
			ev.size = (uint32_t) std::stol(size);
		}

		// timestamp
		{
			std::string timestampStr;
			liness >> timestampStr;  // timestamp_raw
			auto dur = NetLoggerEvent::clock::duration(std::stoll(timestampStr));
			ev.timestamp = NetLoggerEvent::clock::time_point(dur);
			liness >> timestampStr;  // ignore timestamp_ms
		}

		// read affects
		{
			std::string nAffects_str;
			liness >> nAffects_str;
			unsigned int nAffects = (unsigned int) std::stoul(nAffects_str);

			ev.affects.resize(nAffects);
			std::string ghost_str;
			for (unsigned int i = 0; i < nAffects; i++) {
				liness >> ghost_str;
				ev.affects[i] = (GhostID) std::stoul(ghost_str);
			}
		}

		assert(ev.type >= NetLoggerEvent::PACKET_UPDATE && ev.type <= NetLoggerEvent::PACKET_MOVE);
		assert(ev.event >= NetLoggerEvent::SENT && ev.event <= NetLoggerEvent::IGNORED);
		events_.push_back(std::move(ev));
	}

	LOG(LogInfo) << "Read " << (events_.size() - n_begin) << " events.";
}

void NetAnalyzer::load(const std::string & path)
{
  std::ifstream stream(path);
	load(stream);
}

void NetAnalyzer::clear()
{
	events_.clear();
	pairs_.clear();
	clients_.clear();
	pairsDirty_ = false;
	clientsDirty_ = false;
}

unsigned int NetAnalyzer::numEvents(uint32_t packet_types, uint32_t event_types, const std::string& client)
{
	unsigned int count = 0;
	for (unsigned int i = 0; i < events_.size(); i++) {
		const NetLoggerEvent& ev = events_[i];
		if (filter(ev, packet_types, event_types, client))
			count++;
	}

	return count;
}

double NetAnalyzer::avgLatency(uint32_t packet_types, const std::string& client)
{
	const auto& pairs = getPairs();

	auto total_latency = NetLoggerEvent::clock::duration::zero();
	uint32_t n_packets = 0;
	for (auto it = pairs.begin(); it != pairs.end(); it++) {
		if (it->second != NULL && it->second->event == NetLoggerEvent::RECEIVED
			&& filter(*it->first, packet_types, ~0x0, client)) {

			auto latency = (it->second->timestamp - it->first->timestamp);
			total_latency += latency;
			n_packets++;
		}
	}

	if (n_packets == 0) {
		LOG(LogWarning) << "NetAnalyzer::avgLatency - no packets found matching criteria.";
		return 0;
	}

	using namespace std::chrono;
	auto ms = duration_cast<milliseconds>(total_latency).count();
	return ((double) ms) / n_packets;
}

double NetAnalyzer::avgPacketLoss(uint32_t packet_types, const std::string& client)
{
	const auto& pairs = getPairs();

	uint32_t n_sent = 0;
	uint32_t n_received = 0;
	for (auto it = pairs.begin(); it != pairs.end(); it++) {
		if (filter(*it->first, packet_types, ~0x0, client)) {
			n_sent++;
			if (it->second && it->second->event == NetLoggerEvent::RECEIVED)
				n_received++;
		}
	}

	if (n_sent == 0) {
		LOG(LogWarning) << "NetAnalyzer::avgPacketLoss - no packets found matching criteria.";
		return 0;
	}

	return 1.0 - ((double) n_received) / n_sent;
}

const std::map<NetLoggerEvent*, NetLoggerEvent*>& NetAnalyzer::getPairs()
{
	if (!pairsDirty_)
		return pairs_;

	// sort events_ by timestamp so we are always moving from past to future in our main loop below
	std::sort(events_.begin(), events_.end(),
		[](const NetLoggerEvent& lhs, const NetLoggerEvent& rhs) -> bool {
		return lhs.timestamp < rhs.timestamp;
	});

	// working_ matches on type, sequence number, and guid - purposely ignoring event type
	struct Comp {
		inline bool operator() (const NetLoggerEvent* lhs, const NetLoggerEvent* rhs) const {
			typedef std::tuple<NetLoggerEvent::PacketType,
				decltype(NetLoggerEvent::seq_num),
				const decltype(NetLoggerEvent::client_guid)&> tuple;
			tuple a(lhs->type, lhs->seq_num, lhs->client_guid);
			tuple b(rhs->type, rhs->seq_num, rhs->client_guid);
			return a > b;
		}
	};
	std::set<NetLoggerEvent*, Comp> working;

	// TODO - flush working periodically
	// (i.e. consider a packet never received if (last_packet.timestamp - working[i].timestamp) > 60 sec)
	// otherwise this will grow very large if there are a lot of never-received packets
	pairs_.clear();
	uint32_t unknown_source = 0;
	for (unsigned int i = 0; i < events_.size(); i++) {
		NetLoggerEvent* ev = &events_[i];
		if (ev->event == NetLoggerEvent::SENT) {
			// key packet
			working.insert(ev);
		} else {
			// value packet
			auto senderIt = working.find(ev);
			if (senderIt == working.end()) {
				unknown_source++;
			} else {
				pairs_.insert({ *senderIt, ev });
				working.erase(senderIt);
			}
		}
	}

	// remaining in working were never received, so move them into pairs_ as keys with a null value
	for (auto it = working.begin(); it != working.end(); it++)
		pairs_.insert({ *it, NULL });

	if (unknown_source > 0)
		LOG(LogWarning) << "NetAnalyzer - skipped " << unknown_source << " received/ignored packets with no matching SENTs.";

	pairsDirty_ = false;
	return pairs_;
}

const std::vector<std::string>& NetAnalyzer::getClients()
{
	if (!clientsDirty_)
		return clients_;

	std::set<std::string> clientsSet;
	for (unsigned int i = 0; i < events_.size(); i++) {
		clientsSet.insert(events_[i].client_guid);
	}

	// move contents of clientsSet into the clients_ vector
	clients_.clear();
	clients_.reserve(clientsSet.size());
	for (auto it = clientsSet.begin(); it != clientsSet.end(); it++) {
		clients_.push_back(*it);
	}

	clientsDirty_ = false;
	return clients_;
}

bool NetAnalyzer::filter(const NetLoggerEvent& ev, uint32_t packet_types,
	uint32_t event_types, const std::string& guid) const
{
	return ((ev.type & packet_types) != 0
		&& (ev.event & event_types) != 0
		&& (guid.empty() || ev.client_guid == guid));
}
