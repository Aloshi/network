#pragma once

#include <set>
#include <functional>
#include <util/Log.h>
#include <util/Sequence.h>

template <typename StateT, typename MoveT>
class NetStateManager {
protected:
	struct HistoryEntry {
		uint16_t seq_num;
		StateT state;

		inline bool operator<(const HistoryEntry& rhs) const {
			return seq_num_less(this->seq_num, rhs.seq_num);
		}

		HistoryEntry() : seq_num(0) {}
		HistoryEntry(unsigned int n, const StateT& s)
			: seq_num(n), state(s) {}
	};

public:
	typedef std::function<StateT(const StateT& from, const MoveT& move, bool replayingMoves)> advance_func_t;
	typedef std::function<StateT(const StateT& s1, const StateT& s2, float t)> interp_func_t;

	NetStateManager(int maxBufferTime, StateT initialState = StateT())
		: mMaxBufferTime(maxBufferTime) {
		receiveServerState(0, initialState);
	}

	NetStateManager(int maxBufferTime, advance_func_t advance, interp_func_t interp, StateT initialState = StateT())
		: NetStateManager(maxBufferTime, initialState)
	{
		mAdvanceFunc = advance;
		mInterpFunc = interp;
	}

	// for locally controlled objects with client-side prediction
	void addMoveClientside(const MoveT& move) {
		mPendingMoves.insert(move);

		StateT nextState = advanceState(mostRecentStateHistory().state, move, false);
		mStateHistory.insert(HistoryEntry(move.seq_num, nextState));
	}

	// to be called on the server when a client submits a move
	void addMoveServerside(const MoveT& move) {
		StateT nextState = advanceState(mostRecentState(), move, false);
		receiveServerState(move.seq_num, nextState);
	}

	void receiveServerState(uint16_t number, const StateT& state) {
		{
			// drop inputs that have we can conclude have been processed by the server
			// by us receiving this state number - everything up to and including number
			auto it = mPendingMoves.begin();
			while (it != mPendingMoves.end() && (seq_num_less(it->seq_num, number) || it->seq_num == number)) {
				it = mPendingMoves.erase(it);
			}
		}

		{
			// drop previously acked states beyond our max buffer size (we can't keep *every* state in history...)
			auto it = mStateHistory.begin();
			while (it != mStateHistory.end() && seq_num_dist(it->seq_num, number) > mMaxBufferTime) {
				it = mStateHistory.erase(it);
			}

			// drop states we predicted from the previous received state, they should be re-predicted
			while (it != mStateHistory.end()) {
				if (seq_num_greater(it->seq_num, number) || it->seq_num == number) {
					it = mStateHistory.erase(it);
				} else {
					it++;
				}
			}
		}

		// store the new state
		HistoryEntry historyEntry(number, state);
		mStateHistory.insert(historyEntry);
		mLastServerState = historyEntry;

		// calculate new predicted state(s) by applying any unacked inputs to this newly acked state in sequence
		{
			auto it = mPendingMoves.begin();
			StateT predictedState = state;
			while (it != mPendingMoves.end()) {
				predictedState = advanceState(predictedState, *it, true);
				// is it->seq_num right here?
				mStateHistory.insert(HistoryEntry(it->seq_num, predictedState));
				it++;
			}
		}
	}

	StateT calcState(uint16_t number, float lerp) const {
		// find the two states (as iterators) that bound targetNum
		auto right_it = mStateHistory.begin();
		while (right_it != mStateHistory.end() && (seq_num_less(right_it->seq_num, number) || right_it->seq_num == number))
			right_it++;

		// is the earliest state we have still after targetNum?
		// if yes, just return our earliest state...no use trying to extrapolate into the past
		if (right_it == mStateHistory.begin())
			return right_it->state;

		// is the latest state we have still before targetNum?
		// if yes, just return our latest state...don't bother trying to extrapolate into the future
		// (although we *could* do it)
		if (right_it == mStateHistory.end())
			return mostRecentStateHistory().state;

		auto left_it = std::prev(right_it);

		// time between left and right states
		unsigned int len = seq_num_dist(left_it->seq_num, right_it->seq_num);

		// calculate time in the range 0..1 to interpolate between left and right states
		float t = (seq_num_dist(left_it->seq_num, number) + lerp) / (float)len;

		return interpolateState(left_it->state, right_it->state, t);
	}

	// calc state for rendering based on the most recent state - some buffer factor
	inline StateT calcState(float lerp, unsigned int bufferFor = 1) const {
		// bufferFor should be at least 1 or lerp will be meaningless
		uint16_t number = seq_num_sub(mostRecentStateHistory().seq_num, bufferFor);
		return calcState(number, lerp);
	}

	// last state passed into receiveServerState()
	inline const StateT& lastServerState() const {
		return mLastServerState.state;
	}

	inline uint16_t lastServerStateNumber() const {
		return mLastServerState.seq_num;
	}

	// most recent state, including any client-side predicted states
	const StateT& mostRecentState() const {
		return mostRecentStateHistory().state;
	}

	// for modifying the most recent state
	StateT& mostRecentState() {
		// C++ protects us from modifying the value pointed at by the iterator, as it could
		// potentially modify the variables that control the sort order, invalidating the set
		// however, we made the HistoryEntry struct, so we know for a fact that this is not the case
		// we override the safety mechanism with a const_cast

		// return mStateHistory.rbegin()->state;
		return const_cast<StateT&>(std::prev(mStateHistory.end())->state);
	}

	// most recent state's sequence number, including any client-side predicted states
	uint16_t mostRecentStateNumber() const {
		return mostRecentStateHistory().seq_num;
	}

protected:
	virtual StateT advanceState(const StateT& from, const MoveT& move, bool replayingMoves) const {
		assert(mAdvanceFunc != nullptr);
		return mAdvanceFunc(from, move, replayingMoves);
	}
	virtual StateT interpolateState(const StateT& s1, const StateT& s2, float t) const {
		assert(mInterpFunc != nullptr);
		return mInterpFunc(s1, s2, t);
	}

	inline const HistoryEntry& mostRecentStateHistory() const {
		return *std::prev(mStateHistory.end());
	}

	// how many ticks to buffer state
	int mMaxBufferTime;

	std::set<MoveT> mPendingMoves;  // moves the server has not yet acked
	HistoryEntry mLastServerState;  // last state passed to receiveServerState (useful when using delta-encoded states)
	std::set<HistoryEntry> mStateHistory;  // contains both buffered states previously passed to receiveServerState and predicted states

	// implementations of advanceState/interpolateState
	advance_func_t mAdvanceFunc;
	interp_func_t mInterpFunc;
};
