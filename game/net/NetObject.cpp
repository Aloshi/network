#include "NetObject.h"
#include <client/ServerConnection.h>

std::set<NetObject*> NetObject::sNetObjects;
Simple::Signal<void(NetObject* obj)> NetObject::smRemovedSignal;
ServerConnection* NetObject::sServerConnection = NULL;

NetObject::NetObject()
{
	sNetObjects.insert(this);
}

NetObject::~NetObject()
{
	sNetObjects.erase(sNetObjects.find(this));
	smRemovedSignal.emit(this);
}

bool NetObject::isGhost() const
{
	return (serverConnection() && serverConnection()->getGhostID(this) != 0);
}

NetObject::SignalReceipt NetObject::registerRemovedCallback(const std::function<void(NetObject*)>& callback)
{
	return SignalReceipt(&smRemovedSignal, smRemovedSignal.connect(callback));
}

ServerConnection* NetObject::serverConnection() const
{
	return sServerConnection;
}

void NetObject::setServerConnection(ServerConnection* conn)
{
	sServerConnection = conn;
}