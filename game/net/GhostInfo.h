#pragma once

#include <net/NetObject.h>

#include <stdint.h>

#define MAX_GHOSTS 4096

typedef uint16_t GhostID;

class NetObject;

struct GhostInfo
{
	NetObject* object;
	DirtyMask dirtyMask;

	// I am not really happy with this, this should really get changed to multiple variables or something
	// WAITING_FOR_GHOST, GHOSTING, and GHOSTED should be mutually exclusive
	// WAITING_FOR_DESTROY and DESTROYING should be mutually exclusive
	enum GhostFlags : unsigned char {
		NOT_IN_USE = 0,                 // this ghost is free
		WAITING_FOR_GHOST = (1 << 0),   // this ghost should be sent to the client in the next net update (no information has been sent yet)
		GHOSTING = (1 << 1),            // this ghost is reserved on the server, and we are waiting for the client to acknowledge it
		GHOSTED = (1 << 2),             // this ghost exists on the client
		WAITING_FOR_DESTROY = (1 << 3), // this ghost exists on the client, but no longer exists on the server, and should be destroyed
		DESTROYING = (1 << 4),          // the client has been told to delete this ghost, and we are waiting for an ack
	} flags;

	inline bool validOnClient() const { return (flags & GHOSTED) != 0; } // GHOSTED is set
	inline bool validOnServer() const { return ((flags & (WAITING_FOR_DESTROY | DESTROYING)) == 0) && flags != NOT_IN_USE; } // not WAITING_FOR_DESTROY or DESTROYING
	inline bool validOnBoth() const { return validOnClient() && validOnServer(); }

	GhostInfo() : object(0), dirtyMask(~0), flags(NOT_IN_USE) {}
};
