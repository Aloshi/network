#include "GlobalNetObject.h"

Simple::Signal<void(NetObject*)> GlobalNetObject::smAddedSignal;
std::vector<GlobalNetObject*> GlobalNetObject::smGlobalNetObjs;

GlobalNetObject::GlobalNetObject()
{
	smGlobalNetObjs.push_back(this);
	smAddedSignal.emit(this);
}

GlobalNetObject::~GlobalNetObject()
{
	smGlobalNetObjs.erase(std::find(smGlobalNetObjs.begin(), smGlobalNetObjs.end(), this));
}

GlobalNetObject::SignalReceipt GlobalNetObject::registerAddedCallback(const std::function<void(NetObject*)>& cb)
{
	return SignalReceipt(&smAddedSignal, smAddedSignal.connect(cb));
}