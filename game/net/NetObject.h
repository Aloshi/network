#pragma once

#include <script/ScriptEngine.h>
#include <script/ScriptObject.h>
#include <util/Signal.h>

#include <stdint.h>
#include <set>

#define BIT(x) (1 << x)

typedef uint32_t DirtyMask;

typedef int32_t NetObjectTypeID;

class Client;
class ServerConnection;
class BitStreamReader;
class BitStreamWriter;

SCRIPT_CLASS("NetObject", NetObject, void, "Base class for networked objects.");
class NetObject : public ScriptObject
{
public:
	NetObject();
	virtual ~NetObject();

	virtual void packConstructorArgs(Client* client, BitStreamWriter& stream) = 0;

	// return remaining dirty mask, i.e. 0 if everything is up-to-date, dirtyMask if we updated nothing
	virtual DirtyMask packUpdate(Client* client, DirtyMask dirtyMask, BitStreamWriter& stream) = 0;

	// return true on success, false on error
	virtual void unpackUpdate(ServerConnection* conn, BitStreamReader& stream, unsigned int lastReceivedMoveNum) = 0;

	SCRIPT_PROPERTY(NetObject, "netObjectTypeID", &NetObject::netObjectTypeID, nullptr, "Class ID.");
	virtual NetObjectTypeID netObjectTypeID() = 0;

	inline void clearDirtyMask() { mDirtyMask = 0x0; }
	inline DirtyMask dirtyMask() const { return mDirtyMask; }

	bool isGhost() const;

	static inline const std::set<NetObject*>& allNetObjects() { return sNetObjects; }

	typedef Simple::Connection<void(NetObject*)> SignalReceipt;
	static SignalReceipt registerRemovedCallback(const std::function<void (NetObject*)>& callback);

	static void setServerConnection(ServerConnection* conn);

protected:
	inline void setDirty(DirtyMask orMask) {
		mDirtyMask |= orMask;
	}

	ServerConnection* serverConnection() const;

private:
	static Simple::Signal<void(NetObject* obj)> smRemovedSignal;

	static std::set<NetObject*> sNetObjects;
	static ServerConnection* sServerConnection;
	DirtyMask mDirtyMask;
};
