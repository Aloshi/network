#pragma once

#include <stdint.h>
#include <yojimbo.h>
#include <net/GhostInfo.h>
#include <Move.h>
#include <util/BitStream.h>

typedef uint16_t PacketID;

typedef uint64_t ClientGUID;  // TODO this isn't really a GUID anymore

enum NetMessageTypes : unsigned char
{
	ID_CONTROL_OBJECT_CHANGED,
	ID_CREATE_GHOSTS,
	ID_UPDATE_GHOSTS,
	ID_DESTROY_GHOSTS,
	ID_PACKET_ACK,
	ID_MOVE_PLAYER,
	ID_SERVERCMD,
	ID_CLIENTCMD,
	ID_SERVERCMD_RESPONSE,
	ID_HEARTBEAT,
	NUM_NET_MESSAGE_TYPES
};

enum NetObjectTypes : int32_t
{
	CLASS_PLAYER = 1,  // TODO is this exploited anywhere?
	CLASS_STATICSHAPE,
	CLASS_TILECHUNK,
	CLASS_TILESET,
	CLASS_ENTITY,
	CLASS_COIN,
	CLASS_OBSERVER,
	COMPONENT_TRIGGER,
	COMPONENT_SPRITE,
	NUM_NET_OBJECT_TYPES
};

enum NetChannel {
	CHANNEL_RPC,
	CHANNEL_GHOSTS,
	CHANNEL_ACKS,
	CHANNEL_MOVES,
	NUM_NET_CHANNELS
};

// ---

// yojimbo message types

// generic message type that just holds a stream of data
// can be extended with additional fields
template <int MaxBytes = 1400>
class StreamMessage : public yojimbo::Message {
public:
	static const int MAX_BYTES = MaxBytes;

	uint8_t* streamData;
	size_t streamBytes;
	yojimbo::Allocator* streamAllocator;

	StreamMessage() : streamData(NULL), streamBytes(0), streamAllocator(NULL) {}
	StreamMessage(const StreamMessage& rhs) = delete;
	StreamMessage& operator= (const StreamMessage& rhs) = delete;

	void Free() {
		if (streamData) {
			assert(streamAllocator != NULL);
			YOJIMBO_FREE(*streamAllocator, streamData);
			streamBytes = 0;
		}
	}

	virtual ~StreamMessage() {
		Free();
	}

  // message takes ownership of data
	void SetStreamData(uint8_t* data, size_t bytes, yojimbo::Allocator& allocator) {
		Free();

    streamData = data;
    streamBytes = bytes;
		streamAllocator = &allocator;
	}

	template <typename Stream>
	bool Serialize(Stream& stream)
	{
		serialize_int(stream, streamBytes, 0, MAX_BYTES);
    if (Stream::IsReading) {
      streamAllocator = &yojimbo::GetDefaultAllocator();
      streamData = (uint8_t*) YOJIMBO_ALLOCATE(*streamAllocator, streamBytes);
    }
		serialize_bytes(stream, streamData, streamBytes);
		return true;
	}

	YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
};

// serializes an std::vector<GhostID>
#define serialize_vec(stream, vec, min, max) \
	do { \
		uint32_t len = vec.size(); \
		serialize_uint32(stream, len); \
		if (Stream::IsReading) \
			vec.resize(len); \
		for (size_t i = 0; i < len; i++) { \
			serialize_int(stream, vec[i], min, max); \
		} \
	} while (0)

#define serialize_ghosts_vec(stream, ghostIDs) serialize_vec(stream, ghostIDs, 0, MAX_GHOSTS - 1)

#define serialize_std_string(stream, str) \
  do { \
    uint32_t len = str.size(); \
    serialize_uint32(stream, len); \
    if (Stream::IsReading) \
      str.resize(len); \
    serialize_bytes(stream, (uint8_t*) str.data(), len); \
  } while (0)

class ControlObjectChangedMessage : public yojimbo::Message {
public:
	GhostID ghostID;

	template <typename Stream>
	bool Serialize(Stream& stream)
	{
		serialize_int(stream, ghostID, 0, MAX_GHOSTS - 1);
		return true;
	}

	YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
};

class CreateGhostsMessage : public StreamMessage<1200> {
public:
	PacketID packetID;
	std::vector<GhostID> ghostIDs;
	std::vector<NetObjectTypeID> objectTypes;

	template <typename Stream>
	bool Serialize(Stream& stream)
	{
		serialize_uint16(stream, packetID);
		serialize_ghosts_vec(stream, ghostIDs);
		serialize_vec(stream, objectTypes, 0, NUM_NET_OBJECT_TYPES - 1);

    serialize_check(stream);
		if (!StreamMessage<1200>::template Serialize(stream))
			return false;
    serialize_check(stream);

		return true;
	}

	YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
};
class UpdateGhostsMessage : public StreamMessage<1400> {
public:
	PacketID packetID;
	uint16_t lastReceivedMoveNumber;
	std::vector<GhostID> ghostIDs;

	template <typename Stream>
	bool Serialize(Stream& stream)
	{
		serialize_uint16(stream, packetID);
		serialize_uint16(stream, lastReceivedMoveNumber);
		serialize_ghosts_vec(stream, ghostIDs);

    serialize_check(stream);
		if (!StreamMessage<1400>::template Serialize(stream))
			return false;
    serialize_check(stream);

		return true;
	}

	YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
};

class DestroyGhostsMessage : public yojimbo::Message {
public:
	PacketID packetID;
	std::vector<GhostID> ghostIDs;

	template <typename Stream>
	bool Serialize(Stream& stream)
	{
		serialize_uint16(stream, packetID);
		serialize_ghosts_vec(stream, ghostIDs);
		return true;
	}

	YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
};

class UnreliableSequencedAckMessage : public yojimbo::Message {
public:
	PacketID sequenceNumber;

	template <typename Stream>
	bool Serialize(Stream& stream)
	{
		static_assert(std::is_same<PacketID, uint16_t>::value, "update UnreliableSequencedAckMessage");
		serialize_uint16(stream, sequenceNumber);
		return true;
	}

	YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
};
class MoveMessage : public yojimbo::Message {
public:
	Move move;

	template <typename Stream>
	bool Serialize(Stream& stream)
	{
		serialize_uint16(stream, move.seq_num);
		serialize_int(stream, move.x, -1, 1);
		serialize_int(stream, move.y, -1, 1);
		for (int i = 0; i < Move::MAX_TRIGGERS; i++) {
			serialize_bool(stream, move.triggers[i]);
		}
		return true;
	}

	YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
};
class ServerCmdMessage : public yojimbo::Message {
public:
	std::string cmdName;
	std::vector< std::vector<char> > arguments;
	uint32_t promiseID;

	template <typename Stream>
	bool Serialize(Stream& stream)
	{
    serialize_std_string(stream, cmdName);

    uint32_t n_args = arguments.size();
    serialize_uint32(stream, n_args);
    if (Stream::IsReading)
        arguments.resize(n_args);
    for (uint32_t i = 0; i < n_args; i++) {
      uint32_t arg_len = arguments[i].size();
      serialize_uint32(stream, arg_len);
      if (Stream::IsReading)
        arguments[i].resize(arg_len);
      serialize_bytes(stream, (uint8_t*) arguments[i].data(), arg_len);
    }

    serialize_uint32(stream, promiseID);
    return true;
  }

  YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
};
class ClientCmdMessage : public yojimbo::Message {
public:
	std::string cmdName;
	std::vector< std::vector<char> > arguments;

	template <typename Stream>
	bool Serialize(Stream& stream)
	{
    serialize_std_string(stream, cmdName);

    uint32_t n_args = arguments.size();
    serialize_uint32(stream, n_args);
    if (Stream::IsReading)
        arguments.resize(n_args);
    for (uint32_t i = 0; i < n_args; i++) {
      uint32_t arg_len = arguments[i].size();
      serialize_uint32(stream, arg_len);
      if (Stream::IsReading)
        arguments[i].resize(arg_len);
      serialize_bytes(stream, (uint8_t*) arguments[i].data(), arg_len);
    }

    return true;
  }

  YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
};

class ServerCmdResponseMessage : public yojimbo::Message {
public:
	uint32_t promiseID;
	bool ok;
	std::vector<char> response;

	template <typename Stream>
	bool Serialize(Stream& stream)
	{
    serialize_uint32(stream, promiseID);
    serialize_bool(stream, ok);

    uint32_t len = response.size();
    serialize_uint32(stream, len);
    if (Stream::IsReading)
      response.resize(len);
    serialize_bytes(stream, (uint8_t*) response.data(), len);
    return true;
  }

  YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
};

// intentionally blank message, used to work around yojimbo issue #69
class HeartbeatMessage : public yojimbo::Message {
public:
	template <typename Stream>
	bool Serialize(Stream& stream)
	{
		return true;
	}

	YOJIMBO_VIRTUAL_SERIALIZE_FUNCTIONS();
};

YOJIMBO_MESSAGE_FACTORY_START(NetMessageFactory, NUM_NET_MESSAGE_TYPES);
	YOJIMBO_DECLARE_MESSAGE_TYPE(ID_CONTROL_OBJECT_CHANGED, ControlObjectChangedMessage);
	YOJIMBO_DECLARE_MESSAGE_TYPE(ID_CREATE_GHOSTS, CreateGhostsMessage);
	YOJIMBO_DECLARE_MESSAGE_TYPE(ID_UPDATE_GHOSTS, UpdateGhostsMessage);
	YOJIMBO_DECLARE_MESSAGE_TYPE(ID_DESTROY_GHOSTS, DestroyGhostsMessage);
	YOJIMBO_DECLARE_MESSAGE_TYPE(ID_PACKET_ACK, UnreliableSequencedAckMessage);
	YOJIMBO_DECLARE_MESSAGE_TYPE(ID_MOVE_PLAYER, MoveMessage);
	YOJIMBO_DECLARE_MESSAGE_TYPE(ID_SERVERCMD, ServerCmdMessage);
	YOJIMBO_DECLARE_MESSAGE_TYPE(ID_CLIENTCMD, ClientCmdMessage);
	YOJIMBO_DECLARE_MESSAGE_TYPE(ID_SERVERCMD_RESPONSE, ServerCmdResponseMessage);
	YOJIMBO_DECLARE_MESSAGE_TYPE(ID_HEARTBEAT, HeartbeatMessage);
YOJIMBO_MESSAGE_FACTORY_FINISH();

extern yojimbo::ClientServerConfig NetConfig;
