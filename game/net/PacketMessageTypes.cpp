#include <net/PacketMessageTypes.h>

yojimbo::ClientServerConfig createNetConfig() {
	yojimbo::ClientServerConfig cfg;

	cfg.numChannels = NUM_NET_CHANNELS;

	cfg.channel[CHANNEL_RPC].type = yojimbo::CHANNEL_TYPE_RELIABLE_ORDERED;
	cfg.channel[CHANNEL_RPC].disableBlocks = true;
	cfg.channel[CHANNEL_RPC].messageResendTime = 0.25f;

	cfg.channel[CHANNEL_GHOSTS].type = yojimbo::CHANNEL_TYPE_UNRELIABLE_UNORDERED;
	cfg.channel[CHANNEL_GHOSTS].disableBlocks = true;

	cfg.channel[CHANNEL_ACKS].type = yojimbo::CHANNEL_TYPE_RELIABLE_ORDERED;
	cfg.channel[CHANNEL_GHOSTS].disableBlocks = true;

	cfg.channel[CHANNEL_MOVES].type = yojimbo::CHANNEL_TYPE_RELIABLE_ORDERED;
	cfg.channel[CHANNEL_GHOSTS].disableBlocks = true;

	return cfg;
}

yojimbo::ClientServerConfig NetConfig = createNetConfig();