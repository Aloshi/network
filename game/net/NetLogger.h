#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <chrono>
#include <script/ScriptObject.h>
#include <net/PacketMessageTypes.h>
#include <net/GhostInfo.h>

struct NetLoggerEvent {
	enum PacketType : uint8_t {
		PACKET_UPDATE = 1 << 0,
		PACKET_MOVE = 1 << 1,
	} type;
	enum EventType : uint8_t {
		SENT = 1 << 0,
		RECEIVED = 1 << 1,
		IGNORED = 1 << 2,
	} event;

	typedef std::chrono::system_clock clock;

	uint16_t seq_num;
	uint32_t size;
	std::string client_guid;
	clock::time_point timestamp;
	std::vector<GhostID> affects;  // only used if PACKET_UPDATE and SENT
};

SCRIPT_CLASS("NetLogger", NetLogger, void, "Logs networking packet statistics for offline QoS analysis.");
class NetLogger : public ScriptObject {
public:
	virtual ~NetLogger();

	SCRIPT_METHOD(NetLogger, "start", &NetLogger::start,
		"Begin logging to a file. If append == false and a file already exists, the file will be cleared before writing.");
	void start(const std::string& path, bool append);

	SCRIPT_METHOD(NetLogger, "stop", &NetLogger::stop, "Stop logging.");
	void stop();

	void sentPacket(NetLoggerEvent::PacketType type, uint16_t seq_num,
		ClientGUID to, uint32_t size, const std::vector<GhostID>& affects);
	void receivedPacket(NetLoggerEvent::PacketType type, uint16_t seq_num, ClientGUID me);
	void ignoredPacket(NetLoggerEvent::PacketType type, uint16_t seq_num, ClientGUID me);

protected:
	std::ofstream out_;

	void writeEvent(const NetLoggerEvent* ev);
};

SCRIPT_CLASS("NetAnalyzer", NetAnalyzer, void, "Analyzes packet data output by NetLogger.");
class NetAnalyzer : public ScriptObject {
public:
	SCRIPT_CONSTRUCTOR(NetAnalyzer, "NetAnalyzer");
	NetAnalyzer() : pairsDirty_(false), clientsDirty_(false) {}

	SCRIPT_METHOD(NetAnalyzer, "load", static_cast<void (NetAnalyzer::*)(const std::string&)>(&NetAnalyzer::load),
		"Load data from a file. Multiple loads can be performed.");
	void load(const std::string& path);
	void load(std::ifstream& in);

	SCRIPT_METHOD(NetAnalyzer, "clear", &NetAnalyzer::clear, "Clear all loaded data.");
	void clear();

	// statistics
	SCRIPT_METHOD(NetAnalyzer, "clients", &NetAnalyzer::clients,
		"Get a list of all the client GUIDs found in the currently loaded data.");
	inline const std::vector<std::string>& clients() {
		return getClients();
	}

	SCRIPT_METHOD(NetAnalyzer, "numEvents", &NetAnalyzer::numEvents,
		"Calculate how many events of a certain type occur in the loaded data.");
	unsigned int numEvents(uint32_t packet_types = ~0x0, uint32_t event_types = ~0x0, const std::string& client = "");

	SCRIPT_METHOD(NetAnalyzer, "avgLatency", &NetAnalyzer::avgLatency, "Calculate average latency in milliseconds.");
	// WARNING: on Windows (with MSVC 2017 or older), this does *NOT* have millisecond resolution!!
	double avgLatency(uint32_t packet_types = ~0x0, const std::string& client = "");

	SCRIPT_METHOD(NetAnalyzer, "avgPacketLoss", &NetAnalyzer::avgPacketLoss, "Calculate average packet loss.");
	double avgPacketLoss(uint32_t packet_types = ~0x0, const std::string& client = "");

	// std::map<GhostID, int> calcFairness(uint32_t packet_types = ~0x0, const std::string& client = "");

	// histograms
	/* struct Histogram {
		std::vector<uint32_t> buckets;  // 0..N
		std::vector<uint32_t> values;  // 0..N-1

		void print(std::ostream& out = std::cout, int width = 80);
	}; */

	// Histogram histLostPacketsBySize(int nBuckets);
	// Histogram histLostPacketsBySeqNumber(int nBuckets);

protected:
	// holds all loaded events - can come from multiple sources (i.e. multiple load calls)
	std::vector<NetLoggerEvent> events_;

	// holds matching packet type + sequence number + guid pairs
	// holds all events_ where event == SENT as keys
	// if there is no matching RECEIVED/IGNORED event, then the value will be NULL
	std::map<NetLoggerEvent*, NetLoggerEvent*> pairs_;
	bool pairsDirty_;  // true if pairs_ needs updating
	std::vector<std::string> clients_;  // list of all client GUIDs in loaded data
	bool clientsDirty_;

	const std::map<NetLoggerEvent*, NetLoggerEvent*>& getPairs();
	const std::vector<std::string>& getClients();

	bool filter(const NetLoggerEvent& ev, uint32_t packet_types,
		uint32_t event_types, const std::string& guid) const;
};