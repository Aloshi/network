#pragma once

#include <util/Sequence.h>

struct Move
{
	enum { MAX_TRIGGERS = 4 };

	uint16_t seq_num;  // sequence number
	int8_t x;
	int8_t y;
	bool triggers[MAX_TRIGGERS];

	Move() : seq_num(0), x(0), y(0), triggers{} {}

	inline bool operator==(const Move& rhs) const { return seq_num == rhs.seq_num; }
	inline bool operator!=(const Move& rhs) const { return !(*this == rhs); }

	/**
		Compares two 16 bit sequence numbers and returns true if the first one is greater than the second (considering wrapping).
		IMPORTANT: This is not the same as s1 > s2!
		If the two sequence numbers are close together, it is as normal, but they are far apart, it is assumed that they have wrapped around.
		Thus, sequence_greater_than( 1, 0 ) returns true, and so does sequence_greater_than( 0, 65535 )!
	*/
	inline bool operator>(const Move& rhs) const {
		return seq_num_greater(seq_num, rhs.seq_num);
	}

	inline bool operator<(const Move& rhs) const {
		return seq_num_less(seq_num, rhs.seq_num);
	}
};
