#pragma once

#include <alogui/Vector2.h>
#include <math/Vector2.h>
// #include <alogui/Box2.h>
#include <math/Box2.h>

namespace dukglue {
namespace types {

template<typename NumT>
struct DukType< alogui::Vector2<NumT> > {
	typedef std::true_type IsValueType;

	template <typename FullT>
	static alogui::Vector2<NumT> read(duk_context* ctx, duk_idx_t arg_idx) {
		assert(arg_idx >= 0);
		alogui::Vector2<NumT> vec;
		duk_get_prop_string(ctx, arg_idx, "x");
		duk_get_prop_string(ctx, arg_idx, "y");
		vec.x = DukType<NumT>::template read<NumT>(ctx, -2);
		vec.y = DukType<NumT>::template read<NumT>(ctx, -1);
		duk_pop_2(ctx);
		return vec;
	}

	template <typename FullT>
	static void push(duk_context* ctx, const alogui::Vector2<NumT>& value) {
		duk_idx_t idx = duk_push_object(ctx);
		
		DukType<NumT>::template push<NumT>(ctx, value.x);
		duk_put_prop_string(ctx, idx, "x");

		DukType<NumT>::template push<NumT>(ctx, value.y);
		duk_put_prop_string(ctx, idx, "y");
	}
};

template<typename NumT>
struct DukType< Vector2<NumT> > {
	typedef std::true_type IsValueType;

	template <typename FullT>
	static Vector2<NumT> read(duk_context* ctx, duk_idx_t arg_idx) {
		assert(arg_idx >= 0);
		Vector2<NumT> vec;
		duk_get_prop_string(ctx, arg_idx, "x");
		duk_get_prop_string(ctx, arg_idx, "y");
		vec.x = DukType<NumT>::template read<NumT>(ctx, -2);
		vec.y = DukType<NumT>::template read<NumT>(ctx, -1);
		duk_pop_2(ctx);
		return vec;
	}

	template <typename FullT>
	static void push(duk_context* ctx, const Vector2<NumT>& value) {
		duk_idx_t idx = duk_push_object(ctx);

		DukType<NumT>::template push<NumT>(ctx, value.x);
		duk_put_prop_string(ctx, idx, "x");

		DukType<NumT>::template push<NumT>(ctx, value.y);
		duk_put_prop_string(ctx, idx, "y");
	}
};

template<typename NumT>
struct DukType< Box2<NumT> > {
	typedef std::true_type IsValueType;

	template <typename FullT>
	static Box2<NumT> read(duk_context* ctx, duk_idx_t arg_idx) {
		assert(arg_idx >= 0);
		Box2<NumT> box;
		duk_get_prop_string(ctx, arg_idx, "x");
		duk_get_prop_string(ctx, arg_idx, "y");
		duk_get_prop_string(ctx, arg_idx, "w");
		duk_get_prop_string(ctx, arg_idx, "h");
		box.min.x = DukType<NumT>::template read<NumT>(ctx, -4);
		box.min.y = DukType<NumT>::template read<NumT>(ctx, -3);
		box.max.x = box.min.x + DukType<NumT>::template read<NumT>(ctx, -2);
		box.max.y = box.min.y + DukType<NumT>::template read<NumT>(ctx, -1);
		duk_pop_n(ctx, 4);
		return box;
	}

	template <typename FullT>
	static void push(duk_context* ctx, const Box2<NumT>& value) {
		duk_idx_t idx = duk_push_object(ctx);

		DukType<NumT>::template push<NumT>(ctx, value.min.x);
		duk_put_prop_string(ctx, idx, "x");

		DukType<NumT>::template push<NumT>(ctx, value.min.y);
		duk_put_prop_string(ctx, idx, "y");

		DukType<NumT>::template push<NumT>(ctx, value.max.x - value.min.x);
		duk_put_prop_string(ctx, idx, "w");

		DukType<NumT>::template push<NumT>(ctx, value.max.y - value.min.y);
		duk_put_prop_string(ctx, idx, "h");
	}
};

}
}
