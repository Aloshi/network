#pragma once

#include <dukglue/dukglue.h>
#include <script/DukglueValueTypes.h>

#include <functional>

// Tries to be faithful to:
// https://promisesaplus.com/
// ...with the exception of section 2.3.3 (thenable support).
// Notable modes of failure:
//   - After creating 2^32 Promises, the PromiseIDs will wrap around.
//     

namespace Promise {

typedef uint32_t PromiseID;

typedef std::function<void(const DukValue&)> PromiseCallback;

PromiseID reservePromiseID();
void registerResolvedCallback(PromiseID id, const PromiseCallback& cb);
void registerRejectedCallback(PromiseID id, const PromiseCallback& cb);
void resolve(PromiseID id, const DukValue& result);
void reject(PromiseID id, const DukValue& reason);
void pump(const std::function<void(const DukValue&)>& onRejectIgnored = nullptr);

void refInc(PromiseID id);
void refDec(PromiseID id);

}

void dukglue_promise_init(duk_context* ctx);
void dukglue_promise_shutdown(duk_context* ctx);
Promise::PromiseID dukglue_promise_push(duk_context* ctx);
bool dukglue_is_promise(duk_context* ctx, duk_idx_t idx);
Promise::PromiseID dukglue_get_promise_id(duk_context* ctx, duk_idx_t idx);  // returns 0 (invalid) if idx is not a Promise object