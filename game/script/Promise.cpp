#include "Promise.h"
#include <script/ScriptEngine.h>  // debugging

#include <queue>
#include <map>

using Promise::PromiseID;
using Promise::PromiseCallback;

static PromiseID g_next_id_ = 1;
static std::queue<PromiseID> g_pending_;
static std::queue<DukValue> g_ignored_rejections_;

static const char* PROMISE_PROTO_NAME = "Promise";
static const char* PROP_PROMISE_ID = "\xffpromiseID";

struct PromiseStatus {
	enum State {
		WAITING,
		RESOLVED,
		REJECTED
	} state;

	int refs;

	DukValue value;
	std::vector<Promise::PromiseCallback> on_resolved;
	std::vector<Promise::PromiseCallback> on_rejected;

	PromiseStatus() : state(WAITING), refs(1) {}

	inline bool canFree() {
		return (refs <= 0 &&
			((state == WAITING && on_resolved.empty() && on_rejected.empty())
				|| (state == RESOLVED && on_resolved.empty())
				|| (state == REJECTED && on_rejected.empty())));
	}
};

std::map<PromiseID, PromiseStatus> g_status_map_;

namespace Promise {

PromiseID reservePromiseID() {
	PromiseID id = g_next_id_++;
	g_status_map_.insert({ id, PromiseStatus() });
	return id;
}

void registerResolvedCallback(PromiseID id, const PromiseCallback& cb) {
	auto it = g_status_map_.find(id);
	assert(it != g_status_map_.end());

	if (it->second.state != PromiseStatus::REJECTED) {
		it->second.on_resolved.push_back(cb);

		// wake up again if it was already resolved
		if (it->second.state != PromiseStatus::WAITING)
			g_pending_.push(it->first);
	}
}

void registerRejectedCallback(PromiseID id, const PromiseCallback& cb) {
	auto it = g_status_map_.find(id);
	assert(it != g_status_map_.end());

	if (it->second.state != PromiseStatus::RESOLVED) {
		it->second.on_rejected.push_back(cb);

		// wake up again if it was already rejected
		if (it->second.state != PromiseStatus::WAITING)
			g_pending_.push(it->first);
	}
}

void refInc(PromiseID id) {
	auto it = g_status_map_.find(id);
	assert(it != g_status_map_.end());
	it->second.refs++;
}

void refDec(PromiseID id) {
	auto it = g_status_map_.find(id);
	assert(it != g_status_map_.end());
	it->second.refs--;

	if (it->second.canFree()) {
		std::cout << "Freeing promise ID " << id << " (during deref)" << std::endl;
		g_status_map_.erase(it);
	}
}

void resolve(PromiseID id, const DukValue& result)
{
	auto it = g_status_map_.find(id);
	if (it == g_status_map_.end() || it->second.state != PromiseStatus::WAITING)
		return;

	duk_context* ctx = result.context();
	result.push();
	duk_get_prop_string(ctx, -1, PROP_PROMISE_ID);
	bool is_promise = !duk_is_undefined(ctx, -1);
	PromiseID adopted_id = is_promise ? duk_require_uint(ctx, -1) : 0;
	duk_pop_2(ctx);

	if (is_promise) {
		if (adopted_id == id) {  // 2.3.1
			duk_push_error_object(ctx, DUK_ERR_TYPE_ERROR, "A promise may not resolve to itself.");
			Promise::reject(id, DukValue::take_from_stack(ctx));
			return;
		}

		// 2.3.2 - forward result of adopted_id to id when it completes
		registerResolvedCallback(adopted_id, [id] (const DukValue& adopted_result) {
			Promise::resolve(id, adopted_result);
		});
		registerRejectedCallback(adopted_id, [id](const DukValue& adopted_reason) {
			Promise::reject(id, adopted_reason);
		});
	} else {
		it->second.value = result;
		it->second.state = PromiseStatus::RESOLVED;
		g_pending_.push(it->first);
	}
}

void reject(PromiseID id, const DukValue& reason)
{
	auto it = g_status_map_.find(id);
	if (it == g_status_map_.end() || it->second.state != PromiseStatus::WAITING) {
		g_ignored_rejections_.push(reason);
		return;
	}

	it->second.value = reason;
	it->second.state = PromiseStatus::REJECTED;
	g_pending_.push(it->first);
}

void pump(const std::function<void(const DukValue&)>& onRejectIgnored)
{
	while (!g_pending_.empty()) {
		PromiseID id = g_pending_.front();
		g_pending_.pop();

		auto it = g_status_map_.find(id);
		if (it == g_status_map_.end())
			continue;

		auto& status = it->second;
		if (status.state == PromiseStatus::RESOLVED) {
			for (const auto& cb : status.on_resolved) {
				cb(status.value);
			}
			status.on_resolved.clear();
		} else if (status.state == PromiseStatus::REJECTED) {
			for (const auto& cb : status.on_rejected) {
				cb(status.value);
			}
			status.on_rejected.clear();
		} else {
			// useless wake-up is harmless, may happen on reserve -> add CB -> resolve -> add CB
		}

		if (status.canFree()) {
			std::cout << "Freeing promise ID " << id << " (during pump)" << std::endl;
			g_status_map_.erase(it);
		}
	}

	while (!g_ignored_rejections_.empty()) {
		DukValue reason = g_ignored_rejections_.front();
		g_ignored_rejections_.pop();
		onRejectIgnored(reason);
	}
}

}


// duktape

// this: Promise, [cb_resolved (cb_rejected?) ] -> [NewPromise]
static duk_idx_t then(duk_context* ctx) {
	const bool has_resolve = (duk_get_top(ctx) >= 1 && !duk_is_null(ctx, 0) && !duk_is_undefined(ctx, 0) && duk_is_callable(ctx, 0));
	const bool has_reject = (duk_get_top(ctx) >= 2 && !duk_is_null(ctx, 1) && !duk_is_undefined(ctx, 1) && duk_is_callable(ctx, 1));

	duk_push_this(ctx);
	duk_get_prop_string(ctx, -1, PROP_PROMISE_ID);
	Promise::PromiseID id = duk_require_uint(ctx, -1);
	duk_pop_2(ctx);

	DukValue cb_resolved;
	if (has_resolve) {
		dukglue_read(ctx, 0, &cb_resolved);
	}

	DukValue cb_rejected;
	if (has_reject) {
		dukglue_read(ctx, 1, &cb_rejected);
	}

	Promise::PromiseID next = dukglue_promise_push(ctx);

	if (has_resolve) {
		Promise::registerResolvedCallback(id, [ctx, next, cb_resolved](const DukValue& result) {
			cb_resolved.push();
			result.push();
			duk_int_t rc = duk_pcall(ctx, 1);
			if (rc == 0)
				Promise::resolve(next, DukValue::take_from_stack(ctx));  // 2.2.7.1
			else
				Promise::reject(next, DukValue::take_from_stack(ctx));  // 2.2.7.2
		});
	} else {
		Promise::registerResolvedCallback(id, [next](const DukValue& result) {
			Promise::resolve(next, result);  // 2.2.7.3
		});
	}

	if (has_reject) {
		Promise::registerRejectedCallback(id, [ctx, next, cb_rejected](const DukValue& reason) {
			cb_rejected.push();
			reason.push();
			duk_int_t rc = duk_pcall(ctx, 1);
			if (rc == 0)
				Promise::resolve(next, DukValue::take_from_stack(ctx));  // 2.2.7.1
			else  // returned something or threw another exception? forward return value/exception
				Promise::reject(next, DukValue::take_from_stack(ctx));  // 2.2.7.2
		});
	} else {
		Promise::registerRejectedCallback(id, [next](const DukValue& reason) {
			Promise::reject(next, reason);  // 2.2.7.4
		});
	}

	return 1;
}

// this: Promise, [result]
static duk_idx_t resolve(duk_context* ctx) {
	duk_push_this(ctx);
	duk_get_prop_string(ctx, -1, PROP_PROMISE_ID);
	Promise::PromiseID id = duk_require_uint(ctx, -1);
	duk_pop_2(ctx);

	DukValue result;
	dukglue_read(ctx, 0, &result);

	Promise::resolve(id, result);
	return 0;
}

// this: Promise, [reason]
static duk_idx_t reject(duk_context* ctx) {
	duk_push_this(ctx);
	duk_get_prop_string(ctx, -1, PROP_PROMISE_ID);
	Promise::PromiseID id = duk_require_uint(ctx, -1);
	duk_pop_2(ctx);

	DukValue reason;
	dukglue_read(ctx, 0, &reason);

	Promise::reject(id, reason);
	return 0;
}

duk_idx_t promise_finalizer(duk_context* ctx) {
	duk_get_prop_string(ctx, 0, PROP_PROMISE_ID);
	Promise::PromiseID id = duk_require_uint(ctx, -1);
	Promise::refDec(id);
	return 0;
}

duk_idx_t promise_constructor(duk_context* ctx) {
	if (!duk_is_constructor_call(ctx))
		return DUK_RET_TYPE_ERROR;

	duk_push_this(ctx);

	Promise::PromiseID id = Promise::reservePromiseID();
	duk_push_uint(ctx, id);
	duk_put_prop_string(ctx, -2, PROP_PROMISE_ID);

	return 0;
}

void dukglue_promise_init(duk_context* ctx) {
	duk_push_c_function(ctx, &promise_constructor, 0);

	duk_push_object(ctx);  // prototype object

	// add methods + finalizer to prototype object
	duk_push_c_function(ctx, &then, DUK_VARARGS);
	duk_put_prop_string(ctx, -2, "then");

	duk_push_c_function(ctx, &resolve, 1);
	duk_put_prop_string(ctx, -2, "resolve");

	duk_push_c_function(ctx, &reject, 1);
	duk_put_prop_string(ctx, -2, "reject");

	duk_push_c_function(ctx, &promise_finalizer, 1);
	duk_set_finalizer(ctx, -2);

	// set constructor.prototype to the prototype object
	duk_put_prop_string(ctx, -2, "prototype");
	// duk_set_prototype(ctx, -2);

	// register the constructor to the global object
	duk_put_global_string(ctx, PROMISE_PROTO_NAME);
}

void dukglue_promise_shutdown(duk_context* ctx)
{
	g_status_map_.clear();
	g_pending_ = std::queue<PromiseID>();
}

Promise::PromiseID dukglue_promise_push(duk_context* ctx) {
	duk_get_global_string(ctx, PROMISE_PROTO_NAME);
	if (duk_is_undefined(ctx, -1)) {
		duk_pop(ctx);
		throw DukException() << "Promise prototype object not found - call dukglue_promise_init() in C++ first";
	}

	duk_ret_t rc = duk_pnew(ctx, 0);
	if (rc != 0) {
		DukException err = DukException() << "Error constructing Promise object: " << duk_safe_to_string(ctx, -1);
		duk_pop(ctx);
		throw err;
	}

	duk_get_prop_string(ctx, -1, PROP_PROMISE_ID);
	PromiseID id = duk_require_uint(ctx, -1);
	duk_pop(ctx);

	return id;
}

bool dukglue_is_promise(duk_context* ctx, duk_idx_t idx)
{
	return duk_has_prop_lstring(ctx, idx, PROP_PROMISE_ID, strlen(PROP_PROMISE_ID));
}

Promise::PromiseID dukglue_get_promise_id(duk_context* ctx, duk_idx_t idx)
{
	duk_get_prop_lstring(ctx, idx, PROP_PROMISE_ID, strlen(PROP_PROMISE_ID));
	PromiseID id = duk_get_int_default(ctx, -1, 0);
	duk_pop(ctx);
	return id;
}
