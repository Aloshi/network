#pragma once

#include <dukglue/dukglue.h>
#include <script/DukglueValueTypes.h>

#include <util/Log.h>

#define SCRIPT_CLASS(class_name, class_type, parent_type, doc_string)
#define SCRIPT_CONSTRUCTOR(class_type, constructor_name, ...)
#define SCRIPT_CONSTRUCTOR_MANAGED(class_type, constructor_name, ...)
#define SCRIPT_METHOD(class_type, method_name, method_ptr, doc_string)
#define SCRIPT_METHOD_VARARGS(class_type, method_name, method_ptr, doc_string)
#define SCRIPT_PROPERTY(class_type, prop_name, getter_ptr, setter_ptr, doc_string)
#define SCRIPT_FUNCTION(func_name, func_ptr, doc_string)

class ScriptObject;

class ScriptEngine
{
public:
	static void create(int argc, const char** argv);
	static void destroy();
	static ScriptEngine& get();

	inline operator duk_context*() const {
		return mContext;
	}

	template <typename... ArgTs>
	static bool pcall_noresult(const DukValue& f, ArgTs... args) {
		try {
			dukglue_pcall<void>(get(), f, args...);
			return true;
		} catch (DukException& e) {
			LOG(LogError) << e.what();
			return false;
		}
	}

	template <typename ThisT, typename... ArgTs>
	static bool pcall_method_noresult(ThisT* this_obj, const char* method_name, ArgTs... args) {
		try {
			dukglue_pcall_method<void>(get(), this_obj, method_name, args...);
			return true;
		} catch (DukException& e) {
			LOG(LogError) << e.what();
			return false;
		}
	}

	inline static void invalidate(void* ptr) {
		if (gEngine)
			dukglue_invalidate_object(get(), ptr);
	}

	static void exec(const std::string& path);

	static void pump_events();

	typedef uint32_t PromiseID;
	static PromiseID promise_push();
	static void promise_resolve(PromiseID id, const DukValue& result);
	static void promise_reject(PromiseID id, const DukValue& reason);

private:
	static ScriptEngine* gEngine;

	ScriptEngine();
	~ScriptEngine();

	ScriptEngine(const ScriptEngine&) = delete;
	ScriptEngine& operator=(ScriptEngine& rhs) = delete;

	duk_context* mContext;
};