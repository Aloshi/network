#include "ScriptObject.h"

#include <script/ScriptEngine.h>

ScriptObject::~ScriptObject()
{
	ScriptEngine::invalidate(this);
}