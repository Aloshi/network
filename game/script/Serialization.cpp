#include "Serialization.h"

#include <map>
#include <queue>

struct ObjectNode;
struct ValueNode;

struct Node {
	enum Type {
		INVALID,
		OBJECT,
		VALUE
	} type = INVALID;

	ObjectNode* as_object() const;
	ValueNode* as_value() const;
};

struct ValueNode : public Node {
	std::string value;

	explicit ValueNode(std::string v = std::string()) : value(v) {
		type = VALUE;
	}
};

struct ObjectNode : public Node {
	ObjectNode(uint32_t id) : n_refs(1), obj_id(id) {
		type = OBJECT;
	}

	int n_refs;
	uint32_t obj_id;
	std::string constructor_call;
	std::vector< std::pair<const char*, Node*> > children;

	std::string get_identifier() const {
		std::stringstream ss;
		ss << "obj_" << obj_id;
		return ss.str();
	}

	void write_phase1(std::ostream& out) const {
		std::string identifier = get_identifier();

		// constructor
		if (constructor_call.empty()) {
			out << "var " << identifier << " = {};\n";
		} else {
			out << "var " << identifier << " = " << constructor_call << ";\n";
		}

		// fill in children that are values
		for (auto it = children.begin(); it != children.end(); it++) {
			if (it->second->type == VALUE) {
				out << identifier << "." << it->first << " = ";
				out << it->second->as_value()->value;
				out << ";\n";
			}
		}
	}

	void write_phase2(std::ostream& out) const {
		std::string identifier = get_identifier();

		// fill in children that are objects
		for (auto it = children.begin(); it != children.end(); it++) {
			if (it->second->type == OBJECT) {
				out << identifier << "." << it->first << " = " << it->second->as_object()->get_identifier() << ";\n";
			}
		}
	}
};

ObjectNode* Node::as_object() const {
	assert(this->type == OBJECT);
	return (ObjectNode*) this;
}
ValueNode* Node::as_value() const {
	assert(this->type == VALUE);
	return (ValueNode*) this;
}

Node::Type get_type(duk_context* ctx, int idx) {
	duk_uint_t val_mask = DUK_TYPE_MASK_NULL | DUK_TYPE_MASK_BOOLEAN | DUK_TYPE_MASK_NUMBER | DUK_TYPE_MASK_STRING;
	if (duk_check_type_mask(ctx, idx, val_mask))
		return Node::VALUE;
	if (duk_is_object(ctx, idx))
		return Node::OBJECT;

	return Node::INVALID;
}

// note: converts to string in-place, modifying the duktape stack value at idx
std::string serialize_value(duk_context* ctx, int idx) {
	return duk_json_encode(ctx, idx);
}

void serialize(std::ostream& out, const DukValue& obj) {
	std::vector<Node*> cleanup;  // nodes are kept here make cleanup easy
	std::map<void*, ObjectNode*> obj_to_node;
	std::queue<decltype(obj_to_node)::iterator> unexpanded_objs;

	duk_context* ctx = obj.context();
	obj.push();

	Node::Type root_type = get_type(ctx, -1);
	if (root_type == Node::OBJECT) {
		ObjectNode* root = new ObjectNode(0);
		cleanup.push_back(root);
		auto ret = obj_to_node.insert({ duk_get_heapptr(ctx, -1), root });
		unexpanded_objs.push(ret.first);
		duk_pop(ctx);
	} else if (root_type == Node::VALUE) {
		out << "module.exports = function() {\nreturn ";
		out << serialize_value(ctx, -1);
		out << ";\n}\n";
		duk_pop(ctx);
		return;
	} else {
		assert(false);
	}

	while (!unexpanded_objs.empty()) {
		const auto next = unexpanded_objs.front();
		unexpanded_objs.pop();

		duk_push_heapptr(ctx, next->first);
		ObjectNode* node = next->second;
		assert(node->type == Node::OBJECT);

		duk_uint_t enum_flags = DUK_ENUM_OWN_PROPERTIES_ONLY | DUK_ENUM_SORT_ARRAY_INDICES;
		duk_enum(ctx, -1, enum_flags);

		// enumerate key/value pairs on the object
		while (duk_next(ctx, -1, true)) {
			// create a new child to go into node->children
			std::pair<const char*, Node*> child;
			child.first = duk_require_string(ctx, -2);  // key is taken directly from stack
			Node::Type child_type = get_type(ctx, -1);  // what kind of value?
			if (child_type == Node::OBJECT) {
				// its an object, need to either find the existing node (if this obj has been seen before) or create as new one
				void* child_ptr = duk_get_heapptr(ctx, -1);
				auto exists_it = obj_to_node.find(child_ptr);
				if (exists_it != obj_to_node.end()) {
					// obj has already been seen before, use the saved Node* from obj_to_node
					exists_it->second->n_refs += 1;  // increase ref count
					child.second = exists_it->second;
				} else {
					// first time seeing obj, create a node for it and queue it to be expanded
					ObjectNode* new_node = new ObjectNode(obj_to_node.size());
					cleanup.push_back(new_node);
					auto ret = obj_to_node.insert({ child_ptr, new_node });
					unexpanded_objs.push(ret.first);
					child.second = new_node;
				}
			} else if (child_type == Node::VALUE) {
				// if it's a value type, we serialize it to string directly here
				ValueNode* new_node = new ValueNode(serialize_value(ctx, -1));
				cleanup.push_back(new_node);
				child.second = new_node;
			}

			node->children.push_back(std::move(child));  // finally, add the child

			duk_pop_2(ctx);  // pop key and value
		}
		duk_pop(ctx);  // pop enum object
	}

	// now we actually write stuff
	out << "module.exports = function(ctx) {\n";

	// first, write all the objects in the graph
	out << "// objects\n";
	for (auto it = obj_to_node.begin(); it != obj_to_node.end(); it++) {
		it->second->write_phase1(out);
	}

	// now write fixups
	out << "\n// fixups\n";
	for (auto it = obj_to_node.begin(); it != obj_to_node.end(); it++) {
		it->second->write_phase2(out);
	}

	// root identifier assumed to be obj_0, should match ObjectNode::get_identifier
	out << "\nreturn obj_0;\n};\n";
}

void serializeToFile(const std::string& out_path, const DukValue& obj) {
	std::ofstream out(out_path);
	serialize(out, obj);
}