#pragma once

#include <math/Vector2.h>
#include <script/ScriptEngine.h>

namespace sf {
class Window;
}

// haaaaaack
void ScriptInput_setRenderWindow(sf::Window* window);

SCRIPT_FUNCTION("getMousePosition", &getMousePosition, "Return the current mouse coordinates in screen space.");
Vector2i getMousePosition();

SCRIPT_FUNCTION("isCtrlPressed", &isCtrlPressed, "Returns true if left or right control is pressed.");
bool isCtrlPressed();

SCRIPT_FUNCTION("isShiftPressed", &isShiftPressed, "Returns true if left or right shift are pressed.");
bool isShiftPressed();