#pragma once

#include <fstream>
#include <script/ScriptObject.h>

void serialize(std::ostream& out, const DukValue& obj);

SCRIPT_FUNCTION("serializeToFile", &serializeToFile, "Serialize an object graph (potentially with cycles) to an executable JavaScript file.");
void serializeToFile(const std::string& out_path, const DukValue& obj);