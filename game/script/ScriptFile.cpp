#include "ScriptFile.h"

ScriptFile::ScriptFile(const std::string& path, const std::string& mode)
	: mFile(NULL)
{
	mFile = fopen(path.c_str(), mode.c_str());
	if (!mFile)
		throw std::runtime_error("File could not be opened.");
}

ScriptFile::~ScriptFile()
{
	close();
}

void ScriptFile::close()
{
	if (mFile) {
		fclose(mFile);
		mFile = NULL;
	}
}

duk_idx_t ScriptFile::write(duk_context* ctx)
{
	if (!mFile)
		return duk_error(ctx, DUK_ERR_ERROR, "Cannot write to closed file.");

	duk_size_t len = 0;
	const char* str = duk_require_lstring(ctx, 0, &len);
	fwrite(str, len, 1, mFile);

	return 0;
}

duk_idx_t ScriptFile::readAll(duk_context* ctx)
{
	if (!mFile)
		return duk_error(ctx, DUK_ERR_ERROR, "Cannot read from closed file.");

	size_t start = ftell(mFile);
	fseek(mFile, 0, SEEK_END);
	size_t end = ftell(mFile);
	fseek(mFile, start, SEEK_SET);

	duk_size_t len = end - start;
	void* buff = duk_push_buffer(ctx, len, false);
	fread(buff, len, 1, mFile);

	duk_buffer_to_string(ctx, -1);

	return 1;
}
