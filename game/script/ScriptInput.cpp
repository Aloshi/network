#include "ScriptInput.h"

#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>
#include <SFML/Window/Window.hpp>

// haaaaack
static sf::Window* ScriptInput_gRenderWindow = NULL;

void ScriptInput_setRenderWindow(sf::Window* window)
{
	ScriptInput_gRenderWindow = window;
}

Vector2i getMousePosition()
{
	if (ScriptInput_gRenderWindow == NULL)
		return Vector2i(0, 0);

	sf::Vector2i pos = sf::Mouse::getPosition(*ScriptInput_gRenderWindow);
	return Vector2i(pos.x, pos.y);
}

bool isCtrlPressed()
{
	typedef sf::Keyboard kbd;
	return kbd::isKeyPressed(kbd::LControl) || kbd::isKeyPressed(kbd::RControl);
}

bool isShiftPressed()
{
	typedef sf::Keyboard kbd;
	return kbd::isKeyPressed(kbd::LShift) || kbd::isKeyPressed(kbd::RShift);
}