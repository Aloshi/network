#pragma once

#include <script/ScriptEngine.h>
#include <script/ScriptObject.h>

SCRIPT_CLASS("File", ScriptFile, void, "A class for doing file I/O in script.");
class ScriptFile : public ScriptObject {
public:
	SCRIPT_CONSTRUCTOR_MANAGED(ScriptFile, "File", std::string, std::string);
	ScriptFile(const std::string& path, const std::string& mode);
	virtual ~ScriptFile();

	SCRIPT_METHOD(ScriptFile, "close", &ScriptFile::close,
		"Close the file handle. This is not necessary, but recommended (this is automatically called when the File object is garbage collected).");
	void close();

	SCRIPT_METHOD_VARARGS(ctx, "write", &ScriptFile::write,
		"Write a value to the file. File must be opened in write mode. Value will be coerced to a string.");
	duk_idx_t write(duk_context* ctx);

	SCRIPT_METHOD_VARARGS(ctx, "readAll", &ScriptFile::readAll,
		"Read the entire file into a string.");
	duk_idx_t readAll(duk_context* ctx);

protected:
	FILE* mFile;
};