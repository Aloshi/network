#include "ScriptEngine.h"
#include <util/Log.h>

#include <scene/SceneManager.h>
#include <assert.h>

#include <duk_v1_compat.h>
#include <duk_module_node.h>

#include <boost/filesystem.hpp>

#include <script/Promise.h>

// singleton stuff
ScriptEngine* ScriptEngine::gEngine = NULL;

void ScriptEngine::create(int argc, const char** argv)
{
	assert(gEngine == NULL);
	gEngine = new ScriptEngine();
	
	duk_context* ctx = get();
	duk_push_array(ctx);  // args
	for (int i = 0; i < argc; i++) {
		duk_push_string(ctx, argv[i]);
		duk_put_prop_index(ctx, -2, i);
	}
	duk_put_global_string(ctx, "ProgramArgs");
}

void ScriptEngine::destroy()
{
	delete gEngine;
	gEngine = NULL;
}

ScriptEngine& ScriptEngine::get()
{
	assert(gEngine != NULL);
	return *gEngine;
}

template <LogLevel level = LogInfo>
static duk_ret_t write_print(duk_context* ctx)
{
	const char* data;
	duk_size_t len;
	data = (const char*)duk_safe_to_lstring(ctx, -1, &len);

	LOG(level).write(data, len);
	return 0;
}

static duk_ret_t del_cached_module(duk_context* ctx)
{
	// same as duk__del_cached_module in duktape/duk_module_node.c
	duk_push_global_stash(ctx);
	(void)duk_get_prop_string(ctx, -1, "\xff" "requireCache");
	duk_dup(ctx, 0);
	duk_del_prop(ctx, -1);
	return 0;
}

extern void do_script_bindings(duk_context*);

static duk_ret_t cb_resolve_module(duk_context* ctx)
{
	// Entry stack: [ requested_id parent_id ]
	const char* requested_id = duk_get_string(ctx, 0);
	const char* parent_id = duk_get_string(ctx, 1);
	
	namespace fs = boost::filesystem;
	fs::path requested_path(requested_id);
	fs::path executable_path(fs::initial_path());  // TODO make this the executable path instead of initial working dir...

	boost::system::error_code err;
	fs::path canonical = fs::canonical(fs::path(".") / requested_path, executable_path, err);

	if (err) {
		duk_error(ctx, DUK_ERR_URI_ERROR, "error canonicalizing path - does it exist?");
	}

	// now make it relative again because long paths suck
	canonical = fs::relative(canonical, executable_path, err);

	duk_push_string(ctx, canonical.string().c_str());

	return 1;
}

static duk_ret_t cb_load_module(duk_context* ctx)
{
	// Entry stack: [ resolved_id exports module ]
	// push the contents of the file onto the stack
	const char* path = duk_get_string(ctx, 0);
	duk_push_string_file_raw(ctx, path, 0 /* no flags */);
	return 1;
}

static duk_ret_t script_exit(duk_context* ctx)
{
	exit(duk_get_int_default(ctx, -1, 0));
	return 0;
}

ScriptEngine::ScriptEngine()
	: mContext(NULL)
{
	mContext = duk_create_heap_default();
	assert(mContext != NULL);

	// module system
	duk_push_object(mContext);  // options object
	duk_push_c_function(mContext, cb_resolve_module, DUK_VARARGS);
	duk_put_prop_string(mContext, -2, "resolve");
	duk_push_c_function(mContext, cb_load_module, DUK_VARARGS);
	duk_put_prop_string(mContext, -2, "load");
	duk_module_node_init(mContext);

	duk_push_c_function(mContext, &del_cached_module, 1);
	duk_put_global_string(mContext, "del_cached_module");

	duk_eval_string_noresult(mContext, "function require_fresh(path) { del_cached_module(path); return require(path); };");

	// print
	duk_push_c_function(mContext, &write_print<LogInfo>, 1);
	duk_put_global_string(mContext, "print");

	// warn (non-standard)
	duk_push_c_function(mContext, &write_print<LogWarning>, 1);
	duk_put_global_string(mContext, "warn");

	// error (non-standard)
	duk_push_c_function(mContext, &write_print<LogError>, 1);
	duk_put_global_string(mContext, "error");

	// ServerCmd
	duk_push_object(mContext);
	duk_put_global_string(mContext, "ServerCmd");

	// ClientCmd
	duk_push_object(mContext);
	duk_put_global_string(mContext, "ClientCmd");

	// SceneManager singleton
	dukglue_register_global(mContext, SceneManager::get(), "SceneManager");

	// Promise
	dukglue_promise_init(mContext);

	// exit
	duk_push_c_function(mContext, &script_exit, 1);
	duk_put_global_string(mContext, "exit");

	dukglue_peval<void>(mContext, "print('Initializing script bindings...');");
	do_script_bindings(mContext);
	dukglue_peval<void>(mContext, "print('ScriptEngine initialized.');");
}

void ScriptEngine::exec(const std::string& path)
{
	duk_context* ctx = get();
	duk_get_global_string(ctx, "require");
	duk_push_string(ctx, path.c_str());
	int rc = duk_pcall(ctx, 1);
	if (rc != DUK_EXEC_SUCCESS) {
		duk_get_prop_string(ctx, -1, "stack");
		LOG(LogError) << duk_safe_to_string(ctx, -1);
		duk_pop(ctx);  // pop stack
	}
	duk_pop(ctx);
}

void ScriptEngine::pump_events()
{
	if (!gEngine)
		return;

#ifndef NDEBUG
	duk_int_t top = duk_get_top(ScriptEngine::get());
	if (top != 0) {
		duk_context* ctx = ScriptEngine::get();
		duk_push_context_dump(ctx);
		duk_size_t len;
		const char* data = duk_safe_to_lstring(ctx, -1, &len);
		LOG(LogError) << "ScriptEngine::pump_events - top is nonzero.";
		LOG(LogError).write(data, len);
		duk_pop(ctx);
		assert(top == 0);
	}
#endif

	Promise::pump([&](const DukValue& err) {
		duk_context* ctx = get();
		err.push();
		duk_get_prop_string(ctx, -1, "stack");
		if (duk_is_undefined(ctx, -1))
			duk_pop(ctx);
		else
			duk_remove(ctx, -2);

		duk_size_t len;
		const char* data = duk_safe_to_lstring(ctx, -1, &len);
		LOG(LogWarning) << "Promise rejection ignored:";
		LOG(LogWarning).write(data, len);
		duk_pop(ctx);
	});
}


static_assert(std::is_same<ScriptEngine::PromiseID, Promise::PromiseID>::value, "PromiseID type mismatch");
ScriptEngine::PromiseID ScriptEngine::promise_push()
{
	return dukglue_promise_push(get());
}

void ScriptEngine::promise_resolve(PromiseID id, const DukValue& result)
{
	Promise::resolve(id, result);
}

void ScriptEngine::promise_reject(PromiseID id, const DukValue& reason)
{
	Promise::reject(id, reason);
}

ScriptEngine::~ScriptEngine()
{
	dukglue_promise_shutdown(mContext);
	duk_destroy_heap(mContext);
}
