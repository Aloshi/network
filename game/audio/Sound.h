#pragma once

#include <string>
#include <util/ResourceManager.h>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Sound.hpp>
#include <script/ScriptEngine.h>
#include <script/ScriptObject.h>

class Sound;

class SoundBuffer
{
public:
	// SCRIPT_FUNCTION("getSoundBuffer", &Sound::get, "Get a shared_ptr to a sound buffer. Dynamic properties are not persistent!");
	static std::shared_ptr<SoundBuffer> get(std::string path) {
		return sSoundBufferManager.get(path);
	}

protected:

	typedef std::string SoundKey;

	class SoundBufferManager : public ResourceManager<SoundKey, SoundBuffer> {
	protected:
		inline std::shared_ptr<SoundBuffer> acquire(const SoundKey& key) override {
			auto sound = std::shared_ptr<SoundBuffer>(new SoundBuffer());
			sound->mKey = key;

			if (!sound->mSoundBuffer.loadFromFile(key))
				throw std::runtime_error("Could not load sound.");

			return sound;
		}
	};
	static SoundBufferManager sSoundBufferManager;

	SoundKey mKey;

	friend Sound;
	sf::SoundBuffer mSoundBuffer;
};

SCRIPT_CLASS("Sound", Sound, void, "An instance of a sound. Buffers for the sound data are shared (like textures). Note that the Sound constructor is managed, so if all references to the sound are destroyed before it finishes playing, playback will stop.");
class Sound : public ScriptObject {
public:
	Sound(const std::shared_ptr<SoundBuffer>& buff) : mSoundBuffer(buff) {
		if (mSoundBuffer)
			mSound.setBuffer(buff.get()->mSoundBuffer);
	}

	SCRIPT_CONSTRUCTOR_MANAGED(Sound, "Sound", const std::string&);
	Sound(const std::string& audioPath) : Sound(SoundBuffer::get(audioPath)) {}

	SCRIPT_METHOD(Sound, "play", &Sound::play, "Play the sound. If the sound is already playing, it is restarted. If it was paused, it is resumed.");
	inline void play() {
		mSound.play();
	}
	SCRIPT_METHOD(Sound, "pause", &Sound::pause, "Pause the sound. If play() is called, the sound will resume from the paused position.");
	inline void pause() {
		mSound.pause();
	}
	SCRIPT_METHOD(Sound, "stop", &Sound::stop, "Stop playing the sound. If the sound is played again, it will start from the beginning.");
	inline void stop() {
		mSound.stop();
	}

	SCRIPT_PROPERTY(Sound, "loop", &Sound::loop, &Sound::setLoop, "Whether or not the sound should loop once it reaches the end of the buffer.");
	inline bool loop() const {
		return mSound.getLoop();
	}
	inline void setLoop(bool loop) {
		mSound.setLoop(loop);
	}

	SCRIPT_PROPERTY(Sound, "playingOffset", &Sound::playingOffset, &Sound::setPlayingOffset, "Current playback position.");
	inline float playingOffset() const {
		return mSound.getPlayingOffset().asSeconds();
	}
	inline void setPlayingOffset(float secs) {
		mSound.setPlayingOffset(sf::seconds(secs));
	}

	SCRIPT_PROPERTY(Sound, "pitch", &Sound::pitch, &Sound::setPitch, "Pitch of the sound. Default is 1.0 (no pitch change).");
	inline float pitch() const {
		return mSound.getPitch();
	}
	inline void setPitch(float p) {
		mSound.setPitch(p);
	}

	SCRIPT_PROPERTY(Sound, "volume", &Sound::volume, &Sound::setVolume, "Volume of the sound, from 0.0 to 1.0. Default is 1.0.");
	inline float volume() const {
		return mSound.getVolume() / 100.0f;
	}
	inline void setVolume(float vol) {
		mSound.setVolume(vol * 100.0f);
	}

protected:
	sf::Sound mSound;
	std::shared_ptr<SoundBuffer> mSoundBuffer;
};