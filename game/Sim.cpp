#include "Sim.h"

#include <scene/SceneManager.h>
#include <script/ScriptEngine.h>

#include <vector>
#include <set>
#include <assert.h>

// these aren't in Sim.h because Sim.h can't include ScriptEngine
SCRIPT_FUNCTION("isServer", &isServer, "Returns true if running as a server.");
SCRIPT_FUNCTION("isClient", &isClient, "Returns true if running as a client.");
SCRIPT_FUNCTION("schedule", &Sim::scheduleScript, "Schedule a function to run after some milliseconds have passed.");
SCRIPT_FUNCTION("cancelSchedule", &Sim::cancelSchedule, "Cancel a scheduled function. Takes the ID returned by schedule.");

namespace Sim
{
	uint32_t _currentTick = 0;
	/*uint32_t currentTick() {
		return _currentTick;
	}*/

	struct SchedulerTask {
		SchedulerID id;
		uint32_t activate_on;
		std::function<void()> cb;

		inline bool operator<(const SchedulerTask& rhs) const {
			return activate_on < rhs.activate_on;
		}
	};

	SchedulerID gmNextSchedulerID = 1;
	std::multiset<SchedulerTask> gmTasks;

	double _accumulator = 0.0;
	double accumulator() {
		return _accumulator;
	}

  double _timeSeconds = 0.0;  // includes accumulator (i.e. is not aligned to ticks)
  double getTimeSeconds() {
    return _timeSeconds;
  }

	void tick()
	{
		SceneManager::get()->tickAllScenes();

		// trigger scheduled tasks
		auto it = gmTasks.begin();
		while (it != gmTasks.end() && it->activate_on <= _currentTick) {
			it->cb();
			it = gmTasks.erase(it);  // erase completed task
		}

		_currentTick++;
	}

	bool advanceTime(double microseconds)
	{
		_accumulator += microseconds;
    _timeSeconds += microseconds * 1e-6;

		// if accumulator > 1 second, reset to 1 second
		// to avoid the spiral of death
		if (_accumulator > 1000000)
			_accumulator = 1000000;

		bool ticked = false;
		while (_accumulator >= TICK_RATE_MICROSECONDS) {
			_accumulator -= TICK_RATE_MICROSECONDS;
			Sim::tick();
			ticked = true;
		}
		return ticked;
	}

	SchedulerID schedule(double milliseconds, const std::function<void()>& cb)
	{
		SchedulerTask task;
		task.id = gmNextSchedulerID++;
		task.activate_on = (uint32_t) std::ceil(_currentTick + (milliseconds / TICK_RATE_MILLISECONDS));
		task.cb = cb;
		gmTasks.insert(task);
		return task.id;
	}

	SchedulerID scheduleScript(double milliseconds, const DukValue& cb)
	{
		cb.push();
		duk_bool_t callable = duk_is_callable(cb.context(), -1);
		duk_pop(cb.context());

		if (!callable) {
			return duk_error(cb.context(), DUK_ERR_TYPE_ERROR, "Argument 1 must be callable.");
		}

		return schedule(milliseconds, [cb] {
			ScriptEngine::pcall_noresult(cb);
		});
	}

	bool cancelSchedule(SchedulerID id)
	{
		for (auto it = gmTasks.begin(); it != gmTasks.end(); it++) {
			if (it->id == id) {
				gmTasks.erase(it);
				return true;
			}
		}

		return false;
	}
}
