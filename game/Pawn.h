#pragma once

#include <scene/SceneObject.h>
#include <Move.h>

class Client;

SCRIPT_CLASS("Pawn", Pawn, SceneObject, "Base class for client-controllable SceneObjects.");
class Pawn : public SceneObject {
public:
	typedef SceneObject Parent;

	Pawn(SceneContainer* scene) : SceneObject(scene), mClient(NULL) {}
	virtual ~Pawn();

	virtual Vector2f calcRenderPosition(float lerp) const = 0;
	virtual void addMoveClientside(const Move& move) = 0;
	virtual void addMoveServerside(const Move& move) = 0;

	SCRIPT_PROPERTY(Pawn, "client", &Pawn::client, nullptr, "The client controlling this object (if any).");
	inline Client* client() const {
		return mClient;
	}
	void setClient(Client* cl) {
		mClient = cl;
	}

	bool isLocalPlayer() const;

private:
	Client* mClient;
};