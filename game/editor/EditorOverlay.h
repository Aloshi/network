#pragma once

#include <editor/EditorGizmo.h>
#include <script/ScriptObject.h>
#include <Camera.h>
#include <alogui/Widget.h>

SCRIPT_CLASS("EditorOverlay", EditorOverlay, alogui::Widget, "Widget for highlighting objects in the editor.")
class EditorOverlay : public alogui::Widget, public ScriptObject {
public:
	SCRIPT_CONSTRUCTOR(EditorOverlay, "EditorOverlay", ServerConnection*);
	EditorOverlay(ServerConnection* serverConnection);

	alogui::Vector2i getSizeHint() const override;

	SCRIPT_METHOD(EditorOverlay, "show", &EditorOverlay::show, "Show the overlay.");
	void show();

	SCRIPT_METHOD(EditorOverlay, "hide", &EditorOverlay::hide, "Hide the overlay.");
	void hide();

	SCRIPT_METHOD(EditorOverlay, "highlightObject", &EditorOverlay::highlightObject, "Highlight an object (e.g. as selected).")
	void highlightObject(SceneObject* obj);

	virtual TypeMask ignoreTypeMask() const;

protected:
	virtual void onUpdate(float dt) override;
	virtual void onDraw(sf::RenderTarget& target) override;

private:
	ServerConnection* mServerConnection;
	std::map< SceneObject*, std::unique_ptr<EditorGizmo> > mGizmos;

	EditorGizmo* createGizmoForObject(SceneObject* obj) const;

	SceneContainer* getContainer() const;
	const Camera* getCamera() const;
};