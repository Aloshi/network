#pragma once

#include <editor/EditorGizmo.h>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/CircleShape.hpp>

class SceneObject;

class EditorGizmoStandard : public EditorGizmo {
public:
	EditorGizmoStandard(SceneObject* obj);

	virtual void onUpdate(const Camera* camera, float dt) override;
	virtual void onDraw(sf::RenderTarget& target) override;

protected:
	sf::CircleShape mPositionHandle;
	sf::RectangleShape mBoundsRect;
	SceneObject* mTarget;

	bool mouseOnPositionHandle(const Camera* camera) const;
};