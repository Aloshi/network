#pragma once

#include <editor/EditorGizmo.h>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

class StaticShape;

class EditorGizmoStaticShape : public EditorGizmo {
public:
	EditorGizmoStaticShape(StaticShape* obj);

	virtual void onUpdate(const Camera* camera, float dt) override;
	virtual void onDraw(sf::RenderTarget& target) override;

protected:
	std::vector<sf::CircleShape> mHandles;
	sf::RectangleShape mBoundsRect;

	StaticShape* mTarget;
	int mouseOnPositionHandle(const Camera* camera) const;
};