#include <editor/EditorOverlay.h>
#include <client/ServerConnection.h>
#include <scene/SceneContainer.h>

#include <alogui/Desktop.h>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

#include <editor/EditorGizmoStandard.h>
#include <editor/EditorGizmoStaticShape.h>

EditorOverlay::EditorOverlay(ServerConnection* serverConnection)
	: mServerConnection(serverConnection)
{
}

alogui::Vector2i EditorOverlay::getSizeHint() const
{
	// TODO should return screen resolution - don't hardcode...
	float scale = alogui::Desktop::get()->getDPIScale();
	return alogui::Vector2i(800.f * scale, 600.f * scale);
}

void EditorOverlay::show()
{
	onUpdate(0.0f);
	alogui::Desktop::get()->add(this);
}

void EditorOverlay::hide()
{
	alogui::Desktop::get()->remove(this);
	mGizmos.clear();
}

void EditorOverlay::highlightObject(SceneObject* obj)
{
	for (const auto& it : mGizmos) {
		it.second->setActive(it.first == obj);
	}

	if (obj && mGizmos.find(obj) == mGizmos.end()) {
		auto it = mGizmos.insert({ obj, std::unique_ptr<EditorGizmo>(createGizmoForObject(obj)) });
		it.first->second->setActive(true);
	}
}

TypeMask EditorOverlay::ignoreTypeMask() const
{
	return SCENEOBJECT_TYPE_PLAYER | SCENEOBJECT_TYPE_OBSERVER;
}

void EditorOverlay::onUpdate(float dt)
{
	const Camera* camera = getCamera();
	SceneContainer* scene = getContainer();
	if (!camera || !scene)
		return;

	auto bounds = camera->getBounds();
	auto objects = scene->findObjects(bounds, SCENEOBJECT_TYPE_ANY, ignoreTypeMask());

	// this should be doable in O(n_new*log(n_new) + max(n_new + n_old)) with a sort + mergesort-style algorithm
	// current implementation is something like O(n_new * log(n_old) + n_removed)

	std::map<SceneObject*, std::unique_ptr<EditorGizmo> > newGizmos;
	for (SceneObject* obj : objects) {
		auto it = mGizmos.find(obj);
		if (it != mGizmos.end()) {
			it->second->onUpdate(camera, dt);
			newGizmos.insert({ it->first, std::move(it->second) });
			mGizmos.erase(it);
		} else {
			auto it = newGizmos.insert({ obj, std::unique_ptr<EditorGizmo>(createGizmoForObject(obj)) });
			it.first->second->onUpdate(camera, dt);
		}
	}

	mGizmos = std::move(newGizmos);
}

void EditorOverlay::onDraw(sf::RenderTarget & target)
{
	const Camera* camera = getCamera();
	if (!camera)
		return;

	auto orig_view = target.getView();
	auto view = camera->getView();

	target.setView(view);
	for (auto& it : mGizmos) {
		it.second->onDraw(target);
	}
	target.setView(orig_view);
}

EditorGizmo* EditorOverlay::createGizmoForObject(SceneObject * obj) const
{
	switch (obj->netObjectTypeID()) {
	case CLASS_STATICSHAPE:
		return new EditorGizmoStaticShape((StaticShape*) obj);
	default:
		return new EditorGizmoStandard(obj);
	}
}

SceneContainer * EditorOverlay::getContainer() const
{
	if (mServerConnection && mServerConnection->controlObject())
		return mServerConnection->controlObject()->container();
	return NULL;
}

const Camera* EditorOverlay::getCamera() const {
	return mServerConnection ? mServerConnection->camera() : NULL;
}
