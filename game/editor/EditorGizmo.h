#pragma once

#include <math/Vector2.h>

namespace sf {
	class RenderTarget;
}

class Camera;

class EditorGizmo {
public:
	EditorGizmo() : mActive(false) {}
	virtual ~EditorGizmo() {}

	virtual void onUpdate(const Camera* camera, float dt) {}
	virtual void onDraw(sf::RenderTarget& target) {}
	virtual void setActive(bool active) { mActive = active; }

protected:
	bool mActive;
};