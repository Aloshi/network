#include "EditorGizmoStandard.h"

#include <SFML/Graphics/RenderTarget.hpp>
#include <scene/SceneObject.h>
#include <script/ScriptInput.h>
#include <Camera.h>

EditorGizmoStandard::EditorGizmoStandard(SceneObject* obj)
	: mTarget(obj)
{
	mBoundsRect.setFillColor(sf::Color::Transparent);
	mBoundsRect.setOutlineThickness(2.0f);
	mBoundsRect.setOutlineColor(sf::Color::Red);

	mPositionHandle.setOrigin(0.5f, 0.5f);
	mPositionHandle.setOutlineColor(sf::Color::Yellow);
	mPositionHandle.setOutlineThickness(2.0f);
	mPositionHandle.setRadius(3.0f);
}

void EditorGizmoStandard::onUpdate(const Camera* camera, float dt)
{
	Box2f world = mTarget->worldBox();
	mBoundsRect.setPosition(world.min);
	mBoundsRect.setSize(world.max - world.min);

	Vector2f pos = mTarget->position();
	mPositionHandle.setPosition(pos);
	mPositionHandle.setFillColor(mouseOnPositionHandle(camera) ? sf::Color::Yellow : sf::Color::Transparent);
}

void EditorGizmoStandard::onDraw(sf::RenderTarget& target)
{
	target.draw(mBoundsRect);
	target.draw(mPositionHandle);
}

bool EditorGizmoStandard::mouseOnPositionHandle(const Camera* camera) const {

	Vector2f handlePos = Vector2f(mPositionHandle.getPosition().x, mPositionHandle.getPosition().y);
	Vector2f mousePos = camera->screenToWorld(getMousePosition());
	float radius = 8.0f;
	return (handlePos - mousePos).len2() <= (radius * radius);
}