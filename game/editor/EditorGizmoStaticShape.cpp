#include <editor/EditorGizmoStaticShape.h>

#include <SFML/Graphics/RenderTarget.hpp>
#include <scene/SceneObject.h>
#include <game/StaticShape.h>
#include <script/ScriptInput.h>
#include <Camera.h>

EditorGizmoStaticShape::EditorGizmoStaticShape(StaticShape* obj)
	: mTarget(obj)
{
	mBoundsRect.setFillColor(sf::Color::Transparent);
	mBoundsRect.setOutlineThickness(2.0f);
	mBoundsRect.setOutlineColor(sf::Color::Red);
}

void EditorGizmoStaticShape::onUpdate(const Camera* camera, float dt)
{
	Box2f world = mTarget->worldBox();
	mBoundsRect.setPosition(world.min);
	mBoundsRect.setSize(world.max - world.min);

	if (mActive) {
		// TODO find a way to not do this every frame
		const auto& vertices = mTarget->vertices();
		mHandles.resize(1 + vertices.size());
		for (unsigned int i = 0; i < vertices.size(); i++) {
			const auto& v = vertices[i];
			mHandles[i].setPosition(mTarget->position() + v);
			mHandles[i].setFillColor(sf::Color::Transparent);
			mHandles[i].setOutlineColor(sf::Color::Cyan);
			mHandles[i].setOutlineThickness(1.0f);
			mHandles[i].setOrigin(0.5f, 0.5f);
			mHandles[i].setRadius(3.0f);
		}

		auto& positionHandle = mHandles[mHandles.size() - 1];
		positionHandle.setPosition(mTarget->position());
		positionHandle.setFillColor(sf::Color::Transparent);
		positionHandle.setOutlineColor(sf::Color::Yellow);
		positionHandle.setOutlineThickness(1.0f);
		positionHandle.setOrigin(0.5f, 0.5f);
		positionHandle.setRadius(3.0f);
	} else {
		mHandles.clear();
	}

	int onHandle = mouseOnPositionHandle(camera);
	if (onHandle != -1)
		mHandles[onHandle].setFillColor(mHandles[onHandle].getOutlineColor());
}

void EditorGizmoStaticShape::onDraw(sf::RenderTarget& target)
{
	target.draw(mBoundsRect);
	for (auto& handle : mHandles)
		target.draw(handle);
}

int EditorGizmoStaticShape::mouseOnPositionHandle(const Camera* camera) const {
	Vector2f mousePos = camera->screenToWorld(getMousePosition());
	for (unsigned int i = 0; i < mHandles.size(); i++) {
		const auto& handle = mHandles[i];
		Vector2f handlePos = Vector2f(handle.getPosition().x, handle.getPosition().y);
		float radius = 8.0f;
		bool inside = (handlePos - mousePos).len2() <= (radius * radius);
		if (inside)
			return i;
	}

	return -1;
}