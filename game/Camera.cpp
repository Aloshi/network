#include "Camera.h"
#include "scene/SceneContainer.h"
#include "Sim.h"

#include <SFML/Graphics.hpp>

const sf::RenderTarget* Camera::smRenderTarget;

void Camera::setFollowPawn(Pawn* obj)
{
	mFollowObject = obj;
}

void Camera::render(sf::RenderTarget& target)
{
	if (!mFollowObject)
		return;

	SceneContainer* scene = mFollowObject->container();
	if (scene == NULL)
		return;

	const float lerp = static_cast<float>(Sim::accumulator() / TICK_RATE_MICROSECONDS);

	mPosition = mFollowObject->calcRenderPosition(lerp);

	const Vector2f size = getSize();

	sf::View orig_view = target.getView();
	sf::View view = getView();

	const Box2f bounds = getBounds();
	const std::vector<SceneObject*> objs = scene->findObjects(bounds);

	target.setView(view);
	for (auto it = objs.begin(); it != objs.end(); it++) {
		(*it)->render(target, lerp);
	}
	target.setView(orig_view);
}

Vector2f Camera::getSize() const
{
	return Vector2f(800, 600);
}

Box2f Camera::getBounds() const
{
	Vector2f size = getSize();
	return Box2f(mPosition - (size * 0.5f), mPosition + (size * 0.5f));
}

sf::View Camera::getView() const
{
	sf::View view;
	view.setCenter(mPosition);
	view.setSize(getSize());
	return view;
}

Vector2f Camera::screenToWorld(const Vector2i& screen) const
{
	sf::Vector2f world = smRenderTarget->mapPixelToCoords(sf::Vector2i(screen.x, screen.y), getView());
	return Vector2f(world.x, world.y);
}

Vector2i Camera::worldToScreen(const Vector2f& world) const
{
	sf::Vector2i screen = smRenderTarget->mapCoordsToPixel(sf::Vector2f(world.x, world.y), getView());
	return Vector2i(screen.x, screen.y);
}
