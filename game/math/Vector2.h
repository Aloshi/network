#pragma once

#include <SFML/System/Vector2.hpp>

#include <assert.h>
#include <iostream>
#include <cmath>

template <typename T>
class Vector2
{
public:
	T x;
	T y;

	constexpr Vector2() : x(0), y(0) {}
	constexpr Vector2(T x_, T y_) : x(x_), y(y_) {}

	inline bool isZero() const {
		return (fabs(x) < 1e-15 && fabs(y) < 1e-15);
	}

	// vector length/magnitude
	T len() const { return sqrt(x*x + y*y); }

	// vector length/magnitude squared
	T len2() const { return x*x + y*y; }

	T dot(const Vector2& rhs) const
	{
		return x * rhs.x + y * rhs.y;
	}

	Vector2<T> normalized() const
	{
		T mag = len();
		assert(mag != 0);
		return Vector2<T>(x / mag, y / mag);
	}

	Vector2<T> project(const Vector2<T>& rhs) const {
		return *this * (dot(rhs) / len2());
	}

	T cross(const Vector2<T>& rhs) const {
		return (x * rhs.y) - (y * rhs.x);
	}

	Vector2<T>& operator+= (const Vector2<T>& rhs) {
		x += rhs.x;
		y += rhs.y;
		return *this;
	}
	Vector2<T>& operator-= (const Vector2<T>& rhs) {
		x -= rhs.x;
		y -= rhs.y;
		return *this;
	}
	Vector2<T> operator+ (const Vector2<T>& rhs) const { return Vector2<T>(x + rhs.x, y + rhs.y); }
	Vector2<T> operator- (const Vector2<T>& rhs) const { return Vector2<T>(x - rhs.x, y - rhs.y); }
	Vector2<T> operator* (T rhs) const { return Vector2<T>(x * rhs, y * rhs); }
	Vector2<T> operator/ (T rhs) const { return Vector2<T>(x / rhs, y / rhs); }
	bool operator==(const Vector2<T>& rhs) const { return (x == rhs.x && y == rhs.y); }
	inline bool operator!=(const Vector2<T>& rhs) const { return !(*this == rhs); }

	operator sf::Vector2f() const { return sf::Vector2f(x, y); }
};

typedef Vector2<float> Vector2f;
typedef Vector2<int> Vector2i;

template<typename T>
std::ostream& operator<<(std::ostream& stream, const Vector2<T>& vec) {
	stream << vec.x << ", " << vec.y;
	return stream;
}
