// modified from https://github.com/Crackshell/bayazit.h/blob/master/bayazit.h

#pragma once

#include <algorithm>
#include <limits>
#include <vector>

#include <math/Vector2.h>

namespace bayazit
{
	std::vector< std::vector<Vector2f> > decompose(const std::vector<Vector2f>& vertices);
}