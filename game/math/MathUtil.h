#pragma once

#include <cmath>

#ifndef M_PI
#define M_PI        3.14159265358979323846264338327950288
#define M_PI_2      1.57079632679489661923132169163975144
#endif

#define deg2rad(x) (((M_PI * 2) / 360) * x)
#define rad2deg(x) ((360 / (M_PI * 2)) * x)