#pragma once

#include "Vector2.h"
#include <algorithm>

template <typename T>
class Box2
{
public:
	Vector2<T> min;
	Vector2<T> max;

	inline Vector2<T> size() const { return max - min; }
	inline Vector2<T> center() const { return min + size() / 2; }

	Box2() {}
	Box2(const Vector2<T>& _min, const Vector2<T>& _max) {
		this->min = _min;
		this->max = _max;
	}
	Box2(T x1, T y1, T x2, T y2) {
		this->min = Vector2<T>(x1, y1);
		this->max = Vector2<T>(x2, y2);
	}

	inline bool operator==(const Box2<T>& rhs) {
		return (min == rhs.min && max == rhs.max);
	}

	// return a box that contains the entirety of a and b
	static Box2<T> merge(const Box2<T>& a, const Box2<T>& b) {
		//assert(a.min.x <= a.max.x && a.min.y <= a.max.y);
		//assert(b.min.x <= b.max.x && b.min.y <= b.max.y);
		return Box2<T>(std::min(a.min.x, b.min.x),
					   std::min(a.min.y, b.min.y),
					   std::max(a.max.x, b.max.x),
					   std::max(a.max.y, b.max.y));
	}

	inline T width() const {
		return max.x - min.x;
	}
	inline T height() const {
		return max.y - min.y;
	}

	inline bool contains(const Vector2<T>& pt) const {
		return pt.x >= min.x && pt.y >= min.y && pt.x <= max.x && pt.y <= max.y;
	}
};

typedef Box2<float> Box2f;
typedef Box2<int> Box2i;