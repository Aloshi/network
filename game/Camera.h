#pragma once

#include <script/ScriptObject.h>
#include <SFML/Graphics/View.hpp>
#include <scene/SceneObjectPtr.h>
#include <Pawn.h>

namespace sf {
class RenderTarget;
}

class Camera : public ScriptObject
{
public:
	// used in world <-> screen coordinate calculations
	inline static void setRenderTarget(const sf::RenderTarget* target) {
		smRenderTarget = target;
	}

	void setFollowPawn(Pawn* obj);

	void render(sf::RenderTarget& target);

	SCRIPT_METHOD(Camera, "screenToWorld", &Camera::screenToWorld, "Convert screen coordinates to world coordinates.");
	Vector2f screenToWorld(const Vector2i& screenPos) const;

	SCRIPT_METHOD(Camera, "worldToScreen", &Camera::worldToScreen, "Convert world coordinates to screen coordinates.");
	Vector2i worldToScreen(const Vector2f& worldPos) const;

	Box2f getBounds() const;
	sf::View getView() const;

private:
	static const sf::RenderTarget* smRenderTarget;

	Vector2f getSize() const;

	Vector2f mPosition;

	// this is Player so we can call Player->calcRenderPosition()
	SceneObjectPtr<Pawn> mFollowObject;
};
