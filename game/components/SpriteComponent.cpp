#include "SpriteComponent.h"

#include <net/PacketMessageTypes.h>
#include <util/BitStream.h>

SpriteComponent::SpriteComponent(Entity* e) : Component(e)
{
}

void SpriteComponent::onRender(sf::RenderTarget& target, float lerp)
{
	mSprite.setOffset(mOffset - (getSize() * 0.5f) + transform().position);
	mSprite.draw(target);
}

DirtyMask SpriteComponent::packComponentUpdate(Client* conn, DirtyMask mask, BitStreamWriter& stream)
{
	if (stream.writeBool(mask & FLAG_TEXTURE)) {
		bool hasTexture = (mSprite.getTexture() != nullptr);
		stream.writeBool(hasTexture);
		if (hasTexture) {
			mSprite.getTexture()->serialize(stream);
		}
	}

	if (stream.writeBool(mask & FLAG_OFFSET)) {
		stream.writeVector2f(getOffset());
	}

	if (stream.writeBool(mask & FLAG_SIZE)) {
		stream.writeVector2f(getSize());
	}

	if (stream.writeBool(mask & FLAG_COLORSHIFT)) {
		sf::Color sfcolor = mSprite.getColorshift();
		stream.writeUint32(sfcolor.toInteger());  // hope this doesn't have endianness bugs
	}

	return 0;
}

void SpriteComponent::unpackComponentUpdate(ServerConnection* conn, BitStreamReader& stream)
{
	if (stream.readBool()) {  // FLAG_TEXTURE
		bool hasTexture = stream.readBool();
		if (hasTexture) {
			mSprite.setTexture(Texture::deserialize(stream));
		}
	}

	if (stream.readBool()) {  // FLAG_OFFSET
		mOffset = stream.readVector2f();
	}

	if (stream.readBool()) {  // FLAG_SIZE
		mSprite.setSize(stream.readVector2f());
	}

	if (stream.readBool()) {  // FLAG_COLORSHIFT
		sf::Color sf(stream.readUint32());  // hope this doesn't have endianness bugs
		mSprite.setColorshift(sf.r, sf.g, sf.b, sf.a);
	}
}

NetObjectTypeID SpriteComponent::netObjectTypeID()
{
	return COMPONENT_SPRITE;
}

const char* SpriteComponent::componentName() const
{
	return "Sprite";
}

Box2f SpriteComponent::worldBox() const
{
	Vector2f center = transform().position + getOffset();
	Vector2f size = getSize();
	return Box2f(center - size * 0.5f, center + size * 0.5f);
}

void SpriteComponent::setTexture(const std::shared_ptr<Texture>& tex)
{
	mSprite.setTexture(tex);
	setDirty(FLAG_TEXTURE);
}

void SpriteComponent::setOffset(const Vector2f& off)
{
	mOffset = off;
	setDirty(FLAG_OFFSET);
}

void SpriteComponent::setSize(const Vector2f& size)
{
	mSprite.setSize(size);
	setDirty(FLAG_SIZE);
}

void SpriteComponent::setColorshift(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
	mSprite.setColorshift(r, g, b, a);
	setDirty(FLAG_COLORSHIFT);
}
