#include <components/TriggerComponent.h>

#include <collision/ShapeList.h>
#include <SFML/Graphics/Color.hpp>
#include <net/PacketMessageTypes.h>
#include <scene/SceneContainer.h>
#include <util/BitStream.h>

TriggerComponent::TriggerComponent(Entity* e) : Component(e)
{
}

Box2f TriggerComponent::worldBox() const
{
	return mShape.calcWorldBox(transform());
}

void TriggerComponent::onRender(sf::RenderTarget& target, float lerp)
{
	mShape.debugDraw(target, transform(), sf::Color::Yellow);
}

void TriggerComponent::onTick()
{
	// check if any objects entered the trigger
	const Box2f workingBounds = worldBox();
	const std::vector<SceneObject*> nearbyObjs = container()->findObjects(workingBounds, SCENEOBJECT_TYPE_DYNAMIC);
	ShapeList workingList;
	for (unsigned int i = 0; i < nearbyObjs.size(); i++) {
		nearbyObjs[i]->buildCollisionList(&workingList, workingBounds);
	}

	std::vector<CollisionInfo> hits;
	workingList.findAllCollisions(&mShape, transform(), position(), &hits);
	std::vector< SceneObjectPtr<SceneObject> > nowInTrigger(hits.size());
	for (unsigned int i = 0; i < hits.size(); i++) {
		SceneObject* obj = hits.at(i).hitObject;
		nowInTrigger.at(i).reset(obj);

		// is obj new?
		if (std::find(mInTrigger.begin(), mInTrigger.end(), obj) == mInTrigger.end()) {
			onEnter(obj);
		}
	}

	// leavers
	for (unsigned int i = 0; i < mInTrigger.size(); i++) {
		const auto& check = mInTrigger.at(i);
		if (check != nullptr && std::find(nowInTrigger.begin(), nowInTrigger.end(), check.get()) == nowInTrigger.end()) {
			onLeave(check);
		}
	}

	mInTrigger = nowInTrigger;
}

DirtyMask TriggerComponent::packComponentUpdate(Client* conn, DirtyMask mask, BitStreamWriter& stream)
{
	if (stream.writeBool(mask & FLAG_SHAPE)) {
		stream.writeUint32(mShape.vertices().size());
		for (unsigned int i = 0; i < mShape.vertices().size(); i++) {
			stream.writeVector2f(mShape.vertices()[i]);
		}
	}

	return 0;
}

void TriggerComponent::unpackComponentUpdate(ServerConnection* conn, BitStreamReader& stream)
{
	if (stream.readBool()) { // FLAG_SHAPE
		uint32_t numVerts = stream.readUint32();

		std::vector<Vector2f> verts(numVerts);
		for (unsigned int i = 0; i < numVerts; i++) {
			verts[i] = stream.readVector2f();
		}

		mShape.setVertices(verts);
		notifyWorldBoxChanged();
	}
}

NetObjectTypeID TriggerComponent::netObjectTypeID()
{
	return COMPONENT_TRIGGER;
}

void TriggerComponent::setRectSize(float width, float height)
{
	float hw = width * 0.5f;
	float hh = height * 0.5f;

	mShape.setVertices({
		Vector2f(-hw, -hh),
		Vector2f(hw, -hh),
		Vector2f(hw, hh),
		Vector2f(-hw, hh)
	});

	setDirty(FLAG_SHAPE);
	notifyWorldBoxChanged();
}

void TriggerComponent::onEnter(SceneObject* obj)
{
	if (mEnterCallback.type() != DukValue::UNDEFINED) {
		ScriptEngine::pcall_method_noresult(this, "onEnter", obj);
	}
}

void TriggerComponent::onLeave(SceneObject* obj)
{
	if (mLeaveCallback.type() != DukValue::UNDEFINED) {
		ScriptEngine::pcall_method_noresult(this, "onLeave", obj);
	}
}

const DukValue& TriggerComponent::getEnterCallback() const { return mEnterCallback; }
void TriggerComponent::setEnterCallback(DukValue cb) { mEnterCallback = cb; }

const DukValue& TriggerComponent::getLeaveCallback() const { return mLeaveCallback; }
void TriggerComponent::setLeaveCallback(DukValue cb) { mLeaveCallback = cb; }

NetObjectTypeID netObjectTypeID() { return COMPONENT_TRIGGER; }
const char* TriggerComponent::componentName() const { return "Trigger"; }