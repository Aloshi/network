#pragma once

#include <Entity.h>
#include <scene/SceneObjectPtr.h>
#include <collision/ConvexShape.h>

SCRIPT_CLASS("TriggerComponent", TriggerComponent, Component, "A shape which triggers script callbacks as objects enter and leave.");
class TriggerComponent : public Component {
public:
	SCRIPT_CONSTRUCTOR(TriggerComponent, "TriggerComponent", Entity*);
	TriggerComponent(Entity* e);

	virtual void onTick() override;
	virtual void onRender(sf::RenderTarget& target, float lerp) override;

	virtual DirtyMask packComponentUpdate(Client* conn, DirtyMask mask, BitStreamWriter& stream) override;
	virtual void unpackComponentUpdate(ServerConnection* conn, BitStreamReader& stream) override;
	virtual NetObjectTypeID netObjectTypeID() override;
	virtual const char* componentName() const override;

	virtual Box2f worldBox() const override;

	SCRIPT_METHOD(TriggerComponent, "setRectSize", &TriggerComponent::setRectSize, "Set the width and height of the trigger.");
	void setRectSize(float width, float height);

	SCRIPT_PROPERTY(TriggerComponent, "onEnter", &TriggerComponent::getEnterCallback, &TriggerComponent::setEnterCallback, "Callback triggered when an object enters the trigger area.");
	const DukValue& getEnterCallback() const;
	void setEnterCallback(DukValue cb);

	SCRIPT_PROPERTY(TriggerComponent, "onLeave", &TriggerComponent::getLeaveCallback, &TriggerComponent::setLeaveCallback, "Callback triggered when an object leaves the trigger area.");
	const DukValue& getLeaveCallback() const;
	void setLeaveCallback(DukValue cb);

protected:
	enum TriggerFlags {
		FLAG_SHAPE = (1 << 0),
		NEXT_FREE_FLAG = (1 << 1)
	};

	virtual void onEnter(SceneObject* obj);
	virtual void onLeave(SceneObject* obj);

private:
	ConvexShape mShape;
	DukValue mEnterCallback;
	DukValue mLeaveCallback;

	std::vector< SceneObjectPtr<SceneObject> > mInTrigger;
};