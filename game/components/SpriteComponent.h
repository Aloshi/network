#pragma once

#include <Entity.h>
#include <gfx/Sprite.h>

SCRIPT_CLASS("SpriteComponent", SpriteComponent, Component, "A component which renders a sprite.");
class SpriteComponent : public Component {
public:
	SCRIPT_CONSTRUCTOR(SpriteComponent, "SpriteComponent", Entity*);
	SpriteComponent(Entity* e);

	virtual void onRender(sf::RenderTarget& target, float lerp) override;

	virtual DirtyMask packComponentUpdate(Client* conn, DirtyMask mask, BitStreamWriter& stream) override;
	virtual void unpackComponentUpdate(ServerConnection* conn, BitStreamReader& stream) override;
	virtual NetObjectTypeID netObjectTypeID() override;
	virtual const char* componentName() const override;

	virtual Box2f worldBox() const override;

	// imitate the Sprite class - passthroughs that also set the appropriate dirty flags
	SCRIPT_PROPERTY(SpriteComponent, "texture", &SpriteComponent::getTexture, &SpriteComponent::setTexture, "Texture to render.");
	void setTexture(const std::shared_ptr<Texture>& tex);
	inline const std::shared_ptr<Texture>& getTexture() { return mSprite.getTexture(); }

	SCRIPT_PROPERTY(SpriteComponent, "offset", &SpriteComponent::getOffset, &SpriteComponent::setOffset, "Offset.");
	void setOffset(const Vector2f& off);
	inline const Vector2f& getOffset() const { return mOffset; }

	SCRIPT_PROPERTY(SpriteComponent, "size", &SpriteComponent::getSize, &SpriteComponent::setSize, "Size. Defaults to texture size.");
	void setSize(const Vector2f& size);
	inline const Vector2f& getSize() const { return mSprite.getSize(); }

	SCRIPT_METHOD(SpriteComponent, "setColorshift", &Sprite::setColorshift, "Colorshift by an RGBA value.");
	void setColorshift(unsigned char r, unsigned char g, unsigned char b, unsigned char a);

protected:
	enum TriggerFlags {
		FLAG_TEXTURE = (1 << 0),
		FLAG_OFFSET = (1 << 1),
		FLAG_SIZE = (1 << 2),
		FLAG_COLORSHIFT = (1 << 3),
		NEXT_FREE_FLAG = (1 << 4)
	};

private:
	Vector2f mOffset;
	Sprite mSprite;
};