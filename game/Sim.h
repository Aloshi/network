#pragma once

#define TICK_RATE_MICROSECONDS (1000000.0 / 60.0)
#define TICK_RATE_MILLISECONDS (TICK_RATE_MICROSECONDS / 1000.0)
#define TICK_RATE_SECONDS (TICK_RATE_MILLISECONDS / 1000.0)
#define TIMESCALE 1.0

#include <stdint.h>
#include <functional>

// forward declarations
namespace sf {
	class RenderTarget;
}
class DukValue;

bool isClient();
bool isServer();

typedef uint32_t SchedulerID;

namespace Sim
{
	double accumulator();
  double getTimeSeconds();
	bool advanceTime(double microseconds);

	// scheduler stuff
	SchedulerID schedule(double milliseconds, const std::function<void()>& cb);
	SchedulerID scheduleScript(double milliseconds, const DukValue& cb);
	bool cancelSchedule(SchedulerID id);
}
