#pragma once

#include <string>
#include <gfx/Texture.h>
#include <math/Box2.h>
#include <math/Vector2.h>
#include <collision/ConvexShape.h>
#include <net/GlobalNetObject.h>

typedef uint8_t TileID;

#define TILE_ID_MAX 255  // max possible tile ID

struct TileType {
	Vector2i textureRectPos;
	Vector2i textureRectSize;

	// we pre-compute the collision shapes for all possible configurations of this tile
	// (4 possible rotations + mirrored horizontally)
	// collisionShape[0] = the original collision shape
	// 1 = original shape, but mirrored horizontally ("flipped")
	// 2 = rotated 90 degrees,  3 = rotated 90 degrees, then mirrored
	// 4 = rotated 180 degrees, 5 = rotated 180 degrees then mirrored
	// 6 = rotated 270 degrees, 7 = rotated 270 degrees then mirrored
	ConvexShape collisionShape[8];

	void setCollisionShape(const std::vector<Vector2f>& vertices);

	inline ConvexShape& getCollisionShape(unsigned char rotation, bool mirrorHorizontal) {
		return collisionShape[rotation * 2 + (mirrorHorizontal ? 1 : 0)];
	}
	inline const ConvexShape& getCollisionShape(unsigned char rotation, bool mirrorHorizontal) const {
		return const_cast<TileType*>(this)->getCollisionShape(rotation, mirrorHorizontal);
	}
};

class TileSet : public GlobalNetObject {
public:
	SCRIPT_CONSTRUCTOR(TileSet, "TileSet");
	TileSet() {}
	virtual ~TileSet();

	typedef Simple::Connection<void(TileSet*, bool)> SignalReceipt;
	static SignalReceipt registerChangedCallback(const std::function<void(TileSet*, bool)>& callback);

	SCRIPT_METHOD(TileSet, "addTileType", &TileSet::addTileType, "Add a new tile type to the tileset.");
	TileID addTileType(const Vector2i& texRectPos, const Vector2i& texRectSize, const std::vector<Vector2f>& colVerts);
	// TileID addTileType(Box2i textureRect, ConvexShape colShape);

	//void removeTileType(TileID id);
	//void changeTileType(TileID id, Box2i textureRect, ConvexShape colShape);

	inline TileType& getTileType(TileID id) {
		assert(id < mTileTypes.size());
		return mTileTypes[id];
	}

	inline const TileType& getTileType(TileID id) const {
		assert(id < mTileTypes.size());
		return mTileTypes[id];
	}

	SCRIPT_PROPERTY(TileSet, "texture", &TileSet::getTexture, &TileSet::setTexture, "Texture for this tileset.");
	inline std::shared_ptr<Texture> getTexture() { return mTexture; }
	void setTexture(const std::shared_ptr<Texture>& tex);

	SCRIPT_PROPERTY(TileSet, "name", &TileSet::getName, &TileSet::setName, "Name of this tileset. Purely aesthetic.");
	inline const std::string& getName() const { return mName; }
	void setName(const std::string& name);

	SCRIPT_METHOD(TileSet, "getTileTextureRect", &TileSet::getTileTextureRect, "Get the texture rectangle coordinates for a particular tile type.");
	inline Box2i getTileTextureRect(int id) const {
		auto tileType = getTileType(id);
		return Box2i(tileType.textureRectPos, tileType.textureRectPos + tileType.textureRectSize);
	}

	SCRIPT_METHOD(TileSet, "getTileTypeCount", &TileSet::getTileTypeCount, "Get the number of tile types in this tileset.");
	inline unsigned int getTileTypeCount() const {
		return mTileTypes.size();
	}

	void packConstructorArgs(Client* client, BitStreamWriter& stream) override;
	DirtyMask packUpdate(Client* client, DirtyMask dirty, BitStreamWriter& stream) override;
	void unpackUpdate(ServerConnection* conn, BitStreamReader& stream, unsigned int timestamp) override;
	NetObjectTypeID netObjectTypeID() override;

private:
	static Simple::Signal<void(TileSet* ts, bool deleted)> smChangedSignal;

	enum Flags {
		FLAG_NAME_CHANGED = (1 << 0),
		FLAG_TEXTURE_CHANGED = (1 << 0),
		FLAG_TILE_TYPES_CHANGED = (1 << 0)
	};

	std::string mName;
	std::shared_ptr<Texture> mTexture;
	std::vector<TileType> mTileTypes;
};
