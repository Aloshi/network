#include "StaticShape.h"

#include <collision/ShapeList.h>
#include <math/BayazitDecomposer.h>
#include <net/PacketMessageTypes.h>

#include <SFML/Graphics.hpp>
#include <util/BitStream.h>

StaticShape::StaticShape(SceneContainer* scene) : SceneObject(scene)
{
}

NetObjectTypeID StaticShape::netObjectTypeID()
{
	return CLASS_STATICSHAPE;
}

void StaticShape::render(sf::RenderTarget& target, float lerp)
{
	if (mVertices.empty())
		return;

	auto color = sf::Color::Cyan;
	if (mVertices.size() == 1) {
		sf::CircleShape shape;
		shape.setPosition(mVertices[0] + position());
		shape.setFillColor(color);
		shape.setRadius(1.0f);
		target.draw(shape);
		return;
	}

	std::vector<sf::Vertex> verts;
	for (unsigned int i = 0; i < mVertices.size(); i++) {
		verts.push_back(sf::Vertex(mVertices[i] + position(), color));
	}
	verts.push_back(sf::Vertex(mVertices[0] + position(), color));
	target.draw(verts.data(), verts.size(), sf::LinesStrip);

	//for (unsigned int i = 0; i < mShapes.size(); i++)
	//	mShapes[i].debugDraw(target, transform(), sf::Color::Cyan);
}

DirtyMask StaticShape::packUpdate(Client* conn, DirtyMask mask, BitStreamWriter& stream)
{
	if (stream.writeBool(mask & FLAG_POSITION)) {
		stream.writeVector2f(position());
	}

	if (stream.writeBool(mask & FLAG_SHAPE_CHANGED)) {
		stream.writeUint32(mVertices.size());
		for (unsigned int i = 0; i < mVertices.size(); i++) {
			stream.writeVector2f(mVertices[i]);
		}
	}

	return 0;
}

void StaticShape::unpackUpdate(ServerConnection* conn, BitStreamReader& stream, unsigned int timestamp)
{
	if (stream.readBool()) {  // FLAG_POSITION
		setPosition(stream.readVector2f());
	}

	if (stream.readBool()) {  // FLAG_SHAPE_CHANGED
		uint32_t numVerts = stream.readUint32();

		std::vector<Vector2f> verts(numVerts);
		for (unsigned int i = 0; i < numVerts; i++) {
			verts[i] = stream.readVector2f();
		}

		setVertices(verts);
	}
}

Box2f StaticShape::worldBox() const
{
	if (mShapes.empty())
		return Parent::worldBox();

	const Transform& trans = transform();

	Box2f worldBox = mShapes[0].calcWorldBox(trans);
	for (unsigned int i = 1; i < mShapes.size(); i++) {
		Box2f shapeBox = mShapes[i].calcWorldBox(trans);
		worldBox = Box2f::merge(worldBox, shapeBox);
	}

	return worldBox;
}

void StaticShape::buildCollisionList(ShapeList* list, const Box2f& worldRegion)
{
	for (unsigned int i = 0; i < mShapes.size(); i++)
		list->add(this, &mShapes[i], transform());
}

void StaticShape::setVertices(std::vector<Vector2f> verts)
{
	mVertices = verts;

	// fix up for winding order - if clockwise, reverse order of vertices
	// (bayazit only works for counter-clockwise vertices)
	// https://stackoverflow.com/a/1165943
	if (verts.size() >= 3) {
		float sum = 0.0f;
		for (int i = 0; i < (int)verts.size(); i++) {
			const Vector2f& v1 = verts[i];
			const Vector2f& v2 = verts[(i + 1) % verts.size()];
			sum += (v2.x - v1.x) * (v2.y + v1.y);
		}
		if (sum > 0)
			std::reverse(verts.begin(), verts.end());
	}

	mShapes.clear();
	std::vector< std::vector<Vector2f> > polygons = bayazit::decompose(verts);
	for (unsigned int i = 0; i < polygons.size(); i++) {
		mShapes.push_back(ConvexShape(polygons[i]));
	}

	setDirty(FLAG_SHAPE_CHANGED);
	notifyWorldBoxChanged();
}