#include "AIPlayer.h"

AIPlayer::AIPlayer(SceneContainer* scene) : Player(scene)
{
}

void AIPlayer::tick()
{
	addMoveServerside(mNextMove);
	mNextMove.seq_num++;

	Player::tick();
}

void AIPlayer::setDirection(int8_t x, int8_t y)
{
	mNextMove.x = x;
	mNextMove.y = y;
}

void AIPlayer::setTrigger(int trigger_idx, bool pressed)
{
	if (trigger_idx >= 0 && trigger_idx < Move::MAX_TRIGGERS)
		mNextMove.triggers[trigger_idx] = pressed;
}

void AIPlayer::clearTriggers()
{
	for (int i = 0; i < Move::MAX_TRIGGERS; i++)
		mNextMove.triggers[i] = false;
}
