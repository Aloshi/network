#pragma once

#include <scene/SceneObject.h>
#include <gfx/Sprite.h>

typedef uint64_t CoinID;

SCRIPT_CLASS("Coin", Coin, SceneObject, "A collectable coin with a sprite. Collection status is client-dependent (instanced).");
class Coin : public SceneObject {
public:
	SCRIPT_CONSTRUCTOR(Coin, "Coin", SceneContainer*);
	Coin(SceneContainer* scene);

	virtual void tick() override;
	virtual void render(sf::RenderTarget & target, float lerp) override;
	virtual TypeMask typeMask() const override;

	virtual DirtyMask packUpdate(Client* conn, DirtyMask mask, BitStreamWriter& stream) override;
	virtual void unpackUpdate(ServerConnection* conn, BitStreamReader& stream, unsigned int timestamp) override;
	virtual NetObjectTypeID netObjectTypeID() override;

	virtual Box2f worldBox() const override;
	virtual void buildCollisionList(ShapeList* list, const Box2f& worldRegion) override;

private:
	enum CoinFlags {
		FLAG_COLORSHIFT = NEXT_FREE_FLAG,
		FLAG_COLLECTED = (FLAG_COLORSHIFT << 1),
	};

	void playCollectedEffect();

	CoinID mID;
	Sprite mSprite;
	bool mCollected;  // client-side only; server uses CoinDB
};