#include "TileChunk.h"
#include <net/PacketMessageTypes.h>  // for NetObjectTypes
#include <collision/ShapeList.h>
#include <client/ServerConnection.h>
#include <server/Client.h>
#include <util/BitStream.h>
#include <scene/SceneContainer.h>

#include <SFML/Graphics/RenderTarget.hpp>

const int TileChunk::CHUNK_SIZE;

TileChunk::TileChunk(SceneContainer* scene)
	: SceneObject(scene),
	mTilesetChangedCb(TileSet::registerChangedCallback(std::bind(&TileChunk::tilesetChanged, this, std::placeholders::_1, std::placeholders::_2))),
	mDirty(false)
{

}

void TileChunk::setTile(int layer, int x, int y, TileSet* ts, unsigned short tileId, unsigned char rotation, bool mirror)
{
	assert(layer < N_LAYERS && layer >= 0);
	assert(x < CHUNK_SIZE && x >= 0);
	assert(y < CHUNK_SIZE && y >= 0);

	mTiles[layer][x][y].tileset = ts;
	mTiles[layer][x][y].tileId = tileId;
	mTiles[layer][x][y].rotation = rotation;
	mTiles[layer][x][y].mirrored = mirror;
	setDirty(FLAG_CHANGED);

	mDirty = true;  // need to rebuild mVertices
}

void TileChunk::tick()
{
	if (mDirty) {
		updateVertices();
		notifyWorldBoxChanged();
		mDirty = false;
	}
}

void TileChunk::render(sf::RenderTarget& target, float lerp)
{
	target.resetGLStates();  // conflicts with entity rendering somehow... :/

	// TODO improve this to select vertices based on target.getView()
	for (auto it = mVertices.begin(); it != mVertices.end(); it++) {
		sf::Texture::bind(&it->first->data(), sf::Texture::Pixels);
		target.draw(it->second);
	}
	sf::Texture::bind(NULL);

	// debug draw tile collision
	duk_get_global_string(ScriptEngine::get(), "drawTileShapes");
	bool drawTileShapes = (duk_get_boolean(ScriptEngine::get(), -1) != 0);
	duk_pop(ScriptEngine::get());

	if (drawTileShapes) {
		const unsigned int layer = COLLISION_LAYER;
		for (unsigned int y = 0; y < CHUNK_SIZE; y++) {
			for (unsigned int x = 0; x < CHUNK_SIZE; x++) {
				const Tile& t = mTiles[layer][x][y];
				if (!t.valid())
					continue;

				Transform trans = this->transform();
				trans.position += Vector2f(x * TILE_SIZE, y * TILE_SIZE);
				t.tileset->getTileType(t.tileId).getCollisionShape(t.rotation, t.mirrored).debugDraw(target, trans, sf::Color::Green);
			}
		}
	}
}

void TileChunk::updateVertices()
{
	mVertices.clear();

	for (unsigned int layer = 0; layer < N_LAYERS; layer++) {
		for (unsigned int y = 0; y < CHUNK_SIZE; y++) {
			for (unsigned int x = 0; x < CHUNK_SIZE; x++) {
				const Tile& tile = mTiles[layer][x][y];
				if (!tile.valid())
					continue;

				const TileType& tileType = tile.tileset->getTileType(tile.tileId);
				const std::shared_ptr<Texture>& texture = tile.tileset->getTexture();
				const Vector2i& texPos = tileType.textureRectPos;
				const Vector2i& texSize = tileType.textureRectSize;

				auto it = mVertices.begin();
				for (; it != mVertices.end() && it->first != texture; it++);

				if (it == mVertices.end()) {
					mVertices.push_back(std::make_pair(texture, sf::VertexArray(sf::Triangles)));
					it = mVertices.end() - 1;
				}

				unsigned int idx = it->second.getVertexCount();
				it->second.resize(idx + 6);

				static constexpr Vector2i corners[4] = {
					Vector2i(0, 0), Vector2i(0, 1), Vector2i(1, 1), Vector2i(1, 0)
				};

				static constexpr Vector2i dirs[4][6] = {
					{ corners[0], corners[1], corners[3],  // 0 degrees
					  corners[1], corners[2], corners[3] },
					{ corners[1], corners[2], corners[0],  // 90 degrees
					  corners[2], corners[3], corners[0] },
					{ corners[2], corners[3], corners[1],  // 180 degrees
					  corners[3], corners[0], corners[1] },
					{ corners[3], corners[0], corners[2],  // 270 degrees
					  corners[0], corners[1], corners[2] },
				};

				for (unsigned int i = 0; i < 6; i++) {
					const Vector2i& dir = dirs[0][i];
					Vector2i dirTex = dirs[tile.rotation][i];
					if (tile.mirrored)
						dirTex.x = 1 - dirTex.x;

					it->second[idx + i].position = sf::Vector2f((float)position().x + x * TILE_SIZE + TILE_SIZE*dir.x, (float)position().y + y * TILE_SIZE + TILE_SIZE*dir.y);
					it->second[idx + i].texCoords = sf::Vector2f((float)texPos.x + texSize.x * dirTex.x, (float)texPos.y + texSize.y * dirTex.y);
				}
			}
		}
	}
}

void TileChunk::tilesetChanged(TileSet* ts, bool deleted)
{
	for (unsigned int layer = 0; layer < N_LAYERS; layer++) {
		for (unsigned int y = 0; y < CHUNK_SIZE; y++) {
			for (unsigned int x = 0; x < CHUNK_SIZE; x++) {
				Tile& tile = mTiles[layer][x][y];
				if (tile.tileset == ts) {
					if (deleted) {
						tile.tileId = 0;
						tile.tileset = nullptr;
						setDirty(FLAG_CHANGED);
					}
					mDirty = true;
				}
			}
		}
	}
}

Box2f TileChunk::worldBox() const
{
	// TODO precompute and shrink based on empty tiles
	return Box2f(position().x, position().y, position().x + TILE_SIZE*CHUNK_SIZE, position().y + TILE_SIZE*CHUNK_SIZE);
}

void TileChunk::buildCollisionList(ShapeList* list, const Box2f& worldRegion)
{
	Vector2i min, max;

	const Vector2f chunkPos = position();
	min.x = std::max((int)(worldRegion.min.x - chunkPos.x) / TILE_SIZE, 0);
	min.y = std::max((int)(worldRegion.min.y - chunkPos.y) / TILE_SIZE, 0);
	max.x = std::min((int)(worldRegion.max.x - chunkPos.x + TILE_SIZE) / TILE_SIZE, CHUNK_SIZE);
	max.y = std::min((int)(worldRegion.max.y - chunkPos.y + TILE_SIZE) / TILE_SIZE, CHUNK_SIZE);

	for (int y = min.y; y < max.y; y++) {
		for (int x = min.x; x < max.x; x++) {
			const Tile& tile = mTiles[COLLISION_LAYER][x][y];
			if (!tile.valid())
				continue;

			TileType& tileType = tile.tileset->getTileType(tile.tileId);

			Transform tileTrans;
			tileTrans.position = chunkPos + Vector2f((float)x*TILE_SIZE, (float)y*TILE_SIZE);
			list->add(this, &tileType.getCollisionShape(tile.rotation, tile.mirrored), tileTrans);
		}
	}
}

DirtyMask TileChunk::packUpdate(Client* conn, DirtyMask mask, BitStreamWriter& stream)
{
	DirtyMask remainingMask = Parent::packUpdate(conn, mask, stream);

	if (stream.writeBool(mask & FLAG_POSITION)) {
		stream.writeVector2f(position());
	}

	if (stream.writeBool(mask & FLAG_CHANGED)) {
		// build tileset table
		std::map<TileSet*, int> tilesetTable;
		TileSet* ordered[MAX_TILESETS_PER_CHUNK];
		for (unsigned int layer = 0; layer < N_LAYERS; layer++) {
			for (unsigned int y = 0; y < CHUNK_SIZE; y++) {
				for (unsigned int x = 0; x < CHUNK_SIZE; x++) {
					const auto& t = mTiles[layer][x][y];

					auto it = tilesetTable.find(t.tileset);
					if (it == tilesetTable.end()) {
						int idx = tilesetTable.size();
						assert(idx <= MAX_TILESETS_PER_CHUNK);
						ordered[idx] = t.tileset;
						tilesetTable[t.tileset] = idx;
					}
				}
			}
		}

		// write tileset table
		stream.writeInt(tilesetTable.size(), 0, MAX_TILESETS_PER_CHUNK + 1);
		for (unsigned int i = 0; i < tilesetTable.size(); i++) {
			GhostID ghostID = conn->findGhostID(ordered[i]);
			stream.writeGhostID(ghostID);

			// if the tileset hasn't ghosted yet, this chunk isn't done updating
			// TODO do this check at when choosing to write changed so we don't
			// constantly re-update the entire chunk every frame until all
			// tilesets are ghosted
			if (ordered[i] != NULL && ghostID == 0)
			remainingMask |= FLAG_CHANGED;
		}

		for (unsigned int layer = 0; layer < N_LAYERS; layer++) {
			for (unsigned int y = 0; y < CHUNK_SIZE; y++) {
				for (unsigned int x = 0; x < CHUNK_SIZE; x++) {
					const auto& t = mTiles[layer][x][y];

					stream.writeInt(tilesetTable[t.tileset], 0, MAX_TILESETS_PER_CHUNK);
					stream.writeUint8(t.tileId);
					stream.writeInt(t.rotation, 0, 3);
					stream.writeBool(t.mirrored);
				}
			}
		}
	}

	if ((remainingMask & FLAG_CHANGED) != 0) {
		LOG(LogDebug) << "Incompletely ghosted tilechunk (at least one tileset missing)...";
	}

	return remainingMask;
}

void TileChunk::unpackUpdate(ServerConnection* conn, BitStreamReader& stream, unsigned int timestamp)
{
	Parent::unpackUpdate(conn, stream, timestamp);

	if (stream.readBool()) {
		setPosition(stream.readVector2f());
	}

	if (stream.readBool()) {
		// read tileset table
		int32_t nTilesets = 0;
		stream.readInt(nTilesets, 0, MAX_TILESETS_PER_CHUNK + 1);
		GhostID tilesetGhostIDs[MAX_TILESETS_PER_CHUNK] {};
		for (int i = 0; i < nTilesets; i++) {
		  tilesetGhostIDs[i] = stream.readGhostID();
		}

		for (unsigned int layer = 0; layer < N_LAYERS; layer++) {
			for (unsigned int y = 0; y < CHUNK_SIZE; y++) {
				for (unsigned int x = 0; x < CHUNK_SIZE; x++) {
					auto& t = mTiles[layer][x][y];
					
					// if tileset was not ghosted, the ghost ID will be 0, which safely resolves to NULL
					int32_t tilesetIdx = 0;
					stream.readInt(tilesetIdx, 0, MAX_TILESETS_PER_CHUNK);
					GhostID tilesetGhostID = tilesetGhostIDs[tilesetIdx];

					t.tileset = (TileSet*) conn->resolveGhostID(tilesetGhostID);
					t.tileId = stream.readUint8();
					int32_t rotation;
					stream.readInt(rotation, 0, 3);
					t.rotation = rotation;
					t.mirrored = stream.readBool();
				}
			}
		}
	}

	updateVertices();
	notifyWorldBoxChanged();
}

NetObjectTypeID TileChunk::netObjectTypeID()
{
	return CLASS_TILECHUNK;
}




void ChunkSerializer::addChunk(TileChunk* chunk)
{
	ChunkData newData;
	for (int i = 0; i < TileChunk::N_LAYERS; i++)
		newData.layerEmpty[i] = true;
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 2; j++)
			newData.quadrantEmpty[i][j] = true;

	const int scale = (TileChunk::CHUNK_SIZE * TileChunk::TILE_SIZE);
	newData.position = Vector2i(((int) chunk->position().x) / scale, ((int) chunk->position().y) / scale);

	for (unsigned int layer = 0; layer < TileChunk::N_LAYERS; layer++) {
		for (unsigned int y = 0; y < TileChunk::CHUNK_SIZE; y++) {
			for (unsigned int x = 0; x < TileChunk::CHUNK_SIZE; x++) {
				const int quadX = x / ChunkData::QUADRANT_SIZE;
				const int quadY = y / ChunkData::QUADRANT_SIZE;
				auto& t = chunk->mTiles[layer][x][y];

				if (t.valid()) {
					newData.quadrantEmpty[quadX][quadY] = false;
					newData.layerEmpty[layer] = false;

					auto it = mTilesets.find(t.tileset);
					if (it == mTilesets.end()) {
						uint16_t nextID = (uint16_t)(mTilesets.size() + 1);
						assert(nextID <= MAX_TILESETS);
						it = mTilesets.insert(std::pair<TileSet*, uint16_t>(t.tileset, nextID)).first;
					}
					newData.tiles[layer][x][y] = TileData(it->second, t.tileId, t.rotation, t.mirrored);
				} else {
					newData.tiles[layer][x][y] = TileData();
				}
			}
		}
	}

	mChunks.push_back(newData);
}

duk_idx_t ChunkSerializer::serialize(duk_context* ctx)
{
	duk_size_t tilesetTableBits = 32;  // for number of tilesets in table
	for (auto it = mTilesets.begin(); it != mTilesets.end(); it++)
		tilesetTableBits += 32 + it->first->getName().size() * 8;  // string length + string data
	duk_size_t chunkBits = mChunks.size() * MAX_BYTES_PER_CHUNK * 8;
	duk_size_t bytes = (tilesetTableBits + chunkBits + 7) / 8;  // round up
	bytes = bytes + (4 - (bytes % 4));  // BitStreamWriter requires multiples of 4 bytes

	uint8_t* buff = (uint8_t*) duk_push_buffer(ctx, bytes, true);  // dynamic so we can shrink it at the end
	BitStreamWriter stream(buff, bytes);

	// build the tileset table
	std::vector<const std::string*> tilesetTable(mTilesets.size());
	for (auto it = mTilesets.begin(); it != mTilesets.end(); it++) {
		tilesetTable[it->second - 1] = &it->first->getName();  // 1 -> 0 indexed (0 is reserved to mean NULL)
	}

	// write tileset table
	stream.writeUint32(tilesetTable.size());
	for (unsigned int i = 0; i < tilesetTable.size(); i++) {
		stream.writeString(*tilesetTable[i]);
	}

	// write chunks
	stream.writeUint32(mChunks.size());
	for (auto it = mChunks.begin(); it != mChunks.end(); it++) {
		stream.writeVector2i(it->position);

		for (unsigned int layer = 0; layer < TileChunk::N_LAYERS; layer++) {
			if (stream.writeBool(it->layerEmpty[layer]))
				continue;

			for (unsigned int quadX = 0; quadX < 2; quadX++) {
				for (unsigned int quadY = 0; quadY < 2; quadY++) {
					if (stream.writeBool(it->quadrantEmpty[quadX][quadY]))
						continue;

					for (unsigned int y = quadY * ChunkData::QUADRANT_SIZE; y < (quadY + 1) * ChunkData::QUADRANT_SIZE; y++) {
						for (unsigned int x = quadX * ChunkData::QUADRANT_SIZE; x < (quadX + 1) * ChunkData::QUADRANT_SIZE; x++) {
							auto& t = it->tiles[layer][x][y];
							stream.writeInt(t.tileset, 0, MAX_TILESETS);
							if (t.tileset != 0) {
								stream.writeUint16(t.tileId);
								stream.writeInt(t.rotation, 0, 3);
								stream.writeBool(t.mirror);
							}
						}
					}
				}
			}
		}
	}

	stream.flush();
	duk_resize_buffer(ctx, -1, stream.bytesWritten());

	duk_base64_encode(ctx, -1);
	return 1;
}

duk_idx_t ChunkSerializer::deserialize(duk_context* ctx)
{
	duk_base64_decode(ctx, 0);
	duk_size_t bytes;
	uint8_t* buff = (uint8_t*) duk_require_buffer(ctx, 0, &bytes);
	if (buff == NULL)
		return 0;

	SceneContainer* scene = NULL;
	dukglue_read(ctx, 1, &scene);
	if (scene == NULL)
		return 0;

	std::vector<TileSet*> tilesets;
	dukglue_read(ctx, 2, &tilesets);
	if (tilesets.empty())
		return 0;

	BitStreamReader stream(buff, bytes);

	std::vector<TileSet*> tilesetTable(stream.readUint32());
	for (unsigned int i = 0; i < tilesetTable.size(); i++) {
		std::string tilesetName;
		stream.readString(tilesetName);

		auto it = std::find_if(tilesets.begin(), tilesets.end(), [&](TileSet* t) { return t && t->getName() == tilesetName; });
		tilesetTable[i] = (it == tilesets.end() ? NULL : *it);
	}

	uint32_t nChunks = stream.readUint32();
	std::vector<TileChunk*> chunksOut(nChunks);
	const int scale = (TileChunk::CHUNK_SIZE * TileChunk::TILE_SIZE);
	for (unsigned int i = 0; i < nChunks; i++) {
		TileChunk* newChunk = new TileChunk(scene);
		chunksOut[i] = newChunk;

		Vector2i chunkPos = stream.readVector2i();
		newChunk->setPosition(chunkPos.x * scale, chunkPos.y * scale);

		for (unsigned int layer = 0; layer < TileChunk::N_LAYERS; layer++) {
			if (stream.readBool())  // layerEmpty[layer]
				continue;

			for (unsigned int quadX = 0; quadX < 2; quadX++) {
				for (unsigned int quadY = 0; quadY < 2; quadY++) {
					if (stream.readBool()) {  // quadrantEmpty[quadX][quadY]
						continue;
					}

					for (unsigned int y = quadY * ChunkData::QUADRANT_SIZE; y < (quadY + 1) * ChunkData::QUADRANT_SIZE; y++) {
						for (unsigned int x = quadX * ChunkData::QUADRANT_SIZE; x < (quadX + 1) * ChunkData::QUADRANT_SIZE; x++) {

							int32_t tilesetId;
							stream.readInt(tilesetId, 0, MAX_TILESETS);
							if (tilesetId != 0) {
								TileSet* tileset = tilesetTable[tilesetId - 1];
								TileID tileId = stream.readUint16();
								int32_t rotation;
								stream.readInt(rotation, 0, 3);
								bool mirror = stream.readBool();
								newChunk->setTile(layer, x, y, tileset, tileId, rotation, mirror);
							}
						}
					}
				}
			}
		}
	}

	dukglue_push(ctx, chunksOut);
	return 1;
}