#pragma once

#include <Sim.h>
#include <net/NetStateManager.h>
#include <collision/ConvexShape.h>
#include <Pawn.h>
#include <gfx/Sprite.h>
#include <audio/Sound.h>

#include <SFML/Graphics/Color.hpp>

SCRIPT_CLASS("Player", Player, Pawn, "A controllable 2D platformer character with client-side prediction.");
class Player : public Pawn
{
public:
	SCRIPT_CONSTRUCTOR(Player, "Player", SceneContainer*);
	Player(SceneContainer* scene);
	virtual ~Player();

	virtual void tick() override;
	void render(sf::RenderTarget& target, float lerp) override;

	inline const Vector2f& velocity() const {
		return currentState().velocity;
	}

	SCRIPT_PROPERTY(Player, "velocity", &Player::velocity, &Player::setVelocity, "The Player's velocity.");
	void setVelocity(const Vector2f& newVel);

	void setColor(sf::Color color);

	virtual DirtyMask packUpdate(Client* conn, DirtyMask mask, BitStreamWriter& stream) override;
	virtual void unpackUpdate(ServerConnection* conn, BitStreamReader& stream, unsigned int timestamp) override;
	virtual NetObjectTypeID netObjectTypeID() override;

	virtual Box2f worldBox() const override;
	virtual void buildCollisionList(ShapeList* list, const Box2f& worldRegion) override;
	virtual TypeMask typeMask() const override;

	// Pawn methods
	virtual void addMoveClientside(const Move& move) override;
	virtual void addMoveServerside(const Move& move) override;
	virtual Vector2f calcRenderPosition(float lerp) const override;

protected:
	struct PlayerState {
		Transform transform;
		Vector2f velocity;
		bool onGround;
		float groundAngle;  // in radians, only valid if onGround == true
		bool collisionThisTick;
		bool facingLeft;  // false = facingRight

		PlayerState() {}
		PlayerState(const Transform& t, const Vector2f& v, bool onGround = false, float grAngle = 0.f, bool col = false, bool flip = false)
			: transform(t), velocity(v), onGround(onGround), groundAngle(grAngle), collisionThisTick(col), facingLeft(flip) {}
	};

	PlayerState nextState(const PlayerState& from, const Move& input, bool replayingMoves) const;
	const PlayerState& currentState() const;
	void applyNetState();
	static PlayerState interpolatePlayerState(const PlayerState& s1, const PlayerState& s2, float t);

	void onTransformChanged() override;

	// outside of prediction
	sf::Color mColor;
	ConvexShape mShape;

	Sprite mSprite;
	Sound mJumpSound;

	// dirty flags
	enum Flags {
		FLAG_POSITION = SceneObject::FLAG_POSITION,
		FLAG_VELOCITY = BIT(1),
		FLAG_COLOR = BIT(2)
	};

	NetStateManager<PlayerState, Move> mNetState;
	unsigned int mApproxLatency;
};