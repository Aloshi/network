#pragma once

#include <map>
#include <scene/SceneObject.h>
#include <game/TileSet.h>
#include <SFML/Graphics/VertexArray.hpp>

class ChunkSerializer;

SCRIPT_CLASS("TileChunk", TileChunk, SceneObject, "A chunk of tiles in world space.");
class TileChunk : public SceneObject {
public:
	static const int COLLISION_LAYER = 1;  // layer that has collision
	static const int N_LAYERS = 2;
	static const int TILE_SIZE = 16;  // size in pixels of each tile
	static const int CHUNK_SIZE = 16;  // tiles per chunk in each direction

	SCRIPT_CONSTRUCTOR(TileChunk, "TileChunk", SceneContainer*);
	TileChunk(SceneContainer* scene);

	SCRIPT_METHOD(TileChunk, "setTile", &TileChunk::setTile, "Change the tile at layer, x, y.");
	void setTile(int layer, int x, int y, TileSet* ts, unsigned short tileId, unsigned char rotation, bool mirror);

	void tick() override;
	void render(sf::RenderTarget& target, float lerp) override;

	virtual Box2f worldBox() const override;
	virtual void buildCollisionList(ShapeList* list, const Box2f& worldRegion) override;

	virtual DirtyMask packUpdate(Client* conn, DirtyMask mask, BitStreamWriter& stream) override;
	virtual void unpackUpdate(ServerConnection* conn, BitStreamReader& stream, unsigned int timestamp) override;
	virtual NetObjectTypeID netObjectTypeID() override;

protected:
	static const int MAX_TILESETS_PER_CHUNK = 15;
	typedef SceneObject Parent;

	void updateVertices();
	void tilesetChanged(TileSet* ts, bool deleted);

	enum TileChunkFlags {
		FLAG_CHANGED = NEXT_FREE_FLAG
	};

	struct Tile {
		TileSet* tileset;
		TileID tileId;
		uint8_t rotation;  // 0/1/2/3 (2 bits)
		bool mirrored;

		Tile() : tileset(nullptr), tileId(0), rotation(0), mirrored(false) {}

		inline bool valid() const { return tileset != NULL; }
	};
	Tile mTiles[N_LAYERS][CHUNK_SIZE][CHUNK_SIZE];

private:
	TileSet::SignalReceipt mTilesetChangedCb;

	bool mDirty;
	std::vector< std::pair< std::shared_ptr<Texture>, sf::VertexArray> > mVertices;

	friend ChunkSerializer;
};

SCRIPT_CLASS("ChunkSerializer", ChunkSerializer, void, "Serializes a set of TileChunks efficiently.");
class ChunkSerializer : public ScriptObject
{
public:
	SCRIPT_CONSTRUCTOR_MANAGED(ChunkSerializer, "ChunkSerializer");

	SCRIPT_METHOD(ChunkSerializer, "addChunk", &ChunkSerializer::addChunk, "Add a chunk to the serialization.");
	void addChunk(TileChunk* chunk);

	//SCRIPT_METHOD_VARARGS(ChunkSerializer, "addChunks", &ChunkSerializer::addChunks, "Add an array of chunks.");
	//duk_idx_t addChunks(duk_context* ctx);

	SCRIPT_METHOD_VARARGS(ChunkSerializer, "serialize", &ChunkSerializer::serialize, "Serialize all currently visited chunks to a string.");
	duk_idx_t serialize(duk_context* ctx);

	SCRIPT_METHOD_VARARGS(ChunkSerializer, "deserialize", &ChunkSerializer::deserialize,
		"Deserialize chunks from a string. Arguments: [data: str, scene: SceneContainer*, tilesets: array[TileSet*]]");
	duk_idx_t deserialize(duk_context* ctx);

protected:
	static const int TILESET_BITS = 10;
	static const int MAX_TILESETS = (1 << TILESET_BITS) - 1;

	std::map<TileSet*, uint16_t> mTilesets;

	struct TileData {
		uint16_t tileset;
		TileID tileId;
		uint8_t rotation;
		bool mirror;

		TileData() : tileset(0), tileId(0), rotation(0), mirror(0) {}
		TileData(uint16_t set, TileID id, uint8_t rot, bool flip)
			: tileset(set), tileId(id), rotation(rot), mirror(flip) {}
	};
	struct ChunkData {
		static const int QUADRANT_SIZE = TileChunk::CHUNK_SIZE / 2;

		Vector2i position;  // in chunk space (physical coords / (TILE_SIZE * CHUNK_SIZE)
		bool layerEmpty[TileChunk::N_LAYERS];
		bool quadrantEmpty[2][2];
		TileData tiles[TileChunk::N_LAYERS][TileChunk::CHUNK_SIZE][TileChunk::CHUNK_SIZE];
	};
	static const int MAX_BYTES_PER_CHUNK = 8 + TileChunk::N_LAYERS + 4 + 3 * TileChunk::CHUNK_SIZE * TileChunk::CHUNK_SIZE * TileChunk::N_LAYERS;

	std::vector<ChunkData> mChunks;
};