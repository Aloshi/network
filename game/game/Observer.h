#pragma once

#include <Pawn.h>
#include <net/NetStateManager.h>

SCRIPT_CLASS("Observer", Observer, Pawn, "A pawn that provides free camera movement.");
class Observer : public Pawn {
public:
	SCRIPT_CONSTRUCTOR(Observer, "Observer", SceneContainer*);
	Observer(SceneContainer* scene);

	void tick() {}
	void render(sf::RenderTarget& target, float lerp) override {}

	Vector2f calcRenderPosition(float lerp) const override;
	void addMoveClientside(const Move& move) override;
	void addMoveServerside(const Move& move) override;

	DirtyMask packUpdate(Client* conn, DirtyMask mask, BitStreamWriter& stream) override;
	void unpackUpdate(ServerConnection* conn, BitStreamReader& stream, unsigned int timestamp) override;
	NetObjectTypeID netObjectTypeID() override;
	TypeMask typeMask() const override;

protected:
	void onTransformChanged() override;

	static Vector2f interpState(const Vector2f& a, const Vector2f& b, float lerp);
	Vector2f advanceState(const Vector2f& from, const Move& move, bool replaying);

	void applyNetState();

	enum ObserverFlags {
		NEXT_FREE_FLAG = Parent::NEXT_FREE_FLAG
	};

	NetStateManager<Vector2f, Move> mNetState;
};