#include "Player.h"

#include "Sim.h"
#include <net/PacketMessageTypes.h>
#include <collision/ShapeList.h>
#include <scene/SceneContainer.h>
#include <client/ServerConnection.h>
#include <util/Log.h>
#include <server/Client.h>
#include <math/MathUtil.h>
#include <util/BitStream.h>

#include <iostream>

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iomanip>
const std::vector<Vector2f> playerVerts = { Vector2f(-16.f, -16.f), Vector2f(16.f, -16.f), Vector2f(16.f, 16.f), Vector2f(-16.f, 16.f) };

Player::Player(SceneContainer* scene)
	: Pawn(scene),
	mColor(sf::Color::White),
	mNetState(30,
		std::bind(&Player::nextState, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3),
		&Player::interpolatePlayerState,
		PlayerState(transform(), Vector2f(0, 0), false, false)),
	mApproxLatency(0),
	mJumpSound("assets/sounds/jump.ogg")
{
	mShape.setVertices(playerVerts);
	mJumpSound.setVolume(40);

	mSprite.setSpriteSheet(SpriteSheet::get("assets/spritesheets/slime.js"));
	mSprite.playAnimation("stand");
}

Player::~Player()
{
	
}

NetObjectTypeID Player::netObjectTypeID()
{
	return CLASS_PLAYER;
}

void Player::tick()
{
	mSprite.update(TICK_RATE_SECONDS);
	// only change state through applyMove() or unpacking updates
}

extern bool COLLISION_DEBUG;

Player::PlayerState Player::nextState(const PlayerState& from, const Move& input, bool replayingMoves) const
{
	float dt = (float) TICK_RATE_SECONDS;
	const float units = 1.0f / (TICK_RATE_SECONDS * TICK_RATE_SECONDS);
	
	// maximum x velocity
	const float max_vel = 300.0f;

	bool sliding = (input.y > 0);
	bool facingLeft = from.facingLeft;
	if (input.x != 0)
		facingLeft = input.x < 0;

	if (!replayingMoves) {
		if (input.x != 0 && from.onGround && mSprite.getAnimationName() != "walk")
			const_cast<Player*>(this)->mSprite.playAnimation("walk");
		else if (input.x == 0 || !from.onGround)
			const_cast<Player*>(this)->mSprite.playAnimation("stand");
	}

	// calculate acceleration
	Vector2f accel;

	// acceleration from holding left/right
	const float groundAccelRate = 8.0f * units * dt;
	if (from.onGround) {
		accel += Vector2f(sin(from.groundAngle), cos(from.groundAngle)) * groundAccelRate * input.x;
	} else {
		// full horizontal control in the air
		accel.x += input.x * groundAccelRate;
	}

	// friction
	if (input.x == 0 && from.onGround) {
		Vector2f fricDir = Vector2f(sin(from.groundAngle), cos(from.groundAngle));
		if (fricDir.dot(from.velocity) < 0)
			fricDir = fricDir * -1.0f;

		// clamp mu so that friction force brings dot(fricDir, velocity) to exactly 0 near zero (instead of oscillating)
		float mu = 16.0f * units * dt;
		float velMag2 = fricDir.project(from.velocity).len2();
		if (velMag2 <= (mu * dt) * (mu * dt)) {
			mu = sqrt(velMag2) / dt;
		}

		accel -= fricDir * mu;
	}

	// gravity
	accel.y += 9.8f * units * dt;

	// jump
	if (input.y < 0 && from.onGround) {
		float jumpForce = -270.f * units * dt;
		// accel.y = std::min(jumpForce, accel.y + jumpForce);
		float upAngle = from.groundAngle - M_PI_2;
		accel += Vector2f(sin(upAngle), cos(upAngle)) * jumpForce;

		if (!replayingMoves && isClient())
			const_cast<Player*>(this)->mJumpSound.play();
	}

	// calculate new velocity
	Vector2f prevVel = from.velocity;
	Vector2f newVel = prevVel + accel * dt;

	Vector2f startPos = from.transform.position;
	Vector2f offset = newVel * dt;
	Vector2f newPos = startPos + offset;

	// calculate our "working collision bounds," the region we'll query for collision shapes
	// working bounds must contain old shape + new shape
	ShapeList workingList;
	const auto updateWorkingList = [&]() {
		const Box2f startBounds = mShape.calcWorldBox(from.transform);
		const Box2f endBounds(startBounds.min + offset, startBounds.max + offset);
		const Box2f workingBounds = Box2f::merge(startBounds, endBounds);

		// find objects in that area and add their CollisionShapes to a ShapeList
		workingList.clear();
		const std::vector<SceneObject*> nearbyObjs = container()->findObjects(workingBounds, SCENEOBJECT_TYPE_ANY, SCENEOBJECT_TYPE_PLAYER);
		for (unsigned int i = 0; i < nearbyObjs.size(); i++) {
			if (nearbyObjs[i] != this)
				nearbyObjs[i]->buildCollisionList(&workingList, workingBounds);
		}
	};
	updateWorkingList();

	// iterate until we find a new position that isn't hitting anything
	// (or give up to prevent infinite loops)
	bool collisionThisTick = false;
	bool onGround = false;
	float groundAngle = 0.0;
	CollisionInfo info;
	for (int i = 0; i < 4; i++) {
		Transform startTrans;
		startTrans.position = startPos;
		if (workingList.findFirstCollision(&mShape, startTrans, newPos, &info)) {
			collisionThisTick = true;

			/*if (fabs(info.normal.x + 1.0f) < 1e-4) {
				std::cout << "\n";
				COLLISION_DEBUG = true;

				std::vector<CollisionInfo> infos;
				workingList.findAllCollisions(&mShape, startTrans, newPos, &infos);
				for (unsigned int j = 0; j < infos.size(); j++) {
					LOG(LogDebug) << "info #" << j << ", t: " << infos[j].time << ", normal: " << infos[j].normal << ", mtv: " << infos[j].mtv
						<< ", min overlap vec: " << infos[j].minOverlapVec << " (amt " << infos[j].minOverlapAmt << "), on edge: " << infos[j].minOnEdge;
					const auto& theirShape = (ConvexShape*) infos[j].hitShape;

					LOG(LogDebug) << "Their vertices:";
					for (unsigned int k = 0; k < theirShape->vertices().size(); k++) {
						LOG(LogDebug) << "  vertex " << k << ": " << theirShape->vertices()[k] + infos[j].hitShapeTransform.position;
					}

					LOG(LogDebug) << "Our vertices (before):";
					for (unsigned int k = 0; k < mShape.vertices().size(); k++) {
						LOG(LogDebug) << "  vertex " << k << ": " << mShape.vertices()[k] + startTrans.position;
					}

					LOG(LogDebug) << "Our vertices (time of collision):";
					for (unsigned int k = 0; k < mShape.vertices().size(); k++) {
						LOG(LogDebug) << "  vertex " << k << ": " << mShape.vertices()[k] + startTrans.position + offset * infos[j].time;
					}
				}

				COLLISION_DEBUG = false;
			}*/

			if (info.time == 0) { // stuck inside something?
				// move out of it
				newPos = startTrans.position + info.mtv;

				//if (info.normal.dot(Vector2f(0, -1)) < 0.99) {
					//LOG(LogInfo) << "  inside, normal: " << info.normal << ", mtv: " << info.mtv;
				//}
			} else { // hit something on the way to newPos?
				// snap to the time of collision
				newPos = startTrans.position + offset * info.time;
				//if (fabs(info.normal.x - 1.0f) < 1e-12 && fabs(info.normal.y) < 1e-12) {
					//LOG(LogInfo) << "Velocity: " << std::showpos << std::fixed << std::setprecision(8) << newVel;
					//LOG(LogInfo) << std::showpos << std::fixed << std::setprecision(6) << "started from: " << startPos;
					//LOG(LogInfo) << std::showpos << std::fixed << std::setprecision(6) << "t: " << info.time << ", normal: " << info.normal;
					//LOG(LogInfo) << "    moving player to: " << std::showpos << std::fixed << std::setprecision(8) << newPos;
				//}
				// LOG(LogInfo) << "  not inside";
			}

			/*ConvexShape* hit = (ConvexShape*)info.hitShape;
			for (unsigned int myVert = 0; myVert < mShape.vertices().size(); myVert++) {
				for (unsigned int theirVert = 0; theirVert < hit->vertices().size(); theirVert++) {
					const Vector2f& a = mShape.vertices()[myVert] + startTrans.position;
					const Vector2f& b = hit->vertices()[theirVert] + info.hitShapeTransform.position;
					if ((a - b).len() < 1e-2) {
						LOG(LogInfo) << "  ---- VERTICES MATCH";
					}
				}
			}*/

			// "finalize" this move by moving the start position and recalculating dt
			// we just moved for info.time, so remove that much time
			startPos = newPos;
			prevVel = prevVel + accel * (dt * info.time);
			dt = dt * (1 - info.time);

			// kill our acceleration and velocity along the collision normal
			if (!info.normal.isZero()) {
				// LOG(LogInfo) << "  normal " << info.normal; //<< ", removing accel: " << info.normal.project(accel);
				accel = accel - info.normal.project(accel);
				prevVel = prevVel - info.normal.project(prevVel);
			}

			// is our normal facing up?
			// if so, say we are on the ground
			if (info.normal.dot(Vector2f(0, -1)) > 0) {
				onGround = true;
				groundAngle = atan2(-info.normal.y, info.normal.x);
			}

			// recalculate our new velocity and end position
			newVel = prevVel + accel * dt;
			offset = newVel * dt;
			newPos += offset;
			updateWorkingList();
		} else {
			break;
		}
	}

	Transform newTrans = from.transform;
	newTrans.position = newPos;
	return PlayerState(newTrans, newVel, onGround, groundAngle, collisionThisTick, facingLeft);
}

Player::PlayerState Player::interpolatePlayerState(const PlayerState& s1, const PlayerState& s2, float t)
{
	assert(t >= 0 && t <= 1);
	// linear interpolation between s1 and s2
	PlayerState state = s1;
	state.transform.position += (s2.transform.position - s1.transform.position) * t;
	state.velocity += (s2.velocity - s1.velocity) * t;
	return state;
}

void Player::setVelocity(const Vector2f& newVel)
{
	mNetState.mostRecentState().velocity = newVel;
	setDirty(FLAG_VELOCITY);
}

void Player::setColor(sf::Color color)
{
	mColor = color;
	setDirty(FLAG_COLOR);
}

DirtyMask Player::packUpdate(Client* conn, DirtyMask mask, BitStreamWriter& stream)
{
	DirtyMask remainingMask = Parent::packUpdate(conn, mask, stream);

	const PlayerState state = currentState();

  stream.writeCheck();

	stream.writeUint16(mNetState.lastServerStateNumber());

	if (stream.writeBool(mask & FLAG_POSITION)) {  // FLAG_POSITION
		stream.writeVector2f(state.transform.position);
		stream.writeBool(state.onGround);
		stream.writeFloat(state.groundAngle);
		stream.writeBool(state.facingLeft);
	}

  stream.writeCheck();

	if (stream.writeBool(mask & FLAG_VELOCITY)) {  // FLAG_VELOCITY
		stream.writeVector2f(state.velocity);
	}

	if (stream.writeBool(mask & FLAG_COLOR)) {  // FLAG_COLOR
		stream.writeUint32(mColor.toInteger());
	}

	return remainingMask;
}

void Player::unpackUpdate(ServerConnection* conn, BitStreamReader& stream, unsigned int dummy)
{
	Parent::unpackUpdate(conn, stream, dummy);

	const PlayerState origState = currentState();  // kept to detect warping later
	const Vector2f origRenderPos = calcRenderPosition(0.0f);

	// we need the last received state so we can calculate apply the delta encoding to it
	PlayerState state = mNetState.lastServerState();

  stream.readCheck();

	// override our client's input timestamp with the timestamp of the remote client's input
	// should only do something if !isLocalClient()
	uint16_t timestamp = stream.readUint16();

	// apply delta encoding to state
	if (stream.readBool()) { // FLAG_POSITION
		state.transform.position = stream.readVector2f();
		state.onGround = stream.readBool();
		state.groundAngle = stream.readFloat();
		state.facingLeft = stream.readBool();
	}

  stream.readCheck();

	if (stream.readBool()) { // FLAG_VELOCITY
		state.velocity = stream.readVector2f();
	}

	if (stream.readBool()) { // FLAG_COLOR
		mColor = sf::Color(stream.readUint32());
	}

	mNetState.receiveServerState(timestamp, state);
	applyNetState();

	Vector2f positionDelta = origState.transform.position - currentState().transform.position;
	if (positionDelta.len() > 0.01f && isLocalPlayer()) {
		LOG(LogWarning) << "Local client warping (position)";
	}
	Vector2f velDelta = origState.velocity - currentState().velocity;
	if (velDelta.len() > 0.01f && isLocalPlayer()) {
		LOG(LogWarning) << "Local client warping (velocity)";
	}
	Vector2f renderPosDelta = origRenderPos - calcRenderPosition(0.0f);
	if (renderPosDelta.len() > 0.01f && isLocalPlayer()) {
		LOG(LogWarning) << "Local client warping (render position)";
	}

	int latency = seq_num_dist(timestamp, serverConnection()->lastMove().seq_num);
	int diff = (int)latency - (int)mApproxLatency;
	if (abs(diff) > 3) {
		//LOG(LogInfo) << "lastMoveNumber - timestamp = " << latency;
		mApproxLatency += (int) (diff * 0.25f);
	}
}

void Player::onTransformChanged()
{
	if (isServer()) {
		mNetState.mostRecentState().transform = transform();
		setDirty(FLAG_POSITION);
	}
}

// run on the server when a move is received
void Player::addMoveServerside(const Move& move)
{
	mNetState.addMoveServerside(move);
	applyNetState();
	setDirty(FLAG_POSITION | FLAG_VELOCITY);
}

// run on the client when a local move is generated
void Player::addMoveClientside(const Move& move)
{
	mNetState.addMoveClientside(move);
	applyNetState();
}

// only used by camera to know where to place itself
Vector2f Player::calcRenderPosition(float lerp) const
{
	PlayerState state = mNetState.calcState(lerp);
	return state.transform.position;
}

// apply NetStateManager state to SceneObject class variables
// (i.e. transform)
// this is necessary or the player won't show up in the correct
// position to the physics engine
void Player::applyNetState()
{
	PlayerState state;
	if (isLocalPlayer()) {
		state = mNetState.mostRecentState();
	} else {
		int time = seq_num_sub(serverConnection()->lastMove().seq_num, mApproxLatency + 9);
		state = mNetState.calcState(time, 0.0f);
	}

	setTransform(state.transform);
}

const Player::PlayerState& Player::currentState() const
{
	return mNetState.mostRecentState();
}

void Player::render(sf::RenderTarget& target, float lerp)
{
	PlayerState state;
	if (isLocalPlayer()) {
		state = mNetState.calcState(lerp);
	} else {
		int time = seq_num_sub(serverConnection()->lastMove().seq_num, mApproxLatency + 9);
		state = mNetState.calcState(time, lerp);
	}

	// debug square
	sf::RectangleShape shape(sf::Vector2f(32, 32));
	shape.setFillColor(state.onGround ? sf::Color::Magenta : mColor);
	shape.setOutlineThickness(0);
	shape.setPosition(state.transform.position - Vector2f(16, 16));
	target.draw(shape);

	// draw the player sprite
	mSprite.setOffset(state.transform.position - mSprite.getSize() * 0.5f);
	mSprite.setMirrorHorizontal(state.facingLeft);
	mSprite.draw(target);

	duk_get_global_string(ScriptEngine::get(), "drawServerState");
	bool drawServerState = (duk_get_boolean(ScriptEngine::get(), -1) != 0);
	duk_pop(ScriptEngine::get());

	if (drawServerState)
		mShape.debugDraw(target, mNetState.lastServerState().transform, sf::Color::White);
}

Box2f Player::worldBox() const
{
	return mShape.calcWorldBox(transform());
}

void Player::buildCollisionList(ShapeList* list, const Box2f& worldRegion)
{
	list->add(this, &mShape, transform());
}

TypeMask Player::typeMask() const
{
	return SCENEOBJECT_TYPE_DYNAMIC | SCENEOBJECT_TYPE_PLAYER;
}