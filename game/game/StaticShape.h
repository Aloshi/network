#pragma once

#include <scene/SceneObject.h>
#include <script/ScriptEngine.h>
#include <collision/ConvexShape.h>

SCRIPT_CLASS("StaticShape", StaticShape, SceneObject, "A static shape with collision in the world.");
class StaticShape : public SceneObject
{
public:
	typedef SceneObject Parent;

	SCRIPT_CONSTRUCTOR(StaticShape, "StaticShape", SceneContainer*);
	StaticShape(SceneContainer* scene);
	virtual ~StaticShape() {}

	void tick() override {}
	void render(sf::RenderTarget& target, float lerp) override;

	virtual DirtyMask packUpdate(Client* conn, DirtyMask mask, BitStreamWriter& stream) override;
	virtual void unpackUpdate(ServerConnection* conn, BitStreamReader& stream, unsigned int timestamp) override;
	virtual NetObjectTypeID netObjectTypeID() override;

	virtual Box2f worldBox() const override;
	virtual void buildCollisionList(ShapeList* list, const Box2f& worldRegion) override;

	SCRIPT_PROPERTY(StaticShape, "vertices", &StaticShape::vertices, &StaticShape::setVertices,
		"Get or set the vertices for this shape. Vertices will decomposed into multiple convex shapes.");
	void setVertices(std::vector<Vector2f> verts);
	inline const std::vector<Vector2f>& vertices() const { return mVertices; }

private:
	enum StaticShapeFlags {
		FLAG_SHAPE_CHANGED = NEXT_FREE_FLAG
	};

	std::vector<Vector2f> mVertices;
	std::vector<ConvexShape> mShapes;
};