#pragma once

#include <Move.h>
#include <game/Player.h>

SCRIPT_CLASS("AIPlayer", AIPlayer, Player, "A player that can be programmatically controlled.");
class AIPlayer : public Player {
public:
	SCRIPT_CONSTRUCTOR(AIPlayer, "AIPlayer", SceneContainer*);
	AIPlayer(SceneContainer* scene);
	void tick() override;

	SCRIPT_METHOD(AIPlayer, "setDirection", &AIPlayer::setDirection, "Set the x/y axis inputs (signed 8-bit integers).");
	void setDirection(int8_t x, int8_t y);

	SCRIPT_METHOD(AIPlayer, "setTrigger", &AIPlayer::setTrigger, "Set whether or not a trigger is pressed by index.");
	void setTrigger(int trigger_idx, bool pressed);

	SCRIPT_METHOD(AIPlayer, "clearTriggers", &AIPlayer::clearTriggers, "Set all triggers to not pressed.");
	void clearTriggers();

protected:
	Move mNextMove;
};
