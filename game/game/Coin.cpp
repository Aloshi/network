#include "Coin.h"

#include <net/PacketMessageTypes.h>
#include <collision/ConvexShape.h>
#include <collision/ShapeList.h>
#include <scene/SceneContainer.h>
#include <game/Player.h>
#include <server/Client.h>
#include <client/ServerConnection.h>
#include <util/BitStream.h>

// CoinDB stuff
class CoinDB {
public:
	CoinDB();

	bool isCollected(CoinID id, Client* cl) const;
	void setCollected(CoinID id, Client* cl, bool collected);
	void clearCollected(Client* cl);

	CoinID reserveID();

private:
	CoinID mNextID;

	typedef std::set<CoinID> CoinSet;
	std::map<ClientGUID, CoinSet> mClients;
};

CoinDB::CoinDB() {
	mNextID = 1;
}

CoinID CoinDB::reserveID() {
	return mNextID++;
}

bool CoinDB::isCollected(CoinID id, Client* cl) const {
	if (cl == NULL)
		return false;

	auto clSet = mClients.find(cl->guid());
	if (clSet == mClients.end())
		return false;

	auto coin = clSet->second.find(id);
	return (coin != clSet->second.end());
}

void CoinDB::setCollected(CoinID id, Client* cl, bool collected) {
	if (cl == NULL)
		return;

	auto& clSet = mClients[cl->guid()];
	if (collected)
		clSet.insert(id);
	else
		clSet.erase(id);
}

void CoinDB::clearCollected(Client* cl) {
	if (cl == NULL)
		return;

	mClients[cl->guid()].clear();
}


// Coin stuff
static const char* COIN_TEXTURE_PATH = "assets/sprites/coin2.png";
static Vector2f COIN_SIZE(16.0f, 16.0f);
static Box2f COIN_BOX(Vector2f(0, 0), COIN_SIZE);
static ConvexShape COIN_SHAPE( std::vector<Vector2f> {
	Vector2f(0, 0),
	Vector2f(COIN_SIZE.x, 0),
	COIN_SIZE,
	Vector2f(0, COIN_SIZE.y)
});

static CoinDB gCoinDB;

Coin::Coin(SceneContainer* scene) : SceneObject(scene), mID(0), mCollected(false)
{
	if (!isGhost())
		mID = gCoinDB.reserveID();

	mID = gCoinDB.reserveID();
	mSprite.setTexture(Texture::get(COIN_TEXTURE_PATH, false, false));
	mSprite.setSize(COIN_SIZE);
}

NetObjectTypeID Coin::netObjectTypeID()
{
	return CLASS_COIN;
}

TypeMask Coin::typeMask() const
{
	return SCENEOBJECT_TYPE_DYNAMIC;
}

Box2f Coin::worldBox() const
{
	return COIN_SHAPE.calcWorldBox(transform());
}

void Coin::buildCollisionList(ShapeList* list, const Box2f& worldRegion)
{
	// list->add(this, &COIN_SHAPE, transform());
}

void Coin::tick()
{
	// collision test
	const Box2f workingBounds = worldBox();
	const std::vector<SceneObject*> nearbyObjs = container()->findObjects(workingBounds, SCENEOBJECT_TYPE_PLAYER);
	ShapeList workingList;
	for (unsigned int i = 0; i < nearbyObjs.size(); i++) {
		nearbyObjs[i]->buildCollisionList(&workingList, workingBounds);
	}

	std::vector<CollisionInfo> hits;
	workingList.findAllCollisions(&COIN_SHAPE, transform(), position(), &hits);
	for (unsigned int i = 0; i < hits.size(); i++) {
		SceneObject* obj = hits.at(i).hitObject;
		Player* pl = dynamic_cast<Player*>(obj);
		Client* cl = pl ? pl->client() : NULL;

		bool collected = isGhost() ? mCollected : gCoinDB.isCollected(mID, cl);
		if (collected)
			continue;

		if (isGhost()) {
			if (serverConnection()->controlObject() == pl) {
				mCollected = true;
				playCollectedEffect();
				// TODO correction timeout in case of bad prediction
			} else {
				// collected by another player
			}
		} else {
			gCoinDB.setCollected(mID, cl, true);
			ScriptEngine::pcall_method_noresult(this, "onCoinCollectedServer", cl);
			setDirty(FLAG_COLLECTED);
		}
	}
}

void Coin::render(sf::RenderTarget& target, float lerp)
{
	if (mCollected)
		return;

	mSprite.setOffset(position());
	mSprite.draw(target);
}

DirtyMask Coin::packUpdate(Client* conn, DirtyMask mask, BitStreamWriter& stream)
{
	if (stream.writeBool(mask & FLAG_POSITION)) {
		stream.writeVector2f(position());
	}

	if (stream.writeBool(mask & FLAG_COLORSHIFT)) {
		sf::Color sfcolor = mSprite.getColorshift();
		stream.writeUint32(sfcolor.toInteger());  // hope this doesn't have endianness bugs
	}

	stream.writeBool(gCoinDB.isCollected(mID, conn));

	return 0;
}

void Coin::unpackUpdate(ServerConnection * conn, BitStreamReader& stream, unsigned int timestamp)
{
	if (stream.readBool()) {  // FLAG_POSITION
		Vector2f pos = stream.readVector2f();
		setPosition(pos);
	}

	if (stream.readBool()) {  // FLAG_COLORSHIFT
		uint32_t color = stream.readUint32();
		sf::Color sf(color);  // hope this doesn't have endianness bugs
		mSprite.setColorshift(sf.r, sf.g, sf.b, sf.a);
	}

	mCollected = stream.readBool();
}

void Coin::playCollectedEffect()
{
	// TODO
	ScriptEngine::pcall_method_noresult(this, "onCoinCollectedClient");
}