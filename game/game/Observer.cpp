#include <game/Observer.h>
#include <net/PacketMessageTypes.h>
#include <Sim.h>

#include <util/BitStream.h>

Observer::Observer(SceneContainer * scene)
	: Pawn(scene), mNetState(30,
		std::bind(&Observer::advanceState, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3),
		&Observer::interpState
	)
{
}

Vector2f Observer::calcRenderPosition(float lerp) const
{
	return mNetState.calcState(lerp);
}

void Observer::addMoveClientside(const Move& move)
{
	mNetState.addMoveClientside(move);
	applyNetState();
	setDirty(FLAG_POSITION);
}

void Observer::addMoveServerside(const Move& move)
{
	mNetState.addMoveServerside(move);
	applyNetState();
}

DirtyMask Observer::packUpdate(Client* conn, DirtyMask mask, BitStreamWriter& stream)
{
	if (stream.writeBool(mask & FLAG_POSITION)) {
		stream.writeUint16(mNetState.lastServerStateNumber());
		stream.writeVector2f(mNetState.mostRecentState());
	}
	return 0;
}

void Observer::unpackUpdate(ServerConnection* conn, BitStreamReader& stream, unsigned int /*timestamp*/)
{
	// TODO
	if (stream.readBool()) {  // FLAG_POSITION
		uint16_t timestamp = stream.readUint16();
		Vector2f pos = stream.readVector2f();
		mNetState.receiveServerState(timestamp, pos);
		applyNetState();
	}
}

void Observer::onTransformChanged()
{
	if (isServer())
		mNetState.mostRecentState() = position();
}

Vector2f Observer::interpState(const Vector2f & a, const Vector2f & b, float lerp)
{
	return a + (b - a) * lerp;
}

Vector2f Observer::advanceState(const Vector2f& from, const Move& move, bool replaying)
{
	return from + Vector2f(move.x * 8.0f, move.y * 8.0f);
}

void Observer::applyNetState()
{
	setPosition(mNetState.mostRecentState());
}

NetObjectTypeID Observer::netObjectTypeID()
{
	return CLASS_OBSERVER;
}

TypeMask Observer::typeMask() const
{
	return SCENEOBJECT_TYPE_OBSERVER;
}
