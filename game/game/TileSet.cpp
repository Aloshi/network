#include "TileSet.h"

#include <net/PacketMessageTypes.h>
#include <util/BitStream.h>

#include <game/TileChunk.h>  // for TILE_SIZE

Simple::Signal<void(TileSet*, bool)> TileSet::smChangedSignal;

TileSet::~TileSet()
{
	smChangedSignal.emit(this, true);
}

TileSet::SignalReceipt TileSet::registerChangedCallback(const std::function<void(TileSet*, bool)>& callback)
{
	return SignalReceipt(&smChangedSignal, smChangedSignal.connect(callback));
}

NetObjectTypeID TileSet::netObjectTypeID()
{
	return CLASS_TILESET;
}

TileID TileSet::addTileType(const Vector2i& texRectPos, const Vector2i& texRectSize, const std::vector<Vector2f>& colVerts)
{
	assert(mTileTypes.size() <= TILE_ID_MAX);

	mTileTypes.push_back(TileType{ texRectPos, texRectSize});
	mTileTypes.back().setCollisionShape(colVerts);

	smChangedSignal.emit(this, false);
	setDirty(FLAG_TILE_TYPES_CHANGED);

	return (TileID) (mTileTypes.size() - 1);
}

void TileSet::setTexture(const std::shared_ptr<Texture>& tex)
{
	mTexture = tex;
	smChangedSignal.emit(this, false);
	setDirty(FLAG_TEXTURE_CHANGED);
}

void TileSet::setName(const std::string& name)
{
	mName = name;
	setDirty(FLAG_NAME_CHANGED);
}

void TileSet::packConstructorArgs(Client* client, BitStreamWriter& stream)
{
	// none
}

DirtyMask TileSet::packUpdate(Client* client, DirtyMask dirty, BitStreamWriter& stream)
{
	if (stream.writeBool(dirty & FLAG_NAME_CHANGED)) {
		stream.writeString(mName);
	}

	if (stream.writeBool(dirty & FLAG_TEXTURE_CHANGED)) {
		bool hasTexture = (mTexture != nullptr);
		stream.writeBool(hasTexture);
		if (hasTexture)
			mTexture->serialize(stream);
	}

	if (stream.writeBool(dirty & FLAG_TILE_TYPES_CHANGED)) {
		stream.writeUint16(mTileTypes.size());

		for (unsigned int i = 0; i < mTileTypes.size(); i++) {
			const auto& tt = mTileTypes.at(i);
			stream.writeVector2i(tt.textureRectPos);
			stream.writeVector2i(tt.textureRectSize);

			// collisionShape
			const auto& verts = tt.collisionShape[0].vertices();
			stream.writeUint16(verts.size());
			for (unsigned int v = 0; v < verts.size(); v++) {
				stream.writeVector2f(verts[v]);
			}
		}
	}

	return 0;
}

void TileSet::unpackUpdate(ServerConnection* conn, BitStreamReader& stream, unsigned int timestamp)
{
	if (stream.readBool()) {  // name changed
		stream.readString(mName);
	}

	if (stream.readBool()) {  // texChanged
		bool hasTexture = stream.readBool();
		if (hasTexture)
			mTexture = Texture::deserialize(stream);
		else
			mTexture.reset();
	}

	if (stream.readBool()) {  // tiles changed
		unsigned int n_types = stream.readUint16();

		mTileTypes.resize(n_types);
		for (unsigned int i = 0; i < mTileTypes.size(); i++) {
			auto& tt = mTileTypes.at(i);
			tt.textureRectPos = stream.readVector2i();
			tt.textureRectSize = stream.readVector2i();

			// collisionShape
			unsigned int n_verts = stream.readUint16();

			std::vector<Vector2f> verts(n_verts);
			for (unsigned int i = 0; i < verts.size(); i++) {
				verts[i] = stream.readVector2f();
			}
			tt.setCollisionShape(verts);
		}
	}

	smChangedSignal.emit(this, false);
}

void TileType::setCollisionShape(const std::vector<Vector2f>& baseVerts)
{
	// verts are defined from 0..TILE_SIZE, so first we need to center them
	// at 0, 0 so we can apply a rotation matrix to them
	std::vector<Vector2f> vertsCentered(baseVerts.size());
	for (unsigned int i = 0; i < vertsCentered.size(); i++) {
		vertsCentered[i] = baseVerts[i] - Vector2f(TileChunk::TILE_SIZE / 2.0f, TileChunk::TILE_SIZE / 2.0f);
	}

	// these are precomputed sin(theta) and cos(theta) pairs for 0, 90, 180, and 270 degree rotations
	static constexpr std::pair<float, float> rotations[8] = {
		{1.f, 0.f},   // 0 degrees
		{0.f, 1.f},   // 90 degrees
		{-1.f, 0.f},  // 180 degrees
		{0.f, -1.f},  // 270 degrees
	};

	// now fill in this->collisionShape with each possible rotation/mirror pair
	// we do this by applying a 2D rotation matrix to the centered-at-0 vertices
	// (since I haven't set up a real matrix class yet, this looks kind of dirty)
	// https://en.wikipedia.org/wiki/Rotation_matrix#Common_rotations
	std::vector<Vector2f> verts(vertsCentered.size());
	for (int shape = 0; shape < 8; shape++) {
		const int rotIdx = shape / 2;
		const bool mirror = (shape % 2) == 1;
		for (unsigned int i = 0; i < verts.size(); i++) {
			const float cosTheta = rotations[rotIdx].first;
			const float sinTheta = rotations[rotIdx].second;
			verts[i].x = vertsCentered[i].x * cosTheta - vertsCentered[i].y * sinTheta;
			verts[i].y = vertsCentered[i].x * sinTheta + vertsCentered[i].y * cosTheta;

			if (mirror)
				verts[i].x = -verts[i].x;  // mirror on the Y axis

			// un-center the vertex
			verts[i] += Vector2f(TileChunk::TILE_SIZE / 2.0f, TileChunk::TILE_SIZE / 2.0f);
		}

		// need to keep the verts counter-clockwise, so swap the order when miroring
		if (mirror)
			std::reverse(verts.begin(), verts.end());

		collisionShape[shape].setVertices(verts);
	}

	for (unsigned int i = 0; i < baseVerts.size(); i++) {
		assert(std::abs((collisionShape[0].vertices()[i] - baseVerts[i]).len()) < 1e-6);
	}
}