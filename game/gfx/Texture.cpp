#include "Texture.h"

#include <util/BitStream.h>

Texture::TextureManager Texture::sTextureManager;

void Texture::serialize(BitStreamWriter& stream) const
{
	stream.writeString(mKey.path);
	stream.writeBool(mKey.repeat);
	stream.writeBool(mKey.smooth);
}

std::shared_ptr<Texture> Texture::deserialize(BitStreamReader& stream)
{
	std::string path;
	stream.readString(path);
	bool repeat = stream.readBool();
	bool smooth = stream.readBool();

	return Texture::get(path, repeat, smooth);
}
