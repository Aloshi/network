#pragma once

#include <script/ScriptObject.h>
#include <gfx/Texture.h>
#include <math/Box2.h>
#include <SFML/Graphics/RectangleShape.hpp>
#include <vector>
#include <gfx/SpriteSheet.h>

SCRIPT_CLASS("Sprite", Sprite, void, "Holds information about how to render an image.");
class Sprite : public ScriptObject {
public:
	Sprite();

	SCRIPT_PROPERTY(Sprite, "texture", &Sprite::getTexture, &Sprite::setTexture, "Texture to render. Setting this resets textureRect.");
	void setTexture(const std::shared_ptr<Texture>& tex);
	inline const std::shared_ptr<Texture>& getTexture() { return mTexture; }

	SCRIPT_METHOD(Sprite, "setTextureRect", static_cast<void(Sprite::*)(int,int,int,int)>(&Sprite::setTextureRect), "Set the texture subrect to use (in pixels).");
	void setTextureRect(int x, int y, int w, int h);

	SCRIPT_PROPERTY(Sprite, "textureRect", &Sprite::getTextureRect, static_cast<void(Sprite::*)(const Box2i&)>(&Sprite::setTextureRect), "The texture subrect coordinates.");
	inline const Box2i& getTextureRect() const { return mTexRect; }
	inline void setTextureRect(const Box2i& rect) {
		setTextureRect(rect.min.x, rect.min.y, rect.width(), rect.height());
	}

	SCRIPT_METHOD(Sprite, "setSpriteSheet", &Sprite::setSpriteSheet, "Set a sprite sheet. This overrides the current texture settings and stops any animations.");
	void setSpriteSheet(const std::shared_ptr<SpriteSheet>& ss);

	SCRIPT_METHOD(Sprite, "playAnimation", &Sprite::playAnimation, "Begin playing an animation from the current spritesheet. The animation will restart if it is already being played.");
	void playAnimation(const std::string& name);

	SCRIPT_METHOD(Sprite, "animationName", &Sprite::getAnimationName, "Returns the name of the currently playing animation, or 'invalid' if nothing is playing.")
	inline const std::string& getAnimationName() const { return mAnimation.animationName(); }

	SCRIPT_PROPERTY(Sprite, "offset", &Sprite::getOffset, &Sprite::setOffset, "Offset.");
	void setOffset(const Vector2f& off);
	inline const Vector2f& getOffset() const { return mOffset; }

	SCRIPT_PROPERTY(Sprite, "size", &Sprite::getSize, &Sprite::setSize, "Size. Defaults to texture size.");
	void setSize(const Vector2f& size);
	inline const Vector2f& getSize() const { return mSize; }

	SCRIPT_METHOD(Sprite, "setColorshift", &Sprite::setColorshift, "Colorshift by an RGBA value.");
	void setColorshift(unsigned char r, unsigned char g, unsigned char b, unsigned char a);
	inline const sf::Color& getColorshift() const { return mShape.getFillColor(); }

	SCRIPT_PROPERTY(Sprite, "mirrorHorizontal", &Sprite::getMirrorHorizontal, &Sprite::setMirrorHorizontal,
		"Set to true to mirror the sprite vertically (e.g. a right-facing sprite now faces left).");
	inline bool getMirrorHorizontal() const { return mMirrorHorizontal; }
	void setMirrorHorizontal(bool flip);

	void update(double dt);
	void draw(sf::RenderTarget& target);

protected:
	void autoResize();
	void applyAnimation();  // apply mAnimation to mShape
	sf::IntRect mirroredTexCoords(sf::IntRect r) const;

	Vector2f mOffset;
	Vector2f mSize;
	bool mMirrorHorizontal;
	std::shared_ptr<Texture> mTexture;
	Box2<int> mTexRect;

	std::shared_ptr<SpriteSheet> mSpriteSheet;
	AnimationRef mAnimation;

	sf::RectangleShape mShape;
	Vector2<bool> mAutoResize;
};

SCRIPT_CLASS("ScreenSprite", ScreenSprite, Sprite, "A sprite that draws in screen-space.");
class ScreenSprite : public Sprite {
public:
	SCRIPT_CONSTRUCTOR(ScreenSprite, "ScreenSprite");
	ScreenSprite();
	virtual ~ScreenSprite();

	SCRIPT_METHOD(ScreenSprite, "show", &ScreenSprite::show, "Enable rendering the sprite.");
	void show();

	SCRIPT_METHOD(ScreenSprite, "hide", &ScreenSprite::hide, "Disable rendering the sprite.");
	void hide();

	static void updateAll(double dt);
	static void drawAll(sf::RenderTarget& target);

protected:
	static std::vector<ScreenSprite*> gSprites;
};
