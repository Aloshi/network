#pragma once

#include <script/ScriptObject.h>
#include <util/ResourceManager.h>

class AnimationRef;
class Texture;

SCRIPT_CLASS("SpriteSheet", SpriteSheet, void, "A spritesheet, used to animate sprites.");
class SpriteSheet : public ScriptObject {
public:
	SCRIPT_FUNCTION("getSpriteSheet", &SpriteSheet::get, "Get a shared_ptr to a spritesheet. Dynamic properties are not persistent!");
	static std::shared_ptr<SpriteSheet> get(const std::string& path) {
		return sManager.get(path);
	}

	bool reload();

	inline const std::shared_ptr<Texture>& texture() const {
		return mTexture;
	}

protected:
	friend AnimationRef;
	struct Frame {
		Box2i textureRect;
		double time;

		Frame() : time(0) {}
		Frame(int x, int y, int w, int h, double t)
			: textureRect(x, y, x+w, y+h), time(t) {}
	};
	struct Animation {
		std::vector<Frame> frames;
	};

	SpriteSheet() {}
	SpriteSheet(const SpriteSheet&) = delete;
	SpriteSheet& operator=(const SpriteSheet& rhs) = delete;

	typedef std::string SpriteSheetKey;
	static bool loadSpriteSheet(const SpriteSheetKey& key, std::map<std::string, Animation>* animations_out, std::shared_ptr<Texture>* texture_out);

	static duk_idx_t parse_sprite_sheet(duk_context* ctx, void* udata);
	struct ParserData {
		std::shared_ptr<Texture>* texture_out;
		std::map<std::string, SpriteSheet::Animation>* animations;
	};

	class SpriteSheetManager : public ResourceManager<SpriteSheetKey, SpriteSheet> {
	protected:
		inline std::shared_ptr<SpriteSheet> acquire(const SpriteSheetKey& key) override {
			auto ss = std::shared_ptr<SpriteSheet>(new SpriteSheet());
			ss->mKey = key;
			if (!loadSpriteSheet(key, &ss->mEntries, &ss->mTexture))
				throw std::runtime_error("Could not load spritesheet.");

			return ss;
		}
	};

	static SpriteSheetManager sManager;

	std::shared_ptr<Texture> mTexture;
	std::map<std::string, Animation> mEntries;

	// for reloading
	SpriteSheetKey mKey;
	std::vector<AnimationRef*> mAnimRefs;
};

class AnimationRef {
public:
	AnimationRef();
	AnimationRef(const std::shared_ptr<SpriteSheet>& ss, const std::string& animName);
	~AnimationRef();

	inline const std::string& animationName() const {
		if (!valid())
			return InvalidAnimationName;

		return mAnimation->first;
	}
	inline const std::shared_ptr<Texture>& texture() const {
		if (!valid())
			return InvalidTexture;

		return mSheet->mTexture;
	}
	inline const Box2i& textureRect() const {
		if (!valid())
			return InvalidTextureCoords;

		return mAnimation->second.frames.at(mFrameIndex).textureRect;
	}

	inline bool valid() const {
		return mSheet && (mAnimation != mSheet->mEntries.end());
	}

	inline bool advance(double dt) {
		if (!valid() || mAnimation->second.frames.size() <= 1)
			return false;

		mTime += dt * 1000;  // milliseconds

		bool changed = false;
		while (mTime >= mAnimation->second.frames.at(mFrameIndex).time) {
			mTime -= mAnimation->second.frames.at(mFrameIndex).time;
			mFrameIndex = (mFrameIndex + 1) % mAnimation->second.frames.size();
			changed = true;
		}

		return changed;
	}

	inline void restart() {
		mTime = 0;
		mFrameIndex = 0;
	}

	void clear();
	void reset(const std::shared_ptr<SpriteSheet>& ss, const std::string& animName);

protected:
	static std::string InvalidAnimationName;
	static std::shared_ptr<Texture> InvalidTexture;
	static Box2i InvalidTextureCoords;

	friend SpriteSheet;

	std::shared_ptr<SpriteSheet> mSheet;
	std::map<std::string, SpriteSheet::Animation>::const_iterator mAnimation;
	unsigned int mFrameIndex;
	double mTime;
};
