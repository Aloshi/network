#include "SpriteSheet.h"

#include <gfx/Texture.h>

SpriteSheet::SpriteSheetManager SpriteSheet::sManager;

std::string AnimationRef::InvalidAnimationName = "invalid";
Box2i AnimationRef::InvalidTextureCoords;
std::shared_ptr<Texture> AnimationRef::InvalidTexture;

AnimationRef::AnimationRef()
	: mFrameIndex(0), mTime(0)
{}

AnimationRef::AnimationRef(const std::shared_ptr<SpriteSheet>& ss, const std::string& animName)
	: AnimationRef()
{
	reset(ss, animName);
}

AnimationRef::~AnimationRef()
{
	clear();
}

void AnimationRef::clear()
{
	if (mSheet) {
		for (unsigned int i = 0; i < mSheet->mAnimRefs.size(); i++) {
			if (mSheet->mAnimRefs[i] == this) {
				std::swap(mSheet->mAnimRefs[i], mSheet->mAnimRefs.back());
				mSheet->mAnimRefs.pop_back();
				break;
			}
		}
		mSheet = nullptr;
	}
}

void AnimationRef::reset(const std::shared_ptr<SpriteSheet>& ss, const std::string & animName)
{
	if (ss != mSheet) {
		clear();
		mSheet = ss;

		if (ss)
			ss->mAnimRefs.push_back(this);
	}

	if (ss) {
		mAnimation = ss->mEntries.find(animName);
	}

	restart();
}



bool SpriteSheet::reload()
{
	std::shared_ptr<Texture> newTexture;
	std::map<std::string, Animation> newAnimations;
	bool ok = loadSpriteSheet(mKey, &newAnimations, &newTexture);
	if (!ok)
		return false;

	auto it = mAnimRefs.begin();
	while (it != mAnimRefs.end()) {
		AnimationRef* ptr = *it;
		ptr->mAnimation = newAnimations.find(ptr->mAnimation->first);
		ptr->restart();
		it++;
	}

	mTexture = std::move(newTexture);
	mEntries = std::move(newAnimations);
	return true;
}

duk_idx_t SpriteSheet::parse_sprite_sheet(duk_context* ctx, void* udata)
{
	ParserData* out = (ParserData*) udata;

	duk_get_prop_string(ctx, -1, "path");
	std::string texturePath = duk_require_string(ctx, -1);
	duk_pop(ctx);

	try {
		*out->texture_out = Texture::get(texturePath, false, false);
	} catch (std::exception& e) {
		return duk_error(ctx, DUK_ERR_URI_ERROR, "Unable to load spritesheet texture '%s': %s", texturePath.c_str(), e.what());
	}

	// tile size
	duk_get_prop_string(ctx, -1, "nColumns");
	int nCols = duk_require_int(ctx, -1);
	duk_pop(ctx);

	duk_get_prop_string(ctx, -1, "tileWidth");
	int tileWidth = duk_require_int(ctx, -1);
	duk_pop(ctx);

	duk_get_prop_string(ctx, -1, "tileHeight");
	int tileHeight = duk_require_int(ctx, -1);
	duk_pop(ctx);

	duk_get_prop_string(ctx, -1, "animations");
	if (duk_is_array(ctx, -1)) {
		duk_size_t n = duk_get_length(ctx, -1);
		for (duk_size_t animIdx = 0; animIdx < n; animIdx++) {
			duk_get_prop_index(ctx, -1, animIdx);

			// gather data needed for animation

			duk_get_prop_string(ctx, -1, "name");
			std::string animName = duk_require_string(ctx, -1);
			duk_pop(ctx);

			duk_get_prop_string(ctx, -1, "startX");
			duk_idx_t startX = duk_require_int(ctx, -1);
			duk_pop(ctx);

			duk_get_prop_string(ctx, -1, "startY");
			duk_idx_t startY = duk_require_int(ctx, -1);
			duk_pop(ctx);

			duk_get_prop_string(ctx, -1, "nFrames");
			unsigned int nFrames = duk_require_uint(ctx, -1);
			duk_pop(ctx);

			std::vector<double> frameTimes(nFrames, 100);  // default to 100ms between frames
			if (duk_get_prop_string(ctx, -1, "frameTime")) {
				double frameTimeMS = duk_require_number(ctx, -1);
				frameTimes.resize(nFrames, frameTimeMS);
			}
			duk_pop(ctx);

			if (duk_get_prop_string(ctx, -1, "frameTimes")) {
				if (duk_get_length(ctx, -1) != nFrames)
					return duk_error(ctx, DUK_ERR_ERROR, "Animation %s: frameTimes length does not match nFrames", animName.c_str());

				frameTimes.resize(nFrames);
				for (duk_size_t i = 0; i < nFrames; i++) {
					duk_get_prop_index(ctx, -1, animIdx);
					frameTimes[i] = duk_require_number(ctx, -1);
					duk_pop(ctx);  // pop time
				}
			}
			duk_pop(ctx);  // pop frameTimes

			duk_pop(ctx);  // pop animation element

			// finally add the animation
			Animation anim;
			for (unsigned int i = 0; i < nFrames; i++) {
				int x = (startX + i) % nCols;
				int y = startY + (startX + i) / nCols;
				anim.frames.push_back(Frame(x * tileWidth, y * tileHeight, tileWidth, tileHeight, frameTimes[i]));
			}
			(*out->animations)[animName] = anim;
		}
	}
	duk_pop(ctx);  // pop animations array

	return 0;
}

bool SpriteSheet::loadSpriteSheet(const std::string& path, std::map<std::string, Animation>* animations_out, std::shared_ptr<Texture>* texture_out)
{
	animations_out->clear();
	texture_out->reset();

	duk_context* ctx = ScriptEngine::get();

	// first, evaluate the spritesheet as a javascript file and store the result on the heap
	duk_get_global_string(ctx, "require_fresh");
	duk_push_string(ctx, path.c_str());
	int rc = duk_pcall(ctx, 1);
	if (rc != DUK_EXEC_SUCCESS) {
		duk_get_prop_string(ctx, -1, "stack");
		LOG(LogError) << "Error evaluating spritesheet:";
		LOG(LogError) << duk_safe_to_string(ctx, -1);
		duk_pop_2(ctx);  // pop stack & error
		return false;
	}

	// next, call parse_sprite_sheet on it to fill *_out
	ParserData udata{ texture_out, animations_out };
	rc = duk_safe_call(ctx, &SpriteSheet::parse_sprite_sheet, &udata, 1, 1);
	if (rc != DUK_EXEC_SUCCESS) {
		duk_get_prop_string(ctx, -1, "stack");
		LOG(LogError) << "Error in spritesheet data:";
		LOG(LogError) << duk_safe_to_string(ctx, -1);
		duk_pop_2(ctx);  // pop stack & error
		return false;
	}
	duk_pop(ctx);  // pop result (undefined)

	return true;
}
