#include "Sprite.h"
#include <SFML/Graphics/RenderTarget.hpp>

Sprite::Sprite()
	: mAutoResize(true, true), mMirrorHorizontal(false)
{
}

void Sprite::setTexture(const std::shared_ptr<Texture>& tex)
{
	mTexture = tex;

	if (tex) {
		mShape.setTexture(&mTexture->data());
		setTextureRect(0, 0, mTexture->width(), mTexture->height());  // ensure mMirror is applied
	} else {
		mShape.setTexture(nullptr);
	}

	autoResize();
}

sf::IntRect Sprite::mirroredTexCoords(sf::IntRect rect) const {
	if (mMirrorHorizontal) {
		rect.left += rect.width;
		rect.width = -rect.width;
	}

	return rect;
}

void Sprite::setTextureRect(int x, int y, int w, int h)
{
	mTexRect = Box2<int>(x, y, x+w, y+h);
	mShape.setTextureRect(mirroredTexCoords(sf::IntRect(x, y, w, h)));
	autoResize();
}

void Sprite::setSpriteSheet(const std::shared_ptr<SpriteSheet>& ss)
{
	mSpriteSheet = ss;
}

void Sprite::playAnimation(const std::string & name)
{
	mAnimation.reset(mSpriteSheet, name);
	applyAnimation();
	autoResize();
}

void Sprite::setOffset(const Vector2f& offset)
{
	mOffset = offset;
	mShape.setPosition(offset.x, offset.y);
}

void Sprite::setSize(const Vector2f& size)
{
	mSize = size;
	mAutoResize.x = (size.x <= 0);
	mAutoResize.y = (size.y <= 0);
	autoResize();  // calls mShape.setSize(mSize)
}

// this is based on some EmulationStation code
// https://github.com/Aloshi/EmulationStation/blob/video/es-core/src/components/MediaComponent.cpp
// thanks past self
void Sprite::autoResize()
{
	if (!mTexture)
		return;

	sf::Vector2u texSize(mTexRect.max.x - mTexRect.min.x, mTexRect.max.y - mTexRect.min.y);
	//sf::Vector2u texSize = mTexture->data().getSize();

	// this was originally written with vector graphics in mind
	// if rounding is off enough in the rasterization step (for images with extreme aspect ratios), it can cause cutoff when the aspect ratio breaks
	// so, we always make sure the resultant height is an integer to make sure cutoff doesn't happen, and scale width from that

	if (mAutoResize.x && mAutoResize.y) {
		// if no components are set, we just copy texture size
		mSize.x = (float) texSize.x;
		mSize.y = (float) texSize.y;
	} else if (!mAutoResize.x && !mAutoResize.y) {
		// if both components are set, we just use the given size
	} else if (mAutoResize.x && !mAutoResize.y) {
		// just x is set
		mSize.y = round((mSize.x / texSize.x) * texSize.y);
		mSize.x = (mSize.y / texSize.y) * texSize.x;
	} else if (!mAutoResize.x && mAutoResize.y) {
		// just y is set
		mSize.y = round(mSize.y);
		mSize.x = (mSize.y / texSize.y) * texSize.x;
	}

	mShape.setSize(mSize);
}

void Sprite::applyAnimation()
{
	mTexture = mAnimation.texture();
	mTexRect = mAnimation.textureRect();

	if (&mTexture->data() != mShape.getTexture())
		mShape.setTexture(&mTexture->data());

	sf::IntRect rect(mTexRect.min.x, mTexRect.min.y, mTexRect.max.x - mTexRect.min.x, mTexRect.max.y - mTexRect.min.y);
	mShape.setTextureRect(mirroredTexCoords(rect));
}

void Sprite::setMirrorHorizontal(bool flip)
{
	mMirrorHorizontal = flip;

	// update tex coords
	sf::IntRect rect(mTexRect.min.x, mTexRect.min.y, mTexRect.max.x - mTexRect.min.x, mTexRect.max.y - mTexRect.min.y);
	mShape.setTextureRect(mirroredTexCoords(rect));
}

void Sprite::update(double dt)
{
	if (mAnimation.advance(dt)) {
		applyAnimation();
	}
}

void Sprite::draw(sf::RenderTarget& target)
{
	target.draw(mShape);
}

void Sprite::setColorshift(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
	mShape.setFillColor(sf::Color(r, g, b, a));
}


// ScreenSprite
std::vector<ScreenSprite*> ScreenSprite::gSprites;

ScreenSprite::ScreenSprite()
{
	
}

ScreenSprite::~ScreenSprite()
{
	hide();
}

void ScreenSprite::show()
{
	auto it = std::find(gSprites.begin(), gSprites.end(), this);

	// already visible, remove from list so we can add it to the front (draw order)
	if (it != gSprites.end())
		gSprites.erase(it);
		
	gSprites.push_back(this);
}

void ScreenSprite::hide()
{
	auto it = std::find(gSprites.begin(), gSprites.end(), this);
	if (it != gSprites.end())
		gSprites.erase(it);
}

void ScreenSprite::updateAll(double dt)
{
	for (unsigned int i = 0; i < gSprites.size(); i++) {
		gSprites.at(i)->update(dt);
	}
}

void ScreenSprite::drawAll(sf::RenderTarget& target)
{
	for (unsigned int i = 0; i < gSprites.size(); i++) {
		gSprites.at(i)->draw(target);
	}
}
