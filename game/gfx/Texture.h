#pragma once

#include <string>
#include <util/ResourceManager.h>
#include <SFML/Graphics/Texture.hpp>
#include <script/ScriptEngine.h>

class BitStreamReader;
class BitStreamWriter;

struct TextureKey {
	std::string path;
	bool repeat;
	bool smooth;

	inline bool operator<(const TextureKey& rhs) const {
		return std::tie(path, repeat, smooth) < std::tie(rhs.path, rhs.repeat, rhs.smooth);
	}
};

class Texture
{
public:
	SCRIPT_FUNCTION("getTexture", &Texture::get, "Get a shared_ptr texture. Dynamic properties are not persistent!");
	static std::shared_ptr<Texture> get(std::string path, bool repeat, bool smooth) {
		return sTextureManager.get(TextureKey{ path, repeat, smooth });
	}

	SCRIPT_PROPERTY(Texture, "path", &Texture::path, nullptr, "Get the path to this texture.");
	inline const std::string& path() const { return mKey.path; }
	SCRIPT_PROPERTY(Texture, "width", &Texture::width, nullptr, "Get the width of this texture in pixels.");
	inline unsigned int width() const { return mData.getSize().x; }
	SCRIPT_PROPERTY(Texture, "height", &Texture::height, nullptr, "Get the height of this texture in pixels.");
	inline unsigned int height() const { return mData.getSize().y; }

	inline sf::Texture& data() { return mData; }

	void serialize(BitStreamWriter& stream) const;
	static std::shared_ptr<Texture> deserialize(BitStreamReader& stream);

protected:
	Texture() {};
	class TextureManager : public ResourceManager<TextureKey, Texture> {
	protected:
		inline std::shared_ptr<Texture> acquire(const TextureKey& key) override {
			auto tex = std::shared_ptr<Texture>(new Texture());
			tex->mKey = key;

			if (!tex->mData.loadFromFile(key.path))
				throw std::runtime_error("Could not load texture.");
			tex->mData.setRepeated(key.repeat);
			tex->mData.setSmooth(key.smooth);

			return tex;
		}
	};
	static TextureManager sTextureManager;

	TextureKey mKey;
	sf::Texture mData;
};