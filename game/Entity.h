#pragma once

#include <map>

#include <scene/SceneObject.h>

class Component;

SCRIPT_CLASS("Entity", Entity, SceneObject, "A base entity class that can have components added to it.");
class Entity : public SceneObject {
public:
	SCRIPT_CONSTRUCTOR(Entity, "Entity", SceneContainer*);
	Entity(SceneContainer* scene);
	virtual ~Entity();

	SCRIPT_METHOD(Entity, "component", &Entity::component, "Access a component by name.");
	Component* component(const std::string& name);

	// overrides for SceneObject
	// networking
	virtual DirtyMask packUpdate(Client* client, DirtyMask remainingDirtyMask, BitStreamWriter& stream) override;
	virtual void unpackUpdate(ServerConnection* conn, BitStreamReader& stream, unsigned int timestamp) override;
	virtual NetObjectTypeID netObjectTypeID() override;

	// collision
	virtual Box2f worldBox() const;
	virtual void buildCollisionList(ShapeList* list, const Box2f& worldRegion);

	virtual void tick() override;
	virtual void render(sf::RenderTarget& target, float lerp) override;

protected:
	static const unsigned int MAX_COMPONENTS = UINT8_MAX;  // components_.size() must be less than this value
	enum EntityFlags : DirtyMask {
		FLAG_COMPONENTS_CHANGED = SceneObject::NEXT_FREE_FLAG,
		NEXT_FREE_FLAG = (FLAG_COMPONENTS_CHANGED << 1),
	};

private:
	friend class Component;

	void addComponent(Component* e);
	void removeComponent(Component* e);

	std::vector<Component*> mComponents;
	std::vector<Component*> mToWake;

	bool mWorldBoxDirty;
	Box2f mWorldBox;
};

SCRIPT_CLASS("Component", Component, NetObject, "Base class for components that are added to Entities.");
class Component : public NetObject {
public:
	Component(Entity* e);
	virtual ~Component();

	SCRIPT_PROPERTY(Component, "entity", static_cast<Entity* (Component::*)()>(&Component::entity), nullptr, "A reference to the entity this component is attached to.")
	inline Entity* entity() {
		return mEntity;
	}
	inline const Entity* entity() const {
		return mEntity;
	}

	inline const Transform& transform() const {
		return entity()->transform();
	}
	inline const Vector2f& position() const {
		return entity()->position();
	}
	inline SceneContainer* container() const {
		return entity()->container();
	}

	SCRIPT_METHOD(Component, "componentName", &Component::componentName, "Get the name of this component.");
	virtual const char* componentName() const = 0;

	// NetObject networking
	virtual void packConstructorArgs(Client* client, BitStreamWriter& stream) override;
	virtual DirtyMask packUpdate(Client* client, DirtyMask dirty, BitStreamWriter& stream) override final;
	virtual void unpackUpdate(ServerConnection* conn, BitStreamReader& stream, unsigned int timestamp) override final;
	virtual NetObjectTypeID netObjectTypeID() = 0;

	// collision system
	virtual Box2f worldBox() const;
	virtual void buildCollisionList(ShapeList* list, const Box2f& worldRegion) {}

	inline bool isAwake() const {
		return mAwake;
	}

protected:
	friend class Entity;
	void wakeUp();  // called after entity is set
	void deactivate();  // called just before entity is invalidated

	// Component networking
	virtual DirtyMask packComponentUpdate(Client* client, DirtyMask dirty, BitStreamWriter& stream) = 0;
	virtual void unpackComponentUpdate(ServerConnection* conn, BitStreamReader& stream) = 0;

	// Component callbacks
	virtual void onAwake() {}  // called on the first tick this object has its Entity available on, may be again after onDeactivated() is called
	virtual void onDeactivated() {}  // called when an object's Entity is about to be invalidated
	virtual void onTick() {}
	virtual void onRender(sf::RenderTarget& target, float lerp) {}

	void notifyWorldBoxChanged();

	enum ComponentFlags {
		NEXT_FREE_FLAG = (1 << 0),
	};

private:
	Entity* mEntity;
	bool mAwake;
};