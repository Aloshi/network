#include <Pawn.h>
#include <server/Client.h>
#include <client/ServerConnection.h>

Pawn::~Pawn() {
  if (mClient)
    mClient->setControlObject(NULL);
}

bool Pawn::isLocalPlayer() const
{
	return !serverConnection() || (serverConnection()->controlObject() == this);
}
