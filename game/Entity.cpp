#include "Entity.h"
#include <net/PacketMessageTypes.h>
#include <client/ServerConnection.h>
#include <server/Client.h>
#include <Sim.h>
#include <util/BitStream.h>

// ----- Entity

Entity::Entity(SceneContainer* scene)
	: SceneObject(scene)
{
}

Entity::~Entity()
{
	while (!mComponents.empty()) {
		if (isServer()) {
			delete mComponents.back();
		} else {
			removeComponent(mComponents.back());
		}
	}
}

void Entity::addComponent(Component* c)
{
	assert(std::find(mComponents.begin(), mComponents.end(), c) == mComponents.end());
	assert(c->mEntity == NULL);
	assert(mComponents.size() + 1 < MAX_COMPONENTS);
	mComponents.push_back(c);
	c->mEntity = this;
	mToWake.push_back(c);
}

void Entity::removeComponent(Component* c)
{
	assert(c != NULL);
	assert(c->mEntity == this);

	for (auto it = mComponents.begin(); it != mComponents.end(); it++) {
		if (*it == c) {
			c->deactivate();
			mComponents.erase(it);
			c->mEntity = NULL;
			return;
		}
	}

	LOG(LogWarning) << "could not remove component " << c << " from entity (not added)";
	assert(false);
}

Component* Entity::component(const std::string& name)
{
	for (auto it = mComponents.begin(); it != mComponents.end(); it++) {
		if ((*it)->componentName() == name)
			return *it;
	}

	return NULL;
}

DirtyMask Entity::packUpdate(Client* client, DirtyMask dirty, BitStreamWriter& stream)
{
	DirtyMask remainingMask = 0;

	if (stream.writeBool(dirty & FLAG_POSITION)) {
		stream.writeVector2f(position());
	}

	// component management
	// next bit is whether or not the component list is included in this update
	// if the components changed flag isn't set, this is automatically false
	if ((dirty & FLAG_COMPONENTS_CHANGED) != 0) {
		// it's set, but we can only the updated component list if all the
		// components have been fully ghosted to this client
		// check if all components have been ghosted to the client yet,
		// attaching them on the way if they haven't been attached
		bool allReady = true;
		for (unsigned int i = 0; i < mComponents.size(); i++) {
			Component* component = mComponents.at(i);
			assert(component != NULL);

			GhostID id = client->findGhostID(mComponents.at(i));
			bool ready = (id != 0) && client->getGhostInfo(id)->validOnBoth();
			allReady = allReady && ready;
			if (id == 0) {
				client->attachGhost(component);
			}
		}

		stream.writeBool(allReady);
		if (allReady) {
			// they were all ghosted, we can include the list
			// write list of component ghost IDs
			assert(mComponents.size() < MAX_COMPONENTS);
			int32_t n_components = (int32_t) mComponents.size();
			stream.writeInt(n_components, 0, MAX_COMPONENTS - 1);
			for (int32_t i = 0; i < n_components; i++) {
				GhostID id = client->findGhostID(mComponents.at(i));
				assert(id != 0);
				stream.writeGhostID(id);
			}
		} else {
			// can't write yet, try again next update
			remainingMask |= FLAG_COMPONENTS_CHANGED;
		}
	} else {
		stream.writeBool(false);
	}

	return remainingMask;
}

void Entity::unpackUpdate(ServerConnection* conn, BitStreamReader& stream, unsigned int timestamp)
{
	if (stream.readBool()) {  // FLAG_POSITION
		setPosition(stream.readVector2f());
	}

	if (stream.readBool()) {  // FLAG_COMPONENTS_CHANGED
		int32_t n_components;
		stream.readInt(n_components, 0, MAX_COMPONENTS - 1);

		// do a delta update to our components list (components that match are unaffected, missing are removed, new are added)
		std::vector<bool> keep(mComponents.size(), false);
		std::vector<Component*> add;
		for (int32_t i = 0; i < n_components; i++) {
			GhostID id = stream.readGhostID();

			NetObject* componentNetObj = conn->resolveGhostID(id);
			Component* component = dynamic_cast<Component*>(componentNetObj);
			assert(component != NULL);

			bool found = false;
			for (unsigned int j = 0; j < mComponents.size(); j++) {
				if (mComponents.at(j) == component) {
					keep.at(j) = true;
					found = true;
					break;
				}
			}
			if (!found)
				add.push_back(component);
		}

		// erase components that don't have "keep" set
		unsigned int off = 0;  // offset for loop as we remove
		for (unsigned int i = 0; i < keep.size(); i++) {
			if (keep.at(i))
				continue;

			// this should match removeComponent(), it's copied here to
			// avoid re-iterating the vector since we know the exact index
			auto it = mComponents.begin() + (i - off);
			(*it)->deactivate();
			(*it)->mEntity = NULL;
			mComponents.erase(it);
			off++;
		}

		// add new components
		for (unsigned int i = 0; i < add.size(); i++) {
			addComponent(add.at(i));
		}

		LOG(LogDebug) << "Removed " << off << " components, added " << add.size() << " to entity";
	}
}

NetObjectTypeID Entity::netObjectTypeID()
{
	return CLASS_ENTITY;
}

Box2f Entity::worldBox() const
{
	if (mWorldBoxDirty) {
		Box2f box = Box2f(transform().position, transform().position);
		for (unsigned int i = 0; i < mComponents.size(); i++) {
			Box2f::merge(box, mComponents[i]->worldBox());
		}

		const_cast<Entity*>(this)->mWorldBox = box;
		const_cast<Entity*>(this)->mWorldBoxDirty = false;
	}

	return mWorldBox;
}

void Entity::buildCollisionList(ShapeList* list, const Box2f& worldRegion)
{
	for (unsigned int i = 0; i < mComponents.size(); i++) {
		mComponents.at(i)->buildCollisionList(list, worldRegion);
	}
}

void Entity::tick()
{
	if (!mToWake.empty()) {
		for (unsigned int i = 0; i < mToWake.size(); i++) {
			mToWake.at(i)->wakeUp();
		}
		mToWake.clear();
	}

	for (unsigned int i = 0; i < mComponents.size(); i++) {
		mComponents.at(i)->onTick();
	}
}

void Entity::render(sf::RenderTarget& target, float lerp)
{
	for (unsigned int i = 0; i < mComponents.size(); i++) {
		mComponents.at(i)->onRender(target, lerp);
	}
}

// ----- Component

Component::Component(Entity* entity)
	: mEntity(NULL), mAwake(false)
{
	if (isClient() && entity == NULL)
		return;

	if (isServer() && entity == NULL)
		throw std::runtime_error("Cannot create component for null entity.");

	entity->addComponent(this);
}

Component::~Component()
{
	if (mEntity)
		mEntity->removeComponent(this);
}

void Component::packConstructorArgs(Client* client, BitStreamWriter& stream)
{
}

DirtyMask Component::packUpdate(Client* client, DirtyMask dirty, BitStreamWriter& stream)
{
	return packComponentUpdate(client, dirty, stream);
}

void Component::unpackUpdate(ServerConnection* conn, BitStreamReader& stream, unsigned int timestamp)
{
	unpackComponentUpdate(conn, stream);
}

Box2f Component::worldBox() const
{
	return Box2f(transform().position, transform().position);
}

void Component::wakeUp()
{
	mAwake = true;
	onAwake();
}

void Component::deactivate()
{
	mAwake = false;
	onDeactivated();
}

void Component::notifyWorldBoxChanged()
{
	entity()->mWorldBoxDirty = true;
}
