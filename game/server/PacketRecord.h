#pragma once

#include <net/GhostInfo.h>
#include <net/PacketMessageTypes.h>

#include <vector>

class Client;

class PacketRecorder;

class PacketRecord
{
public:
	PacketRecord(PacketID id) : packetID(id) {}
	virtual ~PacketRecord() {}

	PacketID packetID;

	virtual void onAck(Client* client, PacketRecorder* recorder) = 0;
	virtual void onNack(Client* client, PacketRecorder* recorder) = 0;

	bool operator==(const PacketRecord& rhs) const { return packetID == rhs.packetID; }
	bool operator<(const PacketRecord& rhs) const { return packetID < rhs.packetID; }
};

class GhostCreatePacketRecord : public PacketRecord
{
public:
	GhostCreatePacketRecord(PacketID id, const std::vector<GhostID>& ghostIDs) : PacketRecord(id), newGhostIDs(ghostIDs) {}

	virtual void onAck(Client* client, PacketRecorder* recorder) override;
	virtual void onNack(Client* client, PacketRecorder* recorder) override;

private:
	std::vector<GhostID> newGhostIDs;
};

class GhostDestroyPacketRecord : public PacketRecord
{
public:
	GhostDestroyPacketRecord(PacketID id, const std::vector<GhostID>& ghostIDs) : PacketRecord(id), destroyGhostIDs(ghostIDs) {}

	virtual void onAck(Client* client, PacketRecorder* recorder) override;
	virtual void onNack(Client* client, PacketRecorder* recorder) override;
private:
	std::vector<GhostID> destroyGhostIDs;
};

class GhostUpdatePacketRecord : public PacketRecord
{
public:
	GhostUpdatePacketRecord(PacketID id, const std::vector< std::pair<GhostID, DirtyMask> >& updates) : PacketRecord(id), ghostUpdates(updates) {}

	virtual void onAck(Client* client, PacketRecorder* recorder) override;
	virtual void onNack(Client* client, PacketRecorder* recorder) override;
private:
	std::vector< std::pair<GhostID, DirtyMask> > ghostUpdates;
};

class PacketRecorder
{
public:
	PacketRecorder(const PacketRecorder& rhs) = delete;
	PacketRecorder operator=(const PacketRecorder& rhs) = delete;

	PacketRecorder() : mNextPacketID(0) {}
	virtual ~PacketRecorder();

	PacketID addGhostCreatePacket(const std::vector<GhostID>& ghostIDs);
	PacketID addGhostUpdatePacket(const std::vector< std::pair<GhostID, DirtyMask> >& update);
	PacketID addGhostDestroyPacket(const std::vector<GhostID>& ghostIDs);

	void receivedAck(Client* client, PacketID id);

	std::vector<PacketRecord*>::iterator nextPackets(PacketID packetID);
	inline std::vector<PacketRecord*>::iterator packetsEnd() { return mPackets.end(); }

private:
	std::vector<PacketRecord*> mPackets; // TODO change this to std::unique_ptr
	PacketID mNextPacketID;
};
