#include "Client.h"

#include <Sim.h>
#include <util/Log.h>
#include <net/PacketMessageTypes.h>
#include <scene/SceneContainer.h>
#include <util/BitStream.h>

#include <tuple>
#include <assert.h>

Client::Client(const ClientGUID& guid, int clientIndex)
	: mGUID(guid),
	mClientIndex(clientIndex),
	mControlObject(NULL),
	mSendControlObjectUpdate(true),
	mNetObjRemoveConn(NetObject::registerRemovedCallback(std::bind(&Client::detachGhost, this, std::placeholders::_1))),
	mGlobalnetObjAddConn(GlobalNetObject::registerAddedCallback(std::bind(&Client::attachGhost, this, std::placeholders::_1)))
{
	mLastInputNumberClient = 0;

	// initialize our ghosts array to "no ghosts"
	mGhosts.resize(MAX_GHOSTS);

	// add global net objects
	const auto& globals = GlobalNetObject::all();
	for (unsigned int i = 0; i < globals.size(); i++) {
		attachGhost(globals.at(i));
	}
}

Client::~Client()
{
	if (mControlObject)
		mControlObject->setClient(NULL);
}

void Client::setControlObject(Pawn* object) 
{
	if (mControlObject)
		mControlObject->setClient(NULL);

	mControlObject = object;
	if (mControlObject)
		mControlObject->setClient(this);

	mSendControlObjectUpdate = true;
}

NetObject* Client::resolveGhostID(GhostID id)
{
	GhostInfo* info = getGhostInfo(id);
	if (info == NULL || !info->validOnServer())  // invalid ID?
		return NULL;

	return info->object;
}

duk_ret_t Client::waitForGhost_script(duk_context* ctx)
{
	NetObject* obj;
	dukglue_read(ctx, -1, &obj);

	Promise::PromiseID promiseID = dukglue_promise_push(ctx);

	GhostID ghostID = attachGhost(obj);
	if (ghostID == 0 || !mGhosts[ghostID].validOnServer()) {
		// TODO should handle "already detaching" case by canceling detach if object still exists on server instead of raising an error?
		duk_push_error_object(ctx, DUK_ERR_ERROR, "could not attach ghost (no free GhostIDs or object was already being detached/deleted)");
		Promise::reject(promiseID, DukValue::take_from_stack(ctx));
	} else if (mGhosts[ghostID].validOnBoth()) {
		// already ghosted
		duk_push_uint(ctx, ghostID);
		Promise::resolve(promiseID, DukValue::take_from_stack(ctx));
	} else {
		// not yet ghosted
		mWaitingForGhost.insert({ ghostID, promiseID });
	}

	return 1;
}

GhostInfo* Client::getGhostInfo(GhostID id)
{
	if (id >= mGhosts.size() || id == 0)
		return NULL;

	return &mGhosts[id];
}

GhostID Client::findGhostID(NetObject* obj)
{
	if (obj == NULL)
		return 0;

	for (GhostID i = 1; i < MAX_GHOSTS; i++) {
		if (mGhosts[i].object == obj)
			return i;
	}
	return 0;
}

bool Client::isGhosted(NetObject* obj)
{
	if (obj == NULL)
		return true;

	GhostID id = findGhostID(obj);
	return (id != 0) && mGhosts[id].validOnClient();
}

void Client::onGhostInfoChanged(GhostID id)
{
	// handle waitForGhost calls

	const auto range = mWaitingForGhost.equal_range(id);
	if (range.first == range.second)  // are we listening for id?
		return;

	// is an answer ready? (has the object fully ghosted or been detached? gotta check it's not GHOSTING/WAITING_FOR_GHOST)
	const unsigned char failedMask = (GhostInfo::NOT_IN_USE | GhostInfo::WAITING_FOR_DESTROY | GhostInfo::DESTROYING);
	const unsigned char readyMask = GhostInfo::GHOSTED;
	if ((mGhosts[id].flags & (failedMask | readyMask)) == 0)
		return;  // nope, still in a halfway state

	// trigger the callbacks
	duk_context* ctx = ScriptEngine::get();
	const bool ready = (mGhosts[id].flags & readyMask) != 0;
	for (auto it = range.first; it != range.second; it++) {
		if (ready) {
			// readyMask matched
			duk_push_int(ctx, id);
			Promise::resolve(it->second, DukValue::take_from_stack(ctx));
		} else {
			// failedMask matched
			duk_push_error_object(ctx, DUK_ERR_ERROR, "object not ghosted (object was destroyed or detached)");
			Promise::reject(it->second, DukValue::take_from_stack(ctx));
		}
	}

	// clear this id from the listening list
	mWaitingForGhost.erase(range.first, range.second);
}

GhostID Client::findNextFreeGhost() const
{
	for (GhostID i = 1; i < MAX_GHOSTS; i++)
	{
		if (mGhosts[i].flags == GhostInfo::NOT_IN_USE)
			return i;
	}
	
	assert(false); // hit max simultaneous ghosts limit! should do some sort of recovery but WELP
	return 0;
}

void Client::sendPackets(yojimbo::Server* network, NetLogger* logger)
{
	// these numbers do not include headers, which will be written at max once per type (if any data in that type is required);
	// initial value for bitsRemaining is chosen such that even with packet headers, we should stay under the MTU
	// these are sizes per-object and don't include the variable-length streams for update/create
	// (those are factored in after the stream has been written)
	const int controlObjectUpdateBits = yojimbo::BitsRequired<0, MAX_GHOSTS - 1>::result;
	const int destroyObjectBits = yojimbo::BitsRequired<0, MAX_GHOSTS - 1>::result;
	const int createObjectBits = yojimbo::BitsRequired<0, MAX_GHOSTS - 1>::result
		+ yojimbo::BitsRequired<0, NUM_NET_OBJECT_TYPES - 1>::result;
	const int updateObjectBits = yojimbo::BitsRequired<0, MAX_GHOSTS - 1>::result;
	const int giveUpBits = 4 * 8;  // any less than this and we will skip to the end
	const int bitLimit = 1300 * 8;
	int bitsRemaining = bitLimit;

	yojimbo::Allocator& allocator = yojimbo::GetDefaultAllocator();

	uint8_t* createStreamData = (uint8_t*) YOJIMBO_ALLOCATE(allocator, CreateGhostsMessage::MAX_BYTES);
	uint8_t* updateStreamData = (uint8_t*) YOJIMBO_ALLOCATE(allocator, UpdateGhostsMessage::MAX_BYTES);

	// control object update?
	if (mSendControlObjectUpdate &&
			(mControlObject == NULL || 
				(findGhostID(mControlObject) &&
					getGhostInfo(findGhostID(mControlObject))->validOnBoth()))) {

		LOG(LogNetDebug) << "Sending control object update for client " << guid() << " to ID " << findGhostID(mControlObject);

		auto msg = (ControlObjectChangedMessage*) network->CreateMessage(mClientIndex, ID_CONTROL_OBJECT_CHANGED);
		msg->ghostID = findGhostID(mControlObject);
		network->SendMessage(mClientIndex, CHANNEL_RPC, msg);
		bitsRemaining -= controlObjectUpdateBits;
		mSendControlObjectUpdate = false;
	}

	std::vector< std::pair<GhostID, DirtyMask> > includedUpdates;
	BitStreamWriter updateStream(updateStreamData, UpdateGhostsMessage::MAX_BYTES);

	std::vector<GhostID> destroyIDs;
	
	// Update dirty masks for current ghosts
	// Send destroy if ghost has been detached
	// Send updates for ghosts that are valid on both client and server
	// (mGhosts[0] is reserved, so start at 1)
	for (GhostID id = 1; id < MAX_GHOSTS; id++) {
		// if we're close to going overbudget, just give up, since nothing else will probably fit
		if (bitsRemaining < giveUpBits) {
			LOG(LogNetDebug) << "bitsRemaining < giveUpBits - skipping rest of updates";
			break;
		}

		// update dirty mask if valid on server
		if (mGhosts[id].validOnServer()) {
			mGhosts[id].dirtyMask |= mGhosts[id].object->dirtyMask();
		}
		
		// destroy if no longer valid on server
		if (mGhosts[id].flags & GhostInfo::WAITING_FOR_DESTROY && mGhosts[id].validOnClient() && bitsRemaining >= destroyObjectBits) {
			LOG(LogNetDebug) << "destroying ghost for client " << guid() << ", id " << id;
			// turn off WAITING_FOR_DESTROY, turn on DESTROYING
			mGhosts[id].flags = static_cast<GhostInfo::GhostFlags>((mGhosts[id].flags & ~GhostInfo::WAITING_FOR_DESTROY) | GhostInfo::DESTROYING);
			onGhostInfoChanged(id);
			destroyIDs.push_back(id);

			bitsRemaining -= destroyObjectBits;
		}

		// update if dirty and valid on both
		if (mGhosts[id].validOnBoth() && mGhosts[id].dirtyMask != 0x0) {
			BitStreamWriter::State beforeUpdate = updateStream.state();

			// write the update
      updateStream.writeCheck();  // DEBUG
			DirtyMask remainingMask = mGhosts[id].object->packUpdate(this, mGhosts[id].dirtyMask, updateStream);
      updateStream.writeCheck();  // DEBUG

			int thisUpdateBits = updateObjectBits + (updateStream.bitsWritten() - beforeUpdate.bitsWritten());
			bool overBudget = (bitsRemaining - thisUpdateBits) < 0;

			if (overBudget || updateStream.bufferOverrun() || remainingMask == mGhosts[id].dirtyMask) {
				// discard update
				updateStream.setState(beforeUpdate);
				LOG(LogNetDebug) << "Discarding update for client " << guid() << ", ghost " << id << " ("
					<< (overBudget ? "over bandwidth budget"
						: "remainingMask didn't change")
					<< ")";
				if (thisUpdateBits > bitLimit) {
					LOG(LogWarning) << "Client " << guid() << " ghost " << id
						<< ": packUpdate is too big to send and will never update ("
						<< thisUpdateBits << " bits)";
				}
			} else {
				// accept update
				includedUpdates.push_back(std::pair<GhostID, DirtyMask>(id, mGhosts[id].dirtyMask));
				mGhosts[id].dirtyMask = remainingMask;
				bitsRemaining -= thisUpdateBits;
			}
		}
	}

	// attach any new nearby SceneObjects as new ghosts
	SceneContainer* scene = mControlObject ? mControlObject->container() : NULL;
	if (scene != NULL) {
		const Box2f bounds(mControlObject->position() - Vector2f(1000.f, 1000.f),
			mControlObject->position() + Vector2f(1000.f, 1000.f));
		std::vector<SceneObject*> nearby = scene->findObjects(bounds);

		// always ghost control object
		//if (std::find(nearby.begin(), nearby.end(), mControlObject) == nearby.end())
		//	nearby.push_back(mControlObject);

		for (auto it = nearby.begin(); it != nearby.end(); it++) {
			SceneObject* obj = *it;
			GhostID id = findGhostID(obj);
			if (id == 0) {
				attachGhost(obj);  // reserves an ID, will get created below
			}
		}
	}

	// creates (should happen after updates, in case packUpdate attached a ghost)
	std::vector<GhostID> createIDs;
	std::vector<NetObjectTypeID> createTypes;
	BitStreamWriter createStream(createStreamData, CreateGhostsMessage::MAX_BYTES);

	for (GhostID id = 1; id < MAX_GHOSTS; id++) {
		if (bitsRemaining < giveUpBits) {
			LOG(LogNetDebug) << "bitsRemaining < giveUpBits - skipping rest of creates";
			break;
		}

		if (mGhosts[id].flags & GhostInfo::WAITING_FOR_GHOST) {
			assert(mGhosts[id].validOnServer());

			NetObject* obj = mGhosts[id].object;

			BitStreamWriter::State beforeCreate = createStream.state();

      createStream.writeCheck();  // DEBUG
			obj->packConstructorArgs(this, createStream);
      createStream.writeCheck();  // DEBUG
			
			int thisCreateBits = createObjectBits + (createStream.bitsWritten() - beforeCreate.bitsWritten());
			bool overBudget = (bitsRemaining - thisCreateBits) < 0;

			if (createStream.bufferOverrun() || overBudget) {
				// discard create
				LOG(LogNetDebug) << "Discarding create for client " << guid() << ", ghost " << id << " (over budget)";
				createStream.setState(beforeCreate);
			} else {
				// accept create
				// turn off waiting for ghost, turn on ghosting
				mGhosts[id].flags = (GhostInfo::GhostFlags)((mGhosts[id].flags & ~GhostInfo::WAITING_FOR_GHOST) | GhostInfo::GHOSTING);
				mGhosts[id].dirtyMask = ~0x0;  // update all the state next packet (probably not necessary, since its set on attachGhost)
				onGhostInfoChanged(id);
				createIDs.push_back(id);
				createTypes.push_back(obj->netObjectTypeID());
				bitsRemaining -= thisCreateBits;
			}
		}
	}

	if (createIDs.size() > 0) {
		PacketID packetID = mPacketRecorder.addGhostCreatePacket(createIDs);
		auto msg = (CreateGhostsMessage*) network->CreateMessage(mClientIndex, ID_CREATE_GHOSTS);
		msg->packetID = packetID;
		msg->ghostIDs = std::move(createIDs);  // createIDs no longer valid
		msg->objectTypes = std::move(createTypes);  // createTypes no longer valid

    createStream.flush();
    msg->SetStreamData(createStreamData, createStream.bytesWritten(), allocator);  // createStreamData now owned by msg

		network->SendMessage(mClientIndex, CHANNEL_GHOSTS, msg);
	} else {
		YOJIMBO_FREE(allocator, createStreamData);
	}
	if (destroyIDs.size() > 0) {
		PacketID packetID = mPacketRecorder.addGhostDestroyPacket(destroyIDs);
		auto msg = (DestroyGhostsMessage*) network->CreateMessage(mClientIndex, ID_DESTROY_GHOSTS);
		msg->packetID = packetID;
		msg->ghostIDs = std::move(destroyIDs);  // destroyIDs no longer valid
		network->SendMessage(mClientIndex, CHANNEL_GHOSTS, msg);
	}
	if (includedUpdates.size() > 0) {
		PacketID packetID = mPacketRecorder.addGhostUpdatePacket(includedUpdates);

		auto msg = (UpdateGhostsMessage*) network->CreateMessage(mClientIndex, ID_UPDATE_GHOSTS);
		msg->packetID = packetID;
		msg->lastReceivedMoveNumber = mLastInputNumberClient;
		static_assert(std::is_same<decltype(mLastInputNumberClient), decltype(msg->lastReceivedMoveNumber)>::value,
			"lastReceivedMoveNumber type changed");

		msg->ghostIDs.resize(includedUpdates.size());
		for (size_t i = 0; i < msg->ghostIDs.size(); i++) {
			msg->ghostIDs[i] = includedUpdates[i].first;
		}

		updateStream.flush();
		msg->SetStreamData(updateStreamData, updateStream.bytesWritten(), allocator);  // msg takes ownership of updateStreamData
		network->SendMessage(mClientIndex, CHANNEL_GHOSTS, msg);

		if (logger) {
			yojimbo::MeasureStream measure(allocator);
			msg->SerializeInternal(measure);
			logger->sentPacket(NetLoggerEvent::PACKET_UPDATE, packetID, guid(), measure.GetBytesProcessed(), msg->ghostIDs);
		}
	} else {
		YOJIMBO_FREE(allocator, updateStreamData);
	}
}

void Client::receivedAck(PacketID id)
{
	mPacketRecorder.receivedAck(this, id);
}

void Client::receivedMove(const Move& move)
{
	if (move.seq_num <= mLastInputNumberClient) {
		LOG(LogWarning) << "Received old/duplicate move from client " << guid()
			<< " (move seq num: " << move.seq_num << ", prev received: " << mLastInputNumberClient << ")";
	}

	// does not properly handle wrap-around, but is only for debugging
	const int dist = move.seq_num - mLastInputNumberClient;
	if (dist != 1)
		LOG(LogWarning) << "Client " << guid() << " moving from input sequence " << mLastInputNumberClient << " to " << move.seq_num;

	mLastInputNumberClient = move.seq_num;

	if (controlObject())
		controlObject()->addMoveServerside(move);
}

void Client::detachGhost(NetObject* obj)
{
	for (GhostID i = 1; i < MAX_GHOSTS; i++) {
		if (mGhosts[i].flags != GhostInfo::NOT_IN_USE && mGhosts[i].object == obj) {
			mGhosts[i].object = NULL; // used to catch programmer errors, not necessary

			// turn off WAITING_FOR_GHOST (so we don't try to start ghosting an object that no longer exists)
			if (mGhosts[i].flags & GhostInfo::WAITING_FOR_GHOST) {
				// object was never actually ghosted, we can just free this ghost
				mGhosts[i].flags = GhostInfo::NOT_IN_USE;
			} else {
				// need to destroy on client, turn on WAITING_FOR_DESTROY
				mGhosts[i].flags = (GhostInfo::GhostFlags)(mGhosts[i].flags | GhostInfo::WAITING_FOR_DESTROY);
			}
			onGhostInfoChanged(i);
			break;
		}
	}
}

GhostID Client::attachGhost(NetObject* obj)
{
	// make sure it's not already ghosted
	GhostID id = findGhostID(obj);
	if (id != 0)
		return id;

	id = findNextFreeGhost();
	LOG(LogNetDebug) << "reserving ghost for client GUID " << guid() << " as ghost ID " << id << " (type " << obj->netObjectTypeID() << ")";

	if (id == 0) {
		LOG(LogWarning) << "  no free ghost IDs - cannot attach";
		return 0;
	}

	mGhosts[id].object = obj;
	mGhosts[id].flags = GhostInfo::WAITING_FOR_GHOST;
	mGhosts[id].dirtyMask = ~0x0;
	onGhostInfoChanged(id);

	return id;
}