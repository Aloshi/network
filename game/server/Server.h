#pragma once

#include <script/ScriptEngine.h>
#include <script/ScriptObject.h>
#include <server/Client.h>
#include <net/NetLogger.h>
#include <yojimbo.h>

#include <vector>

SCRIPT_CLASS("Server", Server, void, "Manages currently connected clients.");
class Server : public ScriptObject, public yojimbo::Adapter
{
public:
	Server();
	virtual ~Server();

	void listen(const std::string& addr, unsigned int port, unsigned int maxConnections);
	void stop();

  void advanceTime();
	void processPackets();
	void sendPackets();  // send any pending messages (state updates, rpcs, ...)
	void sendNetUpdates();  // push a state update into every client's message queue

	static const uint8_t MAX_N_SERVERCMD_ARGS = 20;

	SCRIPT_METHOD_VARARGS(Server, "sendClientCmd", &Server::sendClientCmd, "Send an RPC to a client. sendClientCmd(client, cmdName, args...).");
	duk_ret_t sendClientCmd(duk_context* ctx);

	SCRIPT_METHOD(Server, "getClientCount", &Server::getClientCount, "Get the number of clients currently connected to the server.");
	inline unsigned int getClientCount() const { return mClients.size(); }

	SCRIPT_METHOD(Server, "getClient", &Server::getClient, "Get the ith client connected to the server.");
	inline Client* getClient(unsigned int i) { return mClients.at(i); }

	SCRIPT_PROPERTY(Server, "onClientConnected", &Server::getClConnectedCallback, &Server::setClConnectedCallback, "Callback fired when a client connects.");
	inline void setClConnectedCallback(DukValue v) {
		mClientConnectedCb = std::move(v);
	}
	inline const DukValue& getClConnectedCallback() const {
		return mClientConnectedCb;
	}

	SCRIPT_PROPERTY(Server, "onClientDisconnected", &Server::getClDisconnectedCallback, &Server::setClDisconnectedCallback, "Callback fired when a client disconnects.");
	inline void setClDisconnectedCallback(DukValue v) {
		mClientDisconnectedCb = std::move(v);
	}
	inline const DukValue& getClDisconnectedCallback() const {
		return mClientDisconnectedCb;
	}

	SCRIPT_PROPERTY(Server, "logger", &Server::logger, nullptr, "Get the NetLogger for the server.");
	NetLogger* logger() { return &mLogger; }

	Client* findClientByGUID(const ClientGUID& guid);

protected:
	yojimbo::MessageFactory* CreateMessageFactory(yojimbo::Allocator& allocator) override;
	void OnServerClientConnected(int clientIdx) override;
	void OnServerClientDisconnected(int clientIdx) override;

private:
	Server(const Server& rhs) = delete;
	Server& operator= (const Server& rhs) = delete;
	
	void handleMessage(Client* cl, yojimbo::Message* msg);
	
	void handleServerCmd(Client* cl, ServerCmdMessage* msg);
	void sendServerCmdReply(ClientGUID guid, uint32_t clientPromiseID, const std::string& cmdName, const DukValue& reply, bool ok);

	yojimbo::Server* mNetwork;

	std::vector<Client*> mClients;

	DukValue mClientConnectedCb;
	DukValue mClientDisconnectedCb;

	NetLogger mLogger;
};
