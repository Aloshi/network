#include "Server.h"
#include <Sim.h>
#include <Move.h>
#include <net/PacketMessageTypes.h>
#include <scene/SceneManager.h>
#include <script/ScriptEngine.h>  // for ServerCmd

#include <util/Log.h>

// for test scene
#include <game/StaticShape.h>

Server::Server()
{
	mNetwork = NULL;

	// let's make some OBJECTS YEAH
	SceneContainer* scene = SceneManager::get()->getScene("testScene");
	StaticShape* ground = new StaticShape(scene);
	ground->setPosition(Vector2f(400, 580));
	const std::vector<Vector2f> groundVerts = {
		Vector2f(-395.f, -15.f), Vector2f(395.f, -15.f),
		Vector2f(395.f, 15.f), Vector2f(-395.f, 15.f)
	};
	ground->setVertices(groundVerts);

	StaticShape* tri = new StaticShape(scene);
	tri->setPosition(Vector2f(700, 540));
	const std::vector<Vector2f> triVerts = {
		Vector2f(0, -25), Vector2f(0, 25.1f), Vector2f(-25, 25.1f)
	};
	tri->setVertices(triVerts);

	StaticShape* triLong = new StaticShape(scene);
	triLong->setPosition(Vector2f(100, 400));
	const std::vector<Vector2f> triLongVerts = {
		Vector2f(0, -165.1f), Vector2f(120, 165.1f), Vector2f(0, 165.1f)
	};
	triLong->setVertices(triLongVerts);
}

Server::~Server()
{
	stop();
}

void Server::stop()
{
	for (auto it = mClients.begin(); it != mClients.end(); it++) {
		delete *it;
	}

	if (mNetwork) {
		mNetwork->Stop();
		delete mNetwork;
		mNetwork = NULL;
	}
}

void Server::listen(const std::string& addr, unsigned int port, unsigned int maxConnections)
{
	stop();

	// TODO not this
	uint8_t privateKey[yojimbo::KeyBytes];
	memset(privateKey, 0, yojimbo::KeyBytes);

	yojimbo::Address desc(addr.c_str(), port);

	mNetwork = new yojimbo::Server(yojimbo::GetDefaultAllocator(), privateKey, desc, NetConfig, *this, 0.0);
	mNetwork->Start(maxConnections);
}

void Server::advanceTime()
{
  if (mNetwork)
    mNetwork->AdvanceTime(Sim::getTimeSeconds());
}

void Server::processPackets()
{
  if (mNetwork == NULL)
    return;

	mNetwork->ReceivePackets();

	yojimbo::Message* message;
	for (int clientIdx = 0; clientIdx < mNetwork->GetNumConnectedClients(); clientIdx++) {
		Client* client = findClientByGUID(mNetwork->GetClientId(clientIdx));
		for (int channelIdx = 0; channelIdx < NetConfig.numChannels; channelIdx++) {
			while ((message = mNetwork->ReceiveMessage(clientIdx, channelIdx)) != NULL)
			{
				handleMessage(client, message);
				mNetwork->ReleaseMessage(clientIdx, message);
			}
		}
	}
}

void Server::sendPackets()
{
  if (mNetwork)
    mNetwork->SendPackets();
}

yojimbo::MessageFactory * Server::CreateMessageFactory(yojimbo::Allocator & allocator)
{
	return YOJIMBO_NEW(allocator, NetMessageFactory, allocator);
}

void Server::handleMessage(Client* cl, yojimbo::Message* msg)
{
	// Note: cl may be NULL
	NetMessageTypes type = (NetMessageTypes) msg->GetType();

	switch (type) {
	case ID_PACKET_ACK:
	{
		auto* ack = (UnreliableSequencedAckMessage*) msg;
		if (cl) {
			cl->receivedAck(ack->sequenceNumber);
		} else {
			LOG(LogWarning) << "Received ack from missing client!";
		}
		break;
	}
	
	case ID_MOVE_PLAYER:
	{
		Move& move = ((MoveMessage*) msg)->move;
		if (cl) {
			cl->receivedMove(move);
			mLogger.receivedPacket(NetLoggerEvent::PACKET_MOVE, move.seq_num, cl->guid());
		} else {
			LOG(LogWarning) << "Received move from missing client!";
		}
		break;
	}

	case ID_SERVERCMD:
	{
		if (cl) {
			handleServerCmd(cl, (ServerCmdMessage*) msg);
		} else {
			LOG(LogWarning) << "Received ServerCmd from missing client!";
		}
		break;
	}

	// ignore heartbeat packets
	case ID_HEARTBEAT:
		break;
	}
}

void Server::OnServerClientConnected(int clientIdx)
{
	LOG(LogInfo) << "Client connected!";

	Client* client = new Client(mNetwork->GetClientId(clientIdx), clientIdx);
	mClients.push_back(client);

	if (mClientConnectedCb.type() != DukValue::UNDEFINED) {
		ScriptEngine::pcall_noresult(mClientConnectedCb, client);
	}
}

void Server::OnServerClientDisconnected(int clientIdx)
{
	LOG(LogInfo) << "Client disconnected!";

	ClientGUID guid = mNetwork->GetClientId(clientIdx);
	for (auto it = mClients.begin(); it != mClients.end(); it++) {
		if ((*it)->guid() == guid) {

			if (mClientDisconnectedCb.type() != DukValue::UNDEFINED) {
				ScriptEngine::pcall_noresult(mClientDisconnectedCb, *it);
			}

			if ((*it)->controlObject())
				delete (*it)->controlObject();
			delete *it;
			mClients.erase(it);
			return;
		}
	}

	LOG(LogWarning) << "Unknown client (" << clientIdx << ") disconnected!";
	assert(false);  // this should be impossible, since this is called by yojimbo
}

void Server::sendNetUpdates()
{
  if (mNetwork) {
    for (auto it = mClients.begin(); it != mClients.end(); it++) {
      (*it)->sendPackets(mNetwork, &mLogger);
    }
  }

	// TODO why is this here?
	for (auto it = NetObject::allNetObjects().begin(); it != NetObject::allNetObjects().end(); it++) {
		(*it)->clearDirtyMask();
	}
}

Client* Server::findClientByGUID(const ClientGUID& guid)
{
	for (auto it = mClients.begin(); it != mClients.end(); it++) {
		if ((*it)->guid() == guid)
			return *it;
	}

	return NULL;
}

void Server::sendServerCmdReply(ClientGUID guid, uint32_t clientPromiseID, const std::string& cmdName, const DukValue& reply, bool ok) {
	duk_context* ctx = ScriptEngine::get();

	Client* cl = findClientByGUID(guid);
	if (cl == NULL) {
		LOG(LogNetDebug) << "Could not send reply for ServerCmd '" << cmdName << "' to '" << guid << "' as the client disconnected.";
	}

	std::vector<char> buff = reply.serialize();

	if (buff.size() > UINT16_MAX) {
		LOG(LogError) << "ServerCmd '" << cmdName << "' reply for client GUID " << cl->guid()
			<< " is too large to send (" << buff.size() << " bytes)";

		// too big to serialize, send an error message instead
		ok = false;
		duk_push_sprintf(ctx, "ServerCmd reply is too large to send (%u bytes serialized)", (unsigned int)buff.size());
		DukValue errorReply = DukValue::take_from_stack(ctx);
		buff = errorReply.serialize();
		assert(buff.size() <= UINT16_MAX);
	}

	auto response = (ServerCmdResponseMessage*) mNetwork->CreateMessage(cl->clientIndex(), ID_SERVERCMD_RESPONSE);
	response->promiseID = clientPromiseID;
	response->ok = ok;
	response->response = std::move(buff);  // buff is now invalid
	mNetwork->SendMessage(cl->clientIndex(), CHANNEL_RPC, response);
}

void Server::handleServerCmd(Client* cl, ServerCmdMessage* msg)
{
	duk_context* ctx = ScriptEngine::get();

	// read command name
	const std::string& cmdName = msg->cmdName;

	// try and push the function onto the stack (it's a property of the ServerCmd object)
	duk_get_global_string(ctx, "ServerCmd");
	duk_get_prop_lstring(ctx, -1, cmdName.c_str(), cmdName.size());
	duk_remove(ctx, -2);

	// did something exist by that name?
	if (duk_is_undefined(ctx, -1)) {
		LOG(LogWarning) << "Client " << cl << " tried to call undefined ServerCmd '" << cmdName << "'.";
		duk_pop(ctx);  // pop the undefined
		return;
	}

	// was it callable (i.e. a function)?
	if (!duk_is_callable(ctx, -1)) {
		LOG(LogWarning) << "Client " << cl << " tried to call non-callable ServerCmd '" << cmdName << "'.";
		duk_pop(ctx);  // pop the object, whatever it was
		return;
	}

	// read the number of arguments
	size_t n_args = msg->arguments.size();

	// was it a sane value?
	if (n_args > MAX_N_SERVERCMD_ARGS) {
		LOG(LogWarning) << "Client GUID " << cl->guid() << " tried to call ServerCmd '" << cmdName << "' with too many arguments (sent "
			<< n_args << " args, max is " << MAX_N_SERVERCMD_ARGS << ").";
		duk_pop(ctx);  // pop the function
		return;
	}

	duk_require_stack(ctx, n_args + 1);

	// push client onto stack as first argument
	dukglue_push(ctx, cl);

	// push the arguments onto the stack
	for (size_t i = 0; i < n_args; i++) {
		const auto& buff = msg->arguments[i];
		try {
			DukValue v = DukValue::deserialize(ctx, buff.data(), buff.size());
			v.push();
		} catch (DukException& e) {
			LOG(LogError) << "Client GUID " << cl->guid() << " tried to call ServerCmd '" << cmdName
				<< "', but argument #" << (i + 1) << " could not be deserialized:\n"
				<< e.what();
			duk_pop_n(ctx, i + 2);  // also pop function and client
			return;
		}
	}

	// call ServerCmdxxx(cl, args...)
	int rc = duk_pcall(ctx, n_args + 1);
	if (rc != DUK_EXEC_SUCCESS) {
		if (duk_is_object(ctx, -1) && duk_has_prop_string(ctx, -1, "stack"))
			duk_get_prop_string(ctx, -1, "stack");
		else
			duk_dup_top(ctx);

		LOG(LogError) << "Error executing ServerCmd '" << cmdName << "' (sent by GUID " << cl->guid() << "):\n"
			<< duk_safe_to_string(ctx, -1);
		duk_pop(ctx);
	}

	// send response back to client, if they asked for it
	if (msg->promiseID != 0) {
		bool ok = (rc == DUK_EXEC_SUCCESS);

		if (ok && dukglue_is_promise(ctx, -1)) {
			Promise::PromiseID myPromiseID = dukglue_get_promise_id(ctx, -1);

			ClientGUID clientGUID = cl->guid();
			uint32_t clientPromiseID = msg->promiseID;
			Promise::registerResolvedCallback(myPromiseID, [this, clientGUID, clientPromiseID, cmdName] (const DukValue& v) {
				sendServerCmdReply(clientGUID, clientPromiseID, cmdName, v, true);
			});
			Promise::registerRejectedCallback(myPromiseID, [this, clientGUID, clientPromiseID, cmdName] (const DukValue& v) {
				sendServerCmdReply(clientGUID , clientPromiseID, cmdName, v, false);
			});

			duk_pop(ctx);  // pop promise object, we just needed the ID for registerCallback
		} else {
			// reply is immediately available
			sendServerCmdReply(cl->guid(), msg->promiseID, cmdName, DukValue::take_from_stack(ctx), ok);
		}
	} else {
		// pop result, not interested
		duk_pop(ctx);
	}
}

duk_ret_t Server::sendClientCmd(duk_context* ctx)
{
	duk_idx_t n_calling_args = duk_get_top(ctx);
	if (n_calling_args == 0) {
		duk_error(ctx, DUK_ERR_ERROR, "Missing client.");
		return DUK_RET_ERROR;
	}
	if (n_calling_args == 1) {
		duk_error(ctx, DUK_ERR_ERROR, "Missing command name.");
		return DUK_RET_ERROR;
	}

	std::string cmdName = duk_require_string(ctx, 1);

	Client* cl = NULL;
	dukglue_read(ctx, 0, &cl);

	if (cl == NULL) {
		duk_error(ctx, DUK_ERR_ERROR, "Client is null.");
		return DUK_RET_ERROR;
	}

	duk_idx_t n_cmd_args = n_calling_args - 2;
	if (n_cmd_args > MAX_N_SERVERCMD_ARGS) {
		duk_error(ctx, DUK_ERR_ERROR, "Too many arguments (%d > %d).", n_cmd_args, MAX_N_SERVERCMD_ARGS);
		return DUK_RET_ERROR;
	}

	std::vector< std::vector<char> > arguments(n_cmd_args);

	for (size_t i = 0; i < arguments.size(); i++) {
		try {
			DukValue arg = DukValue::copy_from_stack(ctx, 2 + i);
			std::vector<char>& buff = arguments[i];
			buff = arg.serialize();

			if (buff.size() > UINT16_MAX) {
				duk_error(ctx, DUK_ERR_ERROR, "Argument %d is too large to send (%u bytes)", (i + 1), (unsigned int) buff.size());
				return DUK_RET_ERROR;
			}
		} catch (DukException&) {
			duk_error(ctx, DUK_ERR_ERROR, "Failed to serialize argument %u (bad type?).", (i + 1));
			return DUK_RET_ERROR;
		}
	}

	// careful not to leak msg on error
	auto msg = (ClientCmdMessage*)mNetwork->CreateMessage(cl->clientIndex(), ID_CLIENTCMD);
	msg->cmdName = cmdName;
	msg->arguments = std::move(arguments);  // arguments is now invalid
	mNetwork->SendMessage(cl->clientIndex(), CHANNEL_RPC, msg);

	return 0;
}
