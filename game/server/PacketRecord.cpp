#include "PacketRecord.h"

#include "Client.h"
#include <util/Log.h>

#include <assert.h>
#include <iostream>

void GhostCreatePacketRecord::onAck(Client* client, PacketRecorder*)
{
	for (auto it = newGhostIDs.begin(); it != newGhostIDs.end(); it++) {
		GhostInfo* ghost = client->getGhostInfo(*it);

		// turn off GHOSTING, turn on GHOSTED
		ghost->flags = static_cast<GhostInfo::GhostFlags>((ghost->flags & ~GhostInfo::GHOSTING) | GhostInfo::GHOSTED);
		client->onGhostInfoChanged(*it);
	}
}

void GhostCreatePacketRecord::onNack(Client* client, PacketRecorder*)
{
	for (auto it = newGhostIDs.begin(); it != newGhostIDs.end(); it++) {
		GhostInfo* ghost = client->getGhostInfo(*it);

		// the ghost failed to create
		// if it became marked for destruction between send this packet and now, just discard it
		if (ghost->flags & GhostInfo::WAITING_FOR_DESTROY) {
			ghost->flags = GhostInfo::NOT_IN_USE;
		} else {
			// otherwise, try to create it again next tick
			// turn off ghosting, turn on waiting_for_ghost
			ghost->flags = (GhostInfo::GhostFlags)((ghost->flags & ~GhostInfo::GHOSTING) | GhostInfo::WAITING_FOR_GHOST);
		}
		client->onGhostInfoChanged(*it);
	}
}

void GhostUpdatePacketRecord::onAck(Client* client, PacketRecorder* recorder)
{
	// nothing to do, dirtyFlags were already cleared
}

void GhostUpdatePacketRecord::onNack(Client* client, PacketRecorder*)
{
	for (auto it = ghostUpdates.begin(); it != ghostUpdates.end(); it++) {
		GhostID ghostID = it->first;
		GhostInfo* ghost = client->getGhostInfo(ghostID);

		// You might think this can trigger an unnecessary update if:
		// 0. this update packet is sent
		// 1. the ghost is destroyed
		// 2. a new ghost takes this ID
		// 3. this packet is nack'd
		// But this isn't possible because the destroy packet (1) has to be acked before
		// the ghost ID is freed, which will cause this packet to nack since all packets
		// use the same packet ID sequence.
		// tl;dr: It's safe. It's built on a house of cards, but it's safe. (as of Nov 29, 2015)

		// make sure the object hasn't been destroyed
		if (ghost->validOnServer()) {
			ghost->dirtyMask |= it->second;
		}
	}
}

void GhostDestroyPacketRecord::onAck(Client* client, PacketRecorder*)
{
	for (auto it = destroyGhostIDs.begin(); it != destroyGhostIDs.end(); it++) {
		GhostInfo* ghost = client->getGhostInfo(*it);

		// done with the ghost, "free" it (so something else can take this ID)
		ghost->flags = GhostInfo::NOT_IN_USE;
		client->onGhostInfoChanged(*it);
	}
}

void GhostDestroyPacketRecord::onNack(Client* client, PacketRecorder*)
{
	for (auto it = destroyGhostIDs.begin(); it != destroyGhostIDs.end(); it++) {
		GhostInfo* ghost = client->getGhostInfo(*it);

		// turn off DESTROYING, turn back on WAITING_FOR_DESTROY
		ghost->flags = static_cast<GhostInfo::GhostFlags>((ghost->flags & ~GhostInfo::DESTROYING) | GhostInfo::WAITING_FOR_DESTROY);
		client->onGhostInfoChanged(*it);
	}
}

PacketRecorder::~PacketRecorder()
{
	for (auto it = mPackets.begin(); it != mPackets.end(); it++) {
		delete *it;
	}
}

PacketID PacketRecorder::addGhostCreatePacket(const std::vector<GhostID>& ghostIDs)
{
	mPackets.push_back(new GhostCreatePacketRecord(mNextPacketID, ghostIDs));
	return mNextPacketID++;
}

PacketID PacketRecorder::addGhostUpdatePacket(const std::vector< std::pair<GhostID, DirtyMask> >& update)
{
	mPackets.push_back(new GhostUpdatePacketRecord(mNextPacketID, update));
	return mNextPacketID++;
}

PacketID PacketRecorder::addGhostDestroyPacket(const std::vector<GhostID>& ghostIDs)
{
	mPackets.push_back(new GhostDestroyPacketRecord(mNextPacketID, ghostIDs));
	return mNextPacketID++;
}

std::vector<PacketRecord*>::iterator PacketRecorder::nextPackets(PacketID packetID)
{
	for (auto it = mPackets.begin(); it != mPackets.end(); it++) {
		if ((*it)->packetID == packetID) {
			return ++it;
		}
	}

	return mPackets.end();
}

void PacketRecorder::receivedAck(Client* client, PacketID id)
{
	auto it = mPackets.begin();
	while (it != mPackets.end()) {
		if ((*it)->packetID < id) {
			LOG(LogNetDebug) << "CLIENT " << client->guid() << " DROPPED PACKET: " << (*it)->packetID;
			(*it)->onNack(client, this);
			delete *it;
			it = mPackets.erase(it);
			continue;
		}
		if ((*it)->packetID == id) {
			(*it)->onAck(client, this);
			delete *it;
			mPackets.erase(it);
			return;
		}
		it++;
	}

	if (id >= mNextPacketID)
		LOG(LogWarning) << "Warning - client sent ack for unknown packet (" << id << ")";
}
