#pragma once

#include <script/ScriptEngine.h>
#include <script/Promise.h>
#include <server/PacketRecord.h>
#include <net/NetObject.h>
#include <net/GhostInfo.h>
#include <net/GlobalNetObject.h>
#include <net/NetLogger.h>
#include <Pawn.h>

#include <yojimbo.h>

#include <vector>

SCRIPT_CLASS("Client", Client, void, "A client connected to the server.");
class Client : public ScriptObject
{
public:
	explicit Client(const ClientGUID& guid, int clientIndex);
	virtual ~Client();

	GhostID findGhostID(NetObject* obj);  // returns 0 if not ghosted
	bool isGhosted(NetObject* obj);
	GhostInfo* getGhostInfo(GhostID id);
	GhostID attachGhost(NetObject* obj);  // mark obj to be ghosted next update (if it is not already)
	void detachGhost(NetObject* obj);  // mark the ghost for obj for destruction (if it exists)

	inline const ClientGUID& guid() const { return mGUID; }
	inline int clientIndex() const { return mClientIndex; }

	void sendPackets(yojimbo::Server* network, NetLogger* logger);
	void receivedAck(PacketID id);
	
	void receivedMove(const Move& move);

	SCRIPT_PROPERTY(Client, "controlObject", &Client::controlObject, &Client::setControlObject, "The object this client is controlling. May be null.");
	void setControlObject(Pawn* controlObject);
	inline Pawn* controlObject() { return mControlObject; }

	SCRIPT_METHOD(Client, "resolveGhostID", &Client::resolveGhostID, "Map a client ghost ID to the server-side object it corresponds to. Returns null on error.");
	NetObject* resolveGhostID(GhostID id);

	SCRIPT_METHOD_VARARGS(Client, "waitForGhost", &Client::waitForGhost_script,
		"Returns a promise that fulfills with the object's ghost ID only after the object has been fully ghosted. If the object is destroyed, the promise is rejected.");
	duk_ret_t waitForGhost_script(duk_context* ctx);

	inline unsigned int lastMoveNumber() const {
		return mLastInputNumberClient;
	}

protected:
	// called when GhostInfo for id whenever GhostInfo.flags changes
	void onGhostInfoChanged(GhostID id);

private:
	GhostID findNextFreeGhost() const;

	ClientGUID mGUID;
	int mClientIndex;
	std::vector<GhostInfo> mGhosts;  // used as an array of length MAX_GHOSTS
	bool mSendControlObjectUpdate;

	friend GhostCreatePacketRecord;  // for onGhostInfoChanged
	friend GhostDestroyPacketRecord;  // for onGhostInfoChanged
	PacketRecorder mPacketRecorder;

	Pawn* mControlObject;

	uint16_t mLastInputNumberClient;  // sequence # of last received (& applied) move

	const NetObject::SignalReceipt mNetObjRemoveConn;
	const GlobalNetObject::SignalReceipt mGlobalnetObjAddConn;

	std::multimap<GhostID, Promise::PromiseID> mWaitingForGhost;
};
