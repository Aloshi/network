"use strict";

// bad, done for debug to define tilesets
var tilesets = require("assets/tilesets/tilesets.js");

var globals = require("scripts/globals.js");

// Tile editor state
exports.state = {};
exports.state.curTileset = tilesets[0];
exports.state.curTileID = 0;
exports.state.curLayer = 1;
exports.state.paletteScale = 1.5;
exports.state.mirrorHorizontal = false;
exports.state.rotation = 0;

// Tile editor UI
exports.ui = {};
exports.ui.palette = new ScreenSprite();
exports.ui.paletteHighlight = new ScreenSprite();
exports.ui.cursorTile = new ScreenSprite();

exports.ui.tilesetSelect = (function () {
  var tss = {
    win: new Window(),
    scroll: new ScrollContainer(),
    gallery: new Gallery(),

    updateTilesets: function(tilesets) {
      this.gallery.clear();
      tilesets.forEach(function (ts) {
        this.gallery.add(ts.name, ts.texture, undefined);
      }, this);
    },
  };

  tss.scroll.add(tss.gallery);
  tss.win.add(tss.scroll);

  tss.win.title = "Tileset";
  tss.win.canClose = false;

  tss.gallery.onSelected = function(idx) {
    if (idx === -1)
      return;

    var name = tss.gallery.getLabelText(idx);
    var ts = tilesets.find(function (ts) {
      return ts.name === name;
    });

    exports.state.curTileset = ts;
    exports.state.curTileID = 0;
    exports.updatePalette();
  };

  tss.updateTilesets(tilesets);

  var width = 96;
  tss.win.setMinimumSize(width, Math.floor(globals.screenHeight * 0.75));
  tss.win.setPosition(globals.screenWidth - width, globals.screenHeight * 0.1);

  return tss;
})();

exports.close = function() {
  this.ui.palette.hide();
  this.ui.paletteHighlight.hide();
  this.ui.tilesetSelect.win.close();
  this.ui.cursorTile.hide();
  this.actionMap.pop();
  this.isOpen = false;
  warn("Closing tileEditor");
};

exports.open = function() {
  this.ui.palette.show();
  this.ui.paletteHighlight.show();
  this.ui.tilesetSelect.win.open();
  this.ui.cursorTile.show();
  this.updatePalette();
  this.actionMap.push();
  this.isOpen = true;
  warn("Opening tileEditor");
};

exports.updatePalette = function() {
  if (!this.state.curTileset)
    return;

  var curTileset = this.state.curTileset;
  var tileRect = curTileset.getTileTextureRect(this.state.curTileID);
  var texture = curTileset.texture;
  this.ui.palette.texture = texture;
  this.ui.paletteHighlight.texture = texture;

  var paletteScale = this.state.paletteScale;
  var texWidth = texture.width * paletteScale;
  var texHeight = texture.height * paletteScale;

  var topLeft = {x: globals.screenWidth - texWidth - 8, y: globals.screenHeight - texHeight - 8};
  this.ui.palette.offset = topLeft;
  var size = {x: texture.width * paletteScale, y: texture.height * paletteScale};
  this.ui.palette.size = size;

  this.ui.paletteHighlight.textureRect = tileRect;
  var highlightTopLeft = Math.vectorAdd(topLeft, {x: tileRect.x * paletteScale, y: tileRect.y * paletteScale});
  this.ui.paletteHighlight.offset = highlightTopLeft;
  var highlightSize = {x: tileRect.w * paletteScale, y: tileRect.h * paletteScale};
  this.ui.paletteHighlight.size = highlightSize;

  this.ui.palette.setColorshift(128, 128, 128, 192);
  //this.ui.paletteHighlight.setColorshift(128, 128, 128, 255);

  this.ui.paletteHighlight.mirrorHorizontal = this.state.mirrorHorizontal;

  this.ui.cursorTile.texture = texture;
  this.ui.cursorTile.textureRect = tileRect;
  this.ui.cursorTile.setColorshift(192, 192, 192, 128);
};

exports.screenToTileIdx = function(screenPos) {
  var worldPos = ServerConnection.camera.screenToWorld(screenPos);
  var tileIdx = {
    x: Math.floor(worldPos.x / globals.TILE_SIZE),
    y: Math.floor(worldPos.y / globals.TILE_SIZE)
  };
  //print(" tileIdx: " + tileIdx.x + ", " + tileIdx.y);
  return tileIdx;
};

exports.putTile = function(tileIdx) {
  var state = this.state;
  ServerConnection.sendServerCmd('EditorSetTile', ServerConnection.controlObject.scene.name,
    state.curLayer, tileIdx.x, tileIdx.y, state.curTileID, state.curTileset.name, state.rotation, state.mirrorHorizontal);
};

exports.eraseTile = function(tileIdx) {
  ServerConnection.sendServerCmd('EditorEraseTile', ServerConnection.controlObject.scene.name,
    this.state.curLayer, tileIdx.x, tileIdx.y);
};

// Action map
exports.actionMap = new ActionMap();

exports.actionMap.bind("kbm", "mouseScrolled", function (delta) {
  if (!delta)
    return false;

  var state = exports.state;
  if (!state.curTileset)
    return false;

  state.curTileID -= Math.ceil(delta);
  while (state.curTileID < 0)
    state.curTileID += state.curTileset.getTileTypeCount();
  state.curTileID = state.curTileID % state.curTileset.getTileTypeCount();

  exports.updatePalette();
  return true;
});

exports.actionMap.bind("kbm", "shift mouseScrolled", function (delta) {
  if (!delta)
    return false;

  var state = exports.state;
  if (!state.curTileset)
    return false;

  state.rotation -= Math.ceil(delta);
  while (state.rotation < 0)
    state.rotation += 4;
  state.rotation = state.rotation % 4;

  exports.updatePalette();
  return true;
});

var lastTileIdx = null;

function screenPointInPalette(p) {
  var palette = exports.ui.palette;
  var min = palette.offset;
  var max = Math.vectorAdd(min, palette.size);
  return (p.x < max.x && p.x >= min.x && p.y < max.y && p.y >= min.y);
}

exports.actionMap.bind("kbm", "mouse0", function (down) {
  exports.state.dragMode = down ? "put" : null;
  if (down) {
    var mousePos = getMousePosition();
    if (exports.state.curTileset && screenPointInPalette(mousePos)) {
      exports.state.dragMode = null;  // not drawing

      // change current tile
      var tileset = exports.state.curTileset;
      var paletteScale = exports.state.paletteScale;
      for (var i = 0; i < tileset.getTileTypeCount(); i++) {
        var rect = tileset.getTileTextureRect(i);
        var min = Math.vectorAdd({x: rect.x * paletteScale, y: rect.y * paletteScale}, exports.ui.palette.offset);
        var max = Math.vectorAdd(min, {x: rect.w * paletteScale, y: rect.h * paletteScale});
        if (mousePos.x >= min.x && mousePos.x < max.x && mousePos.y >= min.y && mousePos.y < max.y) {
          exports.state.curTileID = i;
          exports.updatePalette();
          break;
        }
      }
      return true;
    } else {
      // place tile
      var tileIdx = exports.screenToTileIdx(mousePos);
      lastTileIdx = tileIdx;
      exports.putTile(tileIdx);
    }
    return true;
  } else {
    lastTileIdx = null;
  }
});

exports.actionMap.bind("kbm", "mouse1", function(down) {
  exports.state.dragMode = down ? "erase" : null;
  if (down) {
    var tileIdx = exports.screenToTileIdx(getMousePosition());
    lastTileIdx = tileIdx;
    exports.eraseTile(tileIdx);
    return true;
  } else {
    lastTileIdx = null;
  }
});

exports.actionMap.bind("kbm", "mouseMoved", function (x, y, dx, dy) {
  // update cursor
  if (exports.state.curTileset && !screenPointInPalette(getMousePosition())) {
    var cursorTile = exports.ui.cursorTile;
    var topLeft = ServerConnection.camera.screenToWorld({x: 0, y: 0});
    var off = exports.screenToTileIdx({x: x, y: y});
    cursorTile.offset = Math.vectorSub(Math.vectorScale(off, globals.TILE_SIZE), topLeft);
  }

  if (exports.state.dragMode) {
    var tileIdx = exports.screenToTileIdx({x: x, y: y});
    if (lastTileIdx && lastTileIdx.x === tileIdx.x && lastTileIdx.y === tileIdx.y)
      return;
    lastTileIdx = tileIdx;

    if (exports.state.dragMode === "put") {
      exports.putTile(tileIdx);
    } else if (exports.state.dragMode === "erase") {
      exports.eraseTile(tileIdx);
    }
  }
});

exports.actionMap.bind("kbm", "f", function (down) {
  if (down) {
    exports.state.mirrorHorizontal = !exports.state.mirrorHorizontal;
    exports.updatePalette();
    return true;
  }
});