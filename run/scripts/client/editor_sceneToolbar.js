"use strict";

var globals = require("scripts/globals.js");

exports.ui = (function() {
  var win = new Window();
  win.title = "Scene Management";

  var width = globals.screenWidth;
  var height = Math.floor(globals.screenHeight * 0.1);
  win.setFixedSize(width, height);

  var container = new BoxContainer();
  container.setOrientation(1);  // horizontal

  var currentScene = new Label();
  container.add(currentScene);

  var saveBtn = new Button();
  saveBtn.text = "Save";
  saveBtn.onClick = function() {
    ServerConnection.sendServerCmd('Editor_saveScene', currentScene.text);
  };
  container.add(saveBtn);

  var clearBtn = new Button();
  clearBtn.text = "Clear";
  clearBtn.onClick = function() {
    ServerConnection.sendServerCmd('Editor_clearScene', currentScene.text);
  };
  container.add(clearBtn);

  var loadBtn = new Button();
  loadBtn.text = "Load";
  loadBtn.onClick = function() {
    ServerConnection.sendServerCmd('Editor_loadScene', currentScene.text);
  };
  container.add(loadBtn);

  win.add(container);

  return {
    window: win,
    currentScene: currentScene
  };
})();


exports.open = function() {
  this.ui.window.open();

  var sceneName = ServerConnection.controlObject ? ServerConnection.controlObject.scene.name : "";
  this.ui.currentScene.text = sceneName;
};

exports.close = function() {
  this.ui.window.close();
};