"use strict";

var globals = require("scripts/globals.js");

exports.actionMap = new ActionMap();
var VERTEX_TO_MOUSE_DIST = 10;

exports.state = {
  selection: null,
  movingVertex: null,
};


// support functions
// returns index of nearest point on shape within maxDist (inclusive)
// returns null if no point within maxDist on shape exists
function findNearestPoint(shape, worldPt, maxDist) {
  if (!shape)
    return null;

  var bestIdx = null;
  var shapePos = shape.position;
  shape.vertices.forEach(function (v, idx) {
    v = Math.vectorAdd(v, shapePos);
    var dist = Math.vectorLen(Math.vectorSub(v, worldPt));
    if (dist <= maxDist) {
      maxDist = dist;
      bestIdx = idx;
    }
  });
  return bestIdx;
}

// https://stackoverflow.com/a/1501725
function sqr(x) { return x * x }
function dist2(v, w) { return sqr(v.x - w.x) + sqr(v.y - w.y) }
function distToSegmentSquared(p, v, w) {
  var l2 = dist2(v, w);
  if (l2 == 0) return dist2(p, v);
  var t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2;
  t = Math.max(0, Math.min(1, t));
  return dist2(p, { x: v.x + t * (w.x - v.x),
                    y: v.y + t * (w.y - v.y) });
}
function distToSegment(p, v, w) { return Math.sqrt(distToSegmentSquared(p, v, w)); }

// returns the line segment nearest to worldPt in shape
// (identified by the first vertex index)
function findNearestSegment(shape, worldPt) {
  var shapePos = shape.position;
  var vertices = shape.vertices;
  if (vertices.length < 3)
    return vertices.length - 1;  // special exception to require at least a triangle

  var bestDist = Infinity;
  var bestSegment = null;
  for (var i = 0; i < vertices.length; i++) {
    var v = Math.vectorAdd(shapePos, vertices[i]);
    var w = Math.vectorAdd(shapePos, vertices[(i + 1) % vertices.length]);
    var dist = distToSegmentSquared(worldPt, v, w);
    if (dist < bestDist) {
      bestDist = dist;
      bestSegment = i;
    }
  }
  return bestSegment;
}

exports.activate = function(obj, data) {
  exports.state.selection = obj;
  exports.state.movingVertex = null;
  this.actionMap.push();
};

exports.deactivate = function() {
  this.actionMap.pop();
};

exports.actionMap.bind("kbm", "mouse0", function (down) {
  var worldPt = ServerConnection.camera.screenToWorld(getMousePosition());
  var shape = exports.state.selection;

  // stop dragging vertex
  if (exports.state.movingVertex !== null) {
    var ghostID = ServerConnection.getGhostID(shape);
    var vertexIdx = exports.state.movingVertex;
    var newPos = Math.vectorSub(worldPt, shape.position);
    ServerConnection.sendServerCmd("ShapeEditor_moveVertex", ghostID, vertexIdx, newPos);
    exports.state.movingVertex = null;
    return true;
  }

  if (down) {
    if (isShiftPressed()) {
      // shift + click = add point
      var nearestSegmentIdx = findNearestSegment(shape, worldPt);
      var ghostID = ServerConnection.getGhostID(shape);
      var localPt = Math.vectorSub(worldPt, shape.position);
      ServerConnection.sendServerCmd("ShapeEditor_addVertex", ghostID, nearestSegmentIdx+1, localPt);
      return true;
    } else {
      // normal click = drag vertex near mouse
      var nearestPtIdx = findNearestPoint(shape, worldPt, VERTEX_TO_MOUSE_DIST);
      if (nearestPtIdx !== null) {
        exports.state.movingVertex = nearestPtIdx;
        return true;
      }
    }
  }

  return false;
});

exports.actionMap.bind("kbm", "mouse1", function (down) {
  if (down) {
    var shape = exports.state.selection;
    var worldPt = ServerConnection.camera.screenToWorld(getMousePosition());
    var nearestPtIdx = findNearestPoint(shape, worldPt, VERTEX_TO_MOUSE_DIST);
    if (nearestPtIdx !== null) {
      var ghostID = ServerConnection.getGhostID(shape);
      ServerConnection.sendServerCmd("ShapeEditor_removeVertex", ghostID, nearestPtIdx);

      // deleted the last vertex, and thus the whole shape, so stop editing
      if (shape.vertices.length == 1)
        exports.deactivate();

      return true;
    }
  }
});