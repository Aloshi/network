"use strict";

var globals = require("scripts/globals.js");

exports.inspectors = {};
exports.inspectors.default = require("scripts/client/editor_defaultInspector.js");
exports.inspectors.shape = require("scripts/client/editor_ShapeInspector.js");

exports.overlay = new EditorOverlay(ServerConnection);
exports.actionMap = new ActionMap();

exports.state = {};
exports.state.selection = null;
exports.state.creatingDef = null;
exports.state.curInspector = null;

exports.ui = {};

exports.ui.palette = (function() {
  var palette = {
    win: new Window(),
    scroll: new ScrollContainer(),
    gallery: new Gallery(),

    populate: function(entityDefs) {
      exports.state.creatingDef = null;
      this.gallery.clear();
      entityDefs.forEach(function (def) {
        this.gallery.add(def.name, getTexture(def.thumbnail, false, false), def);
      }, this);
    },
  };

  palette.scroll.add(palette.gallery);
  palette.win.add(palette.scroll);

  palette.win.title = "Entity Library";
  palette.win.canClose = false;

  palette.gallery.onSelected = function(idx) {
    if (idx === -1) {
      exports.cancelCreate();
      return;
    }

    var name = palette.gallery.getLabelText(idx);
    var def = palette.gallery.getUserData(idx);
    exports.startCreate(def);
  };

  var width = Math.floor(globals.screenWidth * 0.15);
  var height = Math.floor(globals.screenHeight * 0.45);
  palette.win.setMinimumSize(width, height);
  palette.win.setPosition(globals.screenWidth - width, globals.screenHeight * 0.1);

  return palette;
})();

exports.ui.inspector = (function () {
  var insp = {
    win: new Window(),
    scroll: new ScrollContainer(),
  };

  insp.win.add(insp.scroll);

  insp.win.title = "Edit Entity";
  insp.win.canClose = false;

  var width = Math.floor(globals.screenWidth * 0.15);
  var height = Math.floor(globals.screenHeight * 0.45);
  insp.win.setMinimumSize(width, height);
  insp.win.setPosition(globals.screenWidth - width, globals.screenHeight - height);

  return insp;
})();

exports.close = function() {
  this.overlay.hide();
  this.ui.palette.win.close();
  this.ui.inspector.win.close();
  this.actionMap.pop();
  this.isOpen = false;
  warn("Closing entityEditor");
};

exports.open = function() {
  this.overlay.show();
  this.ui.palette.win.open();
  this.ui.inspector.win.open();
  this.downloadEntityDefs();
  this.actionMap.push();
  this.isOpen = true;
  warn("Opening entityEditor");
};

exports.downloadEntityDefs = function() {
  // pull entity def list from server
  ServerConnection.sendServerCmdWithReply("EntityEditor_getEntityDefs")
    .then(function (entityDefs) {
      // update UI
      exports.ui.palette.populate(entityDefs);
    }, function(err) {
      error("Error downloading entity def list:\n  " + err);
    });
};

exports.cancelCreate = function() {
  exports.ui.palette.gallery.setSelectedIdx(-1);  // can cause this function to be double-called
  exports.state.creatingDef = null;
};

exports.cancelInspect = function() {
  if (exports.state.curInspector) {
    exports.state.curInspector.deactivate();
    exports.state.curInspector = null;
  }
  exports.state.selection = null;
  exports.overlay.highlightObject(null);
};

exports.startCreate = function(def) {
  exports.state.creatingDef = def;
  // TODO ui.palette.gallery.setSelected
};

exports.doCreate = function(pos) {
  if (!exports.state.creatingDef)
    return;

  ServerConnection.sendServerCmd("EntityEditor_create", exports.state.creatingDef.name, pos);
};

exports.startInspect = function(obj) {
  var ghostID = ServerConnection.getGhostID(obj);
  if (ghostID === 0) {
    warn("Cannot inspect obj - ghost ID is 0");
    return;
  }

  ServerConnection.sendServerCmdWithReply("EntityEditor_inspect", ghostID)
    .then(function (state) {
      exports.cancelCreate();
      exports.state.selection = obj;

      var newInspector = exports.inspectors[state.inspectorName] || exports.inspectors.default;
      if (exports.state.curInspector && exports.state.curInspector !== newInspector)
        exports.state.curInspector.deactivate();

      exports.state.curInspector = newInspector;
      newInspector.activate(obj, state);
      exports.overlay.highlightObject(obj);

      // re-register our actionmap so newInspector gets priority
      exports.actionMap.pop();
      exports.actionMap.push();
    }, function (err) {
      error("Could not inspect entity (ghost ID " + ghostID + "):\n  " + err);
    });
};

exports.actionMap.bind("kbm", "mouse0", function (down) {
  if (down) {
    var mouseWorldPos = ServerConnection.camera.screenToWorld(getMousePosition());
    if (exports.state.creatingDef) {
      exports.doCreate(mouseWorldPos);
    } else {
      // inspect - find whatever entity is under the mouse and call exports.startInspect with it
      var scene = ServerConnection.controlObject.scene;
      var objs = scene.findObjects(mouseWorldPos, mouseWorldPos, globals.TypeMasks.ANY);
      if (objs.length > 0) {
        exports.startInspect(objs[0]);
      } else {
        exports.cancelInspect();
      }
    }
    return true;
  }
});

exports.actionMap.bind("kbm", "escape", function (down) {
  if (down && (exports.state.creatingDef || exports.state.selection)) {
    exports.cancelCreate();
    exports.cancelInspect();
    return true;
  }
});