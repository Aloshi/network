"use strict";

var globals = require("scripts/globals.js");

var editor = {};
module.exports = editor;

//editor.modeToolbar = new Window();

editor.sceneToolbar = require('scripts/client/editor_sceneToolbar.js');
editor.tileEditor = require('scripts/client/editor_tileEditor.js');
editor.entityEditor = require('scripts/client/editor_entityEditor.js');

// hook up to global action map
var currentEditor = editor.tileEditor;
function setEditor(ed) {
  var wasOpen = currentEditor.isOpen;
  if (wasOpen)
    currentEditor.close();

  currentEditor = ed;
  if (wasOpen)
    currentEditor.open();
}

globals.actionMap.bind("kbm", "tab", function(down) {
  if (down) {
    if (!currentEditor.isOpen) {
      editor.sceneToolbar.open();
      currentEditor.open();
      ServerConnection.sendServerCmd("Editor_onEditorOpened");
    } else {
      editor.sceneToolbar.close();
      currentEditor.close();
      ServerConnection.sendServerCmd("Editor_onEditorClosed");
    }
    return true;
  }
});

// hotkeys for switching between editor
globals.actionMap.bind("kbm", "f1", function(down) {
  if (down) {
    setEditor(editor.tileEditor);
    return true;
  }
});
globals.actionMap.bind("kbm", "f2", function(down) {
  if (down) {
    setEditor(editor.entityEditor);
    return true;
  }
});