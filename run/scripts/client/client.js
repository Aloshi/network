var globals = require("scripts/globals.js");
require("scripts/client/editor.js");
require("scripts/client/coin.js");
/*
var logging = false;
globals.actionMap.bind("kbm", "ctrl f", function(down) {
  if (down) {
    if (!logging) {
      ServerConnection.logger.start("client.txt", false);
      print("Started client logger");
      ServerConnection.sendServerCmd("startLogger");
    } else {
      ServerConnection.logger.stop();
      print("Stopped client logger");
      ServerConnection.sendServerCmd("stopLogger");
    }
    logging = !logging;
  }
});

globals.actionMap.bind("kbm", "ctrl p", function (down) {
  if (!down)
    return;

  var analyzer = new NetAnalyzer();
  analyzer.load("client.txt");
  analyzer.load("server.txt");

  var PACKET_UPDATE = (1 << 0);
  var PACKET_MOVE = (1 << 1);
  var PACKET_ALL = (PACKET_UPDATE | PACKET_MOVE);
  var SENT = (1 << 0);
  var RECEIVED = (1 << 1);

  var clients = analyzer.clients();
  var me = clients[0];

  print("Client GUID: " + me);
  if (me === undefined) {
    warn("No clients found");
    return;
  }

  print("Update packets sent by server: " + analyzer.numEvents(PACKET_UPDATE, SENT, me));
  print("Update packets received by me: " + analyzer.numEvents(PACKET_UPDATE, RECEIVED, me));
  print("Move packets sent by me: " + analyzer.numEvents(PACKET_MOVE, SENT, me));
  print("Move packets received by server: " + analyzer.numEvents(PACKET_MOVE, RECEIVED, me));

  print("Average latency (all traffic between server + me): " + analyzer.avgLatency(PACKET_ALL, me) + "ms",
    ", updates: " + analyzer.avgLatency(PACKET_UPDATE, me) + "ms, moves: " + analyzer.avgLatency(PACKET_MOVE, me) + "ms");
  print("Average packet loss (all traffic between server + me): " + analyzer.avgPacketLoss(PACKET_ALL, me) +
    ", updates: " + analyzer.avgPacketLoss(PACKET_UPDATE, me) + ", moves: " + analyzer.avgPacketLoss(PACKET_MOVE, me));

  analyzer.delete();
});*/

globals.actionMap.bind("kbm", "ctrl space", function(down) {
  if (down) {
    ServerConnection.sendServerCmd("spawnBot");
    return true;
  }
});

globals.actionMap.push();