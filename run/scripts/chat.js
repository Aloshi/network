ServerCmd.ChatMessage = function (cl, msg) {
  for (var i = 0; i < Server.getClientCount(); i++) {
    Server.sendClientCmd(Server.getClient(i), 'AddToChat', msg);
  }
}

ClientCmd.AddToChat = function (msg) {
  ChatWindow.messageQueue.push(msg);
}

function createChatWindow() {
  var win = new Window();

  win.boxContainer = new BoxContainer();
  win.scrollContainer = new ScrollContainer();
  win.messageQueue = new MessageQueue();
  win.input = new LineEdit();

  win.scrollContainer.add(win.messageQueue);

  win.boxContainer.add(win.scrollContainer);
  win.boxContainer.add(win.input);

  win.add(win.boxContainer);

  win.input.onEnterPressed = function() {
    var msg = win.input.text;
    win.input.text = "";

    if (msg[0] == '/') {
      args = msg.split(' ');
      args[0] = args[0].slice(1);  // remove slash

      // if it's a valid command...
      if (args.length > 0 && args[0].length > 0)
        ServerConnection.sendServerCmd.apply(args);
    } else {
      ServerConnection.sendServerCmd('ChatMessage', msg);
    }
  }

  win.title = "Chat";
  win.canClose = false;
  win.setMinimumSize(350, 250);
  return win;
}

exports.ChatWindow = createChatWindow();