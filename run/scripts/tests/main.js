var test = {
  client: function() {
    ServerConnection.logger.start("netlog_client.txt", false);
    print("Logging started on client. Connecting to server in 1000ms.");
    schedule(1000, function() {
      ServerConnection.connect("127.0.0.1", 28000);
      print("Idling for 8000ms.");
    });
    schedule(9000, function() {
      print("Disconnecting.");
      ServerConnection.disconnect();
    });
    schedule(10000, function() {
      print("Exiting client.");
      ServerConnection.logger.stop();
      exit(0);
    });

  },

  server: function() {
    print("Logging started on server.");
    Server.logger.start("netlog_server.txt", false);
    schedule(3000, function() {
      print("Beginning test.");
      print("  Spawning bots.");
      var scene = SceneManager.getScene("testScene");
      var bots = [];
      for (var i = 0; i < 4; i++) {
        var bot = new AIPlayer(scene);
        bot.setPosition(400 + i * 40, 500 - i*10);
        bot.setDirection(0, -1);  // jump
        bots.push(bot);
      }
      print("  Waiting for 5000ms.");

      schedule(5000, function() {
        print("  Cleaning up.");
        for (var i = 0; i < bots.length; i++) {
          bots[i].delete();
        }
      });
    });
    schedule(10000, function() {
      print("Exiting server.");
      Server.logger.stop();
      exit(0);
    });
  }
};

if (test && ProgramArgs && ProgramArgs.slice(1).includes('--net-test')) {
  print("-- beginning network test --");
  if (isClient())
    test.client();
  if (isServer())
    test.server();
}