exports.actionMap = new ActionMap();
exports.dpiScale = getDPIScale();
exports.screenWidth = 800 * exports.dpiScale;
exports.screenHeight = 600 * exports.dpiScale;
exports.TILE_SIZE = 16;
exports.CHUNK_SIZE = 16;
exports.TypeMasks = {
  ANY: 0xFFFFFFFF,
  DEFAULT: (1 << 0) | (1 << 1),

  DYNAMIC: (1 << 0),
  STATIC: (1 << 1),
  PLAYER: (1 << 2),
};
exports.NetObjectTypeIDs = {
  PLAYER: 1,
  STATICSHAPE: 2,
  TILECHUNK: 3,
  TILESET: 4,
  ENTITY: 5,
  COIN: 6,
  OBSERVER: 7,
};