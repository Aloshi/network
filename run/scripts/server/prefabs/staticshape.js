module.exports = {
  name: "Static Shape",
  thumbnail: "assets/misc/triangle.png",
  create: function (scene, pos) {
    var shape = new StaticShape(scene);
    shape.position = pos;
    shape.vertices = [{x: -16.0, y: -16.0}, {x: 16.0, y: 16.0}, {x: -16.0, y: 16.0}];
    return shape;
  },
  serialize: function (obj) {
    return {"vertices": obj.vertices};
  },
  deserialize: function (obj, data) {
    obj.vertices = data.vertices;
  },
  inspectorName: "shape",
  fields: []
};