module.exports = {
  name: "Coin",
  thumbnail: "assets/sprites/coin.png",
  create: function (scene, pos) {
    var coin = new Coin(scene);
    coin.position = pos;
    coin.value = 1;
    return coin;
  },

  fields: [
    {
      name: "value",
      type: "enum",
      values: [ "1", "5", "20" ],
      get: function (obj) {
        return obj.value.toString();
      },
      set: function (obj, val) {
        obj.value = parseInt(val);
      },
    }
  ]
};

Coin.prototype.onCoinCollectedServer = function(client) {
  print("Coin collected on server");
};