"use strict";

// adjust vertices and shape position so that shape position is
// at the worldspace center of all the vertices
function recenterStaticShape(shape) {
  var verts = shape.vertices;
  if (verts.length < 2)
    return;

  // find the center of the shape by taking the average of the vertices
  var localCenter = {x: 0.0, y: 0.0};
  for (var i = 0; i < verts.length; i++) {
    localCenter = Math.vecAdd(localCenter, verts[i]);
  }
  localCenter = Math.vecScale(localCenter, 1.0 / verts.length);

  // in the 'centered' case, localCenter is 0, 0
  // so we just need to offset verts by -localCenter and do the inverse to shape.position
  for (var i = 0; i < verts.length; i++) {
    verts[i] = Math.vecSub(verts[i], localCenter);
  }
  shape.vertices = verts;
  shape.position = Math.vecAdd(shape.position, localCenter);
};

ServerCmd.ShapeEditor_addVertex = function(cl, shapeID, vertexIdx, vertex) {
  var shape = cl.resolveGhostID(shapeID);
  if (!shape)
    throw "could not find shape with ghost ID " + shapeID;

  var newVerts = shape.vertices;
  if (typeof(vertexIdx) !== "number" || vertexIdx < 0 || vertexIdx > newVerts.length)
    throw "invalid vertex index '" + vertexIdx + "'";

  newVerts.splice(vertexIdx, 0, vertex);
  shape.vertices = newVerts;
  recenterStaticShape(shape);
};

ServerCmd.ShapeEditor_moveVertex = function(cl, shapeID, vertexIdx, vertex) {
  var shape = cl.resolveGhostID(shapeID);
  if (!shape)
    throw "could not find shape with ghost ID " + shapeID;

  var newVerts = shape.vertices;
  if (typeof(vertexIdx) !== "number" || vertexIdx < 0 || vertexIdx >= newVerts.length)
    throw "invalid vertex index '" + vertexIdx + "'";

  newVerts[vertexIdx] = vertex;
  shape.vertices = newVerts;
  recenterStaticShape(shape);
};

ServerCmd.ShapeEditor_removeVertex = function(cl, shapeID, vertexIdx) {
  var shape = cl.resolveGhostID(shapeID);
  if (!shape)
    throw "could not find shape with ghost ID " + shapeID;

  var newVerts = shape.vertices;
  if (typeof(vertexIdx) !== "number" || vertexIdx < 0 || vertexIdx >= newVerts.length)
    throw "invalid vertex index '" + vertexIdx + "'";

  newVerts.splice(vertexIdx, 1);
  if (newVerts.length == 0) {
    shape.safeDelete();
  } else {
    shape.vertices = newVerts;
  }
};