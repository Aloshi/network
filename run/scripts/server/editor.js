exports.tileEditor = require('scripts/server/editor_tileEditor.js');

exports.entityEditor = require('scripts/server/editor_entityEditor.js');
exports.shapeEditor = require('scripts/server/editor_shapeEditor.js');

// register entity defs
exports.entityEditor.registerEntity(require('scripts/server/prefabs/coin.js'));
exports.entityEditor.registerEntity(require('scripts/server/prefabs/staticShape.js'));

// scene i/o
var ChunkManager = require('scripts/server/chunk-manager.js');
var sceneDataDir = "assets/scenes/";
ServerCmd.Editor_saveScene = function(client, sceneName) {
  var scene = SceneManager.getScene(sceneName);
  var chunkData = ChunkManager.serializeScene(scene);
  var entityData = exports.entityEditor.serializeScene(scene);

  var data = {
    chunkData: chunkData,
    entityData: entityData
  };

  var file = new File(sceneDataDir + sceneName + ".json", "w");
  file.write(JSON.stringify(data));
  file.close();
};

ServerCmd.Editor_clearScene = function(client, sceneName) {
  var scene = SceneManager.getScene(sceneName);
  ChunkManager.deleteAll(scene);
  exports.entityEditor.deleteAll(scene);
};

ServerCmd.Editor_loadScene = function(client, sceneName) {
  // clear before load to avoid overlaps
  ServerCmd.Editor_clearScene(client, sceneName);

  var file = new File(sceneDataDir + sceneName + ".json", "r");
  var data = JSON.parse(file.readAll());
  file.close();

  var scene = SceneManager.getScene(sceneName);
  ChunkManager.deserializeScene(scene, data.chunkData);
  exports.entityEditor.deserializeScene(scene, data.entityData);
};

ServerCmd.Editor_onEditorOpened = function(client) {
  if (client.observer && client.controlObject === client.player) {
    client.observer.position = client.player.position;
    client.controlObject = client.observer;
  }
};

ServerCmd.Editor_onEditorClosed = function(client) {
  if (client.player && client.controlObject === client.observer) {
    client.player.position = client.observer.position;
    client.controlObject = client.player;
  }
};