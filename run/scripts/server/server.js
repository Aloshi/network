require("assets/tilesets/tilesets.js");
require("scripts/server/editor.js");
require("scripts/server/bot.js");

Server.onClientConnected = function(client) {
  client.player = new Player(SceneManager.getScene("testScene"));
  client.player.setPosition(400, 500);

  client.observer = new Observer(SceneManager.getScene("testScene"));

  client.controlObject = client.player;

  // trigger test
  /*var entity = new Entity(player.scene);
  entity.setPosition(400, 360);
  var trigger = new TriggerComponent(entity);
  trigger.setRectSize(64, 64);
  trigger.onEnter = function(obj) {
    print("Object entered :D");
  }
  trigger.onLeave = function(obj) {
    print("Object left :(");
  }*/
};

Server.onClientDisconnected = function(client) {
  if (client.player)
    client.player.delete();
  if (client.observer)
    client.observer.delete();
};

/*ServerCmd.startLogger = function(client) {
  Server.logger.start("server.txt", false);
  print("Started server net logger");
};

ServerCmd.stopLogger = function(client) {
  Server.logger.stop();
  print("Stopped server net logger");
};*/