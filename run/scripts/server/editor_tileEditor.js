"use strict";

var tilesets = require("assets/tilesets/tilesets.js");
var globals = require("scripts/globals.js");
var ChunkManager = require("scripts/server/chunk-manager.js");

ServerCmd.EditorSetTile = function (cl, sceneName, layer, wx, wy, id, tilesetName, rotation, mirrorHorizontal) {
  var scene = SceneManager.getScene(sceneName);

  var tileset = tilesets.find(function(t) { return t.name === tilesetName; });
  if (tileset === undefined) {
    print("Could not find tileset '" + tilesetName + "'");
    return;
  }

  var chunk = ChunkManager.getChunkContaining(scene, wx, wy);
  var cx = ((wx % globals.CHUNK_SIZE) + globals.CHUNK_SIZE) % globals.CHUNK_SIZE;
  var cy = ((wy % globals.CHUNK_SIZE) + globals.CHUNK_SIZE) % globals.CHUNK_SIZE;
  chunk.setTile(layer, cx, cy, tileset, id, rotation, mirrorHorizontal);
};

ServerCmd.EditorEraseTile = function (cl, sceneName, layer, wx, wy) {
  var scene = SceneManager.getScene(sceneName);
  var chunk = ChunkManager.getChunkContaining(scene, wx, wy);

  // handle negative numbers
  var cx = ((wx % globals.CHUNK_SIZE) + globals.CHUNK_SIZE) % globals.CHUNK_SIZE;
  var cy = ((wy % globals.CHUNK_SIZE) + globals.CHUNK_SIZE) % globals.CHUNK_SIZE;
  chunk.setTile(layer, cx, cy, null, 0, 0, false);
};