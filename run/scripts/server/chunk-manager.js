"use strict";

var globals = require("scripts/globals.js");
var tilesets = require("assets/tilesets/tilesets.js");  // for deserialize

var ChunkManager = {};
module.exports = ChunkManager;

ChunkManager.getChunkContaining = function(scene, x, y) {
  var cx = Math.floor(x / globals.CHUNK_SIZE);
  var cy = Math.floor(y / globals.CHUNK_SIZE);

  if (scene.chunks === undefined)
    scene.chunks = [];

  var chunk = scene.chunks.find(function (c) { return c.chunkX === cx && c.chunkY === cy; });
  if (chunk)
    return chunk;

  warn("Creating new TileChunk for (" + cx + ", " + cy + ") from tileIdx (" + x + ", " + y + ")");
  chunk = new TileChunk(scene);
  chunk.setPosition(cx * globals.CHUNK_SIZE * globals.TILE_SIZE, cy * globals.CHUNK_SIZE * globals.TILE_SIZE);
  chunk.chunkX = cx;
  chunk.chunkY = cy;
  scene.chunks.push(chunk);

  return chunk;
};

ChunkManager.serializeScene = function(scene) {
  var serializer = new ChunkSerializer();

  var chunks = scene.chunks || [];
  for (var i = 0; i < chunks.length; i++) {
    serializer.addChunk(chunks[i]);
  }

  return serializer.serialize();
};

ChunkManager.deserializeScene = function(scene, chunkData) {
  if (!chunkData)
    return;

  var serializer = new ChunkSerializer();
  scene.chunks = serializer.deserialize(chunkData, scene, tilesets);
  scene.chunks.forEach(function (chunk) {
    var pos = chunk.position;
    chunk.chunkX = Math.floor(pos.x / (globals.CHUNK_SIZE * globals.TILE_SIZE));
    chunk.chunkY = Math.floor(pos.y / (globals.CHUNK_SIZE * globals.TILE_SIZE));
    print("pos: " + chunk.chunkX + ", " + chunk.chunkY);
  });
};

ChunkManager.deleteAll = function(scene) {
  if (scene.chunks === undefined)
    return;

  for (var i = 0; i < scene.chunks.length; i++) {
    scene.chunks[i].delete();
  }
  scene.chunks = undefined;
};