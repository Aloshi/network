"use strict";

var globals = require("scripts/globals.js");

var validFieldTypes = ['string', 'vector2', 'enum', 'number', 'integer'];

var entityDefs = {};

// ----- API

exports.registerEntity = function(def) {
  // validation
  if (typeof def.name !== 'string')
    throw "Entity definition missing name";

  if (!Array.isArray(def.fields))
    throw "Entity definition '" + def.name + "' missing fields";

  // validate fields
  var reservedNames = ["position", "defName"];
  var errors = [];
  def.fields.forEach(function (f, i) {
    var fieldName = f.name || ("field #" + i);
    var name = "Entity '" + def.name + "' field '" + fieldName + "'";

    if (typeof f.name !== 'string')
      errors.push(name + " missing name");
    else if (reservedNames.includes(f.name))
      errors.push(name + " using reserved field name");

    if (typeof f.type !== 'string') {
      errors.push(name + " missing type");
    } else {
      if (!validFieldTypes.includes(f.type))
        errors.push(name + " has unknown type '" + f.type + "'");

      if (f.type === 'enum' && (!Array.isArray(f.values) || f.values.length == 0))
        errors.push(name + " is enum typed, but does not define an array 'values'");
    }

    if (typeof f.get !== 'function')
      errors.push(name + " missing get function");
    if (typeof f.set !== 'function')
      errors.push(name + " missing set function");
  });

  if (errors.length > 0)
    throw errors.join("\n");

  if (entityDefs[def.name] != null)
    warn("Re-registering entity '" + def.name + "'");

  // TODO make a copy of def here so it can't be mutated accidentally...
  entityDefs[def.name] = def;
};

function getEntityDefForObject(obj) {
  return entityDefs[obj.entityDefName];
};

function getSceneEntitiesGroup(scene) {
  if (scene.editorEntities)
    return scene.editorEntities;

  scene.editorEntities = new SceneObjectGroup();
  return scene.editorEntities;
}

// serialization stuff
exports.serializeEntity = function(obj) {
  var def = getEntityDefForObject(obj);

  var out = {};
  out["position"] = obj.position;
  out["defName"] = def.name;

  if (def.serialize) {
    Object.assign(out, def.serialize(obj));
  } else {
    // default serialization is to just serialize every field + position
    def.fields.forEach(function (field) {
      out[field.name] = field.get(obj);
    });
  }

  return out;
};

exports.serializeScene = function(scene) {
  var out = [];

  var entities = getSceneEntitiesGroup(scene);
  for (var i = 0; i < entities.size(); i++) {
    var entity = entities.get(i);
    out.push(this.serializeEntity(entity));
  }

  return out;
};

exports.deserializeScene = function(scene, data) {
  if (!data)
    return;

  var entities = getSceneEntitiesGroup(scene);
  var missingDefs = [];
  var missingFields = [];

  data.forEach(function (entityData) {
    var def = entityDefs[entityData.defName];
    if (!def) {
      missingDefs.push(entityData.defName);
      return;
    }

    var entity = def.create(scene, entityData.position);
    entity.entityDefName = def.name;
    entities.add(entity);

    if (def.deserialize) {
      def.deserialize(entity, entityData);
    } else {
      // default deserialization is just apply all fields
      def.fields.forEach(function (field) {
        if (entityData[field.name])
          field.set(entity, entityData[field.name]);
        else
          missingFields.push(def.name + '.' + field.name);
      });
    }
  });

  // TODO remove duplicates
  if (missingDefs.length)
    print('Missing definitions: ' + JSON.stringify(missingDefs));
  if (missingFields.length)
    print('Missing fields: ' + JSON.stringify(missingFields));
};

// deletes all entities created by the editor in a given screen
exports.deleteAll = function(scene) {
  var entityGroup = getSceneEntitiesGroup(scene);
  print("Clearing " + entityGroup.size() + " entities.");
  entityGroup.deleteAll();
};

// ----- ServerCmds

ServerCmd.EntityEditor_getEntityDefs = function(client) {
  // strip functions (which can't be used on the client)
  var defsClient = Object.values(entityDefs).map(function (e) {
    return {
      name: e.name,
      thumbnail: e.thumbnail,
      fields: e.fields.map(function (f) {
        return {
          name: f.name,
          type: f.type,
          values: f.values  // for type == "enum" case
        };
      }),
    };
  });

  return defsClient;
};

ServerCmd.EntityEditor_create = function(client, name, pos) {
  var entityDef = entityDefs[name];
  if (entityDef == null)
    throw "Unknown entity def '" + name + "'";

  if (entityDef.create == null)
    throw "No create function available for entity def '" + name + "'";

  var scene = client.controlObject.scene;
  var entity = entityDef.create(client.controlObject.scene, pos);
  entity.entityDefName = entityDef.name;
  getSceneEntitiesGroup(scene).add(entity);
};

ServerCmd.EntityEditor_inspect = function(client, ghostID) {
  var obj = client.resolveGhostID(ghostID);
  if (obj == null)
    throw "Object for ghost ID '" + ghostID + "' does not exist";

  var entityDef = getEntityDefForObject(obj);
  if (entityDef == null)
    throw "Object does not have an entity def";

  var data = {
    entityDefName: entityDef.name,
    inspectorName: entityDef.inspectorName,
    fieldValues: entityDef.fields.map(function (field) {
      return field.get(obj);
    })
  };

  return data;
};
