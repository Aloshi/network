Math.vectorAdd = function (v1, v2) {
  return {x: v1.x + v2.x, y: v1.y + v2.y};
};
Math.vecAdd = Math.vectorAdd;

Math.vectorSub = function (v1, v2) {
  return {x: v1.x - v2.x, y: v1.y - v2.y};
};
Math.vecSub = Math.vectorSub;

Math.vectorScale = function (v, s) {
  return {x: v.x * s, y: v.y * s};
};
Math.vecScale = Math.vectorScale;

Math.vectorLen = function(v) {
  return Math.sqrt(v.x*v.x + v.y*v.y);
};
Math.vecLen = Math.vectorLen;

// polyfills

Array.prototype.find = function(f, thisArg) {
  for (var i = 0; i < this.length; i++) {
    var elm = this[i];
    if (f.call(thisArg, elm, i)) {
      return elm;
    }
  }
  return undefined;
};

// https://tc39.github.io/ecma262/#sec-array.prototype.includes
if (!Array.prototype.includes) {
  Object.defineProperty(Array.prototype, 'includes', {
    value: function(searchElement, fromIndex) {

      // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If len is 0, return false.
      if (len === 0) {
        return false;
      }

      // 4. Let n be ? ToInteger(fromIndex).
      //    (If fromIndex is undefined, this step produces the value 0.)
      var n = fromIndex | 0;

      // 5. If n ≥ 0, then
      //  a. Let k be n.
      // 6. Else n < 0,
      //  a. Let k be len + n.
      //  b. If k < 0, let k be 0.
      var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

      function sameValueZero(x, y) {
        return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
      }

      // 7. Repeat, while k < len
      while (k < len) {
        // a. Let elementK be the result of ? Get(O, ! ToString(k)).
        // b. If SameValueZero(searchElement, elementK) is true, return true.
        // c. Increase k by 1. 
        if (sameValueZero(o[k], searchElement)) {
          return true;
        }
        k++;
      }

      // 8. Return false
      return false;
    }
  });
}

// https://github.com/tc39/proposal-object-values-entries/blob/master/polyfill.js
const reduce = Function.bind.call(Function.call, Array.prototype.reduce);
const isEnumerable = Function.bind.call(Function.call, Object.prototype.propertyIsEnumerable);
const concat = Function.bind.call(Function.call, Array.prototype.concat);
const keys = Reflect.ownKeys;

if (!Object.values) {
  Object.values = function values(O) {
    return reduce(keys(O), function(v, k) { return concat(v, typeof k === 'string' && isEnumerable(O, k) ? [O[k]] : []) }, []);
  };
}

if (!Object.entries) {
  Object.entries = function entries(O) {
    return reduce(keys(O), function(e, k) { return concat(e, typeof k === 'string' && isEnumerable(O, k) ? [[k, O[k]]] : [])}, []);
  };
}

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
if (typeof Object.assign != 'function') {
  // Must be writable: true, enumerable: false, configurable: true
  Object.defineProperty(Object, "assign", {
    value: function assign(target, varArgs) { // .length of function is 2
      'use strict';
      if (target == null) { // TypeError if undefined or null
        throw new TypeError('Cannot convert undefined or null to object');
      }

      var to = Object(target);

      for (var index = 1; index < arguments.length; index++) {
        var nextSource = arguments[index];

        if (nextSource != null) { // Skip over if undefined or null
          for (var nextKey in nextSource) {
            // Avoid bugs when hasOwnProperty is shadowed
            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
              to[nextKey] = nextSource[nextKey];
            }
          }
        }
      }
      return to;
    },
    writable: true,
    configurable: true
  });
}

// ---

if (isServer()) {
  require("scripts/server/server.js");
  print('Loaded server.js');
}
if (isClient()) {
  require("scripts/client/client.js");
  print('Loaded client.js');

  if (ProgramArgs && ProgramArgs.includes('--connect')) {
    var idx = ProgramArgs.indexOf('--connect') + 1;
    var str = ProgramArgs[idx].split(':');
    var host = str[0];
    var port = parseInt(str[1] || '28000');
    print("Connecting to " + host + " on port " + port + "...");
    ServerConnection.connect(host, port);
  }
}

require("scripts/tests/main.js");