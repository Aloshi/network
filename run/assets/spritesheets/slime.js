module.exports = {
  path: "assets/spritesheets/slime.png",
  nColumns: 8,
  tileWidth: 32,
  tileHeight: 32,
  animations: [
    {
      name: "stand",
      startX: 0,
      startY: 0,
      nFrames: 1
    },
    {
      name: "walk",
      startX: 0,
      startY: 0,
      nFrames: 4,
      frameTime: 100
    },
    {
      name: "jump",
      startX: 4,
      startY: 0,
      nFrames: 15,
      frameTime: 100
    }
  ]
};