var ts_data = [
  {
    name: "Brick",
    texture: "assets/tilesets/brick.png",
    types: [
      {x: 0, y: 0, w: 16, h: 16},
      {x: 16, y: 0, w: 16, h: 16},
      {x: 32, y: 0, w: 16, h: 16},

      {x: 0, y: 16, w: 16, h: 16},
      {x: 16, y: 16, w: 16, h: 16},
      {x: 32, y: 16, w: 16, h: 16},
      
      {x: 0, y: 32, w: 16, h: 16},
      {x: 16, y: 32, w: 16, h: 16},
      {x: 32, y: 32, w: 16, h: 16},
    ]
  },
  {
    name: "Grass",
    texture: "assets/tilesets/grass.png",
    types: [
      {x: 0, y: 0, w: 16, h: 16},
      {x: 16, y: 0, w: 16, h: 16},
      {x: 32, y: 0, w: 16, h: 16},

      {x: 0, y: 16, w: 16, h: 16},
      {x: 16, y: 16, w: 16, h: 16},
      {x: 32, y: 16, w: 16, h: 16},
      
      {x: 0, y: 32, w: 16, h: 16},
      {x: 16, y: 32, w: 16, h: 16},
      {x: 32, y: 32, w: 16, h: 16},
    ]
  },
];

module.exports = ts_data.map(function (data) {
  var ts = new TileSet();
  ts.name = data.name;
  ts.texture = getTexture(data.texture, false, false);

  shape = [
    {x: 0, y: 0},
    {x: 16, y: 0},
    {x: 16, y: 16},
    {x: 0, y: 16}
  ];

  data.types.forEach(function (t) {
    ts.addTileType({x: t.x, y: t.y}, {x: t.w, y: t.h}, shape);
  });

  return ts;
});


var placeholder = new TileSet();
placeholder.name = "Placeholder";
placeholder.texture = getTexture("assets/tilesets/placeholder.png", false, false);

for (var i = 0; i < 5; i++) {
  // texture coords
  var x = (i % 8) * 16;
  var y = Math.floor(i / 8) * 16;
  var w = 16;
  var h = 16;

  var shape = [];  // no collision
  if (i === 0) {
    shape = [
      {x: 0, y: 0},
      {x: 16.0001, y: 16.0001},
      {x: 0, y: 16}
    ];
  } else if (i === 1) {
    shape = [
      {x: 0, y: 8},
      {x: 16, y: 12},
      {x: 16, y: 16},
      {x: 0, y: 16}
    ];
  } else if (i === 2) {
    shape = [
      {x: 0, y: 12},
      {x: 16, y: 16.0001},
      {x: 0, y: 16}
    ];
  } else if (i === 3) {
    shape = [
      {x: 0, y: 0},
      {x: 16, y: 0},
      {x: 16, y: 16},
      {x: 0, y: 16}
    ];
  } else if (i === 4) {
    shape = [
      {x: 0, y: 8},
      {x: 16, y: 8},
      {x: 16, y: 16},
      {x: 0, y: 16}
    ];
  }

  placeholder.addTileType({x: x, y: y}, {x: w, y: h}, shape);
}

module.exports.push(placeholder);


var cf = new TileSet();
cf.name = "CrystalForest";
cf.texture = getTexture("assets/tilesets/crystalforest.png", false, false);

for (var i = 1; i < 5*8+1; i++) {
  // texture coords
  var x = (i % 8) * 8;
  var y = Math.floor(i / 8) * 8;
  var w = 8;
  var h = 8;

  var shape = [];  // no collision

  if ([1, 2, 3].includes(i)) {  // square collision
    shape = [
      {x: 0, y: 0},
      {x: 16, y: 0},
      {x: 16, y: 16},
      {x: 0, y: 16}
    ];
  }
  if (i === 4) {
    shape = [
      {x: 0, y: 0},
      {x: 12, y: 16},
      {x: 0, y: 16}
    ];
  } else if (i === 7) {
    shape = [
      {x: 0, y: 0},
      {x: 12, y: 0},
      {x: 16, y: 6},
      {x: 16, y: 16},
      {x: 0, y: 16},
    ];
  } else if (i === 8) {
    shape = [
      {x: 0, y: 4},
      {x: 12, y: 16},
      {x: 0, y: 16},
    ];
  } else if (i === 9) {
    shape = [
      {x: 0, y: 2},
      {x: 16, y: 2},
      {x: 16, y: 16},
      {x: 0, y: 16},
    ];
  } else if (i === 10 || i === 12) {
    shape = [
      {x: 0, y: 0},
      {x: 12, y: 0},
      {x: 16, y: 4},
      {x: 16, y: 16},
      {x: 0, y: 16},
    ];
  }

  cf.addTileType({x: x, y: y}, {x: w, y: h}, shape);
}

module.exports.push(cf);