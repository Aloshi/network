#!/usr/bin/python

import argparse
import filecmp
import re
import sys
import os
import posixpath
from io import StringIO
from collections import defaultdict

class DukClass:
  def __init__(this, class_name, class_type, parent_type, doc_string):
    this.class_name = class_name
    this.class_type = class_type
    this.parent_type = parent_type
    this.doc_string = doc_string

  def __str__(this):
    out = []
    if this.parent_type and this.parent_type != "void":
      out.append("dukglue_set_base_class<" + this.parent_type + ", " + this.class_type + ">(ctx);")
    return "\n".join(out)

class DukConstructor:
  # arg_types is a list of strings
  def __init__(this, class_type, ctor_name, managed, arg_types):
    this.class_type = class_type
    this.ctor_name = ctor_name
    this.managed = managed
    this.arg_types = [at for at in arg_types if at]

  def __str__(this):
    ctor_tp_args = this.class_type
    if len(this.arg_types) > 0:
      ctor_tp_args += ", " + ", ".join(this.arg_types)

    func = None
    if this.managed:
      func = "dukglue_register_constructor_managed"
    else:
      func = "dukglue_register_constructor"


    deleter = "\ndukglue_register_delete<" + this.class_type + ">(ctx);" if not this.managed else ""
    return func + "<" + ctor_tp_args + ">(ctx, \"" + this.ctor_name + "\");" + deleter

class DukMethod:
  def __init__(this, class_type, method_name, method_ptr, doc_string, is_varargs = False):
    this.class_type = class_type
    this.method_name = method_name
    this.method_ptr = method_ptr
    this.doc_string = doc_string
    this.is_varargs = is_varargs

  def __str__(this):
    if this.is_varargs:
      return "dukglue_register_method_varargs(ctx, " + this.method_ptr + ", \"" + this.method_name + "\");"
    else:
      return "dukglue_register_method(ctx, " + this.method_ptr + ", \"" + this.method_name + "\");"

class DukProperty:
  def __init__(this, class_type, prop_name, getter_ptr, setter_ptr, doc_string):
    this.class_type = class_type
    this.prop_name = prop_name
    this.getter_ptr = getter_ptr
    this.setter_ptr = setter_ptr
    this.doc_string = doc_string

  def __str__(this):
    return "dukglue_register_property(ctx, " + this.getter_ptr + ", " + this.setter_ptr + ", \"" + this.prop_name + "\");"

class DukFunction:
  def __init__(this, func_name, func_ptr, doc_string):
    this.func_name = func_name
    this.func_ptr = func_ptr
    this.doc_string = doc_string

  def __str__(this):
    return "dukglue_register_function(ctx, " + this.func_ptr + ", \"" + this.func_name + "\");"

def make_unix_path(p):
  return os.path.normpath(p).replace('\\', '/')

def remove_prefix(s, prefix):
  if s.startswith(prefix):
    return s[len(prefix):]

def dukbind_parse(filePath, include_dirs):
  # SCRIPT_CLASS(class_name, class_type, parent_type, doc_string)
  # SCRIPT_CONSTRUCTOR(class_type, constructor_name, arg_types...)
  # SCRIPT_METHOD(class_type, method_name, method_ptr, doc_string)
  # SCRIPT_PROPERTY(class_type, prop_name, getter_ptr, setter_ptr, doc_string)
  # SCRIPT_FUNCTION(func_name, func_ptr, doc_string)

  with open(filePath, 'r') as f:
    buff = f.read()

    # strip comments (// lines and /* */ pairs)
    buff = re.sub(r'//.*$', "", buff, flags=re.MULTILINE)
    buff = re.sub(r'/\*.*?\*/', "", buff, flags=re.MULTILINE | re.DOTALL)

    includes = []

    # include the file we are parsing if it is a header
    if filePath.endswith(('.h', '.hpp', '.hxx')):
      includes.append(make_unix_path(remove_prefix(filePath, os.path.commonprefix([filePath] + include_dirs))).lstrip('/'))
      # if we're including this file, we don't need to include what it includes
    else:
      # <> paths always exist
      includes += [make_unix_path(m) for m in re.findall(r'#include\s+<(.*?)>', buff)]

      # relative paths need to be validated
      rel_root = os.path.dirname(f.name)
      for m in re.findall(r'#include\s+"(.*?)"', buff):
        path = make_unix_path(os.path.join(rel_root, m))
        if os.path.exists(path):
          prefix = os.path.commonprefix([path] + include_dirs)
          path = remove_prefix(path, prefix)
          path = path.lstrip('/')

          includes.append(path)
        else:
          # if "" fails, we are supposed to treat it like it's a <>, see:
          # https://stackoverflow.com/questions/21593/what-is-the-difference-between-include-filename-and-include-filename
          includes.append(make_unix_path(m))

    parsers = [
      (re.compile('SCRIPT_CLASS\\(\\s*"(.*?)"\\s*,\\s*(.*),\\s*(.*),\\s*"(.*?)"\\s*\\)'),
        lambda m: DukClass(m[0], m[1], m[2], m[3])),
      (re.compile('SCRIPT_CONSTRUCTOR\\(\\s*(.*?),\\s*"(.*?)"\\s*,?\\s*(.*?)\\s*\\)'),
        lambda m: DukConstructor(m[0], m[1], False, [arg.strip() for arg in m[2].split(',')])),
      (re.compile('SCRIPT_CONSTRUCTOR_MANAGED\\(\\s*(.*?),\\s*"(.*?)"\\s*,?\\s*(.*?)\\s*\\)'),
        lambda m: DukConstructor(m[0], m[1], True, [arg.strip() for arg in m[2].split(',')])),
      (re.compile('SCRIPT_METHOD\\(\\s*(.*?),\\s*"(.*?)"\\s*,\\s*(.*?)\\s*,\\s*"(.*?)"\\s*\\)'),
        lambda m: DukMethod(m[0], m[1], m[2], m[3], False)),
      (re.compile('SCRIPT_METHOD_VARARGS\\(\\s*(.*?),\\s*"(.*?)"\\s*,\\s*(.*?)\\s*,\\s*"(.*?)"\\s*\\)'),
        lambda m: DukMethod(m[0], m[1], m[2], m[3], True)),
      (re.compile('SCRIPT_PROPERTY\\(\\s*(.*?),\\s*"(.*?)"\\s*,\\s*(.*?)\\s*,\\s*(.*?)\\s*,\\s*"(.*?)"\\s*\\)'),
        lambda m: DukProperty(m[0], m[1], m[2], m[3], m[4])),
      (re.compile('SCRIPT_FUNCTION\\(\\s*"(.*?)"\\s*,\\s*(.*?)\\s*,\\s*"(.*?)"\\s*\\)'),
        lambda m: DukFunction(m[0], m[1], m[2])),
    ]

    keywords = ['SCRIPT_CLASS', 'SCRIPT_METHOD', 'SCRIPT_PROPERTY', 'SCRIPT_CONSTRUCTOR', 'SCRIPT_FUNCTION']
    expected_matches = 0
    for word in keywords:
      expected_matches += len(re.findall(word, buff))

    defs = []
    for p in parsers:
      matches = re.findall(p[0], buff)
      for match in matches:
        defs.append(p[1](match))

    diff = expected_matches - len(defs)
    if diff != 0 and not filePath.endswith("ScriptEngine.h"):  # ignore file with definition of macros
      sys.stderr.write("Found " + str(len(defs)) + " matches, expected " + str(expected_matches) + " in '" + filePath + "' - maybe regex problems?\n")

  return (includes, defs)

def get_files_recursive(path, extensions):
  files = []
  for dp, dn, filenames in os.walk(path):
    for f in filenames:
      for ext in extensions:
        if f.endswith(ext):
          files.append(os.path.join(dp, f))
  return files

def directory(path):
  if os.path.isdir(path):
    return path
  raise argparse.ArgumentException("Include directory '" + str(path) + "'' does not exist.")

parser = argparse.ArgumentParser(description="Generate Dukglue bindings from comments in source code.")
parser.add_argument('--sources', nargs='*', type=argparse.FileType('r'))
parser.add_argument('--include-dirs', nargs='*', type=directory)
parser.add_argument('--extensions', nargs='*', default=['.h', '.hpp', '.hxx', '.cpp', '.cxx', '.cc'])
parser.add_argument('--output', type=str, default='-',
                    help='Name of the file to write to. Use "-" to write to stdout (default).')
parser.add_argument('--docs', type=argparse.FileType('w'), default=None)
args = parser.parse_args(sys.argv[1:])

include_dirs = [make_unix_path(p) for p in args.include_dirs]

# build a list of files to parse from the include dirs and extensions
source_files = []
for d in args.include_dirs:
  source_files += get_files_recursive(d, args.extensions)

# build a list of bindings by parsing the files
defs = []
includes = []
for f in source_files:
  file_includes, file_defs = dukbind_parse(f, include_dirs)
  if len(file_defs) > 0:
    defs += [d for d in file_defs if not d in defs]
    includes += [i for i in file_includes if not i in includes]


# write the file
if args.output != None:
  tmpOut = StringIO()
  tmpOut.write(u"#include <dukglue/dukglue.h>\n\n")

  # includes
  for i in includes:
    tmpOut.write(u"#include <" + i + ">\n")
  tmpOut.write(u"\n")

  functions = []
  classes = defaultdict(lambda: {'class': None, 'constructors': [], 'methods': [], 'properties': []})
  # sort things by class type
  for d in defs:
    if isinstance(d, DukClass):
      classes[d.class_type]['class'] = d
    elif isinstance(d, DukConstructor):
      classes[d.class_type]['constructors'].append(d)
    elif isinstance(d, DukMethod):
      classes[d.class_type]['methods'].append(d)
    elif isinstance(d, DukProperty):
      classes[d.class_type]['properties'].append(d)
    elif isinstance(d, DukFunction):
      functions.append(d)

  # binding function
  tmpOut.write(u"void do_script_bindings(duk_context* ctx) {\n")

  tmpOut.write(u"\t// functions\n")
  for f in functions:
    tmpOut.write(u"\t" + "\n\t".join(str(f).split("\n")) + "\n")

  tmpOut.write(u"\n\t// classes\n")
  for class_type, c in classes.items():
    if c['class'] != None:
      tmpOut.write(u"\t// class '" + c['class'].class_name + "' (" + c['class'].class_type + ")\n")

      if not len(str(c['class'])) == 0:
        tmpOut.write(u"\t" + "\n\t".join(str(c['class']).split("\n")) + "\n")
    for ctor in c['constructors']:
      tmpOut.write(u"\t" + "\n\t".join(str(ctor).split("\n")) + "\n")
    for m in c['methods']:
      tmpOut.write(u"\t" + "\n\t".join(str(m).split("\n")) + "\n")
    for p in c['properties']:
      tmpOut.write(u"\t" + "\n\t".join(str(p).split("\n")) + "\n")

    tmpOut.write(u'\n')

  tmpOut.write(u"}\n")

  # only write the file if it has changed
  if args.output == '-':
    sys.stdout.write(tmpOut.getvalue())
  else:
    prevContents = ""
    newContents = tmpOut.getvalue()
    if os.path.exists(args.output):
      with open(args.output, 'r') as f:
        prevContents = f.read()

    if newContents != prevContents:
      with open(args.output, 'w') as f:
        f.write(newContents)
    else:
      sys.stderr.write('No new bindings detected - not updating output file.')

if args.docs != None:
  args.docs.write('<h1>Classes</h1>\n')
  for class_type, c in classes.items():
    class_name = c['class'].class_name if c['class'] != None else class_type
    args.docs.write("<h2>" + class_name + "</h2>\n\n")

    args.docs.write("<h3>Constructors</h3>\n")
    if len(c['constructors']) == 0:
      args.docs.write("<p>This class has no constructors.</p>\n")
    else:
      for ctor in c['constructors']:
        managed = " (managed)" if ctor.managed else ""
        args.docs.write("<h4>" + ctor.ctor_name + "(" + ", ".join(ctor.arg_types) + ")" + managed + "</h4>\n")

    if len(c['methods']) == 0:
      args.docs.write("<p>This class has no methods.</p>\n")
    else:
      for m in c['methods']:
        args.docs.write("<h4>" + class_name + '.' + m.method_name + "(...)</h4>\n")
        args.docs.write("<p>" + m.doc_string + "</p>\n")

    if len(c['properties']) == 0:
      args.docs.write("<p>This class has no properties.</p>\n")
    else:
      for p in c['properties']:
        args.docs.write("<h4>" + class_name + '.' + p.prop_name + "</h4>\n")
        can_get = (p.getter_ptr != "nullptr")
        can_set = (p.setter_ptr != "nullptr")
        yes = "yes"
        no = "no"
        args.docs.write("<p>Get: " + (yes if can_get else no) + ", set: " + (yes if can_set else no) + "<br/>\n")
        args.docs.write(p.doc_string + "</p>\n")

    args.docs.write('\n')

  args.docs.write('<h1>Functions</h1>\n')
  for func in functions:
    args.docs.write("<h4>" + func.func_name + "(...)</h4>\n")
    args.docs.write("<p>" + func.doc_string + "</p>\n")